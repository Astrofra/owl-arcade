��    b     ,              <&     =&     \&  .   x&     �&     �&  �   �&     �'     �'     �'     �'  #   �'  !   �'  #   (  #   6(  #   Z(  !   ~(  "   �(  +   �(  +   �(     )     4)  
   T)  	   _)     i)     q)  	   w)     �)     �)  	   �)     �)     �)  "   �)  	   �)  .   �)  +   *     1*     A*  
   O*     Z*     n*     �*     �*     �*     �*     �*     �*     �*     �*     +     +     +      8+     Y+     g+  	   p+     z+  3   �+     �+     �+     �+     �+     	,     ,  	   ,,  	   6,     @,     X,     p,     u,     �,     �,     �,     �,  
   �,     �,  
   �,     �,     -     '-     +-     ?-     O-     d-  ^   h-     �-  /   �-      .  .   (.     W.     \.     l.     }.     �.     �.     �.     �.     �.     �.     �.     /     /     -/     @/     R/     W/  
   h/     s/     �/     �/     �/  "   �/     �/  D   �/     30     90     K0  
   X0     c0     0  -   �0     �0     �0     �0     �0     1     1     &1     31     B1     I1      Q1     r1     �1     �1     �1     �1     �1     �1     �1     2     +2     F2     X2     m2     �2     �2     �2     �2     �2     �2     �2     �2     �2     3     3     13     :3     G3     W3     k3  *   p3  $   �3     �3     �3     �3     �3     4     4     84     I4     g4     y4     �4     �4     �4  !   �4  
   �4     �4     �4     �4  *   5  )   95  >   c5     �5     �5     �5     �5     �5     6     6     6     %6     ,6     36     A6  %   G6  
   m6     x6     }6     �6     �6     �6     �6     �6     �6     �6     7     #7     27     L7     R7  	   W7     a7     n7  (   ~7     �7     �7     �7     �7     �7     �7     8     8     !8     '8     78     G8     \8      j8     �8     �8     �8     �8     �8     �8     �8     �8     9     9     +9     >9     G9     g9     �9     �9     �9      �9     �9     :     $:  $   9:     ^:     v:     :     �:  
   �:     �:  	   �:     �:     �:     �:     �:  	   �:     �:     ;      ;     ,;     C;     [;     h;     ~;     �;     �;     �;     �;     �;     �;     <     <     -<     5<     F<     V<     c<     s<     v<      �<  �   �<     �=     �=     �=     �=     �=     �=     �=  Y   �=  G   >     f>     u>     �>     �>     �>     �>     �>     �>     ?     '?     @?     E?  
   Y?     d?     |?     �?  
   �?     �?  !   �?     �?     �?     �?     �?     @     )@     <@     S@  ,   d@     �@     �@  '   �@     �@  	   A  
   A     A  
   A     *A     =A     KA     aA     tA     �A     �A     �A     �A     �A     B  	   B     B     /B     AB     YB     hB     oB     B     �B     �B     �B     �B     �B  F   �B     C  !   *C     LC     eC  
   jC     uC  
   �C     �C     �C  
   �C     �C     �C     �C  3   D     <D     JD     ]D     pD     �D     �D     �D     �D     �D     �D     �D     �D     E     E     %E     6E     QE  "   jE  "   �E     �E     �E     �E     �E     �E     F     F     %F     6F  	   BF     LF     aF     oF     �F  	   �F     �F     �F     �F     �F     �F     �F     G     G     %G     3G     FG     UG  2   jG  0   �G     �G     �G     �G     �G     
H  �   H  ,   �H  E   �H     I  ]   I  .   {I  z   �I     %J     .J     HJ     NJ     `J  
   vJ     �J     �J     �J     �J     �J     �J     �J     K      K  	   2K  8   <K     uK     �K     �K     �K     �K     �K     �K      L     L  
   L     !L     /L     =L     LL     _L     rL     xL     �L     �L     �L  
   �L     �L     �L  �   �L  �   ?M     �M     �M     �M     �M     �M      N     N     'N     ?N     ON     cN     vN     �N     �N     �N     �N     �N  '   �N  (   O     DO  (   bO     �O     �O  #   �O  &   �O     P     )P     CP     ^P     }P     �P     �P     �P     �P     �P     �P      Q     Q     'Q     ;Q     NQ     fQ     zQ     �Q     �Q     �Q     �Q     �Q     �Q     R     -R     DR     _R     zR     �R     �R     �R     �R     �R     S     S     4S     HS     dS     |S     �S     �S      �S     �S     T     T     >T     VT     tT     �T     �T     �T     �T     �T     U     8U     OU     cU     wU     �U     �U     �U     �U     �U     V     V     ,V     ?V     QV     bV     tV     �V     �V     �V     �V     �V     �V     W     W  	   W     &W     FW     ]W     uW     �W     �W     �W     �W     �W     X     X     1X     HX     bX     yX     �X     �X     �X     �X     �X  #   Y     :Y     TY     sY     �Y     �Y     �Y     �Y     �Y     Z  t  Z  4   �[  '   �[  1   �[  +   \  	   K\  �   U\  	   )]     3]     ;]     Z]  ,   u]  *   �]  *   �]  *   �]  *   #^  *   N^  )   y^  -   �^  -   �^     �^  )   _     G_     Y_     f_     o_  	   w_     �_     �_  	   �_     �_     �_  .   �_  	   �_  /   �_  .   `     A`     M`  
   \`     g`     �`     �`     �`     �`     �`     �`     a     a     +a     1a     Na  %   Qa  '   wa     �a     �a     �a  
   �a  D   �a     b     'b  +   :b     fb     �b     �b  
   �b  
   �b     �b     �b     c     c     !c     .c     ;c  $   Mc     rc     �c     �c     �c     �c     �c  !   �c     �c     d  
   'd  e   2d     �d  A   �d  *   �d  4   e  	   Le     Ve  %   re  #   �e  $   �e     �e     �e     �e     f  0   )f     Zf     uf     �f     �f     �f     �f     �f     �f     g     g     .g  !   7g  $   Yg     ~g  P   �g     �g     �g     �g     h     "h     @h  -   ^h     �h     �h     �h     �h     �h     �h     i     i     %i     -i     3i     Ri     fi     zi     �i     �i     �i     �i     �i  %   �i  #   "j     Fj     Xj  "   mj      �j     �j     �j     �j     �j     �j     k     !k     -k     Ik     [k     qk     }k     �k     �k     �k  1   �k  '   �k     l     $l     Cl  "   bl     �l  '   �l     �l  &   �l     m     m     %m     .m     =m  &   Xm     m     �m     �m      �m  3   �m  2   n  T   6n     �n  .   �n     �n     �n  /   �n     o     -o     4o     @o     Xo     no     �o  *   �o     �o     �o     �o     �o     �o     p     p     9p     Np     jp     �p     �p     �p     �p     �p     �p     �p     �p  *   q     @q  	   Xq     bq     uq  	   �q  (   �q     �q  )   �q     �q     r     $r     6r     Nr  7   br     �r     �r     �r     �r     �r     �r     s     %s     =s     Ns     cs     ~s  *   �s  -   �s  (   �s  +   t     3t  0   Ot     �t     �t     �t  3   �t     u     +u     :u     Yu     au     ~u     �u     �u     �u     �u     �u     �u     v     v     %v     6v     Mv     dv     qv     �v     �v     �v     �v     �v     �v     �v     
w  "   w     :w     Bw     Rw     fw     w     �w  0   �w  8   �w  �   x      y     y     y     $y     9y     <y     Oy  g   Sy  G   �y     z     z  "   +z     Nz     cz  "   {z     �z     �z     �z     �z     �z      {     {     &{     ={  
   U{     `{     x{  8   �{     �{     �{  !   �{     �{  %   |     =|     S|     n|  D   �|  .   �|  ,   �|  )   !}  )   K}     u}     �}     �}     �}     �}     �}     �}     �}     �}     ~     ~     /~     @~     [~     v~     �~  '   �~     �~     �~     �~  	   �~               0     6     K     ^     t  P   �  2   �  4   	�  0   >�     o�     w�     ��     ��  (   ��     �     �     ��  '   �  %   4�  8   Z�     ��     ��     ��     ρ  1   �  #   �     ;�     V�  
   u�     ��     ��     ��     ��     Ƃ     �  %   ��  %   !�  ,   G�  )   t�  	   ��     ��     ��  $   у     ��  !   �     -�     4�  
   G�     R�     d�     |�     ��  
   ��     ��     ��  	   ׄ     �     ��      �     /�     I�     ^�     s�     ��     ��     ��  >   ��  >   ��     8�     Q�     j�     }�     ��  �   ��  /   "�  S   R�     ��  l   Ç  2   0�  �   c�     �     �     $�     +�     D�     b�     o�     ��     ��      ��  &   ��     �     �     �  !   -�     O�  \   j�     Ǌ     �  &   �     .�     I�     b�     v�     |�     ��     ��     ��     ��     ȋ     ߋ  %   ��     !�     .�     >�  
   G�     R�  
   [�     f�  
   j�  �   u�  �   $�     ō     ڍ     �     �     ��     �  !    �  !   B�     d�     x�     ��     ��     ��     ��     ��     ��     ��     ��     ֎     ��     �     3�     P�     ]�     t�     ��     ��     ��     ُ     �     �     ��     ��     �     �     �     �     !�     (�     1�  	   8�     B�     I�     V�     g�  	   v�  
   ��     ��     ��  	   ��     ��  	   ��     ��  
   Ő     А  	   א     �     �     ��     �  
   �     �     �  
   "�     -�     4�  	   I�     S�  
   g�  	   r�     |�     ��     ��     ��     ��     ��     ő     ّ     �     �     �     �     �     �     .�     4�     C�     W�  	   j�     t�     {�     ��     ��     ��     ��     ��     ��     ��     ��     ƒ     Ӓ     ܒ     �     ��     �     �     #�     )�     7�  	   H�     R�     Z�     h�     o�  
   t�     �     ��     ��     ��  
   ��     ��     ��     ԓ  	   �     �     ��     �     �     .�  	   @�     J�     X�     d�     q�     v�   

--- DRIVER INFO ---
Driver:  

Press any key to continue 

There are working clones of this machine: %s 
    Configuration saved    

 
Sound:
 
THIS MACHINE DOESN'T WORK. The emulation for this machine is not yet complete. There is nothing you can do to fix this problem except wait for the developers to improve the emulation.
 
Video:
   %1$s
   %1$s    [default: %2$s]
   %1$s    [tag: %2$s]
   Adjuster inputs    [%1$d inputs]
   Analog inputs    [%1$d inputs]
   Gambling inputs    [%1$d inputs]
   Hanafuda inputs    [%1$d inputs]
   Keyboard inputs    [%1$d inputs]
   Keypad inputs    [%1$d inputs]
   Mahjong inputs    [%1$d inputs]
   Screen '%1$s': %2$d × %3$d (H) %4$s Hz
   Screen '%1$s': %2$d × %3$d (V) %4$s Hz
   Screen '%1$s': Vector
   User inputs    [%1$d inputs]
  (default)  (locked)  COLORS  PENS %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Driver: %4$s

CPU:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d machines (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d software packages ) %1$s Brightness %1$s Contrast %1$s Gamma %1$s Horiz Position %1$s Horiz Stretch %1$s Refresh Rate %1$s Vert Position %1$s Vert Stretch %1$s Volume %1$s, %2$-.100s %1$s: %2$s
 %1$s: %2$s - Search: %3$s_ %2$s
 %d total matches found %s %s
 added to favorites list. %s
 removed from favorites list. %s [internal] %s added (playing) (recording) * BIOS settings:
  %1$d options    [default: %2$s]
 * CPU:
 * Configuration settings:
 * DIP switch settings:
 * Input device(s):
 * Media Options:
 * Slot Options:
 * Sound:
 * Video:
 **Error saving %s.ini** **Error saving ui.ini** , %s <set up filters> ARGB Settings Activated: %s Activated: %s = %s Add %1$s Folder - Search: %2$s_ Add Folder Add To Favorites Add filter Adstick Device Assignment Advanced Options All All cheats reloaded Analog Controls Analog Controls	Yes
 Any Are you sure you want to quit?

Press ''%1$s'' to quit,
Press ''%2$s'' to return to emulation. Artwork Options Audit ROMs for %1$u machines marked unavailable Audit ROMs for all %1$u machines Auditing ROMs for machine %2$u of %3$u...
%1$s Auto Auto frame skip Auto rotate left Auto rotate right Automatic save/restore BIOS BIOS Selection BIOS selection: Barcode Reader Barcode length invalid! Beam Intensity Weight Beam Width Maximum Beam Width Minimum Bilinear Filtering Bitmap Prescaling Bold Bookkeeping Info CPU or RAM Camera	Imperfect
 Camera	Unimplemented
 Cancel Cannot save over directory Change %1$s Folder - Search: %2$s_ Change Folder Changes to this only take effect when "Start new search" is selected Cheat Cheat Comment:
%s Cheat Finder Cheat Name Cheat added to cheat.simple Cheat engine not available Cheat written to %s and added to cheat.simple Cheats Choose from palette Clear Watches Coin %1$c: %2$d%3$s
 Coin %1$c: NA%3$s
 Coin impulse Coin lockout Color preview: Colors Command Completely unemulated features:  Configure Directories Configure Machine Configure Options Confirm quit from machines Controls	Imperfect
 Controls	Unimplemented
 Create Crosshair Offset %1$s Crosshair Offset X %1$1.3f Crosshair Offset Y %1$1.3f Crosshair Options Crosshair Scale %1$s Crosshair Scale X %1$1.3f Crosshair Scale Y %1$1.3f Current %1$s Folders Current time Custom Customize UI DIP Switches Data Format Default Default name is %s Device Mapping Dial Device Assignment Disabled Disabled: %s Disk	Imperfect
 Disk	Unimplemented
 Done Double-click or press %1$s to change color Double-click or press %1$s to select Driver Driver is BIOS	No
 Driver is BIOS	Yes
 Driver is Clone of	%1$s
 Driver is Parent	
 Driver is clone of: %1$-.100s Driver is parent Driver: "%1$s" software list  Driver: %1$-.100s Emulated Enabled Enabled: %s Enforce Aspect Ratio Enlarge images in the right panel Enter Code Error accessing %s Exit Export displayed list to file Export list in TXT format (like -listfull) Export list in XML format (like -listxml) Export list in XML format (like -listxml, but exclude devices) External DAT View Failed to save input name file Fast Forward File File Already Exists - Override? File Manager Filter Filter %1$u Flip X Flip Y Folders Setup Fonts Force 4:3 aspect for snapshot display Frame skip GLSL Gameinit General Info General Inputs Graphics	Imperfect
 Graphics	Imperfect Colors
 Graphics	OK
 Graphics	Unimplemented
 Graphics	Wrong Colors
 Graphics: Imperfect,  Graphics: OK,  Graphics: Unimplemented,  Group HLSL Hide Both Hide Filters Hide Info/Image Hide romless machine from available list High Scores History Image Format: Image Information Images Imperfectly emulated features:  Include clones Info auto audit Infos Infos text size Input (general) Input (this Machine) Input Options Input port name file saved to %s Input ports Italic Joystick Joystick deadzone Joystick saturation Keyboard	Imperfect
 Keyboard	Unimplemented
 Keyboard Inputs	Yes
 Keyboard Mode LAN	Imperfect
 LAN	Unimplemented
 Language Laserdisc '%1$s' Horiz Position Laserdisc '%1$s' Horiz Stretch Laserdisc '%1$s' Vert Position Laserdisc '%1$s' Vert Stretch Left equal to right Left equal to right with bitmask Left equal to value Left greater than value Left less than value Left not equal to right with bitmask Left not equal to value Lightgun Lightgun Device Assignment Lines Load State MAMEinfo MARPScore MESSinfo MHz Machine Configuration Machine Information Mamescore Manufacturer	%1$s
 Master Volume Match block Mechanical Machine	No
 Mechanical Machine	Yes
 Menu Preview Microphone	Imperfect
 Microphone	Unimplemented
 Miscellaneous Options Mouse Mouse	Imperfect
 Mouse	Unimplemented
 Mouse Device Assignment Multi-keyboard Multi-mouse Name:             Description:
 Natural Natural keyboard Network Devices New Barcode: New Image Name: No No category INI files found No groups found in category file No machines found. Please check the rompath specified in the %1$s.ini file.

If this is your first time using %2$s, please see the config.txt file in the docs directory for information on configuring %2$s. None None
 Not supported Number Of Screens Off Offscreen reload On One or more ROMs/CHDs for this machine are incorrect. The machine may not run correctly.
 One or more ROMs/CHDs for this machine have not been correctly dumped.
 Other Controls Overall	NOT WORKING
 Overall	Unemulated Protection
 Overall	Working
 Overall: NOT WORKING Overall: Unemulated Protection Overall: Working Overclock %1$s sound Overclock CPU %1$s Paddle Device Assignment Page Partially supported Pause/Stop Pedal Device Assignment Performance Options Play Play Count Player Please enter a file extension too Plugin Options Plugins Positional Device Assignment Press TAB to set Press any key to continue. Printer	Imperfect
 Printer	Unimplemented
 Pseudo terminals ROM Audit 	Disabled
Samples Audit 	Disabled
 ROM Audit Result	BAD
 ROM Audit Result	OK
 Read this image, write to another image Read this image, write to diff Read-only Read-write Record Reload All Remove %1$s Folder Remove Folder Remove From Favorites Remove last filter Requires Artwork	No
 Requires Artwork	Yes
 Requires CHD	No
 Requires CHD	Yes
 Requires Clickable Artwork	No
 Requires Clickable Artwork	Yes
 Reset Reset All Results will be saved to %1$s Return to Machine Return to Previous Menu Revision: %1$s Rewind Rewind capacity Romset	%1$s
 Rotate Rotate left Rotate right Rotation Options Sample Rate Sample text - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Samples Audit Result	BAD
 Samples Audit Result	None Needed
 Samples Audit Result	OK
 Save Save Cheat Save Configuration Save State Save input names to file Screen Screen #%d Screen '%1$s' Screen Orientation	Horizontal
 Screen Orientation	Vertical
 Screen flipping in cocktail mode is not supported.
 Search: %1$s_ Select New Machine Select access mode Select category: Select cheat to set hotkey Select custom filters: Select image format Selection List - Search:  Set Set hotkeys Settings Show All Show DATs view Show mouse pointer Show side panels Simultaneous contradictory Skip BIOS selection menu Skip information screen at startup Skip software parts selection menu Sleep Slider Controls Slot Devices Software is clone of: %1$-.100s Software is parent Software part selection: Sound Sound	Imperfect
 Sound	None
 Sound	OK
 Sound	Unimplemented
 Sound Options Sound: Imperfect Sound: None Sound: OK Sound: Unimplemented Speed Start Out Maximized Start new search State/Playback Options Support Cocktail	No
 Support Save	No
 Support Save	Yes
 Supported: No Supported: Partial Supported: Yes Switch Item Ordering Switched Order: entries now ordered by description Switched Order: entries now ordered by shortname Synchronized Refresh Sysinfo System: %1$-.100s Tape Control Test The selected game is missing one or more required ROM or CHD images. Please select a different game.

Press any key to continue. There are known problems with this machine

 This driver requires images to be loaded in the following device(s):  This machine has no BIOS. This machine has no sound hardware, MAME will produce no sounds, this is expected behaviour.
 This machine requires external artwork files.
 This machine was never completed. It may exhibit strange behavior or missing elements that are not bugs in the emulation.
 Throttle Tickets dispensed: %1$d

 Timer Timing	Imperfect
 Timing	Unimplemented
 Total time Trackball Device Assignment Triple Buffering Type Type name or select: %1$s_ Type name or select: (random) UI Color Settings UI Customization Settings UI Font UI Fonts Settings UI active Unable to write file
Ensure that cheatpath folder exists Undo last search -- # Uptime: %1$d:%2$02d

 Uptime: %1$d:%2$02d:%3$02d

 Use External Samples Use image as background User Interface Value Vector Vector Flicker Video Mode Video Options Visible Delay WAN	Imperfect
 WAN	Unimplemented
 Wait Vertical Sync Watch Window Mode Write X Y Year	%1$s
 Yes [None]
 [This option is NOT currently mounted in the running system]

Option: %1$s
Device: %2$s

If you select this option, the following items will be enabled:
 [This option is currently mounted in the running system]

Option: %1$s
Device: %2$s

The selected option enables the following items:
 [compatible lists] [create] [empty slot] [empty] [failed] [file manager] [no category INI files] [no groups in INI file] [software list] color-channelAlpha color-channelBlue color-channelGreen color-channelRed color-optionBackground color-optionBorder color-optionClone color-optionDIP switch color-optionGraphics viewer background color-optionMouse down background color color-optionMouse down color color-optionMouse over background color color-optionMouse over color color-optionNormal text color-optionNormal text background color-optionSelected background color color-optionSelected color color-optionSlider color color-optionSubitem color color-optionUnavailable color color-presetBlack color-presetBlue color-presetGray color-presetGreen color-presetOrange color-presetRed color-presetSilver color-presetViolet color-presetWhite color-presetYellow color-sampleClone color-sampleMouse Over color-sampleNormal color-sampleSelected color-sampleSubitem default emulation-featureLAN emulation-featureWAN emulation-featurecamera emulation-featurecolor palette emulation-featurecontrols emulation-featuredisk emulation-featuregraphics emulation-featurekeyboard emulation-featuremicrophone emulation-featuremouse emulation-featureprinter emulation-featureprotection emulation-featuresound emulation-featuretiming kHz machine-filterAvailable machine-filterBIOS machine-filterCHD Required machine-filterCategory machine-filterClones machine-filterCustom Filter machine-filterFavorites machine-filterHorizontal Screen machine-filterManufacturer machine-filterMechanical machine-filterNo CHD Required machine-filterNot BIOS machine-filterNot Mechanical machine-filterNot Working machine-filterParents machine-filterSave Supported machine-filterSave Unsupported machine-filterUnavailable machine-filterUnfiltered machine-filterVertical Screen machine-filterWorking machine-filterYear path-optionArtwork path-optionArtwork Previews path-optionBosses path-optionCabinets path-optionCategory INIs path-optionControl Panels path-optionCovers path-optionCrosshairs path-optionDATs path-optionFlyers path-optionHowTo path-optionINIs path-optionIcons path-optionLogos path-optionMarquees path-optionPCBs path-optionROMs path-optionScores path-optionSnapshots path-optionSoftware Media path-optionVersus playing recording selmenu-artworkArtwork Preview selmenu-artworkBosses selmenu-artworkCabinet selmenu-artworkControl Panel selmenu-artworkCovers selmenu-artworkFlyer selmenu-artworkGame Over selmenu-artworkHowTo selmenu-artworkLogo selmenu-artworkMarquee selmenu-artworkPCB selmenu-artworkScores selmenu-artworkSnapshots selmenu-artworkVersus software-filterAvailable software-filterClones software-filterCustom Filter software-filterDevice Type software-filterFavorites software-filterParents software-filterPartially Supported software-filterPublisher software-filterRelease Region software-filterSoftware List software-filterSupported software-filterUnavailable software-filterUnfiltered software-filterUnsupported software-filterYear stopped Project-Id-Version: MAME
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-17 07:08+1100
PO-Revision-Date: 2016-03-05 13:02+0100
Last-Translator: aviloria
Language-Team: Español; Castellano <>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Gtranslator 2.91.7
 

--- INFORMACIÓN DEL CONTROLADOR ---
Controlador:  

Pulsa cualquier tecla para continuar. 

Existen clones funcionales de esta máquina: %s 
    Se ha guardado la configuración    

 
Sonido:
 
ESTA MÁQUINA NO FUNCIONA. La emulación para esta máquina no está todavía completada. No hay nada que se pueda hacer para arreglar el problema salvo esperar a que los desarrolladores mejoren la emulación.
 
Vídeo:
   %1$s
   %1$s    [por defecto: %2$s]
   %1$s    [reseña: %2$s]
   Entradas del regulador    [%1$d entradas]
   Entradas analógicas    [%1$d entradas]
   Entradas de Gambling    [%1$d entradas]
   Entradas de Hanafuda    [%1$d entradas]
   Entradas del teclado    [%1$d entradas]
   Entradas del Keypoad    [%1$d entradas]
   Entradas de Mahjong    [%1$d entradas]
   Pantalla '%1$s': %2$d × %3$d (H) %4$s Hz
   Pantalla '%1$s': %2$d × %3$d (V) %4$s Hz
   Pantalla '%1$s': Vectorial
   Entradas de usuario    [%1$d entradas]
  (predeterminado)  (bloqueado)  COLORES  PLUMAS %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Controlador: %4$s

Procesador:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d máquinas (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d paquetes de software ) Brillo %1$s Contraste %1$s Gamma %1$s Posición horizontal %1$s Estiramiento horizontal %1$s Tasa de refresco %1$s Posición vertical %1$s Estiramiento vertical %1$s Volumen %1$s %1$s, %2$-.100s %1$s: %2$s
 %1$s: %2$s - Buscar: %3$s_ %2$s
 %d coincidencias encontradas %s %s
 añadido a la lista de favoritos. %s
 eliminado de la lista de favoritos. %s [interno] %s añadido (en ejecución) (grabando) * Configuración de la BIOS:
  %1$d opciones    [por defecto: %2$s]
 * Procesador:
 * Configuración:
 * Configuración de los interruptores DIP:
 * Dispositivo(s) de entrada:
 * Opciones de medios:
 * Opciones de ranuras:
 * Sonido:
 * Vídeo:
 Error al guardar «%s.ini»** **Error al guardar «ui.ini»** , %s <configurar filtros> Ajustes ARGB Activado: %s Activado: %s = %s Añadir carpeta %1$s - Buscar: %2$s_ Añadir carpeta Añadir a favoritos Añadir filtro Asignación de palancas Opciones avanzadas Todo Se han recargado todos los trucos Controles analógicos Controles analógicos	Si
 Cualquiera ¿Seguro que quieres salir?

Presiona «%1$s» para salir,
Presiona «%2$s» para volver al emulador. Opciones del arte Auditar ROMs para las %1$u máquinas marcadas como no-disponibles Auditar ROMs para todas las %1$u máquinas Auditando ROMs para la máquina %2$u de %3$u...
%1$s Automát. Salto de frames automático Rotar automáticamente a la izquierda Rotar automáticamente a la derecha Guardado/recuperación automático/a BIOS Selección de BIOS Selección de BIOS: Lector de código de barras El tamaño del código de barras no es correcto. Peso de intensidad del haz Anchura max. del haz Anchura mín. del haz Filtrado bilineal Preescalado de texturas Negrita Información contable Procesador o Memoria RAM Cámara	Imperfecta
 Cámara	No implementada
 Cancelar No se puede guardar en la carpeta Cambiar carpeta %1$s - Buscar: %2$s_ Cambiar carpeta Los cambios solo tendrán efecto cuando se seleccione "Comenzar nueva búsqueda" Truco Comentario del truco:
%s Buscador de Trucos Nombre del Truco Truco añadido a cheat.simple Motor de trucos no disponible Truco escrito en %s y añadido a cheat.simple Trucos Elegir colores más básicos Limpiar inspecciones Moneda %1$c: %2$d%3$s
 Moneda %1$c: NA%3$s
 Impulso de monedas Bloqueo de monedas Vista previa: Colores Orden Características no emuladas:  Configurar carpetas Configurar máquina Configurar opciones Confirmar salida de máquina Controles	Imperfectos
 Controles	No implementados
 Crear Posición de diana %1$s Posición horizontal de diana %1$1.3f Posición vertical de diana %1$1.3f Opciones de diana Escala de diana %1$s Escala horizontal de diana %1$1.3f Escala vertical de diana %1$1.3f Carpetas actuales %1$s Tiempo actual Personalizado Personalizar interfaz Interruptores DIP Formato de los datos Por defecto El nombre por defecto es %s Mapeado de mandos Asignación de diales Desactivado Deshabilitado: %s Disco	Imperfecto
 Disco	No implementado
 Hecho Haz doble clic o pulsa %1$s para cambiar el color Haz doble clic o pulsa %1$s para elegir Controlador El controlador es una BIOS	No
 El controlador es una BIOS	Si
 El controlador es un clon de	%1$s
 Es un controlador padre	
 El controlador es un clon de: %1$-.100s Es un controlador padre Controlador: "%1$s" lista de software  Controlador: %1$-.100s Emulado Activado Habilitado: %s Bloquear forma de pantalla Agrandar imágenes en el panel derecho Escribe el código Error al acceder a %s Salir Exportar esta lista a un archivo Exportar lista en formato TXT (igual que -listfull) Exportar lista en formato XML (igual que -listxml) Exportar lista en formato XML (igual que -listxml, pero excluyendo los dispositivos) Vista DAT externa Error al guardar el fichero nombres de entrada Avance rápido Fichero El archivo ya existe ¿Quieres sobreescribirlo? Administrador de archivos Filtro Filtro %1$u Voltear horizontalmente Voltear verticalmente Ajustes de carpeta Tipografía Forzar aspecto 4:3 al capturar la pantalla Salto de frame GLSL Inicio de partida Información general Entrada general Gráficos	Imperfectos
 Gráficos	Colores imperfectos
 Gráficos	Aceptables Gráficos	No implementados
 Gráficos	Colores erróneos
 Gráficos: Imperfectos, Gráficos: Aceptables,  Gráficos: No implementado,  Grupo HLSL Ocultar ambos Ocultar filtros Ocultar detalles/imagen Ocultar las máquinas sin ROMs de la lista Puntuaciones más altas Historial Formato de imagen: Información de imagen Imágenes Características emuladas parcialmente:  Incluir clones Información de la auditoría automática Informaciones Tamaño del texto informativo Entrada (general) Entrada (esta máquina) Opciones de entrada Fichero de nombres de puertos de entrada guardado en %s Puertos de entrada Cursiva Joystick Zona muerta de los joysticks Saturación de los joysticks Teclado	Imperfecto
 Teclado	No implementado
 Entradas de teclado	Si
 Modo del teclado Red local	Imperfecta Red local	No implementada
 Idioma Posición horizontal de Laserdisc «%1$s» Estiramiento horizontal de Laserdisc «%1$s» Posición vertical de Laserdisc «%1$s» Estiramiento vertical de Laserdisc «%1$s» Izquierdo igual que derecho Izquierdo igual que derecho con máscara de bits Izquierdo igual que el valor Izquierdo mayor que el valor Izquierdo menor que el valor Izquierdo distinto que derecho con máscara de bits Izquierdo distinto que el valor Pistola de luz Asignación de pistolas de luz Líneas Cargar estado de la máquina Información de MAME Puntuación MARP Información de MESS MHz Configuración de máquina Información de máquina Puntuación MAME Fabricante	%1$s
 Volumen principal Coincidir bloque Máquina mecánica	No
 Máquina mecánica	Si
 Vista previa Micrófono	Imperfecto
 Micrófono	No implementado
 Otras opciones Ratón Ratón	Imperfecto
 Ratón	No implementado
 Asignación de ratones Multi-teclado Multi-ratón Nombre:             Descripción:
 Natural Teclado natural Dispositivos de red Nuevo código de barras: Nuevo nombre de imagen: No No se han encontrado ficheros INI de categorías No se han encontrado grupos en el fichero de categorías No se ha encontrado ninguna máquina. Comprueba el «rompath» en el archivo %1$s.ini.

Si es la primera vez que utilizas %2$s, por favor, échale un vistazo al archivo «config.txt» de la carpeta «docs» para informarte sobre cómo configurar %2$s. Ninguno Ninguno
 No está soportado Número de pantallas No Recarga no visible Sí Uno o más ROMs/CHDs de esta máquina son incorrectos. La máquina podría no funcionar correctamente.
 Uno o más ROMs/CHDs de esta máquina no se han volcado correctamente.
 Otros controles En General	NO FUNCIONA
 En General	Protección no emulada
 En General	Funciona
 En general: NO FUNCIONA En general: Protección sin emular En general: Funcionando Overclock de sonido %1$s Acelerar CPU %1$s Asignación de paletas Página Soportado parcialmente Pausar/Detener Asignación de pedales Opciones de rendimiento Reproducir Contador de ejecuciones Jugador Por favor, introduzca también la extensión del fichero Configuración del plugin Plugins Asignación de disp. posicionales Pulsa TAB para establecer Pulsa cualquier tecla para continuar. Impresora	Imperfecta
 Impresora	No implementada
 Pseudo-terminales Auditoría de ROMs 	Desactivada
Auditoría de muestras 	Desactivada
 Resultado de la auditoría de ROMs	INCORRECTA
 Resultado de la auditoría de ROMs	CORRECTA
 Leer esta imagen, escribir en otra imagen Leer esta imagen, escribir en diferencial Solo lectura Lectura y escritura Grabar Recargar todo Borrar carpeta %1$s Borrar carpeta Borrar de favoritos Eliminar último filtro Requiere arte	No
 Requiere arte	Si
 Requiere CHD	No
 Requiere CHD	Si
 Requiere arte clicable	No
 Requiere arte clicable	Si
 Restablecer Reiniciar todo Los resultados serán guardados en %1$s Volver a la máquina Volver al menú anterior Revisión: %1$s Rebobinar Capacidad de rebobinado Conjunto de roms	%1$s
 Girar Rotar a la izquierda Rotar a la derecha Opciones de rotación Tasa de muestreo Texto de ejemplo - La cigüeña olía el paté de atún del camión más lejano. Resultado de la auditoría de muestras	INCORRECTA
 Resultado de la auditoría de muestras	No necesaria
 Resultado de la auditoría de muestras	CORRECTA
 Guardar Guardar Truco Guardar configuración Guardar estado de la máquina Guardar los nombres de entrada a fichero Pantalla %dª pantalla Pantalla «%1$s» Orientación de la pantalla	Horizontal
 Orientación de la pantalla	Vertical
 Voltear la pantalla en modo cóctel no está soportado.
 Buscar: %1$s_ Elegir máquina nueva Elige el modo de acceso Selecciona categoria: Selecciona truco para configurar la tecla-rápida Seleccionar filtros personalizados: Elige el formato de imagen Lista de selección - Buscar:  Configurar Configurar teclas rápidas Ajustes Mostrar todo Mostrar vista de DATs Mostrar puntero del ratón Mostrar paneles laterales Entradas contradictorias simultáneas Saltar el menú de selección de BIOS Evitar la pantalla de información al inicio Saltar el menú de selección de software Suspender Controles deslizantes Ranuras de monedas El software es un clon de: %1$-.100s Es un software padre Selección de partes de software: Sonido Sonido	Imperfecto
 Sonido	No
 Sonido	Aceptable
 Sonido	No implementado
 Opciones de sonido Sonido: Imperfecto Sonido: No Sonido: Aceptable Sonido: No implementado Velocidad Comenzar maximizada Comenzar nueva búsqueda Opciones de estado/reproducción Soporta modo cocktail	No
 Soporta guardado	No
 Soporta guardado	Si
 Soportado: No Soportado: Parcialmente Soportado: Sí Reordenar listado Orden cambiado: las entradas ahora se ordenan por descripción Orden cambiado: las entradas ahora se ordenan por nombre corto Actualización síncrona Información del sistema Sistema: %1$-.100s Control de cinta Probar Al juego elegido le falta una o varias ROMs o imágenes CHD necesarias. Por favor elige otro distinto.
Pulsa cualquier tecla para continuar. Existen problemas conocidos con esta máquina

 Este controlador necesita que se carguen imágenes en los siguientes dispositivos:  Esta máquina no tiene BIOS. Esta máquina no soporta sonido, por lo que MAME no reproducirá audio, este es el comportamiento esperado.
 Esta máquina requiere ficheros de arte externos.
 Esta máquina nunca se terminó. Podría comportarse de forma extraña o mostrar signos de mal funcionamiento que en ningún caso son defectos de emulación.
 Acelerar Boletos emitidos: %1$d

 Tiempo Cronometraje	Imperfecto
 Cronometraje	No implementado
 Tiempo total Asignación de trackballs Triple búfer Tipo Escribe un nombre o elige: %1$s_ Escribe un nombre o elige: (aleatorio) Ajustes de color de interfaz Ajustes de interfaz Tipografía de interfaz Ajustes tipográficos de interfaz Interfaz de usuario activa No se ha podido escribir el fichero
Asegúrese de que existe la ruta de la carpeta cheatpath Deshacer última búsqueda -- # Tiempo encendido: %1$d:%2$02d

 Tiempo encendido: %1$d:%2$02d:%3$02d

 Utilizar muestras externas Utilizar imagen de fondo Interfaz de usuario Valor Vector Vector de parpadeo Modo de vídeo Opciones de vídeo Retardo visible Red global	Imperfecta
 Red global	No implementada
 Esperar a la sincronización vertical Inspeccionar Modo de ventana Escribir Horizontal Vertical Año	%1$s
 Sí [Ninguna]
 [Esta opción NO está actualmente montada en el sistema en ejecución]

Opción: %1$s
Dispositivo: %2$s

Si seleccionas esta opción se activarán los siguientes elementos:
 [Esta opción está actualmente montada en el sistema en ejecución]

Opción: %1$s
Dispositivo: %2$s

La opción seleccionada activa los siguientes elementos:
 [listas compatibles] [crear] [ranura vacía] [vacío] [falló] [administrador de archivos] [Sin ficheros INI de categorías] [No hay grupos en el fichero INI] [lista de software] Transparencia Azul Verde Rojo Fondo Borde Copiar Interruptor DIP Fondo del visor de gráficos Fondo al seleccionar con ratón Color al seleccionar con ratón Fondo al resaltar con ratón Color al resaltar con ratón Texto normal Fondo del texto normal Fondo del texto seleccionado Texto seleccionado Color del control deslizante Color elem. secundarios Color no disponible Negro Azul Gris Verde Naranja Rojo Plateado Violeta Blanco Amarillo Copiar Resaltado Normal Seleccionado Elem. secundario predeterminado Red local Red global cámara paleta de color controles disco gráficos teclado micrófono ratón impresora protección sonido cronometraje kHz Disponible BIOS Requiere CHD Categoría Clones Filtro personalizado Favoritos Pantalla horizontal Fabricante Mecánica No requiere CHD No BIOS No mecánica No Funciona Padres Soporta guardado No soporta guardado No disponible Sin filtrar Pantalla vertical Funciona Año Arte Vista previa del arte Jefes Cabinas arcade INIs de categorías Paneles de control Cubiertas Dianas DATs Folletos Manual INIs Iconos Logos Marquesinas Circuitos impresos ROMs Puntuaciones Capturas Medios del Software Versus en ejecución grabando Vista previa de arte Jefes Cabina arcade Panel de control Cubiertas Folleto Fin del juego Manual Logo Marquesina Circuito impreso Puntuaciones Capturas Versus Disponible Clones Filtro personalizado Tipo de dispositivo Favoritos Padres Parcialmente soportado Editor Región de la versión Lista de software Soportado No disponible Sin filtrar No soportado Año detenido 