��                       "     "  .   )"     X"     w"  �   �"     :#  
   C#  	   N#     X#     `#  	   f#     p#     w#  	   #     �#     �#  "   �#  	   �#  .   �#  +   �#      $     0$  
   >$     I$     ]$     p$     �$     �$     �$     �$     �$     �$     �$     �$     �$      %     1%     ?%  	   H%     R%     ^%     v%     �%     �%     �%     �%     �%     �%  
   �%     �%  
   &     &     4&     E&     I&     ]&     m&  ^   �&     �&  /   �&      !'  .   B'     q'     v'     �'     �'     �'     �'     �'     �'     �'     �'     (     !(     4(     G(     Z(     l(     q(     �(  
   �(     �(     �(     �(     �(  "   �(     )  D   )     U)     [)     m)     �)  -   �)     �)     �)     �)     *     *     "*     /*     >*     E*      M*     n*     �*     �*     �*     �*     �*     �*     �*     +     '+     B+     T+     i+     �+     �+     �+     �+     �+     �+     �+     �+     �+     	,      ,     ),     6,     F,     Z,  $   _,     �,     �,     �,     �,     �,     �,     -     -     -     '-     3-  !   H-  
   j-     u-     z-  *   �-  )   �-  >   �-     ,.     >.     K.     P.     p.     }.     �.     �.     �.     �.     �.  %   �.  
   �.     �.     �.     �.     �.     /     !/     </     I/     a/     x/     �/     �/     �/     �/  	   �/     �/     �/  (   �/     0     0     &0     40     F0     M0     m0     |0     �0     �0     �0     �0     �0     �0     �0     �0     �0     1     1     71     L1     Z1     i1     |1     �1     �1     �1     �1     2     2     *2     32     N2  
   T2     _2  	   h2     r2     {2     2     �2  	   �2     �2     �2     �2     �2     �2     3     '3     -3     >3     S3     k3     z3     �3     �3     �3     �3     �3     �3     �3     �3      4  �   ,4     �4     �4     5     5     %5     )5     :5  Y   =5  G   �5     �5     �5     6     "6     36     H6     g6     x6     �6     �6     �6     �6  
   �6     �6     �6     	7  !   7     07     ?7     G7     d7     u7     �7     �7     �7  '   �7     �7  	   8  
   8     '8  
   .8     98     L8     Z8     p8     �8     �8     �8     �8     �8     �8     9  	   9      9     >9     P9     h9     w9     ~9     �9     �9     �9     �9     �9  F   �9     :  
   :     :     2:  
   M:     X:  
   _:     j:     x:     �:  3   �:     �:     �:     	;     ;     -;     H;     _;     s;     �;     �;     �;     �;     �;     �;     �;     �;  "   <  "   5<     X<     ^<     n<     {<     �<     �<     �<     �<     �<  	   �<     �<     	=     =     (=  	   4=     >=     S=     Y=     m=     ~=  	   �=     �=     �=     �=     �=     �=     �=     >  2   >  0   O>     �>     �>     �>     �>  �   �>  ,   =?  E   j?     �?  ]   �?  .   (@  z   W@     �@     �@     �@     A  
   A     (A     DA     UA     ZA     uA     �A     �A  	   �A     �A     �A     �A     �A     B     &B     -B  
   <B     GB     UB     cB     rB     �B     �B     �B     �B     �B  
   �B     �B     �B     �B     �B     �B     �B     �B     C     C     6C     FC     ZC     mC     �C     �C     �C     �C  (   �C     �C  (   D     BD     `D  #   yD  &   �D     �D     �D     �D     E     4E     GE     YE     kE     ~E     �E     �E     �E     �E     �E     �E     F     F     1F     GF     \F     dF     zF     �F     �F     �F     �F     �F     G     1G     NG     fG     �G     �G     �G     �G     �G     �G     �G     H     3H     IH     fH      H     �H     �H     �H     �H     I     +I     FI     ]I     {I     �I     �I     �I     �I     J     J     .J     KJ     ^J     sJ     �J     �J     �J     �J     �J     �J     K     K     +K     =K     RK     cK     tK     �K     �K     �K     �K  	   �K     �K     �K     L     2L     IL     _L     yL     �L     �L     �L     �L     �L     M     M     2M     IM     gM     �M     �M  #   �M     �M     �M     N     0N     JN     fN     �N     �N     �N  k  �N     &P  2   FP     yP     �P  �   �P     xQ     �Q     �Q     �Q     �Q  	   �Q     �Q     �Q  	   �Q     �Q     �Q  "   �Q  	   �Q  .   R  ,   7R     dR     uR  
   �R     �R     �R     �R     �R     S     S     )S     8S     DS     ^S     dS     gS     �S     �S     �S     �S  
   �S  *   �S  *   �S     'T     ,T     =T     OT     \T  %   nT     �T     �T     �T      �T     �T     �T  #   U     (U     <U  d   UU     �U  B   �U  +   V  8   9V     rV     wV      �V     �V  #   �V     �V     �V     W     W      -W     NW     iW     �W     �W     �W  	   �W      �W     �W  	   �W     �W     X     +X  $   4X  #   YX     }X  N   �X     �X     �X     �X     Y  -   1Y     _Y     eY     Y     �Y     �Y     �Y     �Y     �Y     �Y  )   �Y     Z     0Z     CZ     UZ     gZ     }Z     �Z     �Z     �Z     �Z     �Z     	[     "[     @[     ^[     u[     �[     �[     �[     �[     �[     �[     �[     
\     \     (\     :\     R\  )   X\     �\     �\     �\  !   �\     �\     �\     ]     *]  	   2]     <]     J]  +   b]     �]     �]  (   �]  -   �]  ,   ^  D   0^     u^     �^     �^  "   �^     �^     �^     �^  	   �^  	   �^     �^     _  3   _     E_     V_     [_     m_     �_     �_     �_     �_     �_     �_     `     `     (`     D`     K`     P`     b`     r`  9   �`     �`     �`     �`     �`     a  )   a     8a     Fa     `a     ma     �a     �a     �a     �a     �a     �a     �a     �a     b     (b     ?b     Rb     bb     xb  &   b  )   �b  $   �b  '   �b     c     6c     Tc  !   dc     �c     �c  	   �c     �c  	   �c     �c     �c     �c     �c     �c     d      d     /d     Ed     ad     od     ud     �d     �d     �d     �d     �d     �d     e     e     (e     >e     Se  '   Ve  +   ~e  �   �e     �f     �f     �f     �f  
   �f     �f     �f  m   �f  E   Zg     �g  !   �g  (   �g     �g  !   h  (   ;h     dh     �h     �h     �h     �h     �h  
   �h  "   �h     i     +i     1i     Ni     ]i  $   di     �i     �i     �i     �i     �i  (   j  $   ,j     Qj     ^j     pj     yj     �j     �j     �j     �j     �j     �j     k     k     (k     Hk     hk     ok  $   �k     �k     �k     �k  	   �k     �k  	   �k     	l     l     )l     >l  K   Yl     �l     �l     �l     �l     �l     �l     �l     
m  !   m     ;m  D   [m     �m     �m     �m     �m  (   �m      #n     Dn     _n      yn     �n     �n     �n     �n     �n     �n  #   o  /   3o  .   co     �o     �o     �o  "   �o     �o     �o     p     
p     p  	   -p     7p     Op     ]p     qp  	   �p     �p  	   �p     �p     �p     �p     �p     q     q     0q     Iq     Xq     qq     �q  :   �q  9   �q     r     ,r     <r     Or  �   br  +   �r  L   *s     ws  �   �s  /   t  �   Et     �t     �t     �t  !   u     0u  "   =u     `u     qu  !   vu  %   �u     �u     �u  	   �u      �u  '   v     .v     Dv     ]v  
   pv     {v     �v     �v     �v     �v     �v  %   �v     w     $w     -w     /w  
   1w     <w     ?w     Sw     Zw     gw  	   ow     yw     �w     �w     �w     �w     �w     �w     �w     �w     �w     �w     �w     x     (x     Ax     Sx     ax     vx     �x     �x     �x     �x     �x     �x     �x     �x     �x     �x     y     	y     y     y     y     #y     /y     7y     Cy     Py     \y     `y     dy     ky  	   |y     �y     �y     �y  	   �y     �y  	   �y  
   �y     �y     �y     �y     �y     �y     �y  	   �y     z     z  	   !z     +z  
   Az  	   Lz     Vz     kz     wz     �z     �z     �z     �z     �z     �z     �z     �z     {     {     {     &{     +{     4{     B{  	   X{  	   b{     l{  	   p{  	   z{     �{     �{     �{     �{     �{     �{     �{  
   �{     �{     �{     �{     �{     �{     �{     |  	   |  	   #|  	   -|  	   7|     A|     F|     N|     R|  
   [|     f|     n|     z|     �|     �|  	   �|     �|     �|     �|     �|     �|  
   �|     }     }     %}     4}     9}   

Press any key to continue 

There are working clones of this machine: %s 
    Configuration saved    

 
Sound:
 
THIS MACHINE DOESN'T WORK. The emulation for this machine is not yet complete. There is nothing you can do to fix this problem except wait for the developers to improve the emulation.
 
Video:
  (default)  (locked)  COLORS  PENS %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Driver: %4$s

CPU:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d machines (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d software packages ) %1$s Brightness %1$s Contrast %1$s Gamma %1$s Horiz Position %1$s Horiz Stretch %1$s Refresh Rate %1$s Vert Position %1$s Vert Stretch %1$s Volume %1$s, %2$-.100s %1$s: %2$s
 %1$s: %2$s - Search: %3$s_ %2$s
 %s %s
 added to favorites list. %s
 removed from favorites list. %s [internal] %s added (playing) (recording) **Error saving %s.ini** **Error saving ui.ini** , %s <set up filters> ARGB Settings Activated: %s Activated: %s = %s Add %1$s Folder - Search: %2$s_ Add Folder Add To Favorites Add filter Adstick Device Assignment Advanced Options All All cheats reloaded Analog Controls Analog Controls	Yes
 Are you sure you want to quit?

Press ''%1$s'' to quit,
Press ''%2$s'' to return to emulation. Artwork Options Audit ROMs for %1$u machines marked unavailable Audit ROMs for all %1$u machines Auditing ROMs for machine %2$u of %3$u...
%1$s Auto Auto frame skip Auto rotate left Auto rotate right Automatic save/restore BIOS BIOS Selection BIOS selection: Barcode Reader Barcode length invalid! Beam Intensity Weight Beam Width Maximum Beam Width Minimum Bilinear Filtering Bitmap Prescaling Bold Bookkeeping Info Burn-in CPU or RAM Camera	Imperfect
 Camera	Unimplemented
 Cancel Cannot save over directory Change %1$s Folder - Search: %2$s_ Change Folder Changes to this only take effect when "Start new search" is selected Cheat Cheat Comment:
%s Cheat added to cheat.simple Cheat engine not available Cheat written to %s and added to cheat.simple Cheats Choose from palette Coin %1$c: %2$d%3$s
 Coin %1$c: NA%3$s
 Coin impulse Coin lockout Color preview: Colors Command Completely unemulated features:  Configure Directories Configure Machine Configure Options Confirm quit from machines Controls	Imperfect
 Controls	Unimplemented
 Create Crosshair Offset %1$s Crosshair Offset X %1$1.3f Crosshair Offset Y %1$1.3f Crosshair Options Crosshair Scale %1$s Crosshair Scale X %1$1.3f Crosshair Scale Y %1$1.3f Current %1$s Folders Current time Custom Customize UI Data Format Default Default name is %s Device Mapping Dial Device Assignment Disabled Disabled: %s Disk	Imperfect
 Disk	Unimplemented
 Done Double-click or press %1$s to select Driver Driver is Clone of	%1$s
 Driver is Parent	
 Driver is clone of: %1$-.100s Driver is parent Driver: "%1$s" software list  Driver: %1$-.100s Emulated Enabled Enabled: %s Enforce Aspect Ratio Enlarge images in the right panel Enter Code Exit Export displayed list to file Export list in TXT format (like -listfull) Export list in XML format (like -listxml) Export list in XML format (like -listxml, but exclude devices) External DAT View Fast Forward File File Already Exists - Override? File Manager Filter Filter %1$u Flip X Flip Y Folders Setup Fonts Force 4:3 aspect for snapshot display Frame skip GLSL Gameinit General Info General Inputs Graphics	Imperfect
 Graphics	Imperfect Colors
 Graphics	OK
 Graphics	Unimplemented
 Graphics	Wrong Colors
 Graphics: Imperfect,  Graphics: OK,  Graphics: Unimplemented,  Group HLSL Hide Both Hide Filters Hide Info/Image Hide romless machine from available list High Scores History Image Format: Image Information Images Imperfectly emulated features:  Include clones Info auto audit Infos Infos text size Input (general) Input (this Machine) Input Options Italic Joystick Joystick deadzone Joystick saturation Keyboard	Imperfect
 Keyboard	Unimplemented
 Keyboard Inputs	Yes
 Keyboard Mode LAN	Imperfect
 LAN	Unimplemented
 Language Laserdisc '%1$s' Horiz Position Laserdisc '%1$s' Horiz Stretch Laserdisc '%1$s' Vert Position Laserdisc '%1$s' Vert Stretch Left equal to right Left less than value Lightgun Lightgun Device Assignment Lines Load State MAMEinfo MARPScore MESSinfo MHz Machine Configuration Machine Information Mamescore Manufacturer	%1$s
 Master Volume Menu Preview Microphone	Imperfect
 Microphone	Unimplemented
 Miscellaneous Options Mouse Mouse	Imperfect
 Mouse	Unimplemented
 Mouse Device Assignment Multi-keyboard Multi-mouse Name:             Description:
 Natural Natural keyboard Network Devices New Barcode: New Image Name: No No category INI files found No groups found in category file No machines found. Please check the rompath specified in the %1$s.ini file.

If this is your first time using %2$s, please see the config.txt file in the docs directory for information on configuring %2$s. None None
 Not supported Number Of Screens Off Offscreen reload On One or more ROMs/CHDs for this machine are incorrect. The machine may not run correctly.
 One or more ROMs/CHDs for this machine have not been correctly dumped.
 Other Controls Overall	NOT WORKING
 Overall	Unemulated Protection
 Overall	Working
 Overall: NOT WORKING Overall: Unemulated Protection Overall: Working Overclock %1$s sound Overclock CPU %1$s Paddle Device Assignment Page Partially supported Pause/Stop Pedal Device Assignment Performance Options Play Please enter a file extension too Plugin Options Plugins Positional Device Assignment Press TAB to set Press any key to continue. Printer	Imperfect
 Printer	Unimplemented
 Pseudo terminals Read this image, write to another image Read this image, write to diff Read-only Read-write Record Reload All Remove %1$s Folder Remove Folder Remove From Favorites Remove last filter Requires Artwork	No
 Requires Artwork	Yes
 Requires CHD	No
 Requires CHD	Yes
 Requires Clickable Artwork	No
 Requires Clickable Artwork	Yes
 Reset Reset All Results will be saved to %1$s Return to Machine Return to Previous Menu Revision: %1$s Rewind Romset	%1$s
 Rotate Rotate left Rotate right Rotation Options Sample Rate Sample text - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Save Save Cheat Save Configuration Save Machine Configuration Save State Screen Screen #%d Screen '%1$s' Screen Orientation	Horizontal
 Screen Orientation	Vertical
 Screen flipping in cocktail mode is not supported.
 Search: %1$s_ Select New Machine Select access mode Select category: Select cheat to set hotkey Select custom filters: Select image format Selection List - Search:  Set hotkeys Settings Show All Show DATs view Show mouse pointer Show side panels Simultaneous contradictory Skip BIOS selection menu Skip information screen at startup Skip software parts selection menu Sleep Slider Controls Slot Devices Software is clone of: %1$-.100s Software is parent Software part selection: Sound Sound	Imperfect
 Sound	None
 Sound	OK
 Sound	Unimplemented
 Sound Options Sound: Imperfect Sound: None Sound: OK Sound: Unimplemented Speed Start Out Maximized Start new search State/Playback Options Steadykey Support Cocktail	No
 Support Save	No
 Support Save	Yes
 Supported: No Supported: Partial Supported: Yes Switch Item Ordering Switched Order: entries now ordered by description Switched Order: entries now ordered by shortname Synchronized Refresh Sysinfo System: %1$-.100s Tape Control The selected game is missing one or more required ROM or CHD images. Please select a different game.

Press any key to continue. There are known problems with this machine

 This driver requires images to be loaded in the following device(s):  This machine has no BIOS. This machine has no sound hardware, MAME will produce no sounds, this is expected behaviour.
 This machine requires external artwork files.
 This machine was never completed. It may exhibit strange behavior or missing elements that are not bugs in the emulation.
 Throttle Tickets dispensed: %1$d

 Timing	Imperfect
 Timing	Unimplemented
 Total time Trackball Device Assignment Triple Buffering Type Type name or select: %1$s_ Type name or select: (random) UI Font UI Fonts Settings UI active Uptime: %1$d:%2$02d

 Uptime: %1$d:%2$02d:%3$02d

 Use External Samples Use image as background User Interface Vector Vector Flicker Video Mode Video Options Visible Delay WAN	Imperfect
 WAN	Unimplemented
 Wait Vertical Sync Window Mode Write X Y Year	%1$s
 Yes [compatible lists] [create] [empty slot] [empty] [failed] [file manager] [no category INI files] [no groups in INI file] [software list] color-channelAlpha color-channelBlue color-channelGreen color-channelRed color-optionBackground color-optionBorder color-optionClone color-optionMouse down background color color-optionMouse down color color-optionMouse over background color color-optionMouse over color color-optionNormal text color-optionNormal text background color-optionSelected background color color-optionSelected color color-optionSlider color color-optionSubitem color color-optionUnavailable color color-presetBlack color-presetBlue color-presetGray color-presetGreen color-presetOrange color-presetRed color-presetSilver color-presetViolet color-presetWhite color-presetYellow color-sampleClone color-sampleMouse Over color-sampleNormal color-sampleSelected color-sampleSubitem default emulation-featureLAN emulation-featureWAN emulation-featurecamera emulation-featurecolor palette emulation-featurecontrols emulation-featuredisk emulation-featuregraphics emulation-featurekeyboard emulation-featuremicrophone emulation-featuremouse emulation-featureprinter emulation-featureprotection emulation-featuresound emulation-featuretiming kHz machine-filterAvailable machine-filterBIOS machine-filterCHD Required machine-filterCategory machine-filterClones machine-filterCustom Filter machine-filterFavorites machine-filterHorizontal Screen machine-filterManufacturer machine-filterMechanical machine-filterNo CHD Required machine-filterNot BIOS machine-filterNot Mechanical machine-filterNot Working machine-filterParents machine-filterSave Supported machine-filterSave Unsupported machine-filterUnavailable machine-filterUnfiltered machine-filterVertical Screen machine-filterWorking machine-filterYear path-optionArtwork path-optionArtwork Previews path-optionBosses path-optionCabinets path-optionCategory INIs path-optionControl Panels path-optionCovers path-optionCrosshairs path-optionDATs path-optionFlyers path-optionHowTo path-optionINIs path-optionIcons path-optionLogos path-optionMarquees path-optionPCBs path-optionROMs path-optionScores path-optionSnapshots path-optionSoftware Media path-optionVersus playing recording selmenu-artworkArtwork Preview selmenu-artworkBosses selmenu-artworkControl Panel selmenu-artworkCovers selmenu-artworkFlyer selmenu-artworkGame Over selmenu-artworkHowTo selmenu-artworkLogo selmenu-artworkMarquee selmenu-artworkPCB selmenu-artworkScores selmenu-artworkSnapshots selmenu-artworkVersus software-filterAvailable software-filterClones software-filterCustom Filter software-filterDevice Type software-filterFavorites software-filterParents software-filterPartially Supported software-filterPublisher software-filterRelease Region software-filterSoftware List software-filterSupported software-filterUnavailable software-filterUnfiltered software-filterUnsupported software-filterYear stopped Project-Id-Version: MAME
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-17 07:08+1100
PO-Revision-Date: 2017-08-30 09:37+0200
Last-Translator: theheroGAC
Language-Team: MAME Language Team
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.0.3
 

Premi un tasto per proseguire 

Ci sono Cloni funzionanti di questa macchina: %s 
  Configurazione salvata   

 
Audio:
 
QUESTA MACCHINA NON FUNZIONA. L'emulazione di questa macchina non è ancora completa. Non c'è nulla che tu possa fare per risolvere questo problema eccetto aspettare che gli sviluppatori migliorino l'emulazione.
 
Video:
  (predefinito)  (bloccati)  COLORI  PEN %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Driver: %4$s

CPU:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d macchine (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d pacchetti software ) %1$s Luminosità %1$s Contrasto %1$s Gamma %1$s Posizione Orizzontale %1$s Allungamento Orizzontale %1$s Frequenza di aggiornamento %1$s Posizione Verticale %1$s Allungamento Verticale %1$s Volume %1$s,%2$-.100s %1$s: %2$s
 %1$s: %2$s - Cerca: %3$s_ %2$s
 %s %s
 aggiunto ai preferiti. %s
 rimosso dai preferiti. %s [interno] %s aggiunto (in esecuzione) (registra) **Errore nel salvataggio del file %s.ini** **Errore nel salvataggio del file ui.ini** , %s <imposta filtri> Impostazioni ARGB Attivato: %s Attivato: %s = %s Aggiungi Cartella %1$s - Cerca: %2$s_ Aggiungi Cartella Aggiungi ai Preferiti Aggiungi filtro Assegnazione Dispositivo Adstick Opzioni avanzate TUTTO Tutti i cheat sono stati ricaricati Controlli Analogici Controlli Analogici	Sì
 Sei sicuro di voler uscire?

Premi ''%1$s'' per uscire,
Premi ''%2$s'' per contrinuare l'emulazione. Opzioni Disegni Verifico le ROMs per %1$u le macchine marcate come non disponibili Verifico le ROMs per tutte %1$u le macchine Verifica delle ROMs per la macchina %2$u di %3$u...
%1$s Auto Salto frame automatico Ruota automaticamente a sinistra Ruota automaticamente a destra Salvataggio e ripristino automatico BIOS Selezione BIOS Selezione BIOS: Lettore Codice a Barre Lunghezza codice a barre errata! Peso intensità del Raggio Larghezza Massima del Raggio Larghezza Minima del Raggio Filtro Bilineare Divisore Bitmap Grassetto Info Crediti e Tempi di Utilizzo Brucia CPU o RAM Camera	Imperfetta
 Camera	Non implementata
 Cancella Non posso salvare in questa cartella Cambia Cartella %1$s - Cerca: %2$s_ Cambia Cartella Le modifiche avranno effetto solo quando "Inizia nuova ricerca" è selezionato Trucco Commento Cheat:
%s Cheat aggiunto a cheat.simple Cheat engine non disponibile Cheat scritto su %s e aggiunto a cheat.simple Cheat Seleziona dalla tavolozza Monete %1$c: %2$d%3$s
 Monete %1$c: NA%3$s
 Impulso moneta Bloccaggio della moneta Anteprima colore: Colori Comando Funzionalità assolutamente non emulate:  Configura Cartelle Configura Macchina Configura Opzioni Confermi l'uscita Controlli	Imperfetti
 Controlli	Non implementati
 Crea Offset del Puntatore %1$s Offset del Puntatore X %1$1.3f Offset del Puntatore Y %1$1.3f Opzioni Puntamento Scala del Puntatore %1$s Scala del Puntatore X %1$1.3f Scala del Puntatore Y %1$1.3f Cartella Corrente %1$s Tempo corrente Personalizzato Personalizza UI Formato dati Predefinito Il nome predefinito è %s Mappatura Dispositivo Assegnazione Dispositivo Dial Disabilitato Disabilitato: %s Disco	Imperfetto
 Disco	Non implementato
 Fatto Doppio click o premi %1$s per selezionare Driver Il Driver è un Clone di	%1$s
 Il Driver è un Parent	
 Il Driver è un clone di: %-.100s Il Driver è un parent Driver: "%1$s" lista software  Driver:%1$-.100s Emulata Abilitato Abilitato: %s Forza Proporzioni Video Ingrandisci immagini nel pannello di destra Inserisci codice a barre Esci Esporta la lista visualizzata in un file Esporta lista in formato TXT (come -listfull) Esporta lista in formato XML (come -listxml) Esporta lista in formato XML (come -listxml, ma escludendo i device) Visualizza DAT Aggiuntivi Avanti Veloce File File Già Esistente - Sovrascrivo? Gestore File Filtri Filtro %1$u Inverti X Inverti Y Configura Cartelle Font Forza aspetto 4:3 per la visualizzazione istantanea Salta fotogramma GLSL Inizializza Gioco Informazioni Generali Input Generali Grafica	Non Perfetta
 Grafica	Colori Non Perfetti
 Grafica	OK
 Grafica	Non implementata
 Grafica	Colori Errati
 Grafica: Non Perfetta,  Grafica: OK,  Grafica: Non implementata,  Gruppo HLSL Nascondi Entrambi Nascondi Filtri Nascondi Info/Immagine Nascondi le macchine prive di rom dalla lista disponibile Punteggi Migliori Storico Formato Immagine: Informazioni Software Immagini Funzionalità emulate non perfettamente:  Cloni inclusi Info controllo automatico Informazioni Dimensione testo info Input (generali) Input (questa Macchina) Opzioni di input Corsivo Joystick Punto morto joystick Saturazione joystick Tastiera	Imperfetta
 Tastiera	Non implementata
 Input di Tastiera	Sì
 Modalità Tastiera LAN	Imperfetta
 LAN	Non implementata
 Lingua Laserdisc '%1$s' Posizione Orizzontale Laserdisc '%1$s' Allungamento Orizzontale Laserdisc '%1$s' Posizione Verticale Laserdisc '%1$s' Allungamento Verticale Sinistra uguale a destra Sinistra, inferiore al valore Pistola leggera Assegnazione Dispositivo Lightgun Linee Carica Stato Info MAME Punteggio MARP Info MESS MHz Configurazione Macchina Informazioni Macchina Punteggio MAME Produttore	%1$s
 Volume Principale Anteprima Menu Microfono	Imperfetto
 Microfono	Non implementato
 Opzioni Varie Mouse Mouse	Imperfetto
 Mouse	Non implementato
 Assegnazione Dispositivo Mouse Multi-tastiera Multi-mouse Nome:             Descrizione:
 Naturale Tastiera naturale Dispositivi di Rete Nuovo codice a barre: Nuovo Nome Immagine: No Nessun file INI della categoria trovata Nessun gruppo trovato nel file di categoria Nessuna macchina trovata. Controlla il percorso specificato nel file %1$s.ini.

Se questa è la prima volta che usi %2$s, si prega di consultare il file config.txt nella cartella docs per le informazioni sulla configurazine %2$s. Nessuno Nessuno
 Non supportato Numero di Schermi Non Attivo Ricarica schermo spento Attivo Uno o più ROM/CHD per questa macchina non sono corretti. La macchina potrebbe non funzionare correttamente.
 Uno o più ROM/CHD per questa macchina non è dumpato correttamente.
 Altri Controlli Complessivamente	NON FUNZIONANTE
 Complessivamente	Protezione Non Emulata
 Complessivamente	Funzionante
 Complessivamente: NON FUNZIONANTE Complessivamente: Protezione Non Emulata Complessivamente: Funzionante Overclock %1$s suono Overclock CPU %1$s Assegnazione Dispositivo Paddle Pagina Parzialmente supportato Pausa/Stop Assegnazione Dispositivo Pedaliera Opzioni prestazioni Gioca Inserisci anche l'estensione Opzioni Plugin Plugin Assegnazione Dispositivo Posizionale Premi TAB per impostare Premi un tasto per continuare. Stampante	Imperfetta
 Stampante	Non implementata
 Pseudo terminali Leggi da questo file, scrivi su un altro Leggi da questo file, scrivi su diff Sola-Lettura Lettura-Scrittura Registra Ricarica Tutto Rimuovi Cartella %1$sr Rimuovi Cartella Rimuovi dai Preferiti Rimuovi ultimo filtro Richiede Artwork	No
 Richiede Artwork	Si
 Richiede CHD	No
 Richiede CHD	Si
 Richiede Artwork Cliccabile	No
 Richiede Artwork Cliccabile	Si
 Azzera Ripristina Tutto I risultati verranno salvati su %1$s Ritorna alla Macchina Torna al Menu Precedente Revisione: %1$s Riavvolgi Set di rom	%1$s
 Rotazione Ruota a sinistra Ruota a destra Opzioni di rotazione Frequenza di Campionamento Testo di esempio - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Salva Salva Cheat Salva Configurazione Salva Configurazione Macchina Salva Stato Schermo Schermo #%d Schermo '%1$s' Orientamento Schermo	Orizzontale
 Orientamento Schermo	Verticale
 L'inversione dello schermo in modalità cocktail non è supportata.
 Cerca: %1$s_ Seleziona Nuova Macchina Selezione modalità di accesso Seleziona categoria: Seleziona cheat per impostare gli hotkey Seleziona filtro personalizzato: Seleziona formato immagine Lista Selezione - Cerca:  Imposta i tasti di scelta rapida Impostazioni Mostra Tutti Mostra viste DAT Mostra puntatore mouse Mostra pannelli laterali Contraddizione simultanea Salta il menu di selezione del BIOS Salta la schermata delle informazioni all'avvio Non mostrare il menu di selezione del software Dormi Regolazione Cursori Slot Il software è Clone di: %1$-.100s Il software è parent Selezione parte software: Audio Audio	Non Perfetto
 Suoni	Nessuno
 Audio	OK
 Audio	Non Implementato
 Opzioni Audio Audio: Non Perfetto Suoni: Nessuno Audio: OK Audio: Non Implementato Velocità Avvia Massimizzato Inizia nuova ricerca Opzioni di Stato/Riproduzione Tasto costante Supporto Cocktail	No
 Supporto Salvataggio	No
 Supporto Salvataggio	Si
 Supportato: No Supportato: Parzialmente Supportato: Si Cambia Ordinamento Opzioni Ordinamento Cambiato: Opzioni ordinate ora per descrizione Ordinamento Cambiato: Opzioni ordinate ora per nome breve Aggiornamento Sincronizzato Info di Sistema Sistema: %1$-.100s Controllo Cassetta Il gioco selezionato è mancante di una o più ROM richieste o immagini CHD. Si prega di scegliere un gioco diverso.

Premi qualsiasi tasto per continuare. Ci sono problemi noti per questa macchina

 Questo driver richiede software caricato nel(i) dispositivo(i) seguente(i):  Questa macchina non ha BIOS. Questa macchina non ha alcun componente audio, MAME non produrrà alcun suono e questo è il comportamento atteso e non un bug.
 Questa macchina richiede file di artwork extra
 Questa macchina non è mai stata completata. Potrebbe mostrare comportamenti strani o elementi mancanti che non sono bug nell'emulazione.
 Throttle Biglietti emessi: %1$d

 Temporizzazione	Imperfetta
 Temporizzazione	Non implementata
 Tempo totale Assegnazione Dispositivo Trackball Triplo Buffering Tipo Digita il nome o seleziona: %1$s_ Digita il nome o seleziona: (casuale) Font UI Impostazioni Font UI UI attivo Tempo di utilizzo: %1$d:%2$02d

 Tempo di utilizzo: %1$d:%2$02d:%3$02d

 Usa Sample Aggiuntivi Usa immagine come sfondo Interfaccia Utente Vettoriale Sfarfallio Vettoriale Modalità Video Opzioni Video Ritardo Visibilità WAN	Imperfetta
 WAN	Non implementata
 Attendi la Sincronizzazione Verticale Modalità a Finestra Scrivere X Y Anno	%1$s
 Si [lista compatibile] [crea] [slot vuoto] [vuoto] [fallito] [gestore file] [nessun file INI di categoria] [nessun gruppo nel file INI] [lista software] Alfa Blu Verde Rosso Sfondo Bordo Clone Colore sfondo mouse down Colore mouse down Colore sfondo mouse over Colore mouse over Testo normale Sfondo testo normale Colore sfondo selezionato Colore selezionato Colore cursore Colore opzione menu Colore non disponibile Nero Blu Grigio Verde Arancio Rosso Argento Viola Bianco Giallo Clone Mouse Sopra Normale Selezionato Opzione menu predefinito LAN WAN camera tavolozza colore controlli disco grafica tastiera microfono mouse stampante protezione audio temporizzazione kHz Disponibile BIOS CHD Richiesto Categoria Cloni Filtro Personalizzato Preferiti Schermata Orizzontale Produttore Meccanico Nessun CHD Richiesto Nessun BIOS Non Meccanico Non Funziona Genitori Salvataggio Supportato Salvataggio non Supportato Non disponibile Non filtrato Schermata Verticale Funziona Anno Disegni Anteprima Disegni Boss Cabinati Categoria INI Pannelli di controllo Copertine Puntatori DAT Volantini Come Fare INI Icone Logo Tendoni PCB ROM Punteggi Istantanee Programma Multimediale Contro in esecuzione registra Anteprima Artwork Boss Pannello di controllo Copertine Volantino Game Over Come Fare Logo Tendone PCB Punteggi Istantanee Control Disponibile Cloni Filtro Personalizzato Tipo Dispositivo Preferiti Genitori Parzialmente Supportato Editore Regione di Rilascio Lista Software Supportato Non disponibile Non filtrato Non supportato Anno fermato 