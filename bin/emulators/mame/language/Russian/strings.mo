��    %     D              l"     m"     �"  .   �"     �"     �"  �   �"     �#  
   �#  	   �#     �#     �#     �#  "   �#  .   $  +   ?$     k$     {$  
   �$     �$     �$     �$     �$     �$     �$     �$     %      6%     W%     e%  	   n%     x%     �%     �%     �%     �%     �%     �%     �%  
   &     &  
   0&     ;&     U&     f&     j&     ~&     �&     �&  ^   �&     '  /   '      F'  .   g'     �'     �'     �'     �'     �'     �'     �'     �'     	(     (     0(     F(     Y(     l(     (     �(     �(     �(  
   �(     �(     �(     �(     �(  "   )     ')  D   5)     z)     �)     �)  
   �)     �)     �)  -   �)     *     *     **     8*     M*     `*     m*     z*     �*     �*      �*     �*     �*     �*     �*     +     "+     :+     A+     W+     r+     �+     �+     �+     �+     �+     �+     
,     ,     ,     +,     7,     ?,     R,     a,     x,     �,     �,     �,     �,  $   �,     �,     �,     �,     
-     #-     6-     T-     e-     �-     �-     �-     �-     �-  !   �-  
   �-     �-     �-  *   .  )   B.  >   l.     �.     �.     �.     �.     �.     �.     /     /     /     /     +/  %   1/  
   W/     b/     k/     x/     �/     �/     �/     �/     �/     �/     0     0     10  	   70     A0     N0  (   ^0     �0     �0     �0     �0     �0     �0     �0     �0     1     1     1     '1     <1     J1     Q1     Z1     l1     �1     �1     �1     �1     �1     �1     �1     �1     2     92     X2     v2      �2     �2     �2     �2  $   �2     3     )3     23     M3  
   S3     ^3     b3     x3     �3     �3     �3     �3     �3     �3     �3     4     %4     ;4     A4     R4     g4     4     �4     �4     �4     �4     �4     �4     �4      5     5      5  �   @5     6     6     6     '6     96     =6     N6  Y   Q6  G   �6     �6     7     7     67     G7     \7     {7     �7     �7     �7     �7     �7  
   �7     �7     	8     8  
   "8     -8  !   48     V8     e8     m8     �8     �8     �8     �8     �8     �8     9  '   9     D9  	   c9  
   m9     x9  
   9     �9     �9     �9     �9     �9     �9     �9     :     ":     A:     a:  	   g:     q:     �:     �:     �:     �:     �:     �:     �:     �:     �:     ;  F   ;     `;  !   z;     �;     �;  
   �;     �;  
   �;     �;  
   �;     �;     <     "<  3   ?<     s<     �<     �<     �<     �<     �<     �<     �<     =     =     (=     1=     :=     I=     \=     m=     �=  "   �=  "   �=     �=     �=     �=     
>     *>     =>     V>     \>     m>  	   y>     �>     �>     �>     �>  	   �>     �>     �>     �>     �>     ?  	   $?     .?     C?     T?     f?     t?     �?     �?     �?     �?     �?     �?  �   �?  ,   e@  E   �@     �@  ]   �@  .   PA  z   A     �A     B     B     #B     5B  
   KB     VB     rB     �B     �B     �B     �B     �B  	   �B  8   �B     C     4C     JC     gC     |C     �C     �C     �C     �C  
   �C     �C     �C     �C     �C     D     D     !D     -D  
   3D     >D     BD     UD     ^D     kD     sD     |D     �D     �D     �D     �D     �D     �D     E     E     0E     DE  '   \E  (   �E     �E  (   �E     �E     F  #   +F  &   OF     vF     �F     �F     �F     �F     �F     G     G     0G     DG     UG     iG     }G     �G     �G     �G     �G     �G     �G     H     H     /H     HH     hH     �H     �H     �H     �H     �H     I     I     <I     TI     mI     qI     �I     �I     �I     �I     �I      J     )J     EJ     _J     ~J     �J     �J     �J     �J     K     (K     BK     aK     xK     �K     �K     �K     �K     �K     �K     L     -L     DL     UL     hL     zL     �L     �L     �L     �L     �L     �L     �L     M     *M     =M  	   EM     OM     oM     �M     �M     �M     �M     �M     N     N     .N     FN     ZN     qN     �N     �N     �N     �N     �N  #   O     4O     NO     mO     �O     �O     �O     �O     �O     P     P     ,P  �  EP  4   �Q  H   #R  L   lR  0   �R     �R  (  �R     T     -T     HT     XT     dT  	   pT  9   zT  2   �T  9   �T     !U     5U     KU     [U     {U  (   �U     �U     �U     �U     V  :   3V  6   nV     �V     �V     �V     �V  ,   W  ,   :W  #   gW     �W     �W  !   �W  4   �W     X  &   3X     ZX  7   xX  /   �X     �X  *   �X  )   Y  /   <Y  
   lY  �   wY  '   #Z  e   KZ  :   �Z  D   �Z     1[  $   :[  "   _[  $   �[  B   �[     �[     �[     �[  "   \  1   3\  !   e\  .   �\  ,   �\  )   �\  6   ]     D]     Q]     o]  &   �]  #   �]  )   �]     �]  H   ^  4   K^     �^  �   �^     _  !   '_     I_     __  '   o_  -   �_  ?   �_     `  $   `  !   3`  ,   U`  ,   �`  (   �`  3   �`  $   a  
   1a     <a  G   Ka     �a  !   �a  #   �a  9   �a  +   1b  1   ]b     �b  $   �b  )   �b  )   �b  !   c  "   9c  '   \c  '   �c     �c     �c      �c  '   d     .d     Md     gd  !   d  )   �d  .   �d     �d     e     "e  %   Be     he  M   ue     �e     �e     �e  !   f     .f  &   Hf     of  7   �f     �f     �f     �f     �f  %   g  L   <g     �g  
   �g  B   �g  a   �g  Q   Oh     �h  !   !i  !   Ci     ei  8   ni  !   �i     �i     �i  .   �i  *   j     @j     ^j  T   kj     �j     �j     �j  !   k  %   1k  ,   Wk     �k  )   �k  ,   �k  '   �k     l  -   +l     Yl     fl     zl  :   �l  O   �l     !m     7m     Fm  &   am     �m  H   �m     �m  "    n     #n  "   8n  !   [n  ,   }n     �n     �n     �n  *   �n  %   o  +   =o  1   io  &   �o     �o  2   �o  8   p     Np  4   Wp  2   �p  2   �p  0   �p  $   #q  4   Hq  &   }q  (   �q  (   �q  9   �q  +   0r  !   \r  O   ~r  
   �r  '   �r     s  !   s  &   *s      Qs     rs     �s  /   �s  -   �s  !   
t  '   ,t  -   Tt  /   �t     �t     �t  %   �t  2   u  !   4u     Vu  '   lu     �u  #   �u  #   �u     �u     v     v  5   &v  A   \v  j  �v     	x      x  !   (x     Jx     dx  +   mx     �x  �   �x  |   Fy  1   �y  $   �y  7   z     Rz  $   rz  7   �z     �z     �z  &   {  0   3{     d{  -   u{     �{  6   �{  7   �{     &|     E|  
   a|  9   l|  #   �|     �|  B   �|  &   }  G   C}  %   �}  +   �}     �}  9   �}  +   7~  G   c~  ?   �~     �~            !   ,     N     k  (   �  .   �  <   �  :   �     U�     p�  D   ��  B   ΀  
   �     �  :   4�  "   o�  /   ��          ׁ     �     ��     �     (�  #   D�     h�  v   ��  D   ��  K   >�  6   ��     ��     ԃ  %   �  %   �  
   :�     E�     T�  ?   f�  ;   ��  o   �     R�  (   d�  *   ��  $   ��  :   ݅  A   �  *   Z�  (   ��     ��  0   ņ     ��     	�     !�  0   =�  (   n�  N   ��  /   �  M   �  <   d�  
   ��  )   ��  -   ֈ  *   �     /�  )   K�     u�     ~�      ��     ��  %   ̉     �     �      0�     Q�  %   ^�     ��  5   ��  "   ˊ  F   �     5�  G   U�  5   ��  3   Ӌ  $   �  .   ,�  "   [�  0   ~�  9   ��     �  !   �     %�    .�  Q   @�  s   ��  '   �  �   .�  W   �    I�     [�  #   n�     ��  '   ��  -   Ǒ     ��  :   �  %   F�     l�  4   s�  A   ��     �  4   
�  <   ?�  s   |�  3   �  &   $�  -   K�  4   y�  J   ��  +   ��     %�     6�     I�     c�     y�     ��  2   ��  8   �  B   #�     f�     w�     ��     ��     ��  %   ��     ֖     �     ��     �     "�  *   B�  %   m�     ��  
   ��  
   ��     ��     ̗     ۗ     �     ��  .   �  '   J�     r�  #   ��     ��     И  (   �  ,   �     @�     ^�     x�     ��     ��  
   ��  
   ̙     י     �     ��     �     �  
   4�     ?�     L�     ^�     m�     ��     ��     ��     ǚ     �     �     �     %�     .�     =�     R�     c�     l�     {�     ��     ��     ��     ��     ��     ��     ԛ  -   �     �  '   (�     P�     k�     ��  	   ��     ��     Ĝ     ڜ     ��     �     -�  #   C�     g�     x�  %   �  -   ��  
   ӝ     ޝ     �  !   �     (�     7�     F�     U�     d�     u�     ��     ��     ��  
   ��     ��          �  %   ��     "�     /�     J�  -   c�  
   ��     ��  !   ��     ͟     ܟ     �     �     �     #�  
   2�     =�     ]�     w�     ��  -   ��     Š     �  -   ��     "�  +   3�     _�     q�     ��     ��  !   ��     ߡ     �     ��     �   

--- DRIVER INFO ---
Driver:  

Press any key to continue 

There are working clones of this machine: %s 
    Configuration saved    

 
Sound:
 
THIS MACHINE DOESN'T WORK. The emulation for this machine is not yet complete. There is nothing you can do to fix this problem except wait for the developers to improve the emulation.
 
Video:
  (default)  (locked)  COLORS  PENS %1$3ddB %1$s
%2$s %3$s
Driver: %4$s

CPU:
 %1$s %2$s ( %3$d / %4$d machines (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d software packages ) %1$s Brightness %1$s Contrast %1$s Gamma %1$s Horiz Position %1$s Horiz Stretch %1$s Refresh Rate %1$s Vert Position %1$s Vert Stretch %1$s Volume %1$s: %2$s - Search: %3$s_ %s
 added to favorites list. %s
 removed from favorites list. %s [internal] %s added (playing) (recording) **Error saving %s.ini** **Error saving ui.ini** <set up filters> ARGB Settings Activated: %s Activated: %s = %s Add %1$s Folder - Search: %2$s_ Add Folder Add To Favorites Add filter Adstick Device Assignment Advanced Options All All cheats reloaded Analog Controls Analog Controls	Yes
 Any Are you sure you want to quit?

Press ''%1$s'' to quit,
Press ''%2$s'' to return to emulation. Artwork Options Audit ROMs for %1$u machines marked unavailable Audit ROMs for all %1$u machines Auditing ROMs for machine %2$u of %3$u...
%1$s Auto Auto frame skip Auto rotate left Auto rotate right Automatic save/restore BIOS BIOS Selection BIOS selection: Barcode Reader Barcode length invalid! Beam Intensity Weight Beam Width Maximum Beam Width Minimum Bilinear Filtering Bitmap Prescaling Bold Bookkeeping Info Burn-in CPU or RAM Camera	Imperfect
 Camera	Unimplemented
 Cancel Cannot save over directory Change %1$s Folder - Search: %2$s_ Change Folder Changes to this only take effect when "Start new search" is selected Cheat Cheat Comment:
%s Cheat Finder Cheat Name Cheat added to cheat.simple Cheat engine not available Cheat written to %s and added to cheat.simple Cheats Choose from palette Clear Watches Coin %1$c: %2$d%3$s
 Coin %1$c: NA%3$s
 Coin impulse Coin lockout Color preview: Colors Command Completely unemulated features:  Configure Directories Configure Machine Configure Options Confirm quit from machines Controls	Imperfect
 Controls	Unimplemented
 Create Crosshair Offset %1$s Crosshair Offset X %1$1.3f Crosshair Offset Y %1$1.3f Crosshair Options Crosshair Scale %1$s Crosshair Scale X %1$1.3f Crosshair Scale Y %1$1.3f Current %1$s Folders Current time Custom Customize UI DIP Switches Data Format Default Default name is %s Device Mapping Dial Device Assignment Disabled Disabled: %s Disk	Imperfect
 Disk	Unimplemented
 Done Double-click or press %1$s to select Driver Driver is BIOS	No
 Driver is BIOS	Yes
 Driver is Clone of	%1$s
 Driver is Parent	
 Driver is clone of: %1$-.100s Driver is parent Driver: "%1$s" software list  Driver: %1$-.100s Emulated Enabled Enabled: %s Enforce Aspect Ratio Enlarge images in the right panel Enter Code Exit Export displayed list to file Export list in TXT format (like -listfull) Export list in XML format (like -listxml) Export list in XML format (like -listxml, but exclude devices) External DAT View Fast Forward File File Already Exists - Override? File Manager Filter Filter %1$u Flip X Flip Y Folders Setup Fonts Force 4:3 aspect for snapshot display Frame skip Gameinit General Info General Inputs Graphics	Imperfect
 Graphics	Imperfect Colors
 Graphics	OK
 Graphics	Unimplemented
 Graphics	Wrong Colors
 Graphics: Imperfect,  Graphics: OK,  Graphics: Unimplemented,  Group Hide Both Hide Filters Hide Info/Image Hide romless machine from available list High Scores History Image Format: Image Information Images Imperfectly emulated features:  Include clones Info auto audit Infos Infos text size Input (general) Input (this Machine) Input Options Italic Joystick Joystick deadzone Joystick saturation Keyboard	Imperfect
 Keyboard	Unimplemented
 Keyboard Inputs	Yes
 Keyboard Mode LAN	Imperfect
 LAN	Unimplemented
 Language Laserdisc '%1$s' Horiz Position Laserdisc '%1$s' Horiz Stretch Laserdisc '%1$s' Vert Position Laserdisc '%1$s' Vert Stretch Left equal to right Left equal to right with bitmask Left equal to value Left greater than value Left less than value Left not equal to right with bitmask Left not equal to value Lightgun Lightgun Device Assignment Lines Load State MHz Machine Configuration Machine Information Manufacturer	%1$s
 Master Volume Match block Mechanical Machine	No
 Mechanical Machine	Yes
 Menu Preview Microphone	Imperfect
 Microphone	Unimplemented
 Miscellaneous Options Mouse Mouse	Imperfect
 Mouse	Unimplemented
 Mouse Device Assignment Multi-keyboard Multi-mouse Name:             Description:
 Natural Natural keyboard Network Devices New Barcode: New Image Name: No No category INI files found No groups found in category file No machines found. Please check the rompath specified in the %1$s.ini file.

If this is your first time using %2$s, please see the config.txt file in the docs directory for information on configuring %2$s. None None
 Not supported Number Of Screens Off Offscreen reload On One or more ROMs/CHDs for this machine are incorrect. The machine may not run correctly.
 One or more ROMs/CHDs for this machine have not been correctly dumped.
 Other Controls Overall	NOT WORKING
 Overall	Unemulated Protection
 Overall	Working
 Overall: NOT WORKING Overall: Unemulated Protection Overall: Working Overclock %1$s sound Overclock CPU %1$s Paddle Device Assignment Page Partially supported Pause/Stop Pedal Device Assignment Performance Options Play Play Count Player Please enter a file extension too Plugin Options Plugins Positional Device Assignment Press TAB to set Press any key to continue. Printer	Imperfect
 Printer	Unimplemented
 Pseudo terminals ROM Audit Result	BAD
 ROM Audit Result	OK
 Read this image, write to another image Read this image, write to diff Read-only Read-write Record Reload All Remove %1$s Folder Remove Folder Remove From Favorites Remove last filter Requires Artwork	No
 Requires Artwork	Yes
 Requires CHD	No
 Requires CHD	Yes
 Requires Clickable Artwork	No
 Requires Clickable Artwork	Yes
 Reset Reset All Results will be saved to %1$s Return to Machine Return to Previous Menu Revision: %1$s Rewind Romset	%1$s
 Rotate Rotate left Rotate right Rotation Options Sample Rate Sample text - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Samples Audit Result	BAD
 Samples Audit Result	None Needed
 Samples Audit Result	OK
 Save Save Cheat Save Configuration Save State Screen Screen #%d Screen '%1$s' Screen Orientation	Horizontal
 Screen Orientation	Vertical
 Screen flipping in cocktail mode is not supported.
 Search: %1$s_ Select New Machine Select access mode Select category: Select cheat to set hotkey Select custom filters: Select image format Selection List - Search:  Set Set hotkeys Settings Show All Show DATs view Show mouse pointer Show side panels Simultaneous contradictory Skip BIOS selection menu Skip information screen at startup Skip software parts selection menu Sleep Slider Controls Slot Devices Software is clone of: %1$-.100s Software is parent Software part selection: Sound Sound	Imperfect
 Sound	None
 Sound	OK
 Sound	Unimplemented
 Sound Options Sound: Imperfect Sound: None Sound: OK Sound: Unimplemented Speed Start Out Maximized Start new search State/Playback Options Steadykey Support Cocktail	No
 Support Save	No
 Support Save	Yes
 Supported: No Supported: Partial Supported: Yes Switch Item Ordering Synchronized Refresh System: %1$-.100s Tape Control Test The selected game is missing one or more required ROM or CHD images. Please select a different game.

Press any key to continue. There are known problems with this machine

 This driver requires images to be loaded in the following device(s):  This machine has no BIOS. This machine has no sound hardware, MAME will produce no sounds, this is expected behaviour.
 This machine requires external artwork files.
 This machine was never completed. It may exhibit strange behavior or missing elements that are not bugs in the emulation.
 Throttle Tickets dispensed: %1$d

 Timer Timing	Imperfect
 Timing	Unimplemented
 Total time Trackball Device Assignment Triple Buffering Type Type name or select: %1$s_ Type name or select: (random) UI Font UI Fonts Settings UI active Unable to write file
Ensure that cheatpath folder exists Undo last search -- # Uptime: %1$d:%2$02d

 Uptime: %1$d:%2$02d:%3$02d

 Use External Samples Use image as background User Interface Value Vector Vector Flicker Video Mode Video Options Visible Delay WAN	Imperfect
 WAN	Unimplemented
 Wait Vertical Sync Watch Window Mode Write Year	%1$s
 Yes [compatible lists] [create] [empty slot] [empty] [failed] [file manager] [no category INI files] [no groups in INI file] [software list] color-channelAlpha color-channelBlue color-channelGreen color-channelRed color-optionBackground color-optionBorder color-optionDIP switch color-optionGraphics viewer background color-optionMouse down background color color-optionMouse down color color-optionMouse over background color color-optionMouse over color color-optionNormal text color-optionNormal text background color-optionSelected background color color-optionSelected color color-optionSlider color color-optionSubitem color color-optionUnavailable color color-presetBlack color-presetBlue color-presetGray color-presetGreen color-presetOrange color-presetRed color-presetSilver color-presetViolet color-presetWhite color-presetYellow color-sampleMouse Over color-sampleNormal color-sampleSelected color-sampleSubitem default emulation-featureLAN emulation-featureWAN emulation-featurecamera emulation-featurecolor palette emulation-featurecontrols emulation-featuredisk emulation-featuregraphics emulation-featurekeyboard emulation-featuremicrophone emulation-featuremouse emulation-featureprinter emulation-featureprotection emulation-featuresound emulation-featuretiming kHz machine-filterAvailable machine-filterBIOS machine-filterCHD Required machine-filterCategory machine-filterCustom Filter machine-filterFavorites machine-filterHorizontal Screen machine-filterManufacturer machine-filterMechanical machine-filterNo CHD Required machine-filterNot BIOS machine-filterNot Mechanical machine-filterNot Working machine-filterSave Supported machine-filterSave Unsupported machine-filterUnavailable machine-filterUnfiltered machine-filterVertical Screen machine-filterWorking machine-filterYear path-optionArtwork path-optionArtwork Previews path-optionBosses path-optionCabinets path-optionCategory INIs path-optionControl Panels path-optionCovers path-optionCrosshairs path-optionDATs path-optionFlyers path-optionHowTo path-optionINIs path-optionIcons path-optionLogos path-optionMarquees path-optionPCBs path-optionROMs path-optionScores path-optionSnapshots path-optionSoftware Media path-optionVersus playing recording selmenu-artworkArtwork Preview selmenu-artworkBosses selmenu-artworkCabinet selmenu-artworkControl Panel selmenu-artworkCovers selmenu-artworkFlyer selmenu-artworkGame Over selmenu-artworkHowTo selmenu-artworkLogo selmenu-artworkMarquee selmenu-artworkPCB selmenu-artworkScores selmenu-artworkSnapshots selmenu-artworkVersus software-filterAvailable software-filterCustom Filter software-filterDevice Type software-filterFavorites software-filterPartially Supported software-filterPublisher software-filterRelease Region software-filterSoftware List software-filterSupported software-filterUnavailable software-filterUnfiltered software-filterUnsupported software-filterYear stopped swlist-infoProgrammer swlist-infoRelease Date Project-Id-Version: MAME
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-17 07:08+1100
PO-Revision-Date: 2017-10-23 16:22+0300
Last-Translator: Automatically generated
Language-Team: MAME Language Team
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 

--- ИНФО ДРАЙВЕРА ---
Драйвер:  

Нажмите любую клавишу для продолжения 

Известные работающие clones этой системы: %s 
    Настройки сохранены    

 
Звук:
 
ЭТА СИСТЕМА НЕ РАБОТАЕТ. Эмуляция этой системы пока не завершена. Вы никак не можете исправить эту проблему, лишь подождать когда разработчики улучшат эмуляцию.
 
Видео:
  (по-умолчанию)  (закрыт)  ЦВЕТА  ПЕРЬЯ %1$3dдБ %1$s
%2$s %3$s
Драйвер: %4$s

Процессор:
 %1$s %2$s ( %3$d / %4$d систем (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d пакетов программ ) %1$s Яркость %1$s Контраст %1$s Гамма %1$s гориз. позиция %1$s гориз. размер %1$s Частота обновления %1$s верт. позиция %1$s верт. размер %1$s Громкость %1$s: %2$s - Поиск: %3$s_ %s
 добавлен в список избранного. %s
 убран из списка избранного. %s [внутренний] %s добавлено (проигрывается) (записывается) **Ошибка сохранения %s.ini** **Ошибка сохранения ui.ini** <настроить фильтры> Настройки ARGB Активировано: %s Активировано: %s = %s Добавить папку %1$s - Поиск: %2$s_ Добавить папку Добавить в Избранное Добавить фильтр Назначение устройства AD-стика Дополнительные настройки Все Все читы перезагружены Аналоговое управление Аналоговое управление	Да
 Любое Вы действительно хотите выйти?

Нажмите ''%1$s'' для выхода,
Нажмите ''%2$s'' для возврата к эмуляции. Настройки оформления Проверить ROMы для %1$u систем, помеченных как недоступные Проверить ROMы для всех %1$u систем Проверяем ROMы для системы %2$u из %3$u...%1$s Авто Авто-пропуск кадров Авто-поворот влево Авто-поворот вправо Автоматическое сохранение/загрузка BIOS Выбор BIOS Выбор BIOS: Сканер штрих-кодов Не верная длина штрих-кода! Насыщенность луча Максимальная ширина луча Минимальная ширина луча Билинейная фильтрация Пре-масштабирование битмапов Жирный Статистика игры Сгорание Процессор или память Камера	Не идеально
 Камера	Не эмулируется
 Отмена Не удалось сохранить поверх директории Изменить папку %1$s - Поиск: %2$s_ Изменить папку Эти изменения будут применены только после выбора "Начать новый поиск" Читы Коментарий чита:
%s Поиск читов Имя чита Чит добавлен в cheat.simple Движок читов не доступен Чит записан в %s и добавлен в cheat.simple Читы Выберите из палитры Очистить просмотр Монетоприемник %1$c: %2$d%3$s
 Монетоприемник %1$c: НД%3$s
 Время импульса монеты Блокировка монетоприемника Предпросмотр цвета: Цвета Команда Полностью не эмулируются возможности:  Настройки путей Настройки системы Изменение настроек Подтверждать выход из эмуляции Управление	Не идеально
 Управление	Не эмулируется
 Создать Смещение прицела %1$s Смещение прицела X %1$1.3f Смещение прицела Y %1$1.3f Настройки прицела Масштаб прицела %1$s Масштаб прицела X %1$1.3f Масштаб прицела Y %1$1.3f Текущий %1$s Папок Текущее время Пользовательский Настройка интерфейса DIP-переключатели Формат данных По умолчанию Имя по умолчанию %s Устройства управления Назначение устройства Dial Выключен Запрещено: %s Диск	Не идеально
 Диск	Не эмулируется
 Готово Кликние два раза или нажмите %1$s для выбора Драйвер Драйвер - BIOS	Нет
 Драйвер - BIOS	Да
 Драйвер - Clone от	%1$s
 Драйвер - Parent	
 Драйвер - clone от: %1$-.100s Драйвер - parent Драйвер: "%1$s" списков программ  Драйвер: %1$-.100s Эмулируется Включен Разрешено: %s Сохранять пропорции Увеличивать изображения на правой панели Введите код Выход Экспорт отображаемого списка в файл Экспортировать список в текстовом формате (как -listfull) Экспортировать список в формате XML (как -listxml) Экспортировать список в формате XML (как -listxml, но исключить устройства) Просмотр DAT-файлов Быстрая перемотка Файл Файл уже существует - Заменить? Файловый менеджер Фильтр Фиьтр %1$u Отражение по горизонтали Отражение по вертикали Установки папок Шрифты Принудительно 4:3 при просмотре снимков экрана Пропуск кадров Иниц. игры Общая информация Управление (общее) Графика	Не идеально
 Графика	Не точные цвета
 Графика	OK
 Графика	Не эмуируется
 Графика	Не верные цвета
 Графика: Не идеально,  Графика: OK,  Графика: Не эмулируется,  Группа Скрыть оба Скрыть фильтры Скрыть информацию / изображение Скрывать из списка доступных системы без ROM Лучшие очки История Формат образа: Информация о образах Изображения Несовершенно эмулируемые возможности:  Включить clones Авто-проверка инфо Информация Размер инфо-текста Управление (общее) Управление (эта система) Настройки ввода Наклонный Джойстик Мертвая зона джойстика Сатурация джойстика Клавиатура	Не идеально
 Клавиатура	Не эмулируется
 Ввод с клавиатуры	Да
 Режим клавиатуры Локальная сеть	Не идеально
 Локальная сеть	Не эмулируется
 Язык Лазердиск '%1$s' гориз. позиция Лазердиск '%1$s' гориз. размер Лазердиск '%1$s' верт. позиция Лазердиск '%1$s' верт. размер Левый равен правому Левый равен правому по маске Левый равен значению Левый больше значения Левый меньше значения Левый не равен правому по маске Левый не равен значению Световой пистолет Назначение устройства светового пистолета Линии Загрузить сохранение МГц Настройки системы Информация о системе Производитель	%1$s
 Общая громкость Совпадает блок Механическая система	Нет
 Механическая система	Да
 Предпросмотр меню Микрофон	Не идеально
 Микрофон	Не эмулируется
 Дополнительные настройки Мышь Мышь	Не идеально
 Мышь	Не эмулируется
 Назначение устройства мыши Мульти-клавиатура Мульти-мышь Имя:              Описание:
 Обычная Обычная клавиатура Сетевые устройства Новый штрихкод: Новый образ: Нет Не найдены INI-файлы категорий Не найдены группы в файле категорий Не найдено систем. Пожалуйста, проверьте rompath указанный в файле %1$s.ini.

Если вы в первый раз используете %2$s, пожалуйста, посмотрите файл config.txt в директории docs, чтобы получить информацию о настройке %2$s. Отсутствует Нет
 Не поддерживается Число экранов Выкл Закадровая перезарядка Вкл Один или несколько ROMов/CHD этой системы не верные. Эта система может работать не правильно.
 Один или несколько ROMов/CHD этой системы не были нормально сдамплены.
 Дополнительное управление В целом	НЕ РАБОТАЕТ
 В целом	Не эмулируется защита
 В целом	Работает
 В целом: НЕ РАБОТАЕТ В целом: Не эмулируется защита В целом: Работает Разгон %1$s звука Разгон процессора %1$s Назначение устройства Paddle Страница Частично поддерживается Пауза/Стоп Назначение устройства педали Настройки производительности Воспроизведение Количество игр Игрок Введите также расширение файла Настройки плагинов Плагины Назначение позиционного устройства Нажмите TAB для выбора Нажмите любую клавишу для продолжения. Принтер	Не идеально
 Принтер	Не эмулируется
 Псевдо-терминалы Результат проверки ROM	НЕУДАЧНО
 Результат проверки ROM	OK
 Чтение из этого образа, запись в другой Чтение из этого образа, запись в diff Только чтение Чтение-запись Запись Перезагрузить все Убрать папку %1$s Удалить папку Удалить из Избранного Удалить последний фильтр Требует элементы оформления	Нет
 Требует элементы оформления	Да
 Требует CHD	Нет
 Требует CHD	Да
 Требует кликабельное оформление	Нет
 Требует кликабельное оформление	Да
 Сброс Сбросить все Результаты будут сохранены в %1$s Возврат к эмуляции Возврат в предыдущее меню Ревизия: %1$s Перемотка ROM-набор	%1$s
 Поворот Поворот влево Поворот вправо Настройки поворота Частота звука Пример текста - Съешь же ещё этих французских булок, да выпей чаю. Результат проверки сэмплов	НЕУДАЧНО
 Результат проверки сэмплов	Не требуется
 Результат проверки сэмплов	OK
 Сохранить Сохранить чит Сохранить настройки Записать сохранение Экран Экран #%d Экран '%1$s' Ориентация экрана	Горизонтальная
 Ориентация экрана	Вертикальная
 Переворот экрана в режиме коктейль-стола не поддерживается.
 Поиск: %1$s_ Выбрать новую систему Выберите режим доступа Выберите категорию: Выбрать чит для горячей клавиши Выберите пользовательские фильтры: Выберите формат образа Список выбора - Поиск:  Установлено Назначить горячие клавиши Настройки Показать все Показать вид DAT Отображать указатель мыши Скрыть боковые панели Одновременно противоположные направления Пропускать меню выбора BIOS Пропустить сведения о системе при запуске Пропускать меню выбора частей ПО Спать Управление слайдерами Подключаемые устройства Программа - clone от: %1$-.100s Программа - parent Выбор части программы: Звук Звук	Не идеально
 Звук	Отсутствует
 Звук	OK
 Звук	Не эмулируется
 Настройки звука Звук: Не идеально Звук: Отсутствует Звук: OK Звук: Не эмулируется Скорость Открывать максимизированным Начать новый поиск Настройки сохранения/воспроизведения Задержка нажатий Поддерживает режим коктейль-стола	Нет
 Поддерживает сохранение	Нет
 Поддерживает сохранение	Да
 Поддерживается: Нет Поддерживается: Частично Поддерживается: Да Сменить порядок элементов Синхронизированное обновление Система: %1$-.100s Управление лентой Тест Для выбранной игры отсутствует один или более необходимых образов ROM или CHD. Пожалуйста, выберите другую игру.

Нажмите любую клавишу для продолжения. Имеются известные проблемы с этой системой

 Этот драйвер требует загрузки образов в следующие устройства:  У этой системы нет BIOS. У этой системы нет звукового оборудования, в процессе эмуляции не будет никаких звуков, так и должно быть.
 Эта система требует внешние графические файлы.
 Разработка этой системы так и не была завершена. Может наблюдаться странное поведение или отсутсвующие элементы. Это не является ошибками эмуляции.
 Троттлинг Выдано билетов: %1$d

 Таймер Скорость	Не идеально
 Скорость	Не эмулируется
 Общее время Назначение устройства трекбола Тройная буферизация Тип Введите имя или выберите: %1$s_ Введите имя или выберите: (случайно) Шрифт интерфейса Настройки шрифта интерфейса Активный интерфейс пользователя Не удалось сохранить файл
Убедитесь что папка cheatpath существует Отменить последний поиск -- # Время работы: %1$d:%2$02d

 Время работы: %1$d:%2$02d:%3$02d

 Использовать внешние сэмплы Использовать изображение как задний фон Интерфейс пользователя Значение Векторный Мерцание луча Видео режим Настройки видео Видимая задержка Удаленная сеть	Не идеально
 Удаленная сеть	Не эмулируется
 Ожидать вертикальную синхронизацию Просмотр Оконный режим Запись Год	%1$s
 Да [совместимые списки] [создать] [пустой слот] [пусто] [не удалось] [менеджер файлов] [INI-файлы без категорий] [нет групп в INI-файле] [список ПО] Альфа Синий Зеленый Красный Задний фон Бордюр DIP-переключатель Фон просмотрщика графики Цвет фона мышь нажата Цвет мышь нажата Цвет фона под мышью Цвет под мышью Обычный текст Обычный текст задника Выделенный цвет задника Выделенный цвет Цвет слайдера Цвет подпункта Недоступный цвет Черный Синий Серый Зеленый Оранжевый Красный Серебристый Фиолетовый Белый Желтый Под мышью Обычный Выделенный Подпункт по-умолчанию Локальная сеть Удаленная сеть камера цветовая палитра управление диск графика клавиатура микрофон мышь принтер защита звук скорость кГц Имеющиеся BIOS Требуют CHD Категории Пользовательский фильтр Избранное Горизонтальный экран Производитель Механические Не требуют CHD Не BIOS Не механические Не работают Есть сохранения Нет сохранений Отсутсвующие Без фильтра Вертикальный экран Работают Год Элементы оформления Предпросмотр оформления Боссы Кабинеты INI категорий Панели управления Обложки Прицелы DAT-файлы Буклеты Обучение INI-файлы Иконки Логотипы Маркизы Платы ROMы Таблицы рекордов Снимки экрана Носители Информации Против проигрывается записывается Предпросмотр оформления Боссы Кабинет Панель управления Обложки Буклет Игра Окончена Обучение Логотип Маркиза Плата Таблицы рекордов Снимки экрана Против Имеющиеся Пользовательский фильтр Тип устройства Избранное Частично поддерживается Издатель Регион распространения Список ПО Поддерживается Отсутсвующие Без фильтра Не поддерживается Год остновлено Программист Дата выхода 