��    �     �-              <[     =[     \[  .   x[     �[  �   �[     p\  �   y\     3]     <]     D]     _]  #   v]  !   �]  #   �]  #   �]  #   ^  !   (^  "   J^  +   m^  +   �^     �^     �^  
   �^  	   	_     _     _  	   !_     +_     2_  	   :_     D_     L_  "   S_  	   v_  .   �_  +   �_     �_     �_     �_     `  
   `     %`     9`     L`     ^`     q`     �`     �`     �`     �`     �`     �`     �`     �`     �`      a  
   4a     ?a     Ma  #   Va  #   za     �a     �a     �a  	   �a     �a  3   �a     b     "b     =b     Ub     ib     {b  	   �b  	   �b     �b     �b     �b     �b     �b     �b     �b     c     c  
   >c     Ic     Zc  
   nc     yc  "   �c     �c     �c     �c     �c     �c  3   d     7d     Dd     Td     id  ^   md     �d  /   �d      e     -e  .   9e     he     me     }e     �e     �e     �e  	   �e     �e  /   �e     f     f     f     /f     >f     Vf     df     zf     �f     �f      �f     �f     �f     �f     �f  
   g     g     !g     7g  :   >g  &   yg     �g     �g     �g  "   �g     h  D   h     [h     ah     sh  
   �h     �h     �h  -   �h     �h     �h     i     i     .i     Ai     Ni     [i     ji     qi     yi     �i      �i  
   �i     �i     �i     j     j     *j     Ej     Yj     qj     xj     �j     �j     �j     �j     �j     k     k     4k     Ak     Hk     Uk     bk     nk     vk  B   �k     �k     �k  
   �k     �k     l     l     #l     7l  *   <l  $   gl     �l     �l     �l     �l     �l     �l     m     m     3m     Em     Zm     cm     km     wm  !   �m  
   �m     �m  $   �m     �m     �m     n  *   "n  )   Mn  >   wn     �n     �n     �n     o     o     o     6o     Co     Jo     Vo     ]o     do     ro  %   xo  
   �o     �o     �o     �o     �o     �o     �o     �o     p     p     +p     Bp     Xp     gp     �p     �p  	   �p     �p     �p  (   �p     �p     �p     �p     �p     �p     q     q     !q     Aq     Pq     `q     fq     vq     |q     �q     �q      �q     �q     �q     �q     �q     r     r     +r     ?r     Wr     lr     zr     �r     �r     �r     �r     �r     s     !s     1s      Es     fs     zs     �s     �s     �s     �s  $   �s     t     )t     2t     Mt  
   St     ^t     jt  	   st     }t     �t     �t     �t     �t     �t     �t     �t     u  	   &u     0u  !   7u     Yu     lu     zu     �u     �u     �u     �u     �u     �u     v     v     6v     Lv     Rv     cv     xv     �v     �v     �v     �v     �v     �v     �v     w     w     w     /w     2w      Nw  �   ow     =x     Nx     cx     wx     |x     �x     �x  '   �x  (   �x     �x  
   �x     y     y  	   y  Y    y  G   zy     �y     �y     �y     �y     z     z     3z     Rz     cz     xz     �z     �z     �z     �z     �z  
   �z  
   �z     �z  !   {  ,   *{  -   W{  $   �{     �{     �{  
   �{     �{     �{  !   �{     |     |     #|     @|     V|     l|     �|     �|     �|     �|     �|     �|  <   }     B}  2   ]}     �}     �}     �}     �}     �}     �}  ,   ~     0~     F~     [~  '   {~     �~  	   �~  
   �~     �~  
   �~     �~     �~     
        �   3  �   �     T�     i�     �     ��     ��     ��     �  	   �     �     �     &�     8�     P�     h�     w�     ~�     ��     ��     ��     ��     ��     ́  F   ؁     �  !   9�     [�     t�  
   y�     ��     ��  
   ��  $   ��     �     ��  
   �     �     �     :�  3   W�     ��     ��     ��     ��     ܃     �     �     �     3�     K�     `�     z�     ~�     ��     ��     ��     ��     ��     τ     �  !   �  "   %�  "   H�     k�     q�     ��     ��     ��     ��     ��     օ     �     �      �     &�     7�  	   C�     M�     b�     p�     ��  	   ��     ��     ��     ��     ��     Ԇ     �  	   ��     �     �     ,�     >�     L�     _�     n�  2   ��  0   ��     �     ��     �     �     #�     0�     5�     J�  �   `�  �   �  ,   f�  E   ��     ى  (   �  ]   �  .   z�  z   ��     $�     -�     G�     M�     _�  
   u�     ��     ��     ��     ��     ŋ     ��     ��     �     *�     2�  	   D�  '   N�  &   v�  8   ��     ֌     �     �     �     4�     L�  e   l�  Z   ҍ     -�     <�     B�     I�  
   X�     c�     q�     �     ��     ��     ��     Ȏ     Ύ     ڎ     ��     �     �     ��     ��  
    �     �     �     +�     7�     E�     Y�     m�     u�  �   ��  �   �     ��  
   ��          Ր     ސ     �     �     ��     �     #�  
   ;�  
   F�     Q�     a�     u�     ��     ��     ��     Ƒ     ڑ     �  '   �  (   -�     V�  (   t�     ��     ��  #   Ԓ  &   ��     �     ;�     U�     p�     ��     ��     ��     Ɠ     ٓ     �     ��     �     &�     9�     M�     `�     x�     ��     ��     ��     ��     Ք     �  "   �     '�      G�     h�     ��     ��     ��     Е     �     �     (�     E�     ]�     w�     ��  %   ��     ז     �     �     �     ,�     F�     a�     |�     ��     ��     ͗     �     �     �     2�     J�     b�     z�     ��     ��          ژ     �     �     !�     9�     Q�     i�     ��     ��     ��     ə     �     ��     �     (�     @�     X�     p�     ��     ��     ��     К     ߚ     �     	�     &�     8�     J�     ]�     p�     ��     ��     ��     ��     ˛     ݛ     �     �     �     *�     :�     J�     ]�     o�     ��     ��     ��     ��     ɜ     ۜ     �     ��     �     (�     <�     P�     d�     x�     ��     ��     ��     ĝ     ޝ     �     �     $�     =�     T�     d�     v�     ��     ��     ��     ��     О     �     ��     	�     �     8�     P�     h�     ��     ��     ��     ȟ     ��     ��     �     '�     ?�     W�     o�     ��     ��     ��     Ϡ     �     ��     �     $�     7�     M�     b�     w�     ��     ��     ��     ˡ     �     ��     �     �     3�     H�     ]�     r�     ��     ��     ��     Ƣ     �     ��     �     *�     B�     Z�     r�     ��     ��     ��     У     �     ��     �     ,�     C�     Z�     m�     ��     ��     ��     ٤     ��     �     *�     E�     _�     z�     ��     ��     ɥ     �     �     �     4�     N�     h�     ��     ��     ��     Ҧ     �     �     "�     :�     R�     j�     ��     ��     ��  !   ̧     �     �     �     6�     N�     h�     ��     ��     ��      Ҩ     �     	�     �     5�     I�     h�     ��     ��     ĩ     ٩     ��     ��     �     /�     H�     a�     z�     ��     ��     Ū     ݪ     ��     �     %�     =�     U�     m�     ��     ��     ��     ̫     �     �     (�     ?�     V�     m�     ��     ��      ¬     �     �     �     ,�     >�     U�     m�     ��     ��     ��     ͭ     �     ��     �     +�     B�     Y�     p�     ��     ��     ��     Ȯ     �     ��     �     4�     O�     j�     ��     ��     ��     կ     �     �     $�     C�     _�     w�     ��     ��     ð     ۰     ��     �     -�     E�     ]�     }�     ��     ��     ű     ݱ     ��     �  !   '�     I�     a�     y�     ��     ��     ò     ۲     ��     �      -�     N�     d�     z�     ��     ��     ó     �     �     �     4�     H�     Y�     p�     ��     ��     ��     д     �      �     �     /�     F�     ]�     t�     ��     ��     ��     е     �     ��     �     2�     Q�     m�     ��     ��     ��     ö     �     �     !�     >�     S�     g�     x�     ��     ��     ��     ׷     �     �     �     7�     N�     e�     |�     ��     ��     ��     ظ     �     �     �     3�     Q�     p�     ��     ��     ��     ι     �     �      �     @�     ]�     r�     ��     ��     ��     ƺ     ޺     ��     �     &�     >�     V�     m�     ��     ��     ��     ɻ     �     ��     �     !�     4�     R�     p�     ��     ��     ��     ׼     ��     �      �     ?�     _�     |�     ��     ��     ��     ͽ     �     ��     �     -�     E�     ]�     u�     ��     ��     ��     Ѿ     �     ��     �     -�     @�     S�     q�     ��     ��     ʿ     �     ��     �      �     ?�     ^�     ~�     ��     ��     ��     ��     ��     �     �     4�     L�     d�     |�     ��     ��     ��     ��     ��     �     �     5�     L�     _�     r�     ��     ��     ��     ��     ��     �     +�     ?�     ^�     }�     ��     ��     ��     ��     ��     �     #�     ;�     S�     k�     ��     ��     ��     ��     ��     ��     �     &�     =�     T�     k�     ~�     ��     ��     ��     ��     �     �     4�     J�     ^�     }�     ��     ��     ��     ��     �     �     *�     B�     Z�     r�     ��     ��     ��     ��     ��      �     �     .�     E�     \�     s�     ��     ��     ��     ��     ��     �     '�     =�     S�     i�     }�     ��     ��     ��     ��     �     !�     2�     D�     Y�     m�     ��     ��     ��     ��     ��     ��     ��     �     $�     :�     P�     f�     |�     ��     ��     ��     ��     ��     �     �     ,�     E�     ]�     u�     ��     ��     ��     ��     ��     �     �     8�     R�     l�     ��     ��     ��     ��     ��     �     �     1�     G�     ]�     v�     ��     ��     ��     ��     ��     �     �      ,�     M�     a�     z�     ��     ��     ��     ��     ��      �     �     -�     A�     Q�     c�     u�     ��     ��     ��     ��     ��     ��     �     �     .�     C�     X�     m�     ��     ��     ��     ��     ��     ��     ��     �     )�     >�      S�     t�  !   ��     ��     ��     ��     ��     �     2�     E�     W�     q�     ��     ��     ��     ��     ��     �     �     4�     M�     j�     ��     ��     ��     ��     ��     ��     �     �     0�  !   E�     g�     k�     ��     ��     ��     ��     ��     ��      �     9�     U�     o�     ��     ��     ��     ��     ��     �     4�     O�     i�     ��     ��  	   ��     ��     ��     ��     �     �     0�     H�     c�     v�     ��     ��     ��     ��     ��     ��     �     �     /�     D�     U�     m�     ��     ��     ��     ��     ��     ��     �     �     5�     Q�     d�  	   l�     v�     ��     ��     ��     ��     ��     �     '�     A�     W�     l�     ��     ��     ��     ��     ��     ��     �     +�     E�     \�     z�     ��     ��     ��     ��  #   ��     "�     =�     W�     v�     ��     ��     ��     ��     �     �     �     :�     M�     h�     ~�     ��  %   ��     ��     ��     ��     �     $�     ;�     T�     n�     ��     ��  %  ��  +   ��     �  ,   *�  "   W�  �   z�     
�  �   �     ��     ��     ��     ��  #   ��      �      4�      U�      v�  &   ��      ��  *   ��  ,   
�     7�  #   R�  	   v�     ��     ��     ��  	   ��     ��     ��  	   ��     ��     ��  $   ��  	   ��  ,    �  #   -�     Q�     e�     r�     ~�     ��     ��     ��     ��     ��     ��     ��     ��     �     �     (�     G�     M�     d�     g�     ��  
   ��     ��     ��  &   ��  &   ��     �     ,�  	   B�     L�     _�  3   r�     ��     ��     ��     ��     ��     ��  
   �     �     *�     F�     b�     g�     v�  	   ��     ��     ��  '   ��     ��     ��     ��     �     �     4�     S�     i�  	   v�     ��     ��  8   ��     ��     ��     ��     �  H   �     [�  5   k�  &   ��     ��  6   ��     �     �     &�     6�     F�     M�     Z�     a�  '   u�     ��     ��     ��     ��     ��     ��     ��      �     �     &�     3�     R�     e�     l�     y�     ��     ��     ��     ��  4   ��  $   ��     �     2�     D�  '   Y�     ��  1   ��     ��     ��     ��     ��  "   	�     ,�  5   B�     x�     ��     ��     ��     ��     ��     ��     ��     �  	   �     �     (�     =�     U�     \�     i�     v�     ��     ��     ��     ��     ��     ��     ��     �     $�     7�     O�     l�     ��     ��     ��  	   ��     ��     ��     ��     ��  G   ��     ?�     L�     _�  	   f�  
   p�     {�     ��     ��  !   ��     ��     ��     ��     �     %�     ;�     M�     h�     x�     ��     ��     ��  	   ��  
   ��     ��     ��     �     �  0   �     P�     W�     g�  +   ��  *   ��  ?   ��     �     0�  !   L�     n�     u�  #   |�     ��     ��     ��     ��     ��     ��     ��     ��     �     �     �     "�     +�     8�     E�     W�  
   o�     z�     ��     ��     ��     ��     ��     ��     ��     ��     �  &   !�     H�     O�     V�     ]�     `�     p�     ��     ��     ��     ��     ��     ��     ��     ��     ��     �  !   '�  	   I�     S�     i�     p�     w�     ��     ��     ��     ��     ��     ��     ��     �     �     9�     X�     w�     ��     ��  !   ��     ��     ��     �     �     *�     =�  $   S�     x�  	   ��     ��     ��     ��  	   ��     ��  
   ��     ��     ��     ��     ��     �     �     +�     =�     R�  	   e�     o�     v�     ��  	   ��     ��     ��     ��     ��     ��  $   �     2�     ?�     T�     l�     y�     ��     ��     ��     ��  	   ��     ��  	   ��     ��     �     �     +�     8�     E�     [�     _�  $   }�  �   ��     L�     \�     x�     ��     ��  	   ��  	   ��     ��     ��     ��     ��     ��     
�     �  ^   �  B   z�     ��  
   ��     ��     ��     �     �     .�     H�     _�     r�     ��     ��     ��     ��     ��     ��     ��     ��      ��  /     0   F  %   w     �     �     �     �     �     �     �     �             ,    @    T    h    �    �    �    � 9   �     '   &    N    U    j 	   �    �    � .   �    �     !    -   8 $   f    �    �    �    �    �    �    �    � �   � |   �    �        )    9     I     j    �    �    �    �    �    �    �                 #    0 	   7 	   A    K 	   X G   b    � #   �    �                *    = *   J    u    � 
   �    �    �    � 3   �            /    B    ^ !   l    �    �    �    �    �    	    	    	    	    ,	    ?	    R	    b	    ~	 $   �	    �	    �	    �	    �	 	   
    
    %
    2
    E
    b
     r
    �
    �
    �
    �
 
   �
    �
    �
    �
            *    A    H    U    e    u 	   �    �    �    �    �    �         -   ' 0   U    �    �    �    �    �    �    �    � t    w   { &   � 7       R !   j X   � $   � a   
    l    y 	   �    �    � 	   �    �    �    �    �     !   *    L    \ 	   l    v 	   �     �     � 9   �        $ $   B    g    � !   � c   � N       n    ~    �    �    �    �    �    �    �    �    	            *    1 	   3    =    X 	   Z    d    q    u    �    �    �    �    �    � �   � z       �            .    7    C 
   K    V    j    � 
   � 
   �    � 	   �    �    �    �    �    �    �    �            2    E    ^    q    ~    �    �    �    �    �    �    �    �    �    �    �    �                           	   ' 	   1    ; 	   B 	   L    V    ]    j    q    x        �    �    �    �    � 	   �    � 	   �    � 	   �    �    �    �    �            )    9    I    Y    i    y    �    �    �    �    �    �    �    �             1    B    Q    c    t    �    �    �    �    �    �    �    �            .    ?    P    a    r    �    �    �    �    �    �    �    �    �    �    �                +    7    C    O    [    h 	   o    y    �    �    �    �    �    �    �    �    �    �                +    9    G    U    c    q    x    �    �    �    �    �    �    �    �    �    �    �    �    �    � 	            
   % 
   0 
   ; 
   F 
   Q 
   \ 
   g 
   r    }    � 
   � 
   � 
   � 
   � 
   � 
   � 
   � 
   �    �    �    �         
     
   + 
   6 
   A 
   L 
   W 
   b 
   m    x    � 
   � 
   � 
   � 
   � 
   � 
   � 
   � 
   �    �    �    �            %    2    ?    L    Y    e    q    }    �    �    �    �    �    �    �    �    �    �              "     0     >     L     S     d     u     �     �     �     �     �     �     �     �     �     !    !    !    1!    =!    I!    U!    a!    m!    {!    �!    �!    �!    �!    �!    �!    �!    �!    �!    "    "    #"    /"    ;"    G"    N"    _"    p"    �" 	   �" 	   �"    �"    �"    �"    �"    �"    �"    �"     #    #    #    )#    6#    C#    P#    ]#    j#    w#    �#    �#    �#    �#    �#    �#    �#    �#    �#    $    $    $    /$    A$ 
   S$ 
   ^$    i$    q$    }$    �$    �$    �$    �$    �$    �$    �$    �$    �$    �$    %    %     %    ,%    8%    ?%    M%    [%    i%    w%    �%    �%    �%    �%    �%    �%    �%    �%    �%    &    &    "&    .&    ?&    M&    Y&    g&    s&    �&    �&    �&    �&    �&    �&    �&    �&    �&    �&    '    '    )'    5'    A'    M'    ['    g'    u'    �'    �'    �'    �'    �'    �'    �'    �'    �'     ( 	   ( 	   (    %(    ,(    8(    E(    R(    _(    l(    y(    �(    �(    �(    �(    �(    �(    �(    �(    �(    �(    �(    )    )    #)    4)    E)    Q)    ])    i)    p)    �)    �)    �) 	   �) 	   �)    �)    �)    �)    �)    �)    *    *    *    )*    6*    B*    N*    Z*    f*    r*    ~*    �*    �*    �*    �*    �*    �*    �*    �*    �*     +    +    +    $+    5+    F+ 	   W+ 	   a+    k+    r+    ~+    �+    �+    �+    �+    �+    �+    �+    �+    �+    �+    	,    ,    !,    -,    9,    @,    G,    X,    i,    z,    �,    �,    �,    �,    �,    �,    �,    �, 	   �, 	   -    -    -    !-    .-    ;-    H-    U-    b-    o-    |-    �-    �-    �-    �-    �-    �-    �-    �-    �-    �-    �-    .    .    ..    :.    F.    R.    Y.    j.    {.    �. 	   �. 	   �.    �.    �.    �.    �.    �.    �.    �.    /    /    /    +/    7/    C/    O/    [/    g/    s/    /    �/    �/    �/    �/    �/    �/    �/    �/    �/    �/    0    0    /0 	   @0 	   J0    T0    [0    g0    t0    �0    �0    �0    �0    �0    �0    �0    �0    �0    �0    �0    
1    1    "1    )1    01    A1    R1    c1    t1    �1    �1    �1    �1    �1    �1    �1 	   �1 	   �1    �1    �1    
2    2    $2    12    >2    K2    X2    e2    q2    }2    �2    �2    �2    �2    �2    �2    �2    �2    �2    �2    3    3    #3    /3    ;3    B3    S3    d3    u3 	   �3 	   �3    �3 	   �3    �3    �3    �3    �3    �3    �3    �3     4    4    4    $4    34    A4    O4    ]4    k4    y4    �4    �4    �4    �4    �4 	   �4    �4    �4    �4    �4    �4    5    5     5    ,5    85    D5    S5    a5    o5    }5    �5    �5    �5    �5    �5    �5 
   �5 
   �5    �5    �5    
6    6    $6    16    @6    O6    ^6    m6 
   �6    �6    �6    �6    �6    �6    �6    �6    �6    7    7    7    (7    77    F7    U7    d7    t7    �7    �7    �7    �7    �7    �7    �7    �7    �7    �7    8    8    (8    68    D8    R8    `8    n8    |8    �8    �8     �8    �8    �8    9    9    '9    ;9    H9    Y9    m9    �9    �9    �9    �9    �9    �9    �9    :    :    ':    A:    R:    `:    q:    :    �:    �:    �:    �:    �:    �:    �:    �: 
   �:    �: 	   ;    ;    ;    ; 	   ,; 	   6;    @;    N;    W;    d;    q;    x;    �; 	   �; 	   �;    �;    �;    �; 	   �; 	   �;    �;    �;    < 
   < 	   $<    .<    ;<    B<    O<    V<    i<    v< 	   �<    �<    �< 	   �<    �<    �<    �<    �<    �<    �<    �<    �<    �<    =    = 	    = 	   *=    4=    ;=    H=    U=    e= 	   l=    v=    �=    �= 	   �=    �= 	   �= 	   �=    �=    �=    �=    �=    �=    �=    �=    �=    �= 	   >    > 	   >    #> 	   0>    :>    A>    H>    U> 	   \>    f>    s> 	   �> 	   �> 	   �> 	   �>    �> 	   �> 	   �>    �>    �> 	   �> 	   �>    �>    �>    �>    ?    ?    ?    ? 	   &?    0?    7?    K?    X?  

--- DRIVER INFO ---
Driver:  

Press any key to continue 

There are working clones of this machine: %s 
    Configuration saved    

 
Elements of this machine cannot be emulated as they require physical interaction or consist of mechanical devices. It is not possible to fully experience this machine.
 
Sound:
 
THIS MACHINE DOESN'T WORK. The emulation for this machine is not yet complete. There is nothing you can do to fix this problem except wait for the developers to improve the emulation.
 
Video:
   %1$s
   %1$s    [default: %2$s]
   %1$s    [tag: %2$s]
   Adjuster inputs    [%1$d inputs]
   Analog inputs    [%1$d inputs]
   Gambling inputs    [%1$d inputs]
   Hanafuda inputs    [%1$d inputs]
   Keyboard inputs    [%1$d inputs]
   Keypad inputs    [%1$d inputs]
   Mahjong inputs    [%1$d inputs]
   Screen '%1$s': %2$d × %3$d (H) %4$s Hz
   Screen '%1$s': %2$d × %3$d (V) %4$s Hz
   Screen '%1$s': Vector
   User inputs    [%1$d inputs]
  (default)  (locked)  COLORS  PENS %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Driver: %4$s

CPU:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d machines (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d software packages ) %1$s (%2$s) - %3$s
 %1$s - %2$s
 %1$s Brightness %1$s Contrast %1$s Gamma %1$s Horiz Position %1$s Horiz Stretch %1$s Refresh Rate %1$s Vert Position %1$s Vert Stretch %1$s Volume %1$s [root%2$s] %1$s, %2$-.100s %1$s: %2$s
 %1$s: %2$s - Search: %3$s_ %2$s
 %d total matches found %s %s
 added to favorites list. %s
 removed from favorites list. %s [%d Hz] %s [internal] %s added %s.txt saved in UI settings folder. %s.xml saved in UI settings folder. (EP)ROM	Imperfect
 (EP)ROM	Unimplemented
 (empty) (playing) (recording) * BIOS settings:
  %1$d options    [default: %2$s]
 * CPU:
 * Configuration settings:
 * DIP switch settings:
 * Input device(s):
 * Media Options:
 * Slot Options:
 * Sound:
 * Video:
 **Error saving %s.ini** **Error saving ui.ini** , %s <set up filters> ARGB Settings About %s Activated: %s Activated: %s = %s Add %1$s Folder - Search: %2$s_ Add Folder Add To Favorites Add autofire button Add filter Add or remove favorite Adjust speed to match refresh rate Adstick Device Assignment Advanced Options Aligned only All All cheats reloaded All slots cleared and current state saved to Slot 1 Allow rewind Analog Controls Analog Controls	Yes
 Any Are you sure you want to quit?

Press ''%1$s'' to quit,
Press ''%2$s'' to return to emulation. Artwork Options Audit ROMs for %1$u machines marked unavailable Audit ROMs for all %1$u machines Audit media Auditing ROMs for machine %2$u of %3$u...
%1$s Auto Auto frame skip Auto rotate left Auto rotate right Autofire Autofire buttons Automatic Automatic save/restore Automatically toggle pause with on-screen menus BIOS BIOS Selection BIOS selection: Barcode Reader Barcode length invalid! Beam Dot Size Beam Intensity Weight Beam Width Maximum Beam Width Minimum Bilinear Filtering Bilinear filtering for snapshots Bitmap Prescaling Bold Bookkeeping Info Burn-in CPU or RAM Camera	Imperfect
 Camera	Unimplemented
 Cancel Cancel audit?

Press %1$s to cancel
Press %2$s to continue Cannot change options while recording! Cannot save over directory Capture	Imperfect
 Capture	Unimplemented
 Change %1$s Folder - Search: %2$s_ Change Folder Changes to this only take effect when "Start new search" is selected Cheat Cheat Comment:
%s Cheat Finder Cheat Name Cheat added to cheat.simple Cheat engine not available Cheat written to %s and added to cheat.simple Cheats Choose from palette Clear Watches Coin %1$c: %2$d%3$s
 Coin %1$c: NA%3$s
 Coin impulse Coin lockout Color preview: Colors Command Communications	Imperfect
 Communications	Unimplemented
 Completely unemulated features:  Compressor Configure Directories Configure Machine Configure Machine: Configure Options Confirm quit from machines Controls	Imperfect
 Controls	Unimplemented
 Create Crosshair Offset %1$s Crosshair Offset X %1$1.3f Crosshair Offset Y %1$1.3f Crosshair Options Crosshair Scale %1$s Crosshair Scale X %1$1.3f Crosshair Scale Y %1$1.3f Current %1$s Folders Current time Custom Customize UI DIP Switches Data Format Default Default name is %s Delete saved state %1$s?
Press %2$s to delete
Press %3$s to cancel Device Mapping Dial Device Assignment Difference Disabled Disabled: %s Disk	Imperfect
 Disk	Unimplemented
 Done Double-click or press %1$s to change color Double-click or press %1$s to select Driver Driver is BIOS	No
 Driver is BIOS	Yes
 Driver is Clone of	%1$s
 Driver is Parent	
 Driver is clone of: %1$-.100s Driver is parent Driver: "%1$s" software list  Driver: %1$-.100s Edit autofire button Emulated Enabled Enabled: %s Enforce Aspect Ratio Enlarge images in the right panel Enter Code Error accessing %s Error removing saved state file %1$s Exit Expand to fit Export displayed list to file Export list in TXT format (like -listfull) Export list in XML format (like -listxml) Export list in XML format (like -listxml, but exclude devices) External DAT View Failed to load autofire menu Failed to save input name file Fast Forward File File Already Exists - Override? File Manager Filter Filter %1$u Flip X Flip Y Folders Setup Fonts Force 4:3 aspect for snapshot display Frame skip GHz GLSL Gameinit General Info General Inputs Graphics	Imperfect
 Graphics	Imperfect Colors
 Graphics	OK
 Graphics	Unimplemented
 Graphics	Wrong Colors
 Graphics: Imperfect,  Graphics: OK,  Graphics: Unimplemented,  Group HLSL Hide Both Hide Filters Hide Info/Image Hide romless machine from available list High Scores History Hotkey Hz Image Format: Image Information Images Imperfectly emulated features:  Include clones Info auto audit Infos Infos text size Input Input (general) Input (this Machine) Input Options Input port name file saved to %s Input ports Invalid sequence entered Italic Joystick Joystick deadzone Joystick saturation Keyboard	Imperfect
 Keyboard	Unimplemented
 Keyboard Inputs	Yes
 Keyboard Mode LAN	Imperfect
 LAN	Unimplemented
 Language Laserdisc '%1$s' Horiz Position Laserdisc '%1$s' Horiz Stretch Laserdisc '%1$s' Vert Position Laserdisc '%1$s' Vert Stretch Last Slot Value Left equal to right Left equal to right with bitmask Left equal to value Left greater than right Left greater than value Left less than right Left less than value Left not equal to right Left not equal to right with bitmask Left not equal to value Lightgun Lightgun Device Assignment Lines Load State Low latency MAMEinfo MARPScore MESSinfo MHz Machine Configuration Machine Information Mag. Drum	Imperfect
 Mag. Drum	Unimplemented
 Mag. Tape	Imperfect
 Mag. Tape	Unimplemented
 Maintain Aspect Ratio Mamescore Manual Manually toggle pause when needed Manufacturer	%1$s
 Master Volume Match block Mechanical Machine	No
 Mechanical Machine	Yes
 Media	Imperfect
 Media	Unimplemented
 Memory state saved to Slot %d Menu Preview Microphone	Imperfect
 Microphone	Unimplemented
 Miscellaneous Options Mouse Mouse	Imperfect
 Mouse	Unimplemented
 Mouse Device Assignment Multi-keyboard Multi-mouse Mute when unthrottled NOT SET Name:             Description:
 Natural Natural keyboard Network Devices New Barcode: New Image Name: No No category INI files found No groups found in category file No machines found. Please check the rompath specified in the %1$s.ini file.

If this is your first time using %2$s, please see the config.txt file in the docs directory for information on configuring %2$s. No plugins found No save states found Non-Integer Scaling None None
 Not supported Number Of Screens Number of frames button will be pressed Number of frames button will be released Off Off frames Offscreen reload On On frames One or more ROMs/CHDs for this machine are incorrect. The machine may not run correctly.
 One or more ROMs/CHDs for this machine have not been correctly dumped.
 Other Controls Other:  Overall	NOT WORKING
 Overall	Unemulated Protection
 Overall	Working
 Overall: NOT WORKING Overall: Unemulated Protection Overall: Working Overclock %1$s sound Overclock CPU %1$s P%d Crosshair P%d Visibility Paddle Device Assignment Page Partially supported Pause Mode Pause/Stop Pedal Device Assignment Perform Compare  :  Slot %d %s %d Perform Compare  :  Slot %d %s Slot %d %s %d Perform Compare  :  Slot %d BITWISE%s Slot %d Perform Compare : Slot %d %s Slot %d Performance Options Play Play Count Player Player %1$d Controls Please enter a file extension too Plugin Options Plugins Positional Device Assignment Press %1$s to append
 Press %1$s to cancel
 Press %1$s to clear
 Press %1$s to delete Press %1$s to restore default
 Press %1$s to set
 Press %s to clear hotkey Press %s to delete Press TAB to set Press a key or joystick button, or select state to overwrite Press any key to continue. Press button for hotkey or wait to leave unchanged Pressed Printer	Imperfect
 Printer	Unimplemented
 Pseudo terminals Punch Tape	Imperfect
 Punch Tape	Unimplemented
 ROM Audit 	Disabled
Samples Audit 	Disabled
 ROM Audit Result	BAD
 ROM Audit Result	OK
 Re-select last machine launched Read this image, write to another image Read this image, write to diff Read-only Read-write Record Reload All Remove %1$s Folder Remove Folder Remove From Favorites Remove last filter Required ROM/disk images for the selected software are missing or incorrect. Please acquire the correct files or select a different software item.

 Required ROM/disk images for the selected system are missing or incorrect. Please acquire the correct files or select a different system.

 Requires Artwork	No
 Requires Artwork	Yes
 Requires CHD	No
 Requires CHD	Yes
 Requires Clickable Artwork	No
 Requires Clickable Artwork	Yes
 Reset Reset All Restore default colors Results will be saved to %1$s Return to Machine Return to Previous Menu Return to previous menu Revision: %1$s Rewind Rewind capacity Romset	%1$s
 Rotate Rotate left Rotate right Rotation Options Sample Rate Sample text - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Samples Audit Result	BAD
 Samples Audit Result	None Needed
 Samples Audit Result	OK
 Save Save Cheat Save Configuration Save Machine Configuration Save State Save current memory state to Slot %d Save input names to file Screen Screen #%d Screen '%1$s' Screen Orientation	Horizontal
 Screen Orientation	Vertical
 Screen flipping in cocktail mode is not supported.
 Search: %1$s_ Select New Machine Select access mode Select an input for autofire Select category: Select cheat to set hotkey Select custom filters: Select image format Select initial contents Select state to load Selection List - Search:  Set Set hotkeys Settings Show All Show DATs view Show mouse pointer Show side panels Simultaneous contradictory Skip BIOS selection menu Skip imperfect emulation warnings Skip information screen at startup Skip software parts selection menu Sleep Slider Controls Slot %d Slot 1 Value Slot Devices Software List Info Software is clone of: %1$-.100s Software is parent Software list/item: %1$s:%2$s Software part selection: Sound Sound	Imperfect
 Sound	None
 Sound	OK
 Sound	Unimplemented
 Sound Options Sound: Imperfect Sound: None Sound: OK Sound: Unimplemented Speed Start Machine Start Out Maximized Start new search State/Playback Options Steadykey Support Cocktail	No
 Support Save	No
 Support Save	Yes
 Supported: No Supported: Partial Supported: Yes Switch Item Ordering Switched Order: entries now ordered by description Switched Order: entries now ordered by shortname Synchronized Refresh Sysinfo System Names System: %1$-.100s Tape Control Test Test Cheat %08X_%02X Test/Write Poke Value The selected game is missing one or more required ROM or CHD images. Please select a different game.

Press any key to continue. The software selected is missing one or more required ROM or CHD images.
Please acquire the correct files or select a different one. There are known problems with this machine

 This driver requires images to be loaded in the following device(s):  This machine has no BIOS. This machine has no configurable inputs. This machine has no sound hardware, MAME will produce no sounds, this is expected behaviour.
 This machine requires external artwork files.
 This machine was never completed. It may exhibit strange behavior or missing elements that are not bugs in the emulation.
 Throttle Tickets dispensed: %1$d

 Timer Timing	Imperfect
 Timing	Unimplemented
 Total time Trackball Device Assignment Triple Buffering Type Type name is empty Type name or select: %1$s_ Type name or select: (random) UI Color Settings UI Customization Settings UI Font UI Fonts Settings UI active UI controls disabled
Use %1$s to toggle UI controls enabled
Use %1$s to toggle Unable to write file
Ensure that cheatpath folder exists Undo last search -- # Uptime: %1$d:%2$02d

 Uptime: %1$d:%2$02d:%3$02d

 Use External Samples Use image as background Use this if you want to poke %s Use this if you want to poke the Last Slot value (eg. You started without an item but finally got it) Use this if you want to poke the Slot 1 value (eg. You started with something but lost it) User Interface Value Vector Vector Flicker Video Mode Video Options Visible Delay WAN	Imperfect
 WAN	Unimplemented
 Wait Vertical Sync Warning Information Watch Window Mode Write X X Only X or Y (Auto) Y Y Only Year	%1$s
 Yes You can enter any type name Zoom = %1$d Zoom = 1/%1$d Zoom to Screen Area Zoom to screen area [None]
 [Start empty] [This option is NOT currently mounted in the running system]

Option: %1$s
Device: %2$s

If you select this option, the following items will be enabled:
 [This option is currently mounted in the running system]

Option: %1$s
Device: %2$s

The selected option enables the following items:
 [Use file manager] [built-in] [compatible lists] [create] [empty slot] [empty] [failed] [file manager] [no category INI files] [no groups in INI file] [root%1$s] [root%2$s] [software list] color-channelAlpha color-channelBlue color-channelGreen color-channelRed color-optionBackground color-optionBorder color-optionClone color-optionDIP switch color-optionGraphics viewer background color-optionMouse down background color color-optionMouse down color color-optionMouse over background color color-optionMouse over color color-optionNormal text color-optionNormal text background color-optionSelected background color color-optionSelected color color-optionSlider color color-optionSubitem color color-optionUnavailable color color-presetBlack color-presetBlue color-presetGray color-presetGreen color-presetOrange color-presetRed color-presetSilver color-presetViolet color-presetWhite color-presetYellow color-sampleClone color-sampleMouse Over color-sampleNormal color-sampleSelected color-sampleSubitem default emulation-featureLAN emulation-featureWAN emulation-featurecamera emulation-featurecapture hardware emulation-featurecolor palette emulation-featurecommunications emulation-featurecontrols emulation-featuredisk emulation-featuregraphics emulation-featurekeyboard emulation-featuremagnetic drum emulation-featuremagnetic tape emulation-featuremedia emulation-featuremicrophone emulation-featuremouse emulation-featureprinter emulation-featureprotection emulation-featurepunch tape emulation-featuresolid state storage emulation-featuresound emulation-featuretiming incorrect checksum incorrect length input-name1 Player Start input-name2 Players Start input-name3 Players Start input-name4 Players Start input-name5 Players Start input-name6 Players Start input-name7 Players Start input-name8 Players Start input-nameAD Stick X input-nameAD Stick X 10 input-nameAD Stick X 2 input-nameAD Stick X 3 input-nameAD Stick X 4 input-nameAD Stick X 5 input-nameAD Stick X 6 input-nameAD Stick X 7 input-nameAD Stick X 8 input-nameAD Stick X 9 input-nameAD Stick Y input-nameAD Stick Y 10 input-nameAD Stick Y 2 input-nameAD Stick Y 3 input-nameAD Stick Y 4 input-nameAD Stick Y 5 input-nameAD Stick Y 6 input-nameAD Stick Y 7 input-nameAD Stick Y 8 input-nameAD Stick Y 9 input-nameAD Stick Z input-nameAD Stick Z 10 input-nameAD Stick Z 2 input-nameAD Stick Z 3 input-nameAD Stick Z 4 input-nameAD Stick Z 5 input-nameAD Stick Z 6 input-nameAD Stick Z 7 input-nameAD Stick Z 8 input-nameAD Stick Z 9 input-nameBet input-nameBill 1 input-nameBook-Keeping input-nameBreak in Debugger input-nameCancel input-nameCoin 1 input-nameCoin 10 input-nameCoin 11 input-nameCoin 12 input-nameCoin 2 input-nameCoin 3 input-nameCoin 4 input-nameCoin 5 input-nameCoin 6 input-nameCoin 7 input-nameCoin 8 input-nameCoin 9 input-nameConfig Menu input-nameDeal input-nameDial input-nameDial 10 input-nameDial 2 input-nameDial 3 input-nameDial 4 input-nameDial 5 input-nameDial 6 input-nameDial 7 input-nameDial 8 input-nameDial 9 input-nameDial V input-nameDial V 10 input-nameDial V 2 input-nameDial V 3 input-nameDial V 4 input-nameDial V 5 input-nameDial V 6 input-nameDial V 7 input-nameDial V 8 input-nameDial V 9 input-nameDoor input-nameDoor Interlock input-nameDouble Up input-nameFast Forward input-nameFrameskip Dec input-nameFrameskip Inc input-nameHalf Gamble input-nameHigh input-nameHold 1 input-nameHold 2 input-nameHold 3 input-nameHold 4 input-nameHold 5 input-nameKey In input-nameKey Out input-nameKeyboard input-nameKeypad input-nameLightgun X input-nameLightgun X 10 input-nameLightgun X 2 input-nameLightgun X 3 input-nameLightgun X 4 input-nameLightgun X 5 input-nameLightgun X 6 input-nameLightgun X 7 input-nameLightgun X 8 input-nameLightgun X 9 input-nameLightgun Y input-nameLightgun Y 10 input-nameLightgun Y 2 input-nameLightgun Y 3 input-nameLightgun Y 4 input-nameLightgun Y 5 input-nameLightgun Y 6 input-nameLightgun Y 7 input-nameLightgun Y 8 input-nameLightgun Y 9 input-nameLoad State input-nameLow input-nameMemory Reset input-nameMouse X input-nameMouse X 10 input-nameMouse X 2 input-nameMouse X 3 input-nameMouse X 4 input-nameMouse X 5 input-nameMouse X 6 input-nameMouse X 7 input-nameMouse X 8 input-nameMouse X 9 input-nameMouse Y input-nameMouse Y 10 input-nameMouse Y 2 input-nameMouse Y 3 input-nameMouse Y 4 input-nameMouse Y 5 input-nameMouse Y 6 input-nameMouse Y 7 input-nameMouse Y 8 input-nameMouse Y 9 input-nameOn Screen Display input-nameP1 Button 1 input-nameP1 Button 10 input-nameP1 Button 11 input-nameP1 Button 12 input-nameP1 Button 13 input-nameP1 Button 14 input-nameP1 Button 15 input-nameP1 Button 16 input-nameP1 Button 2 input-nameP1 Button 3 input-nameP1 Button 4 input-nameP1 Button 5 input-nameP1 Button 6 input-nameP1 Button 7 input-nameP1 Button 8 input-nameP1 Button 9 input-nameP1 Down input-nameP1 Hanafuda A/1 input-nameP1 Hanafuda B/2 input-nameP1 Hanafuda C/3 input-nameP1 Hanafuda D/4 input-nameP1 Hanafuda E/5 input-nameP1 Hanafuda F/6 input-nameP1 Hanafuda G/7 input-nameP1 Hanafuda H/8 input-nameP1 Hanafuda No input-nameP1 Hanafuda Yes input-nameP1 Left input-nameP1 Left Stick/Down input-nameP1 Left Stick/Left input-nameP1 Left Stick/Right input-nameP1 Left Stick/Up input-nameP1 Mahjong A input-nameP1 Mahjong B input-nameP1 Mahjong Bet input-nameP1 Mahjong Big input-nameP1 Mahjong C input-nameP1 Mahjong Chi input-nameP1 Mahjong D input-nameP1 Mahjong Double Up input-nameP1 Mahjong E input-nameP1 Mahjong F input-nameP1 Mahjong Flip Flop input-nameP1 Mahjong G input-nameP1 Mahjong H input-nameP1 Mahjong I input-nameP1 Mahjong J input-nameP1 Mahjong K input-nameP1 Mahjong Kan input-nameP1 Mahjong L input-nameP1 Mahjong Last Chance input-nameP1 Mahjong M input-nameP1 Mahjong N input-nameP1 Mahjong O input-nameP1 Mahjong P input-nameP1 Mahjong Pon input-nameP1 Mahjong Q input-nameP1 Mahjong Reach input-nameP1 Mahjong Ron input-nameP1 Mahjong Small input-nameP1 Mahjong Take Score input-nameP1 Pedal 1 input-nameP1 Pedal 2 input-nameP1 Pedal 3 input-nameP1 Right input-nameP1 Right Stick/Down input-nameP1 Right Stick/Left input-nameP1 Right Stick/Right input-nameP1 Right Stick/Up input-nameP1 Select input-nameP1 Start input-nameP1 Up input-nameP10 Button 1 input-nameP10 Button 10 input-nameP10 Button 11 input-nameP10 Button 12 input-nameP10 Button 13 input-nameP10 Button 14 input-nameP10 Button 15 input-nameP10 Button 16 input-nameP10 Button 2 input-nameP10 Button 3 input-nameP10 Button 4 input-nameP10 Button 5 input-nameP10 Button 6 input-nameP10 Button 7 input-nameP10 Button 8 input-nameP10 Button 9 input-nameP10 Down input-nameP10 Left input-nameP10 Left Stick/Down input-nameP10 Left Stick/Left input-nameP10 Left Stick/Right input-nameP10 Left Stick/Up input-nameP10 Pedal 1 input-nameP10 Pedal 2 input-nameP10 Pedal 3 input-nameP10 Right input-nameP10 Right Stick/Down input-nameP10 Right Stick/Left input-nameP10 Right Stick/Right input-nameP10 Right Stick/Up input-nameP10 Select input-nameP10 Start input-nameP10 Up input-nameP2 Button 1 input-nameP2 Button 10 input-nameP2 Button 11 input-nameP2 Button 12 input-nameP2 Button 13 input-nameP2 Button 14 input-nameP2 Button 15 input-nameP2 Button 16 input-nameP2 Button 2 input-nameP2 Button 3 input-nameP2 Button 4 input-nameP2 Button 5 input-nameP2 Button 6 input-nameP2 Button 7 input-nameP2 Button 8 input-nameP2 Button 9 input-nameP2 Down input-nameP2 Hanafuda A/1 input-nameP2 Hanafuda B/2 input-nameP2 Hanafuda C/3 input-nameP2 Hanafuda D/4 input-nameP2 Hanafuda E/5 input-nameP2 Hanafuda F/6 input-nameP2 Hanafuda G/7 input-nameP2 Hanafuda H/8 input-nameP2 Hanafuda No input-nameP2 Hanafuda Yes input-nameP2 Left input-nameP2 Left Stick/Down input-nameP2 Left Stick/Left input-nameP2 Left Stick/Right input-nameP2 Left Stick/Up input-nameP2 Mahjong A input-nameP2 Mahjong B input-nameP2 Mahjong Bet input-nameP2 Mahjong Big input-nameP2 Mahjong C input-nameP2 Mahjong Chi input-nameP2 Mahjong D input-nameP2 Mahjong Double Up input-nameP2 Mahjong E input-nameP2 Mahjong F input-nameP2 Mahjong Flip Flop input-nameP2 Mahjong G input-nameP2 Mahjong H input-nameP2 Mahjong I input-nameP2 Mahjong J input-nameP2 Mahjong K input-nameP2 Mahjong Kan input-nameP2 Mahjong L input-nameP2 Mahjong Last Chance input-nameP2 Mahjong M input-nameP2 Mahjong N input-nameP2 Mahjong O input-nameP2 Mahjong P input-nameP2 Mahjong Pon input-nameP2 Mahjong Q input-nameP2 Mahjong Reach input-nameP2 Mahjong Ron input-nameP2 Mahjong Small input-nameP2 Mahjong Take Score input-nameP2 Pedal 1 input-nameP2 Pedal 2 input-nameP2 Pedal 3 input-nameP2 Right input-nameP2 Right Stick/Down input-nameP2 Right Stick/Left input-nameP2 Right Stick/Right input-nameP2 Right Stick/Up input-nameP2 Select input-nameP2 Start input-nameP2 Up input-nameP3 Button 1 input-nameP3 Button 10 input-nameP3 Button 11 input-nameP3 Button 12 input-nameP3 Button 13 input-nameP3 Button 14 input-nameP3 Button 15 input-nameP3 Button 16 input-nameP3 Button 2 input-nameP3 Button 3 input-nameP3 Button 4 input-nameP3 Button 5 input-nameP3 Button 6 input-nameP3 Button 7 input-nameP3 Button 8 input-nameP3 Button 9 input-nameP3 Down input-nameP3 Left input-nameP3 Left Stick/Down input-nameP3 Left Stick/Left input-nameP3 Left Stick/Right input-nameP3 Left Stick/Up input-nameP3 Pedal 1 input-nameP3 Pedal 2 input-nameP3 Pedal 3 input-nameP3 Right input-nameP3 Right Stick/Down input-nameP3 Right Stick/Left input-nameP3 Right Stick/Right input-nameP3 Right Stick/Up input-nameP3 Select input-nameP3 Start input-nameP3 Up input-nameP4 Button 1 input-nameP4 Button 10 input-nameP4 Button 11 input-nameP4 Button 12 input-nameP4 Button 13 input-nameP4 Button 14 input-nameP4 Button 15 input-nameP4 Button 16 input-nameP4 Button 2 input-nameP4 Button 3 input-nameP4 Button 4 input-nameP4 Button 5 input-nameP4 Button 6 input-nameP4 Button 7 input-nameP4 Button 8 input-nameP4 Button 9 input-nameP4 Down input-nameP4 Left input-nameP4 Left Stick/Down input-nameP4 Left Stick/Left input-nameP4 Left Stick/Right input-nameP4 Left Stick/Up input-nameP4 Pedal 1 input-nameP4 Pedal 2 input-nameP4 Pedal 3 input-nameP4 Right input-nameP4 Right Stick/Down input-nameP4 Right Stick/Left input-nameP4 Right Stick/Right input-nameP4 Right Stick/Up input-nameP4 Select input-nameP4 Start input-nameP4 Up input-nameP5 Button 1 input-nameP5 Button 10 input-nameP5 Button 11 input-nameP5 Button 12 input-nameP5 Button 13 input-nameP5 Button 14 input-nameP5 Button 15 input-nameP5 Button 16 input-nameP5 Button 2 input-nameP5 Button 3 input-nameP5 Button 4 input-nameP5 Button 5 input-nameP5 Button 6 input-nameP5 Button 7 input-nameP5 Button 8 input-nameP5 Button 9 input-nameP5 Down input-nameP5 Left input-nameP5 Left Stick/Down input-nameP5 Left Stick/Left input-nameP5 Left Stick/Right input-nameP5 Left Stick/Up input-nameP5 Pedal 1 input-nameP5 Pedal 2 input-nameP5 Pedal 3 input-nameP5 Right input-nameP5 Right Stick/Down input-nameP5 Right Stick/Left input-nameP5 Right Stick/Right input-nameP5 Right Stick/Up input-nameP5 Select input-nameP5 Start input-nameP5 Up input-nameP6 Button 1 input-nameP6 Button 10 input-nameP6 Button 11 input-nameP6 Button 12 input-nameP6 Button 13 input-nameP6 Button 14 input-nameP6 Button 15 input-nameP6 Button 16 input-nameP6 Button 2 input-nameP6 Button 3 input-nameP6 Button 4 input-nameP6 Button 5 input-nameP6 Button 6 input-nameP6 Button 7 input-nameP6 Button 8 input-nameP6 Button 9 input-nameP6 Down input-nameP6 Left input-nameP6 Left Stick/Down input-nameP6 Left Stick/Left input-nameP6 Left Stick/Right input-nameP6 Left Stick/Up input-nameP6 Pedal 1 input-nameP6 Pedal 2 input-nameP6 Pedal 3 input-nameP6 Right input-nameP6 Right Stick/Down input-nameP6 Right Stick/Left input-nameP6 Right Stick/Right input-nameP6 Right Stick/Up input-nameP6 Select input-nameP6 Start input-nameP6 Up input-nameP7 Button 1 input-nameP7 Button 10 input-nameP7 Button 11 input-nameP7 Button 12 input-nameP7 Button 13 input-nameP7 Button 14 input-nameP7 Button 15 input-nameP7 Button 16 input-nameP7 Button 2 input-nameP7 Button 3 input-nameP7 Button 4 input-nameP7 Button 5 input-nameP7 Button 6 input-nameP7 Button 7 input-nameP7 Button 8 input-nameP7 Button 9 input-nameP7 Down input-nameP7 Left input-nameP7 Left Stick/Down input-nameP7 Left Stick/Left input-nameP7 Left Stick/Right input-nameP7 Left Stick/Up input-nameP7 Pedal 1 input-nameP7 Pedal 2 input-nameP7 Pedal 3 input-nameP7 Right input-nameP7 Right Stick/Down input-nameP7 Right Stick/Left input-nameP7 Right Stick/Right input-nameP7 Right Stick/Up input-nameP7 Select input-nameP7 Start input-nameP7 Up input-nameP8 Button 1 input-nameP8 Button 10 input-nameP8 Button 11 input-nameP8 Button 12 input-nameP8 Button 13 input-nameP8 Button 14 input-nameP8 Button 15 input-nameP8 Button 16 input-nameP8 Button 2 input-nameP8 Button 3 input-nameP8 Button 4 input-nameP8 Button 5 input-nameP8 Button 6 input-nameP8 Button 7 input-nameP8 Button 8 input-nameP8 Button 9 input-nameP8 Down input-nameP8 Left input-nameP8 Left Stick/Down input-nameP8 Left Stick/Left input-nameP8 Left Stick/Right input-nameP8 Left Stick/Up input-nameP8 Pedal 1 input-nameP8 Pedal 2 input-nameP8 Pedal 3 input-nameP8 Right input-nameP8 Right Stick/Down input-nameP8 Right Stick/Left input-nameP8 Right Stick/Right input-nameP8 Right Stick/Up input-nameP8 Select input-nameP8 Start input-nameP8 Up input-nameP9 Button 1 input-nameP9 Button 10 input-nameP9 Button 11 input-nameP9 Button 12 input-nameP9 Button 13 input-nameP9 Button 14 input-nameP9 Button 15 input-nameP9 Button 16 input-nameP9 Button 2 input-nameP9 Button 3 input-nameP9 Button 4 input-nameP9 Button 5 input-nameP9 Button 6 input-nameP9 Button 7 input-nameP9 Button 8 input-nameP9 Button 9 input-nameP9 Down input-nameP9 Left input-nameP9 Left Stick/Down input-nameP9 Left Stick/Left input-nameP9 Left Stick/Right input-nameP9 Left Stick/Up input-nameP9 Pedal 1 input-nameP9 Pedal 2 input-nameP9 Pedal 3 input-nameP9 Right input-nameP9 Right Stick/Down input-nameP9 Right Stick/Left input-nameP9 Right Stick/Right input-nameP9 Right Stick/Up input-nameP9 Select input-nameP9 Start input-nameP9 Up input-namePaddle input-namePaddle 10 input-namePaddle 2 input-namePaddle 3 input-namePaddle 4 input-namePaddle 5 input-namePaddle 6 input-namePaddle 7 input-namePaddle 8 input-namePaddle 9 input-namePaddle V input-namePaddle V 10 input-namePaddle V 2 input-namePaddle V 3 input-namePaddle V 4 input-namePaddle V 5 input-namePaddle V 6 input-namePaddle V 7 input-namePaddle V 8 input-namePaddle V 9 input-namePause input-namePause - Single Step input-namePayout input-namePositional input-namePositional 10 input-namePositional 2 input-namePositional 3 input-namePositional 4 input-namePositional 5 input-namePositional 6 input-namePositional 7 input-namePositional 8 input-namePositional 9 input-namePositional V input-namePositional V 10 input-namePositional V 2 input-namePositional V 3 input-namePositional V 4 input-namePositional V 5 input-namePositional V 6 input-namePositional V 7 input-namePositional V 8 input-namePositional V 9 input-namePower Off input-namePower On input-nameRecord AVI input-nameRecord MNG input-nameReset Machine input-nameRewind - Single Step input-nameSave Snapshot input-nameSave State input-nameService input-nameService 1 input-nameService 2 input-nameService 3 input-nameService 4 input-nameShow Decoded Graphics input-nameShow FPS input-nameShow Profiler input-nameSoft Reset input-nameStand input-nameStop All Reels input-nameStop Reel 1 input-nameStop Reel 2 input-nameStop Reel 3 input-nameStop Reel 4 input-nameTake Score input-nameThrottle input-nameTilt input-nameTilt 1 input-nameTilt 2 input-nameTilt 3 input-nameTilt 4 input-nameToggle Cheat input-nameTrack X input-nameTrack X 10 input-nameTrack X 2 input-nameTrack X 3 input-nameTrack X 4 input-nameTrack X 5 input-nameTrack X 6 input-nameTrack X 7 input-nameTrack X 8 input-nameTrack X 9 input-nameTrack Y input-nameTrack Y 10 input-nameTrack Y 2 input-nameTrack Y 3 input-nameTrack Y 4 input-nameTrack Y 5 input-nameTrack Y 6 input-nameTrack Y 7 input-nameTrack Y 8 input-nameTrack Y 9 input-nameUI (First) Tape Start input-nameUI (First) Tape Stop input-nameUI Add/Remove favorite input-nameUI Audit Media input-nameUI Cancel input-nameUI Clear input-nameUI Default Zoom input-nameUI Display Comment input-nameUI Down input-nameUI End input-nameUI Export List input-nameUI External DAT View input-nameUI Focus Next input-nameUI Focus Previous input-nameUI Home input-nameUI Left input-nameUI Next Group input-nameUI Page Down input-nameUI Page Up input-nameUI Paste Text input-nameUI Previous Group input-nameUI Release Pointer input-nameUI Right input-nameUI Rotate input-nameUI Select input-nameUI Toggle input-nameUI Up input-nameUI Zoom In input-nameUI Zoom Out input-nameVolume Down input-nameVolume Up input-nameWrite current timecode kHz machine-filterAvailable machine-filterBIOS machine-filterCHD Required machine-filterCategory machine-filterClones machine-filterCustom Filter machine-filterFavorites machine-filterHorizontal Screen machine-filterManufacturer machine-filterMechanical machine-filterNo CHD Required machine-filterNot BIOS machine-filterNot Mechanical machine-filterNot Working machine-filterParents machine-filterSave Supported machine-filterSave Unsupported machine-filterUnavailable machine-filterUnfiltered machine-filterVertical Screen machine-filterWorking machine-filterYear not found path-optionArtwork path-optionArtwork Previews path-optionBosses path-optionCabinets path-optionCategory INIs path-optionCheat Files path-optionControl Panels path-optionCovers path-optionCrosshairs path-optionDATs path-optionFlyers path-optionGame Endings path-optionGame Over Screens path-optionHowTo path-optionINIs path-optionIcons path-optionLogos path-optionMarquees path-optionPCBs path-optionPlugin Data path-optionPlugins path-optionROMs path-optionScores path-optionSelect path-optionSnapshots path-optionSoftware Media path-optionSound Samples path-optionTitle Screens path-optionUI Settings path-optionUI Translations path-optionVersus playing recording selmenu-artworkArtwork Preview selmenu-artworkBosses selmenu-artworkCabinet selmenu-artworkControl Panel selmenu-artworkCovers selmenu-artworkEnding selmenu-artworkFlyer selmenu-artworkGame Over selmenu-artworkHowTo selmenu-artworkLogo selmenu-artworkMarquee selmenu-artworkPCB selmenu-artworkScores selmenu-artworkSelect selmenu-artworkSnapshots selmenu-artworkTitle Screen selmenu-artworkVersus software-filterAuthor software-filterAvailable software-filterClones software-filterCustom Filter software-filterDeveloper software-filterDevice Type software-filterDistributor software-filterFavorites software-filterParents software-filterPartially Supported software-filterProgrammer software-filterPublisher software-filterRelease Region software-filterSoftware List software-filterSupported software-filterUnavailable software-filterUnfiltered software-filterUnsupported software-filterYear stopped swlist-infoAlternate Title swlist-infoAuthor swlist-infoBarcode Number swlist-infoDeveloper swlist-infoDistributor swlist-infoISBN swlist-infoInstallation Instructions swlist-infoOEM swlist-infoOriginal Publisher swlist-infoPCB swlist-infoPart Number swlist-infoProgrammer swlist-infoRelease Date swlist-infoSerial Number swlist-infoSoftware list/item swlist-infoUsage Instructions swlist-infoVersion Project-Id-Version: MAME
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-17 07:08+1100
PO-Revision-Date: 2021-10-16 12:57+0800
Last-Translator: YuiFAN
Language-Team: MAME Language Team
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 

--- 驱动程式信息 ---
驱动程式:  

按任意按键继续 

本机械尚有可执行的彷製版： %s 
        设定已保存        

 
此机械的部分元件无法模拟，因其需要实际活动的互动或机械装置的组合。故无法达成此机械的完成体验。
 
声音：
 
此机械无法执行，机械的模拟还不完全。除了等待开发人员改良模拟之外，没有方法可以解决这个问题。
 
视讯：
   %1$s
   %1$s    [缺省： %2$s]
   %1$s    [标签： %2$s]
   调节器输入    [%1$d 输入]
   类比输入    [%1$d 输入]
   博奕输入    [%1$d 输入]
   花牌输入    [%1$d 输入]
   键盘输入    [%1$d 输入]
   数字键盘输入    [%1$d 输入]
   麻将输入    [%1$d 输入]
   画面 '%1$s': %2$d × %3$d (H) %4$s Hz
   画面 '%1$s'： %2$d × %3$d (V) %4$s Hz
   画面 '%1$s'： 向量
   使用者输入    [%1$d 输入]
  (缺省)  （已锁定）  色彩  PENS %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
驱动： %4$s

CPU:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d 机械 (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d 软件包 ) %1$s (%2$s) - %3$s
 %1$s - %2$s
 %1$s 亮度 %1$s 对比 %1$s 迦玛值 %1$s 水平位置 %1$s 水平延展 %1$s 刷新率 %1$s 垂直位置 %1$s 垂直延展 %1$s 音量 %1$s [root%2$s] %1$s, %2$-.100s %1$s: %2$s
 %1$s： %2$s - 搜寻： %3$s_ %2$s
 总计发现 %d 符合 %s %s
 已新增至最爱清单。 %s
 已从最爱清单移除。 %s [%d Hz] %s [内部] %s 已加入 %s.txt 已保存于 UI 设定文件夹 %s.xml 已保存于 UI 设定文件夹 (EP)ROM	不完美
 (EP)ROM	无法执行
 （空） （正在执行） （正在记录） * BIOS 设定：
  %1$d 选项    [缺省： %2$s]
 * CPU:
 * 配置设定：
 * 指拨开关设定：
 * 输入装置： * 媒体选项：
 * 插槽选项：
 * 声音:
 * 视频：
 **保存 %s.ini 时错误** **保存 ui.ini 时错误** , %s <设定过滤> ARGB 设定 关于 %s 已激活: %s 已激活: %s = %s 新增 %1$s 文件夹 - 搜寻： %2$s_ 新增文件夹 新增至最爱 新增连发按钮 新增过滤器 新增或移除最爱项目 调整速度以符合更新率 分配 Adstick 装置 进阶选项 仅对齐 所有 全部作弊码已重新载入 全部插槽已清除且目前状态已保存至插槽 1 允许倒带 类比控制 类比控制	是
 任意 确定要退出吗？

按 ''%1$s'' 退出,
按 ''%2$s'' 继续模拟。 装饰图选项 校验标记为不可用的 %1$u 机台的 ROM 档案 校验所有 %1$u 机台的 ROM 档案 校验媒体 正在校验供机台 %2$u 的 %3$u ROM 档案...
%1$s 自动 自动省略帧数 自动左旋转 自动右旋转 连发 连发按钮 自动 自动保存/还原 萤幕菜单出现时自动切换暂停 BIOS 选择 BIOS 选择 BIOS： 条码读取器 条码长度无效！ 光束点大小 光束强度 最大光束宽度 最小光束宽度 双线过滤 供截图使用的双线过滤 点阵图预缩放 粗体 收入信息 烙印撷图 CPU 或内存 相机	不完美
 相机	无法执行
 取消 取消校验?

按下 %1$s 取消
按下 %2$s 继续 正在录制时无法变更选项！ 无法跨目录保存 撷取	不完美
 撷取	无法执行
 变更 %1$s 文件夹 - 搜寻： %2$s_ 变更文件夹 此变更仅当选定 "开始新搜寻" 时生效 游戏作弊 作弊码注解：
%s 作弊码搜寻器 作弊码名称 作弊码已加入至 cheat.simple 作弊引擎不可用 作弊码已写入至 %s 且已加入至 cheat.simple 作弊码文件 从色盘选择 清除监视 投币数 %1$c： %2$d%3$s
 投币数 %1$c： NA%3$s
 投币脉冲 投币锁定 颜色预览： 颜色 出招表 通讯	不完美
 通讯	无法执行
 未完整模拟功能:  压缩 设定目录 设定机台 设定机台: 设定选项 确认离开机台 控制	不完美
 控制	无法执行
 建立 十字准星位移 %1$s 十字准星位移 X %1$1.3f 十字准星位移 Y %1$1.3f 十字准星选项 十字准星尺度 %1$s 十字准星尺度 X %1$1.3f 十字准星尺度 Y %1$1.3f 当前 %1$s 文件夹 当前时数 自订 自订 UI 机台设定开关 资料格式 缺省 缺省名称为 %s 删除已保存的状态档 %1$s?
按下 %2$s 删除
按下 %3$s 取消 装置映对 分配转盘装置 差异 已停用 停用: %s 磁盘	不完美
 磁盘	无法执行
 完成 双击或按下 %1$s 改变颜色 双击或按下 %1$s 以选择 驱动 驱动程序为 BIOS	否
 驱动程序为 BIOS	是
 驱动相容于	%1$s
 驱动为主档	
 驱动相容于： %-.100s 驱动为主档 驱动： "%1$s" 软件清单  驱动： %-.100s
 编辑连发按钮 模拟 已启用 启用: %s 强制比例 放大右侧面版的图片 输入条码 错误存取 %s 移除已保存的状态档 %1$s 时发生错误 结束 展开以符合 汇出显示的清单至档案 汇出 TXT 格式列表 （同 -listfull） 汇出 XML 格式列表 （同 -listxml） 汇出 TXT 格式列表 （同 -listxml，但不包含装置） 檢視外部文件 载入连射菜单时失败 保存输入名称档案时失败 快进 档案 档案已存在 — 是否覆写？ 文件管理器 筛选 过滤 %1$u 翻转 X 翻转 Y 文件夹设定 字体 强制撷图显示比例为 4:3 省略帧数 GHz GLSL Gameinit 一般信息 一般输入 图形	不完美
 图形	色彩不完美
 图形	OK
 图形	无法执行
 图形	色彩错误
 图形： 不完美，  图形： OK，  图形： 不完整， 群组 HLSL 隐藏两者 隐藏过滤器 隐藏信息/图片 在可用清单中隐藏无 ROM 机台 高分 历史 热键 Hz 映像档格式 映像档信息 图像 未完美模拟功能:  包含克隆版 信息自动校验 信息 信息字体大小 输入 输入设定 （一般） 输入设定 （本机器） 输入选项 输入埠名称档案保存至 %s 输入埠 输入了无效序列 斜体 摇杆 摇杆无反应区 摇杆饱和值 键盘	不完美
 键盘	无法执行
 键盘输入	是
 键盘模式 区域网路	不完美
 区域网路	无法执行
 语言 激光影碟 %1$s 水平位置 激光影碟 %1$s 水平延展 激光影碟 %1$s 垂直位置 激光影碟 %1$s 垂直延展 最后插槽的值 左值等于右值 位元遮罩下左值等于右值 左值等于原值 左值多于右值 左值多于原值 左值少于右值 左值少于原值 左值不等于右值 位元遮罩下左值不等于右值 左值不等于原值 光线枪 分配光线枪装置 行 载入状态 低延迟 MAMEinfo MARP得分 MESSinfo MHz 机器设定 机器信息 磁鼓	不完美
 磁鼓	无法执行
 磁带	不完美
 磁带	无法执行
 维持长宽比例 Mamescore 手动 当需要时手动切换暂停 制造商	%1$s
 主音量 符合区块 机械式机台	否
 机械式机台	是
 媒体	不完美
 媒体	无法执行
 记忆体状态已保存至插槽 %d 菜单预览 麦克风	不完美
 麦克风	无法执行
 其他选项 鼠标 鼠标	不完美
 鼠标	无法执行
 分配鼠标装置 多重键盘 多鼠标 当未限制速度时静音 未设定 名称：            描述：
 自然 自然键盘 网路装置 新条码： 新映像档名称： 否 没有发现群组 INI 档案 在类别档案中没有找到群组 找不到机械，请检查 %1$s.ini 的 ROM 目录设定

如果此为首次使用 %2$s，请参阅 docs 目录中的 config.txt 以取得设定 %2$s 的相关信息。 未找到插件 未找到已保存的状态 非整数缩放 无 无
 不支援 画面数 经过帧数后按钮将按下 经过帧数后按钮将放开 关 放开帧数 画面外重新装填 开 按下帧数 本机械有一个或更多的 ROM/CHD 是不正确的。此机械可能无法正确执行。
 本机械有一个或更多的 ROM/CHD 尚未正确地被 DUMP。
 其他控制 其他：  整体	不可执行
 整体	未模拟保护
 整体	可以执行
 整体： 不可执行 整体： 未模拟保护 整体： 可以执行 超频 %1$s 声音 超频 CPU %1$s P%d 十字准星 P$d 可视度 分配划桨装置 页 部分支援 暂停模式 暂停／停止 分配踏板装置 执行比较  :  插槽 %d %s %d 执行比较  ：  插槽 %d %s 插槽 %d %s %d 执行比较  ：  插槽 %d BITWISE%s 插槽 %d 执行比较：插槽 %d %s 插槽 %d 效能选项 执行 游戏次数 玩家 玩者 %1$d 控制 请一并输入副档名 插件选项 插件 分配指向装置 按下 %1$s 加入
 按下 %1$s 取消
 按下 %1$s 清除
 按下  %1$s 删除 按下 %1$s 还原缺省值
 按下 %1$s 设定
 按下 %s 清除热键 按下 %s 删除 按下 TAB 键设定 按下键盘按键、摇杆按钮或选择状态以覆写 按任意键继续。 按下热键按键或稍后保持不变 按住 打印机	不完美
 打印机	无法执行
 伪终端 打孔带	不完美
 打孔带	无法执行
 ROM 验证 	停用
采样文件验证 	停用
 ROM 校验结果	错误
 ROM 校验结果	OK
 重新选择上次执行的机台 读取此映像档，写入至其他映像档 读取此映像档，写入差异档 唯读 读写 记录 全部重新载入 移除 %1$s 文件夹 移除文件夹 从最爱移除 移除最后过滤器 选定软体所需要的 ROM / 磁碟映像档为缺少或不正确。 请取得正确的档案或选择不同的软体项目。

 选定系统所需要的 ROM / 磁碟映像档为缺少或不正确。 请取得正确的档案或选择不同的系统。

 需要装饰图	否
 需要装饰图	是
 需要 CHD	否
 需要 CHD	是
 需要可点击的装饰图	否
 需要可点击的装饰图	是
 重设 全部重设 还原至缺省颜色 结果将保存在 %1$s 回到机械 回上一层菜单 回上一层菜单 版本： %1$s 倒带 倒带容量 ROM组	%1$s
 旋转 左旋转 右旋转 选转选项 采样率 采样文字 - Lorem ipsum dolor sit amet, consectetur adipiscing elit. 样本档校验结果	错误
 采样文件校验结果	不需要
 样本档校验结果	OK
 保存 保存作弊码 保存设定 保存机台设定 保存状态 保存现有的记忆体状态至插槽 %d 保存输入名称至档案 画面 画面 #%d 画面 '%1$s' 画面方向	水平
 画面方向	垂直
 尚未支援台面型筐体模式的画面翻转。 搜寻： %1$s_ 选择新机器 选择存取模式 选择供连发用的按键 选择类别: 选择要设定热键的作弊码 选择自订过滤器: 选择映像档格式 选择初始化内容 选择要载入的状态 选项清单 - 搜寻：  设定 设定热键 设定 显示全部 显示文件检视 显示鼠标指标 显示侧面版 同时发生的对立状况 略过 BIOS 选择菜单 略过未完整模拟的警示讯息 略过起始的信息画面 略过软件部分选择菜单 休眠 参数调整 插槽 %d 插槽 1 的值 插槽设备 软件列表信息 软件相容于： %1$-.100s 软件为主档 软体列表/项目： %1$s:%2$s 软件部分选项： 声音 声音	不完美
 声音	无
 声音	OK
 声音	无法执行
 声音选项 声音： 不完美 声音： 无 声音： OK 声音： 无法执行 速度 启动机械 最大化开始 开始新搜寻 状态/播放选项 稳定键 支援檯面型筐体	否
 支援即时存档	否
 支援即时存档	是
 已支援： 否 已支援： 部分 已支援： 是 切换项目顺序 切换顺序：当前项目依照描述排序 切换顺序：当前项目依照短档名排序 同步更新 系统信息 系统名称 系统： %1$-.100s 磁带控制 测试 测试作弊码 %08X_%02X 测试/写入塞入值 选定的游戏缺少一个至多个必要的 ROM 或 CHD 映像档，请选择其他游戏。
按任意键继续。 选定的软体缺少一个或多个必要的 ROM 或 CHD 映像档
请取得正确的档案或选择不同的档案。 本机械已知具有下列的问题

 此驱动需要额外载入下列装置的映像档：  此机台没有 BIOS。 此机台没有可设定的输入 本机械无声音硬体，故 MAME 将不会产生声音，这是可预期的行为。
 此机械需要额外的装饰图档 本机械无法完成，并非模拟中的错误而可能呈现怪异的行为或缺少元件。
 限制速度 彩票分配数： %1$d

 计时器 计时	不完美
 计时	无法执行
 总时数 分配轨迹球装置 三重缓冲 类型 类型名称为空 输入名称或选择： %1$s_ 输入名称或选择： (随机) UI 色彩设定 UI 自订设置 UI 字体 UI 字体设定 UI 启动 停用 UI 控制
以 %1$s 切换 启用 UI 控制
以 %1$s 切换 无法写入档案
确认 cheatpath 资料夹是否存在 撤销前次搜寻 -- # 执行时间： %1$d:%2$02d

 执行时间： %1$d:%2$02d:%3$02d

 使用外部采样文件 使用图片作为背景 如果你想塞入 %s，用这个 如果你想塞入最后的插槽值（如你开始时忘了某项但终于找到了），用这个 如果你想塞入插槽 1 的值（如你开始某事但忘了），用这个 使用者介面 值 向量 向量闪烁度 视频模式 视频选项 可见度延迟 外部网路	不完美
 外部网路	无法执行
 等待垂直同步 警示信息 监视 视窗模式 写入 X 仅 X 轴 X 轴或 Y 周（自动） Y 仅 Y 轴 年代	%1$s
 是 可输入任意类型名称 缩放 = %1$d 缩放 = 1/%1$d 缩放至画面区域 缩放至画面区域 [ 无 ]
 [ 无卡启动 ] [ 此选项当前「尚未」挂载於运行中的系统 ]

选项： %1$s
装置： %2$s

如选用此选项，下列项目將启用：
 [ 此选项当前已挂载於运行中的系统 ]

选项： %1$s
装置： %2$s

已选用的选项启用下列项目：
 [ 使用文件管理器 ] [内建] [ 完整清单 ] [建立] [空插槽] [ 空 ] [ 错误 ] [ 文件管理器 ] [没有类别 INI 档案] [INI 档案中没有群组] [root%1$s] [root%2$s] [软件清单] 透明度 蓝 绿 红 背景 边框 克隆 机械设定开关 图形检视器背景 鼠标按下背景颜色 鼠标按下颜色 鼠标经过背景颜色 鼠标经过颜色 一般文本 一般文本背景 选择背景颜色 选择色彩 参数调整 子项目颜色 未拥有游戏颜色 黑 蓝 灰 绿 橘 红 银 紫 白 黄 克隆 鼠标经过 一般 已选择 子项目 缺省 区域网 广域网 相机 撷取硬体 色盘 通讯 控制 磁盘 图形 键盘 磁鼓 磁带 媒体 麦克风 鼠标 印表机 保护 打孔带 固态保存 声音 计时 不正确的检查码 不正确的长度 玩家 1 开始 玩家 2 开始 玩家 3 开始 玩家 4 开始 玩家 5 开始 玩家 6 开始 玩家 7 开始 玩家 8 开始 AD 游戏杆 X AD 游戏杆 X 10 AD 游戏杆 X 2 AD 游戏杆 X 3 AD 游戏杆 X 4 AD 游戏杆 X 5 AD 游戏杆 X 6 AD 游戏杆 X 7 AD 游戏杆 X 8 AD 游戏杆 X 9 AD 游戏杆 Y AD 游戏杆 Y 10 AD 游戏杆 Y 2 AD 游戏杆 Y 3 AD 游戏杆 Y 4 AD 游戏杆 Y 5 AD 游戏杆 Y 6 AD 游戏杆 Y 7 AD 游戏杆 Y 8 AD 游戏杆 Y 9 AD 游戏杆 Z AD 游戏杆 Z 10 AD 游戏杆 Z 2 AD 游戏杆 Z 3 AD 游戏杆 Z 4 AD 游戏杆 Z 5 AD 游戏杆 Z 6 AD 游戏杆 Z 7 AD 游戏杆 Z 8 AD 游戏杆 Z 9 押分 投钞孔 1 记帐 除错器内中断 取消 投币孔 1 投币孔 10 投币孔 11 投币孔 12 投币孔 2 投币孔 3 投币孔 4 投币孔 5 投币孔 6 投币孔 7 投币孔 8 投币孔 9 组态菜单 发牌 方向盘 方向盘 10 方向盘 2 方向盘 3 方向盘 4 方向盘 5 方向盘 6 方向盘 7 方向盘 8 方向盘 9 方向盘 V 方向盘 V 10 方向盘 V 2 方向盘 V 3 方向盘 V 4 方向盘 V 5 方向盘 V 6 方向盘 V 7 方向盘 V 8 方向盘 V 9 机门 机门连锁 比倍 快转 减少省略帧数 增加省略帧数 半押 大 保留 1 保留 2 保留 3 保留 4 保留 5 开分 洗分 键盘 小键盘 光枪 X 光枪 X 10 光枪 X 2 光枪 X 3 光枪 X 4 光枪 X 5 光枪 X 6 光枪 X 7 光枪 X 8 光枪 X 9 光枪 Y 光枪 Y 10 光枪 Y 2 光枪 Y 3 光枪 Y 4 光枪 Y 5 光枪 Y 6 光枪 Y 7 光枪 Y 8 光枪 Y 9 载入状态 小 记忆体重置 鼠标 X 鼠标 X 10 鼠标 X 2 鼠标 X 3 鼠标 X 4 鼠标 X 5 鼠标 X 6 鼠标 X 7 鼠标 X 8 鼠标 X 9 鼠标 Y 鼠标 Y 10 鼠标 Y 2 鼠标 Y 3 鼠标 Y 4 鼠标 Y 5 鼠标 Y 6 鼠标 Y 7 鼠标 Y 8 鼠标 Y 9 参数调整 P1 按钮 1 P1 按钮 10 P1 按钮 11 P1 按钮 12 P1 按钮 13 P1 按钮 14 P1 按钮 15 P1 按钮 16 P1 按钮 2 P1 按钮 3 P1 按钮 4 P1 按钮 5 P1 按钮 6 P1 按钮 7 P1 按钮 8 P1 按钮 9 P1 下 P1 花牌 A/1 P1 花牌 B/2 P1 花牌 C/3 P1 花牌 D/4 P1 花牌 E/5 P1 花牌 F/6 P1 花牌 G/7 P1 花牌 H/8 P1 花牌 否 P1 花牌 是 P1 左 P1 左摇杆/下 P1 左摇杆/左 P1 左摇杆/右 P1 左摇杆/上 P1 麻将 A P1 麻将 B P1 麻将 押分 P1 麻将 大 P1 麻将 C P1 麻将 吃 P1 麻将 D P1 麻将 加倍 P1 麻将 E P1 麻将 F P1 麻将 交换牌 P1 麻将 G P1 麻将 H P1 麻将 I P1 麻将 J P1 麻将 K P1 麻将 杠 P1 麻将 L P1 麻将 海底机会 P1 麻将 M P1 麻将 N P1 麻将 O P1 麻将 P P1 麻将 碰 P1 麻将 Q P1 麻将 听 P1 麻将 胡 P1 麻将 小 P1 麻将 得分 P1 踏板 1 P1 踏板 2 P1 踏板 3 P1 右 P1 右摇杆/下 P1 右摇杆/左 P1 右摇杆/右 P1 右摇杆/上 P1 选择 P1 开始 P1 上 P10 按钮 1 P10 按钮 10 P10 按钮 11 P10 按钮 12 P10 按钮 13 P10 按钮 14 P10 按钮 15 P10 按钮 16 P10 按钮 2 P10 按钮 3 P10 按钮 4 P10 按钮 5 P10 按钮 6 P10 按钮 7 P10 按钮 8 P10 按钮 9 P10 下 P10 左 P10 左摇杆/下 P10 左摇杆/左 P10 左摇杆/右 P10 左摇杆/上 P10 踏板 1 P10 踏板 2 P10 踏板 3 P10 右 P10 右摇杆/下 P10 右摇杆/左 P10 右摇杆/右 P10 右摇杆/上 P10 选择 P10 开始 P10 上 P2 按钮 1 P2 按钮 10 P2 按钮 11 P2 按钮 12 P2 按钮 13 P2 按钮 14 P2 按钮 15 P2 按钮 16 P2 按钮 2 P2 按钮 3 P2 按钮 4 P2 按钮 5 P2 按钮 6 P2 按钮 7 P2 按钮 8 P2 按钮 9 P2 下 P2 花牌 A/1 P2 花牌 B/2 P2 花牌 C/3 P2 花牌 D/4 P2 花牌 E/5 P2 花牌 F/6 P2 花牌 G/7 P2 花牌 H/8 P2 花牌 否 P2 花牌 是 P2 左 P2 左摇杆/下 P2 左摇杆/左 P2 左摇杆/右 P2 左摇杆/上 P2 麻将 A P2 麻将 B P2 麻将 押分 P2 麻将 大 P2 麻将 C P2 麻将 吃 P2 麻将 D P2 麻将 加倍 P2 麻将 E P2 麻将 F P2 麻将 交换牌 P2 麻将 G P2 麻将 H P2 麻将 I P2 麻将 J P2 麻将 K P2 麻将 杠 P2 麻将 L P2 麻将 海底机会 P2 麻将 M P2 麻将 N P2 麻将 O P2 麻将 P P2 麻将 碰 P2 麻将 Q P2 麻将 听 P2 麻将 胡 P2 麻将 小 P2 麻将 得分 P2 踏板 1 P2 踏板 2 P2 踏板 3 P2 右 P2 右摇杆/下 P2 右摇杆/左 P2 右摇杆/右 P2 右摇杆/上 P2 选择 P2 开始 P2 上 P3 按钮 1 P3 按钮 10 P3 按钮 11 P3 按钮 12 P3 按钮 13 P3 按钮 14 P3 按钮 15 P3 按钮 16 P3 按钮 2 P3 按钮 3 P3 按钮 4 P3 按钮 5 P3 按钮 6 P3 按钮 7 P3 按钮 8 P3 按钮 9 P3 下 P3 左 P3 左摇杆/下 P3 左摇杆/左 P3 左摇杆/右 P3 左摇杆/上 P3 踏板 1 P3 踏板 2 P3 踏板 3 P3 右 P3 右摇杆/下 P3 右摇杆/左 P3 右摇杆/右 P3 右摇杆/上 P3 选择 P3 开始 P3 上 P4 按钮 1 P4 按钮 10 P4 按钮 11 P4 按钮 12 P4 按钮 13 P4 按钮 14 P4 按钮 15 P4 按钮 16 P4 按钮 2 P4 按钮 3 P4 按钮 4 P4 按钮 5 P4 按钮 6 P4 按钮 7 P4 按钮 8 P4 按钮 9 P4 下 P4 左 P4 左摇杆/下 P4 左摇杆/左 P4 左摇杆/右 P4 左摇杆/上 P4 踏板 1 P4 踏板 2 P4 踏板 3 P4 右 P4 右摇杆/下 P4 右摇杆/左 P4 右摇杆/右 P4 右摇杆/上 P4 选择 P4 开始 P4 上 P5 按钮 1 P5 按钮 10 P5 按钮 11 P5 按钮 12 P5 按钮 13 P5 按钮 14 P5 按钮 15 P5 按钮 16 P5 按钮 2 P5 按钮 3 P5 按钮 4 P5 按钮 5 P5 按钮 6 P5 按钮 7 P5 按钮 8 P5 按钮 9 P5 下 P5 左 P5 左摇杆/下 P5 左摇杆/左 P5 左摇杆/右 P5 左摇杆/上 P5 踏板 1 P5 踏板 2 P5 踏板 3 P5 右 P5 右摇杆/下 P5 右摇杆/左 P5 右摇杆/右 P5 右摇杆/上 P5 选择 P5 开始 P5 上 P6 按钮 1 P6 按钮 10 P6 按钮 11 P6 按钮 12 P6 按钮 13 P6 按钮 14 P6 按钮 15 P6 按钮 16 P6 按钮 2 P6 按钮 3 P6 按钮 4 P6 按钮 5 P6 按钮 6 P6 按钮 7 P6 按钮 8 P6 按钮 9 P6 下 P6 左 P6 左摇杆/下 P6 左摇杆/左 P6 左摇杆/右 P6 左摇杆/上 P6 踏板 1 P6 踏板 2 P6 踏板 3 P6 右 P6 右摇杆/下 P6 右摇杆/左 P6 右摇杆/右 P6 右摇杆/上 P6 选择 P6 开始 P6 上 P7 按钮 1 P7 按钮 10 P7 按钮 11 P7 按钮 12 P7 按钮 13 P7 按钮 14 P7 按钮 15 P7 按钮 16 P7 按钮 2 P7 按钮 3 P7 按钮 4 P7 按钮 5 P7 按钮 6 P7 按钮 7 P7 按钮 8 P7 按钮 9 P7 下 P7 左 P7 左摇杆/下 P7 左摇杆/左 P7 左摇杆/右 P7 左摇杆/上 P7 踏板 1 P7 踏板 2 P7 踏板 3 P7 右 P7 右摇杆/下 P7 右摇杆/左 P7 右摇杆/右 P7 右摇杆/上 P7 选择 P7 开始 P7 上 P8 按钮 1 P8 按钮 10 P8 按钮 11 P8 按钮 12 P8 按钮 13 P8 按钮 14 P8 按钮 15 P8 按钮 16 P8 按钮 2 P8 按钮 3 P8 按钮 4 P8 按钮 5 P8 按钮 6 P8 按钮 7 P8 按钮 8 P8 按钮 9 P8 下 P8 左 P8 左摇杆/下 P8 左摇杆/左 P8 左摇杆/右 P8 左摇杆/上 P8 踏板 1 P8 踏板 2 P8 踏板 3 P8 右 P8 右摇杆/下 P8 右摇杆/左 P8 右摇杆/右 P8 右摇杆/上 P8 选择 P8 开始 P8 上 P9 按钮 1 P9 按钮 10 P9 按钮 11 P9 按钮 12 P9 按钮 13 P9 按钮 14 P9 按钮 15 P9 按钮 16 P9 按钮 2 P9 按钮 3 P9 按钮 4 P9 按钮 5 P9 按钮 6 P9 按钮 7 P9 按钮 8 P9 按钮 9 P9 下 P9 左 P9 左摇杆/下 P9 左摇杆/左 P9 左摇杆/右 P9 左摇杆/上 P9 踏板 1 P9 踏板 2 P9 踏板 3 P9 右 P9 右摇杆/下 P9 右摇杆/左 P9 右摇杆/右 P9 右摇杆/上 P9 选择 P9 开始 P9 上 操纵杆 操纵杆 10 操纵杆 2 操纵杆 3 操纵杆 4 操纵杆 5 操纵杆 6 操纵杆 7 操纵杆 8 操纵杆 9 操纵杆 V 操纵杆 V 10 操纵杆 V 2 操纵杆 V 3 操纵杆 V 4 操纵杆 V 5 操纵杆 V 6 操纵杆 V 7 操纵杆 V 8 操纵杆 V 9 暂停 暂停 - 单步 退币 定位器 定位器 10 定位器 2 定位器 3 定位器 4 定位器 5 定位器 6 定位器 7 定位器 8 定位器 9 定位器 V 定位器 V 10 定位器 V 2 定位器 V 3 定位器 V 4 定位器 V 5 定位器 V 6 定位器 V 7 定位器 V 8 定位器 V 9 关机 开机 录制 AVI 录制 MNG 重新启动机台 倒带 - 单步 保存撷图 保存状态 业务模式 业务开关 1 业务开关 2 业务开关 3 业务开关 4 显示解码图形 显示 FPS 显示效能分析 软体重新启动 停牌 停止所有卷轴 停止卷轴 1 停止卷轴 2 停止卷轴 3 停止卷轴 4 得分 限制速度 碰撞机台 碰撞机台 1 碰撞机台 2 碰撞机台 3 碰撞机台 4 切换作弊码 轨迹球 X 轨迹球 X 10 轨迹球 X 2 轨迹球 X 3 轨迹球 X 4 轨迹球 X 5 轨迹球 X 6 轨迹球 X 7 轨迹球 X 8 轨迹球 X 9 轨迹球 Y 轨迹球 Y 10 轨迹球 Y 2 轨迹球 Y 3 轨迹球 Y 4 轨迹球 Y 5 轨迹球 Y 6 轨迹球 Y 7 轨迹球 Y 8 轨迹球 Y 9 菜单 (首)磁带开始 菜单 (首)磁带停止 菜单 新增/移除最爱项目 菜单 校验媒体 菜单 取消键 菜单 清除键 菜单 预设缩放 菜单 显示注解 UI 向下键 菜单 最下键 菜单 汇出列表 菜单 检视外部 DAT 菜单 下一焦点 菜单 上一焦点 菜单 最上键 菜单 向左键 菜单 下一组 菜单 下一页 菜单 上一页 菜单 贴上文字 菜单 上一组 菜单 解除指标锁定 菜单 向右键 菜单 旋转 菜单 选定键 菜单 切换 菜单 向上键 菜单 放大 菜单 缩小 音量降低 音量提升 写入目前的时间码 kHz 可用 BIOS 需要 CHD 类别 克隆版 自订过滤 最爱 水平萤幕 制造商 机械式 不需要 CHD 非 BIOS 非机械式 不可执行 母档 已支援即时存档 不支援即时存档 不可用 未筛选 垂直萤幕 可以执行 年代 未找到 装饰图 装饰图预览 首领 机械图 (cabinets) 类别 INI 作弊档 控制面板 封面 十字准星 文件 广告图 (flyers) 游戏结局 游戏结束画面 说明图 INI 图示 标题图 贴画 (marquees) PCB 图 外挂资料 外挂 ROM 得分 精选 快照 (snap) 软件媒介 声音样本档 标题画面 UI 设定 UI 翻译 对战 正在执行 正在记录 装饰图预览 首领 机械图 控制面板 封面 结局 广告图 游戏结束 说明图 标题图 贴画 PCB 图 得分 精选 快照 标题画面 对战 作者 可用 克隆版 自订过滤 开发商 装置类别 经销商 最爱 母档 部分支援 编码 出版商 释出区域 软件清单 已支援 不可用 未筛选 不支援 年代 已停止 别题名 作者 条码编号 开发商 经销商 ISBN 安装说明 OEM 原出版商 PCB 配件编号 编码 发行日 序号 软体列表/项目 使用说明 版本 