��    \     �
              �     �  .   �     (     G  �   P     
  
     	        (     0  	   6     @     G  	   O     Y     a  "   h  	   �  .   �     �     �  
   �     �               &     9     K     W     g     s     y      �     �  	   �     �     �     �            
   9     D  
   U     `     z     �  ^   �     �                     8     N     a     t     �     �     �     �  "   �     �     �                    .     C     V     e     l     t     �     �     �     �     �     �     
          1     K     e     z     �     �     �     �  $   �     �               2     C     a     s     |     �  !   �  
   �     �     �     �     �               -     :     A     O     U     Z     c     p          �     �     �     �     �  	   �     �     �               "     4     ;     A     Q     a     v     }     �     �     �     �     �           +      1      5      K   	   _      i      |      �      �      �      �      �      �      �      
!     !  �   !     �!     �!     �!     "  Y   "  G   `"     �"     �"     �"     �"     �"     #     0#     A#     T#     m#  
   �#     �#     �#  !   �#     �#     �#     �#     $  '   $     :$  	   Y$  
   c$     n$  
   u$     �$     �$     �$     �$     �$  	   �$     �$     �$     %     %     %     '%     .%  F   :%     �%     �%  
   �%     �%     �%     �%  3   �%     $&     7&     J&     a&     u&     �&     �&     �&     �&     �&     �&  "   �&     '      '     -'     M'     `'     y'     '  	   �'     �'     �'     �'  	   �'     �'     �'     (     (     "(     1(  2   F(  0   y(     �(     �(     �(     �(  ,   �(  E   )  ]   Y)  z   �)     2*     L*     h*     y*     �*     �*     �*     �*     �*     �*     +     ,+     ;+     B+  
   Q+     \+     j+     x+     �+     �+     �+  
   �+     �+     �+     �+     �+     �+     �+     �+     �+     ,     ,     *,     >,     P,     h,     |,     �,  (   �,     �,  (   �,     -     5-  #   N-  &   r-     �-     �-     �-     �-     	.     .     ..     @.     S.     g.     x.     �.     �.     �.     �.     �.     �.     /     /     1/     9/     =/     Y/     m/     �/     �/     �/     �/     �/     �/     0     0     -0     >0     P0     b0     w0     �0     �0     �0     �0     �0  	   �0     �0     1     1     51     O1     e1     |1     �1     �1     �1     �1     2     2  �  2  %   �3  2   �3     4     64  �   >4     �4     �4     �4      5     5  	   5     5     5  	   &5     05     85  "   ?5  	   b5  1   l5     �5     �5  
   �5     �5     �5     �5     �5     6     !6     06     @6     L6  %   R6  '   x6     �6     �6     �6     �6     �6     7  $   7     77     G7     _7      m7     �7     �7  c   �7     &8     28     @8  "   [8     ~8     �8     �8     �8     �8     �8     9     9  $   09     U9     e9     k9     �9     �9     �9     �9     �9     �9     �9     �9     �9     :  	   +:     5:     M:     j:     �:     �:     �:     �:     �:     ;     ;     -;  $   D;     i;  $   w;     �;     �;     �;     �;     �;     <  
   <     *<     6<  "   K<     n<  
   {<  &   �<     �<     �<     �<      �<     =     =     =     .=     5=     :=     C=     X=     j=     =     �=     �=     �=     �=     �=     �=     �=     >     
>     >     1>  	   :>     D>     Y>     f>     >     �>     �>     �>     �>     �>     �>  !   ?     9?     ??     C?     Z?  
   r?     }?     �?     �?     �?     �?     �?     �?     @     @     /@     F@  �   I@  	   A     #A     2A     :A  j   BA  O   �A     �A     B     &B     EB     WB     kB     �B     �B     �B     �B  
   �B     �B  	   C  #   C     <C  "   DC     gC     �C  1   �C  %   �C     �C     �C     	D     D     %D     <D     ND     gD     �D     �D     �D     �D     �D     �D     �D     �D     �D  J   E     WE     iE  	   oE     yE  #   �E  !   �E  5   �E     F     F     5F     PF     gF  
   �F     �F     �F     �F     �F      �F  ,    G     -G     @G     VG     rG     �G     �G     �G     �G     �G     �G     �G     �G     H     H     0H     <H     RH     _H  7   rH  ;   �H     �H     I     	I  %   I  -   BI  V   pI  d   �I  �   ,J     �J  "   �J     �J     �J  %   K  	   EK     OK     dK      ~K     �K     �K     �K     �K     �K  
   L     L      L     7L     HL     UL     WL  	   YL     cL     gL     L     �L     �L  	   �L     �L     �L     �L     �L     �L  	   �L     �L     M     M     M     !M     AM  "   YM     |M     �M     �M     �M     �M     �M     N     N     .N     6N     =N     DN  
   LN  	   WN     aN     mN     vN     }N     �N     �N  
   �N     �N     �N     �N     �N  	   �N     �N     �N     �N     �N     �N     O     O     !O     &O  	   /O     9O     >O     DO     IO     RO     _O     dO     kO     tO     {O  	   �O     �O     �O     �O  	   �O  	   �O     �O     �O     �O     �O  	   �O     �O     P  	   P   

Press any key to continue 

There are working clones of this machine: %s 
    Configuration saved    

 
Sound:
 
THIS MACHINE DOESN'T WORK. The emulation for this machine is not yet complete. There is nothing you can do to fix this problem except wait for the developers to improve the emulation.
 
Video:
  (default)  (locked)  COLORS  PENS %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Driver: %4$s

CPU:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d machines (%5$d BIOS) ) %1$s Brightness %1$s Contrast %1$s Gamma %1$s Horiz Position %1$s Horiz Stretch %1$s Refresh Rate %1$s Vert Position %1$s Vert Stretch %1$s Volume %1$s, %2$-.100s %1$s: %2$s
 %2$s
 %s
 added to favorites list. %s
 removed from favorites list. %s [internal] (playing) (recording) **Error saving %s.ini** **Error saving ui.ini** ARGB Settings Add %1$s Folder - Search: %2$s_ Add Folder Add To Favorites Add filter Adstick Device Assignment All cheats reloaded Analog Controls Are you sure you want to quit?

Press ''%1$s'' to quit,
Press ''%2$s'' to return to emulation. Auto BIOS Selection Barcode Reader Barcode length invalid! Beam Intensity Weight Beam Width Maximum Beam Width Minimum Bilinear Filtering Bitmap Prescaling Bold Bookkeeping Info Cannot save over directory Change %1$s Folder - Search: %2$s_ Change Folder Cheat Cheat Comment:
%s Cheats Choose from palette Coin %1$c: %2$d%3$s
 Coin %1$c: NA%3$s
 Color preview: Colors Command Configure Directories Configure Options Confirm quit from machines Create Crosshair Offset %1$s Crosshair Offset X %1$1.3f Crosshair Offset Y %1$1.3f Crosshair Options Crosshair Scale %1$s Crosshair Scale X %1$1.3f Crosshair Scale Y %1$1.3f Current %1$s Folders Customize UI DIP Switches Device Mapping Dial Device Assignment Disabled Double-click or press %1$s to select Driver is Clone of	%1$s
 Driver is Parent	
 Driver is clone of: %1$-.100s Driver is parent Driver: "%1$s" software list  Driver: %1$-.100s Emulated Enabled Enforce Aspect Ratio Enlarge images in the right panel Enter Code Exit Export displayed list to file External DAT View Fast Forward File File Already Exists - Override? File Manager Filter Folders Setup Fonts GLSL Gameinit General Info General Inputs Graphics	Imperfect
 Graphics	Imperfect Colors
 Graphics	OK
 Graphics: Imperfect,  Graphics: OK,  HLSL Hide Both Hide Filters Hide Info/Image History Image Format: Image Information Images Infos Infos text size Input (general) Input (this Machine) Italic Keyboard Mode Language Laserdisc '%1$s' Horiz Position Laserdisc '%1$s' Horiz Stretch Laserdisc '%1$s' Vert Position Laserdisc '%1$s' Vert Stretch Lightgun Device Assignment Lines MHz Machine Configuration Machine Information Mamescore Manufacturer	%1$s
 Master Volume Menu Preview Miscellaneous Options Mouse Device Assignment Name:             Description:
 Natural Network Devices New Barcode: New Image Name: No No machines found. Please check the rompath specified in the %1$s.ini file.

If this is your first time using %2$s, please see the config.txt file in the docs directory for information on configuring %2$s. None
 Not supported Off On One or more ROMs/CHDs for this machine are incorrect. The machine may not run correctly.
 One or more ROMs/CHDs for this machine have not been correctly dumped.
 Other Controls Overall	NOT WORKING
 Overall	Unemulated Protection
 Overall	Working
 Overall: NOT WORKING Overall: Unemulated Protection Overall: Working Overclock CPU %1$s Paddle Device Assignment Partially supported Pause/Stop Pedal Device Assignment Play Please enter a file extension too Plugins Positional Device Assignment Press TAB to set Pseudo terminals Read this image, write to another image Read this image, write to diff Read-only Read-write Record Reload All Remove %1$s Folder Remove Folder Remove From Favorites Remove last filter Reset Reset All Return to Machine Return to Previous Menu Revision: %1$s Rewind Romset	%1$s
 Rotate Sample Rate Sample text - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Save Configuration Screen Screen #%d Screen '%1$s' Screen Orientation	Horizontal
 Screen Orientation	Vertical
 Screen flipping in cocktail mode is not supported.
 Select New Machine Select access mode Select custom filters: Select image format Selection List - Search:  Settings Show All Show DATs view Show mouse pointer Show side panels Skip BIOS selection menu Skip software parts selection menu Slider Controls Slot Devices Software is clone of: %1$-.100s Software is parent Software part selection: Sound Sound	Imperfect
 Sound	OK
 Sound	Unimplemented
 Sound Options Sound: Imperfect Sound: OK Sound: Unimplemented Start Out Maximized Supported: No Supported: Partial Supported: Yes Switch Item Ordering Switched Order: entries now ordered by description Switched Order: entries now ordered by shortname Synchronized Refresh Sysinfo System: %1$-.100s Tape Control There are known problems with this machine

 This driver requires images to be loaded in the following device(s):  This machine has no sound hardware, MAME will produce no sounds, this is expected behaviour.
 This machine was never completed. It may exhibit strange behavior or missing elements that are not bugs in the emulation.
 Tickets dispensed: %1$d

 Trackball Device Assignment Triple Buffering Type name or select: %1$s_ Type name or select: (random) UI Font UI Fonts Settings Uptime: %1$d:%2$02d

 Uptime: %1$d:%2$02d:%3$02d

 Use External Samples Use image as background User Interface Vector Vector Flicker Video Mode Video Options Visible Delay Wait Vertical Sync Window Mode X Y Year	%1$s
 Yes [compatible lists] [create] [empty slot] [empty] [failed] [file manager] [software list] color-channelAlpha color-channelBlue color-channelGreen color-channelRed color-optionBackground color-optionBorder color-optionClone color-optionDIP switch color-optionMouse down background color color-optionMouse down color color-optionMouse over background color color-optionMouse over color color-optionNormal text color-optionNormal text background color-optionSelected background color color-optionSelected color color-optionSlider color color-optionSubitem color color-optionUnavailable color color-presetBlack color-presetBlue color-presetGray color-presetGreen color-presetOrange color-presetRed color-presetSilver color-presetViolet color-presetWhite color-presetYellow color-sampleClone color-sampleMouse Over color-sampleNormal color-sampleSelected color-sampleSubitem default kHz machine-filterManufacturer machine-filterYear path-optionArtwork Previews path-optionBosses path-optionCabinets path-optionControl Panels path-optionCovers path-optionCrosshairs path-optionDATs path-optionFlyers path-optionHowTo path-optionINIs path-optionIcons path-optionLogos path-optionMarquees path-optionPCBs path-optionROMs path-optionScores path-optionSnapshots path-optionVersus playing recording selmenu-artworkArtwork Preview selmenu-artworkBosses selmenu-artworkCovers selmenu-artworkGame Over selmenu-artworkHowTo selmenu-artworkScores selmenu-artworkSnapshots selmenu-artworkVersus software-filterDevice Type software-filterPublisher software-filterSoftware List software-filterYear stopped Project-Id-Version: MAME
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-17 07:08+1100
PO-Revision-Date: 2016-04-01 16:38+0200
Last-Translator: Karel Brejcha <pankabre@gmail.com>
Language-Team: MAME Language Team
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Poedit 1.8.7
 

Stiskni klávesu pro pokračování 

Existují funkční klony tohoto zařízení: %s 
    Configurace uložena    

 
Zvuk:
 
ZAŘÍZENÍ NEFUNGUJE. Emulace tohoto zařízení není ještě kompletní. Nic s tím nemůžete udělat, jen čekat na vývojáře až vylepší emulaci.
 
Video:
  (standardní)  (zamčeno)  BARVY  PERA %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Driver: %4$s

CPU:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d zařízení (%5$d BIOS) ) %1$s Jas %1$s Kontrast %1$s Gamma %1$s Horiz Pozice %1$s Horiz Roztah %1$s Obnovovací frekvence %1$s Vert Pozice %1$s Vert Roztah %1$s hlasitost %1$s, %2$-.100s %1$s: %2$s
 %2$s
 %s
 přidán do seznamu oblíbených. %s
 odstraněn ze seznamu oblíbených. %s [interní] (přehrává) (nahrává) **Chyba ukládání %s.ini** **Chyba ukládání ui.ini** ARGB nastavení Přidat %1$s Složku - Hledat: %2$s_ Přidat složku Přidat do oblíbených Přidat filtr ADStick zařízení přiřazení Všechny cheaty znovu načteny Analogové ovladače Určitě chcete odejít?

Stiskni ''%1$s'' pro ukončení,
Stiskni ''%2$s'' pro návrat do emulace. Automaticky Výběr BIOSu Čtečka čárového kódu Špatná délka čárového kódu! Váha intenzity svazku Maximální šířka svazku Minimální šířka svazku Bilineární filtrování Bitmap Prescaling Tučné Zapsané informace Nemohu zapisovat do složky Změnit %1$s Složku - Hledat: %2$s_ Změnit složku Cheat Cheat komentář:
%s Cheaty Výběr z palety Mince %1$c: %2$d%3$s
 Mince %1$c: NA%3$s
 Barevný náhled: Barvy Příkaz Konfigurace složek Možnosti konfigurace Potvrď odchod ze zařízení Vytvořit Zaměřovač posun %1$s Zaměřovač posun X %1$1.3f Zaměřovač posun Y %1$1.3f Nastavení zameřovače Zaměřovač škála %1$s Zaměřovač škála X %1$1.3f Zaměřovač škála Y %1$1.3f Aktuální %1$s složky Upravení UI DIP přepínače Mapování zařízení Vytáčecí zařízení přiřazení Deaktivováno Dvojklik nebo stisk %1$s pro výběr Driver je Klon	%1$s
 Driver je Parent	
 Driver je klon: %1$-.100s Driver je parent Driver: "%1$s" software list  Driver: %1$-.100s Emulovaná Aktivováno Vynutit poměr stran Zvětši obrázky v pravém panelu Vložit kód Ukončení Export zobrazeného seznamu do souboru Externí DAT zobrazení Přetočit dopředu Soubor Soubor již existuje - Přepsat? Souborový manažer Filtr Nastavení složek Písma GLSL Gameinit Základní informace Základní vstupy Grafika	Nedokonalá
 Grafika	Nedokonalé barvy
 Grafika	OK
 Grafika: Nedokonalá,  Grafika: OK,  HLSL Skrýt obojí Skrýt filtry Skrýt info/obrázek Historie Formát obrázku: Informace o programu Obrázky Informace Velikost info textů Vstup (vše) Vstup (toto zařízení) Kurzíva Režim klávesnice Jazyk Laserdisc '%1$s' Horiz Pozice Laserdisc '%1$s' Horiz Roztah Laserdisc '%1$s' Vert Pozice Laserdisc '%1$s' Vert Roztah Lightgun zařízení přiřazení Linie MHz Nastavení zařízení Informace o zařízení Mameskóre Výrobce	%1$s
 Hlavní hlasitost Menu náhled Různé možnosti Myš zařízení přiřazení Jméno:             Popis:
 Přirozená Síťová zařízení Nový čárový kód: Nové jméno obrázku: Ne Žádná zařízení nenalezena. Prosím zkontrolujte rompath v %1$s.ini souboru.

Pokud používáte %2$s poprvé, prosím podívejte se na config.txt soubor ve složce docs na informace o konfiguraci %2$s. Žádný
 Nepodporováno Vypnuto Zapnuto Jedna nebo více ROMs/CHDs pro toto zařízení je nekorektních. Zařízení nemusí fungovat správně.
 Jedna nebo více ROM/CHD pro toto zařízení je špatně stažena (dumpnuta).
 Ostatní ovládání Celkově	NEFUNGUJE
 Celkově	Neemulovaná ochrana
 Celkově	Funguje
 Celkově: NEFUNGUJE Celkově: Neemulovaná ochrana Celkově: Funguje Přetakt CPU %1$s Pádlo zařízení přiřazení Částečná podpora Pauza/Stop Pedál zařízení přiřazení Přehrát Prosím vložte i příponu souboru Pluginy Poziční zařízení přiřazení Stiskni TAB pro nastavení Pseudo terminály Číst tento obrázek, zapsat do jiného obrázku Číst tento obrázek, zapsat do diff Pouze čtení Čtení-Zápis Nahrát Znovu načíst vše Odstranit %1$s Složku Odstranit složku Odstranit z oblíbených Odstranit poslední filtr Reset Resetovat vše Zpět na zařízení Zpět na předchozí menu Verze: %1$s Přetočit zpět Romset	%1$s
 Rotace Vzorkovací frekvence Ukázkový text - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ulož konfiguraci Obraz Obraz #%d Obraz '%1$s' Orientace obrazovky	Horizontální
 Orientace obrazovky	Vertikální
 Otočení obrazu v koktail módu není podporováno.
 Vyber nové zařízení Vyber přístupový režim Vyber uživatelský filtr: Vyber formát obrázku Seznam výběru - Hledání:  Nastavení Zobrazit vše Zobraz DAT pohled Zobraz ukazatel myši Zobraz postranní panely Přeskoč menu s výběrem BIOSu Přeskoč menu s výběrem částí software Posuvné ovladače Zásuvná zařízení Software je klon: %1$-.100s Software je parent Výběr části software: Zvuk Zvuk	Nedokonalý
 Zvuk	OK
 Zvuk	Neimplementován
 Možnosti zvuku Zvuk: Nedokonalý Zvuk: OK Zvuk: Neimplementován Spustit maximalizovaně Podpora: Ne Podpora: Částečně Podpora: Ano Přepnout řazení Přepnuté řazení: záznamy nyní řazeny podle popis Přepnuté řazení: záznamy nyní řazeny podle shortname Synchronizované obnovení Sysinfo Systém: %1$-.100s Ovládání kazetového přehrávače S tímto zařízením jsou známy problémy

 Tento ovladač vyžaduje aby byl obraz media nahrán do následujících zařízení:  Zařízení nemá zvukový hardware, MAME nebyde přehrávat zvuky, toto je očekávané chování.
 Zařízení nebylo nikdy dokončeno. Může se objevit zvláštní chování nebo chybějící části, jež nejsou chybou emulace.
 Lístků zahozeno: %1$d

 Trackball zařízení přiřazení Triple Buffering Napiš jméno nebo vyber: %1$s_ Napiš jméno nebo vyber: (náhodně) UI Písmo Nastavení písma UI Čas běhu: %1$d:%2$02d

 Čas běhu: %1$d:%2$02d:%3$02d

 Použít externí samply Použij obrázek jako pozadí Uživatelské rozhraní Vektor Vektor Blikač Video Mód Nastavení videa Viditelné zpoždění Čekat na V-Sync Okenní mód X Y Rok	%1$s
 Ano [kompatibilní seznamy] [vytvořit] [prázdný slot] [prázdný] [chybný] [souborový manažer] [software list] Průhlednost Modrá Zelená Červená Pozadí Okraj Klon DIP přepínač Barva pozadí při stisku myši Barva při stisku myši Barva pozadí při přejetí myši Barva při přejetí myši Normální text Pozadí normálního textu Barva pozadí vybraného textu Vybraná barva Barva posuvníku Barva podpoložky Barva nedostupné položky Černá Modrá Šedá Zelená Oranžová Červená Stříbrná Fialová Bílá Žlutá Klon Přejetí myši Normální Vybraný Podpoložka standardní kHz Kategorie Rok Náhledy artworků Bossové Kastle Ovládací panely Obaly Zaměřovače DATs Plakáty Jak na to INIs Ikony Loga Marquees Desky spojů ROMs Skóre Náhledy Versus přehrává nahrává Náhled artworku Bossové Obaly Konec hry Jak na to Skóre Náhledy Versus Typ zařízení Vydavatel Software List Rok zastaveno 