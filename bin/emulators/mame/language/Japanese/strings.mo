��    1     �!              ,C     -C     LC  .   hC     �C     �C  �   �C     yD     �D     �D  #   �D  !   �D  #   �D  #   E  #   BE  !   fE  "   �E  +   �E  +   �E     F     F  
   <F  	   GF     QF     YF  	   _F     iF     pF  "   xF  .   �F  +   �F     �F     G  
   G     G     3G     FG     XG     kG     }G     �G     �G     �G     �G     �G     �G     �G      �G     H     ,H  	   5H     ?H  3   KH     H     �H     �H     �H     �H  	   �H  	   �H     �H     I     -I     2I     CI     QI     _I     rI  
   �I     �I  
   �I     �I     �I     �I     �I     �I     J     #J     8J  ^   <J     �J  /   �J      �J  .   �J     +K     0K     @K     QK     cK     lK     �K     �K     �K     �K     �K     �K     �K     �K     
L     L     /L     4L     EL  
   ML     XL     jL     �L     �L  "   �L     �L  D   �L     M     M     0M  
   =M     HM     dM  -   M     �M     �M     �M     �M     �M     �M     N     N     'N     .N      6N     WN     mN     N     �N     �N     �N     �N     �N     �N     O     +O     =O     RO     lO     �O     �O     �O     �O     �O     �O     �O     �O     �O     �O     P     P     ,P     <P     PP  *   UP  $   �P     �P     �P     �P     �P     �P     �P     Q     .Q     LQ     ^Q     gQ     oQ     {Q  !   �Q  
   �Q     �Q     �Q     �Q  *   �Q  )   R  >   HR     �R     �R     �R     �R     �R     �R     �R     �R     
S     S     S     &S  %   ,S  
   RS     ]S     bS     kS     xS     �S     �S     �S     �S     �S     �S     T     T     1T     7T  	   <T     FT     ST  (   cT     �T     �T     �T     �T     �T     �T     �T     �T     U     U     U     ,U     AU      OU     pU     |U     �U     �U     �U     �U     �U     �U     �U     V     V     #V     ,V     LV     kV     �V     �V      �V     �V     �V     	W     !W     6W     KW  $   cW     �W     �W     �W     �W  
   �W     �W  	   �W     �W     �W     �W     X  	   X     )X     <X     JX     VX     mX     �X     �X     �X     �X     �X     �X     �X     Y     Y     +Y     7Y     WY     _Y     pY     �Y     �Y     �Y     �Y      �Y  �   �Y     �Z     �Z     �Z     �Z     �Z     �Z     �Z  Y   �Z  G   H[     �[     �[     �[     �[     �[     �[     \     )\     >\     Q\     j\     o\  
   �\     �\     �\     �\  
   �\     �\  !   �\     �\     ]     
]     ']     8]     S]     f]     }]  ,   �]     �]     �]  '   �]     ^  	   -^  
   7^     B^  
   I^     T^     g^     u^     �^     �^     �^     �^     �^     �^     _     +_  	   1_     ;_     R_     p_     �_     �_     �_     �_     �_     �_     �_     �_     �_     �_  F   
`     Q`  !   k`     �`     �`  
   �`     �`  
   �`     �`     �`  
   �`     �`     a     ,a  3   Ia     }a     �a     �a     �a     �a     �a     �a     b     "b     &b     2b     ;b     Db     Sb     fb     wb     �b  "   �b  "   �b     �b     �b     c     c     4c     Gc     `c     fc     wc  	   �c     �c     �c     �c     �c  	   �c     �c     �c     �c     d     d     .d     Cd     Td     fd     td     �d     �d  2   �d  0   �d     e     $e     ,e     >e     Ke  �   Pe  ,   �e  E   �e     Df  ]   ^f  .   �f  z   �f     fg     og     �g     �g     �g  
   �g     �g     �g     �g     �g     h     -h     ?h     Gh  	   Yh  8   ch     �h     �h     �h     �h     �h     i     !i     'i     .i  
   =i     Hi     Vi     di     si     �i     �i     �i     �i  
   �i     �i     �i  �   �i  �   bj     �j     �j     k     k     k     #k     2k     Jk     bk     rk     �k     �k     �k     �k     �k     �k     �k  '   l  (   >l     gl  (   �l     �l     �l  #   �l  &   	m     0m     Lm     fm     �m     �m     �m     �m     �m     �m     �m     n     #n     7n     Jn     ^n     qn     �n     �n     �n     �n     �n     �n     �n     o     5o     Po     go     �o     �o     �o     �o     �o     	p     !p     :p     Tp     op     �p     �p     �p     �p     �p     q     #q     6q     Iq     \q     nq     �q     �q     �q     �q     �q     �q     �q     r     r     3r     Kr     cr     {r     �r     �r     �r     �r     �r     
s     "s     :s     Rs     js     �s     �s     �s     �s     �s     �s     t     t     2t     Gt     \t     qt     �t     �t     �t     �t     �t     �t     u     u     -u     Bu     Wu     lu     �u     �u     �u     �u     �u     �u     v     +v     Bv     Yv     pv     �v     �v     �v     �v     �v     �v     	w     'w     Ew     dw     �w     �w     �w     �w     �w     �w     x     .x     Nx     fx     ~x     �x     �x     �x     �x     �x     y     0y  !   Hy     jy     �y     �y     �y     �y     �y     �y     z     2z      Nz     oz     �z     �z     �z     �z     �z     {     #{     @{     U{     i{     z{     �{     �{     �{     �{     �{     |     (|     A|     Y|     q|     �|     �|     �|     �|     �|     }     }     )}     H}     g}     �}     �}     �}     �}     �}     �}     ~      >~     _~     }~     �~     �~     �~     �~     �~               1     I     a     y     �     �     �     �     �     �     �     1�     D�     W�     u�     ��     ��     ΀     �     ��     �     2�     J�     d�     |�     ��     ��     ́     �     �     �     4�     L�     d�     ~�  !   ��     ��     Ђ     �      �     �     2�     J�     f�     ��      ��     ��     Ӄ     �     ��     �     2�     Q�     q�     ��     ��     ��     Ȅ     ߄     ��     �     '�     ?�     W�     o�     ��     ��     ��     ̅     �     ��     �     (�     ?�     R�     e�     ��     ��     ��     ܆     �     �     �     2�     Q�     p�     ��     ��          և     �     ��     �     .�     F�     ^�     v�     ��     ��     ��     Ԉ     �     �     �     0�     G�     ^�     q�     ��     ��     ��     ߉     ��     �     '�     =�     Q�     p�     ��     ��     ̊     �     ��     �     �     5�     M�     e�     }�     ��     ��     ŋ     ܋     �     
�     !�     8�     O�     f�     }�     ��     ��     ��     ߌ     ��     �     0�     F�     \�     p�     ��     ��     ΍     �      �     �     %�     <�     T�     l�     ��     ��     ��     ̎     �     ��     �     )�     @�     W�     n�     ��     ��     ��          ��     ��     �     9�     O�     e�     {�     ��     ��     ͐     �     
�     �     3�     D�     [�     s�     ��     ��     ��     ӑ     �     �     �     1�     H�     _�     v�     ��     ��     ��     Β     �     ��     �     <�     X�     n�     ��     ��     ��     ͓     �     �     )�     >�     R�     c�     z�     ��     ��          ڔ     �     
�     "�     9�     P�     g�     ~�     ��     ��     Õ     ڕ     �      �     �     <�     [�     w�     ��     ��     ��     ͖     �     �     +�     H�     ]�     q�     ��     ��     ��     ɗ     �     ��     �     )�     A�     X�     o�     ��     ��     ��     ˘     �     ��     �     �     =�     [�     z�     ��     ��          ؙ     �     �     *�     J�     g�     |�     ��     ��     ��     ɚ     ޚ     �     �     �     0�     C�     W�     l�     }�     ��     ��     ��     Λ     �     �      �     ;�     W�     q�     ��     ��     Ɯ     �     ��     �     6�     Q�     k�     ��     ��     ��     ɝ     �     ��     �     (�     @�     [�     n�     ��     ��     ��     ��     ̞     ޞ     �     �     �     '�     :�     P�     k�     ��     ��  	   ��     ��     ʟ     �     ��     �     .�     D�     ^�     t�     ��     ��     ��     ̠     �     �     �     4�     K�     i�     ��     ��     ��  #   ѡ     ��     �     .�     L�     f�     ��     ��     ��     ΢     ֢     �     �  r  �  +   ��  ;   ��  =   ��  &   2�     Y�  �   k�     �  #   �     @�  /   [�  &   ��  )   ��      ܦ  )   ��  )   '�      Q�  ,   r�  ,   ��     ̧  #   �     �     &�     6�     ;�  	   C�     M�     T�  -   \�  /   ��  ?   ��     ��     �     "�     3�     K�  "   c�     ��     ��     ��     ĩ     թ     �     ��  &   �     ,�  3   /�  6   c�     ��     ��     ��     Ϊ  C   ު     "�     5�     Q�     j�     ��     ��     ��      ��      �     �  !   �  
   )�     4�     M�  0   k�     ��     ��     ˬ  '   �  3   	�     =�     J�     Q�     p�  &   ��     ��  W   ��     	�  F   "�  (   i�  .   ��     ��     Ȯ     �     ��     �     �     6�  
   ;�     F�     T�  +   m�     ��     ��     ȯ  $   �  $   �     +�     2�     Q�     ^�     n�     ��     ��  -   ��  0   ְ     �  _   �  	   }�     ��     ��     ��  -   ̱  -   ��  A   (�  	   j�     t�     ��     ��     ��     ϲ     �     �     �     !�  4   :�     o�     ��     ��     ��     ʳ     �     �     �  "   +�  "   N�     q�     ~�     ��     ��     д     �     ��     �     %�     5�     Q�     a�     y�  $   ��     ��     ��     ε     �     ��  O   �  @   U�     ��     ��     ��  '   ض      �  -   �     L�  7   h�     ��     ��     ̷     ӷ  !   �  $   �     '�     =�     X�  6   _�  3   ��  2   ʸ  J   ��     H�  3   [�  	   ��     ��  9   ��     �     ��     	�     �     +�     <�     O�  B   \�     ��     ��     ��     Һ     ߺ     ��  #   �     7�     R�      p�  !   ��     ��  !   һ     ��     �     �  !   "�  $   D�  B   i�     ��     ��     ռ     �     ��  (   �     ,�     E�     [�     b�     ��     ��     ��  8   ��     ��     	�     �  *   2�  0   ]�     ��     ��     ľ     �     ��     �     %�  2   ,�  2   _�  2   ��  2   ſ     ��  -   �     9�     L�     e�     ��     ��     ��  6   ��     �     "�  !   2�     T�     [�     n�     ��     ��     ��     ��     ��     ��     ��     ��     �  &   �  #   =�     a�     ��     ��     ��  	   ��     ��     ��  !   �     %�     >�      Q�     r�     ��     ��     ��     ��  	   ��  -    �  ?   .�  �   n�     h�     o�     w�  	   ��     ��     ��     ��  z   ��  Q   ?�     ��     ��  /   ��     ��     �  0   �     N�  +   a�  #   ��  !   ��  	   ��     ��     ��  !   �     (�     D�     K�     [�  3   k�     ��     ��     ��     ��  9    �     :�     U�     p�  +   ��     ��     ��  H   ��  F   +�     r�     ��     ��     ��     ��     ��     ��     
�  &   )�  #   P�     t�     ��  8   ��  5   ��     �     �     0�  "   C�     f�     y�     ��     ��     ��     ��     ��     ��     ��      �     �  S   #�      w�      ��      ��     ��     ��     ��     
�  !   �     ?�  
   F�     Q�     b�     w�  U   ��     ��     ��     �     ,�  0   E�     v�     ��     ��     ��     ��     ��     ��     �     �     7�     S�  +   o�  '   ��  B   ��     �     �     &�  3   ?�  !   s�  *   ��     ��     ��     ��      �     �     -�     @�     Y�     o�     ��     ��     ��     ��  !   ��  &   ��     #�     >�     V�     o�     ��  !   ��  4   ��  7   ��     -�     I�     ]�     v�  	   ��  �   ��  A   Q�  X   ��  .   ��  v   �  I   ��  �   ��     q�     ��     ��     ��     ��     ��  -   ��     �  	   4�  *   >�  7   i�     ��     ��     ��     ��  j   ��  #   H�     l�  #   ��     ��     ��  $   ��     
�     �     �     7�     J�     W�     j�     �     ��     ��     ��     ��     ��     ��  	   ��  �   ��  �   ��     ��     ��     ��  	   ��     ��  !   ��  3   �  <   5�  !   r�     ��     ��     ��     ��     ��     ��     ��     ��  '   ��  !    �     "�  $   >�     c�     ��     ��     ��  	   ��     ��     ��     �     �     �     �     %�     )�     6�     :�     A�     E�     I�     M�     Z�     p�     }�     ��     ��  	   ��  	   ��  	   ��     ��     ��     ��     ��     �     �  	   4�     >�     N�     ^�     k�     {�     ��     ��     ��     ��     ��     �     1�  
   K�     V�     b�     n�  
   z�  
   ��  
   ��  
   ��  
   ��  
   ��  
   ��  
   ��     ��     ��     ��     �     &�     ;�     P�     e�     z�     ��     ��     ��     ��     ��     ��     �     "�     7�     L�     a�     v�     ��     ��     ��     ��     ��     ��     ��     ��     ��     �     �     *�     9�     H�     W�     f�     u�     ��     ��     ��     ��     ��     ��     ��     ��     ��     �     �     �     +�     8�     E�     R�     _�     l�     y�     �     ��     ��     ��     ��     ��     ��     ��     �     �     !�     0�     <�     W�     c�      o�     ��     ��     ��     ��     ��     ��     ��     ��     �     �     �     )�     5�     D�     P�     b�     q�     ��     ��     ��     ��     ��     ��     ��     ��     �     %�     4�     C�     I�     W�     f�     u�     ��     ��     ��     ��     ��     ��     ��     ��     ��     �     �     "�     0�     7�     >�     T�     j�     ��     ��     ��     ��     ��     ��     ��     ��     	�     �     /�     ?�     F�     S�     a�     o�     }�     ��     ��     ��     ��     ��     ��     ��     ��     ��     �     �     �     #�     )�     >�     S�     h�     }�     ��     ��     ��     ��     ��     ��     ��     ��     �      �     4�     @�     L�     X�     d�     p�     �     ��     ��     ��     ��     ��     ��     ��     ��     �     �     *�     H�     U�     b�     o�     u�     ��     ��     ��     ��     ��     ��     ��     ��     �     �     $�     2�     @�     N�     \�     i�     v�     ��     ��     ��     ��     ��     ��     ��     ��     ��     ��     �     $�     1�     >�     K�     Q�     f�     {�     ��     ��     ��     ��     ��     ��     ��     ��      �     �     �     *�     8�     E�     R�     _�     l�     y�     ��     ��     ��     ��     ��     ��     ��     ��      �     �     �     '�     -�     B�     W�     l�     ��     ��     ��     ��     ��     ��     ��     ��     ��     ��     �     �     !�     .�     ;�     H�     U�     b�     o�     |�     ��     ��     ��     ��     ��     ��     ��     ��     �     	�     �     3�     H�     ]�     l�     {�     ��     ��     ��     ��     ��     ��     ��     ��     ��     ��     
�     �     $�     1�     >�     K�     X�     ^�     d�     y�     ��     ��     ��     ��     ��     ��     ��     ��     �     $�     9�     H�     W�     ]�     j�     x�     ��     ��     ��     ��     ��     ��     ��     ��     ��      �     �     �     '�     4�     :�     @�     U�     j�     �     ��     ��     ��     ��     ��     ��     ��      �     �     $�     3�     9�     F�     T�     b�     p�     ~�     ��     ��     ��     ��     ��     ��     ��     ��     ��     �     �     �     �     1�     F�     [�     p�     }�     ��     ��     ��     ��     ��     ��     ��      �     �     �     "�     0�     >�     L�     Z�     h�     v�     ��     ��     ��     ��     ��     ��     ��     ��     ��     ��     ��     �     "�     7�     L�     Y�     f�     s�     y�     ��     ��     ��     ��     ��     ��     ��     ��     �     �     (�     6�     H�     N�     T�     Z�     c�     i�     m�     z�     ��     ��     ��     ��  	   ��  	   ��     ��     ��  
   ��     �     �     )�     6�     F�     Y�     i�  	   |�  	   ��     ��     ��  $   ��     ��     ��     �     �     4�  	   J�     T�     m�     }�     ��     ��     ��     ��     ��     ��     ��  	   ��     �     �     ?�     R�  	   _�  	   i�  $   s�     ��     ��     ��  	   ��     ��     ��     ��     �     �     �  	   +�     5�     Q�     d�     q�     ~�     ��  	   ��     ��     ��     ��     ��  	   ��     ��          '     .     >  	   Q     [     b  	   i  	   s     }   

--- DRIVER INFO ---
Driver:  

Press any key to continue 

There are working clones of this machine: %s 
    Configuration saved    

 
Sound:
 
THIS MACHINE DOESN'T WORK. The emulation for this machine is not yet complete. There is nothing you can do to fix this problem except wait for the developers to improve the emulation.
 
Video:
   %1$s    [default: %2$s]
   %1$s    [tag: %2$s]
   Adjuster inputs    [%1$d inputs]
   Analog inputs    [%1$d inputs]
   Gambling inputs    [%1$d inputs]
   Hanafuda inputs    [%1$d inputs]
   Keyboard inputs    [%1$d inputs]
   Keypad inputs    [%1$d inputs]
   Mahjong inputs    [%1$d inputs]
   Screen '%1$s': %2$d × %3$d (H) %4$s Hz
   Screen '%1$s': %2$d × %3$d (V) %4$s Hz
   Screen '%1$s': Vector
   User inputs    [%1$d inputs]
  (default)  (locked)  COLORS  PENS %1$-.100s %1$.3f %1$1.2f %1$s
%2$s %3$s
Driver: %4$s

CPU:
 %1$s %2$s ( %3$d / %4$d machines (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d software packages ) %1$s Brightness %1$s Contrast %1$s Gamma %1$s Horiz Position %1$s Horiz Stretch %1$s Refresh Rate %1$s Vert Position %1$s Vert Stretch %1$s Volume %1$s, %2$-.100s %1$s: %2$s
 %1$s: %2$s - Search: %3$s_ %2$s
 %d total matches found %s %s
 added to favorites list. %s
 removed from favorites list. %s [internal] %s added (playing) (recording) * BIOS settings:
  %1$d options    [default: %2$s]
 * Configuration settings:
 * DIP switch settings:
 * Input device(s):
 * Media Options:
 * Slot Options:
 * Sound:
 * Video:
 **Error saving %s.ini** **Error saving ui.ini** , %s <set up filters> ARGB Settings Activated: %s Activated: %s = %s Add %1$s Folder - Search: %2$s_ Add Folder Add To Favorites Add filter Add or remove favorite Adstick Device Assignment Advanced Options All All cheats reloaded Analog Controls Analog Controls	Yes
 Any Are you sure you want to quit?

Press ''%1$s'' to quit,
Press ''%2$s'' to return to emulation. Artwork Options Audit ROMs for %1$u machines marked unavailable Audit ROMs for all %1$u machines Auditing ROMs for machine %2$u of %3$u...
%1$s Auto Auto frame skip Auto rotate left Auto rotate right Autofire Automatic save/restore BIOS BIOS Selection BIOS selection: Barcode Reader Barcode length invalid! Beam Intensity Weight Beam Width Maximum Beam Width Minimum Bilinear Filtering Bitmap Prescaling Bold Bookkeeping Info Burn-in CPU or RAM Camera	Imperfect
 Camera	Unimplemented
 Cancel Cannot save over directory Change %1$s Folder - Search: %2$s_ Change Folder Changes to this only take effect when "Start new search" is selected Cheat Cheat Comment:
%s Cheat Finder Cheat Name Cheat added to cheat.simple Cheat engine not available Cheat written to %s and added to cheat.simple Cheats Choose from palette Clear Watches Coin %1$c: %2$d%3$s
 Coin %1$c: NA%3$s
 Coin impulse Coin lockout Color preview: Colors Command Completely unemulated features:  Configure Directories Configure Machine Configure Options Confirm quit from machines Controls	Imperfect
 Controls	Unimplemented
 Create Crosshair Offset %1$s Crosshair Offset X %1$1.3f Crosshair Offset Y %1$1.3f Crosshair Options Crosshair Scale %1$s Crosshair Scale X %1$1.3f Crosshair Scale Y %1$1.3f Current %1$s Folders Current time Custom Customize UI DIP Switches Data Format Default Default name is %s Device Mapping Dial Device Assignment Disabled Disabled: %s Disk	Imperfect
 Disk	Unimplemented
 Done Double-click or press %1$s to change color Double-click or press %1$s to select Driver Driver is BIOS	No
 Driver is BIOS	Yes
 Driver is Clone of	%1$s
 Driver is Parent	
 Driver is clone of: %1$-.100s Driver is parent Driver: "%1$s" software list  Driver: %1$-.100s Emulated Enabled Enabled: %s Enforce Aspect Ratio Enlarge images in the right panel Enter Code Error accessing %s Exit Export displayed list to file Export list in TXT format (like -listfull) Export list in XML format (like -listxml) Export list in XML format (like -listxml, but exclude devices) External DAT View Failed to save input name file Fast Forward File File Already Exists - Override? File Manager Filter Filter %1$u Flip X Flip Y Folders Setup Fonts Force 4:3 aspect for snapshot display Frame skip GLSL Gameinit General Info General Inputs Graphics	Imperfect
 Graphics	Imperfect Colors
 Graphics	OK
 Graphics	Unimplemented
 Graphics	Wrong Colors
 Graphics: Imperfect,  Graphics: OK,  Graphics: Unimplemented,  Group HLSL Hide Both Hide Filters Hide Info/Image Hide romless machine from available list High Scores History Image Format: Image Information Images Imperfectly emulated features:  Include clones Info auto audit Infos Infos text size Input (general) Input (this Machine) Input Options Input port name file saved to %s Input ports Italic Joystick Joystick deadzone Joystick saturation Keyboard	Imperfect
 Keyboard	Unimplemented
 Keyboard Inputs	Yes
 Keyboard Mode LAN	Imperfect
 LAN	Unimplemented
 Language Laserdisc '%1$s' Horiz Position Laserdisc '%1$s' Horiz Stretch Laserdisc '%1$s' Vert Position Laserdisc '%1$s' Vert Stretch Left equal to right Left equal to right with bitmask Left equal to value Left greater than right Left greater than value Left less than right Left less than value Left not equal to right Left not equal to right with bitmask Left not equal to value Lightgun Lightgun Device Assignment Lines Load State MAMEinfo MARPScore MESSinfo MHz Machine Configuration Machine Information Mamescore Manufacturer	%1$s
 Master Volume Match block Mechanical Machine	No
 Mechanical Machine	Yes
 Menu Preview Microphone	Imperfect
 Microphone	Unimplemented
 Miscellaneous Options Mouse Mouse	Imperfect
 Mouse	Unimplemented
 Mouse Device Assignment Multi-keyboard Multi-mouse Name:             Description:
 Natural Natural keyboard Network Devices New Barcode: New Image Name: No No category INI files found No groups found in category file No machines found. Please check the rompath specified in the %1$s.ini file.

If this is your first time using %2$s, please see the config.txt file in the docs directory for information on configuring %2$s. None None
 Not supported Number Of Screens Off Offscreen reload On One or more ROMs/CHDs for this machine are incorrect. The machine may not run correctly.
 One or more ROMs/CHDs for this machine have not been correctly dumped.
 Other Controls Overall	NOT WORKING
 Overall	Unemulated Protection
 Overall	Working
 Overall: NOT WORKING Overall: Unemulated Protection Overall: Working Overclock %1$s sound Overclock CPU %1$s Paddle Device Assignment Page Partially supported Pause/Stop Pedal Device Assignment Performance Options Play Play Count Player Please enter a file extension too Plugin Options Plugins Positional Device Assignment Press TAB to set Press any key to continue. Printer	Imperfect
 Printer	Unimplemented
 Pseudo terminals ROM Audit 	Disabled
Samples Audit 	Disabled
 ROM Audit Result	BAD
 ROM Audit Result	OK
 Read this image, write to another image Read this image, write to diff Read-only Read-write Record Reload All Remove %1$s Folder Remove Folder Remove From Favorites Remove last filter Requires Artwork	No
 Requires Artwork	Yes
 Requires CHD	No
 Requires CHD	Yes
 Requires Clickable Artwork	No
 Requires Clickable Artwork	Yes
 Reset Reset All Restore default colors Results will be saved to %1$s Return to Machine Return to Previous Menu Revision: %1$s Rewind Rewind capacity Romset	%1$s
 Rotate Rotate left Rotate right Rotation Options Sample Rate Sample text - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Samples Audit Result	BAD
 Samples Audit Result	None Needed
 Samples Audit Result	OK
 Save Save Cheat Save Configuration Save State Save input names to file Screen Screen #%d Screen '%1$s' Screen Orientation	Horizontal
 Screen Orientation	Vertical
 Screen flipping in cocktail mode is not supported.
 Search: %1$s_ Select New Machine Select access mode Select category: Select cheat to set hotkey Select custom filters: Select image format Selection List - Search:  Set Set hotkeys Settings Show All Show DATs view Show mouse pointer Show side panels Simultaneous contradictory Skip BIOS selection menu Skip information screen at startup Skip software parts selection menu Sleep Slider Controls Slot Devices Software is clone of: %1$-.100s Software is parent Software part selection: Sound Sound	Imperfect
 Sound	None
 Sound	OK
 Sound	Unimplemented
 Sound Options Sound: Imperfect Sound: None Sound: OK Sound: Unimplemented Speed Start Out Maximized Start new search State/Playback Options Support Cocktail	No
 Support Save	No
 Support Save	Yes
 Supported: No Supported: Partial Supported: Yes Switch Item Ordering Switched Order: entries now ordered by description Switched Order: entries now ordered by shortname Synchronized Refresh Sysinfo System: %1$-.100s Tape Control Test The selected game is missing one or more required ROM or CHD images. Please select a different game.

Press any key to continue. There are known problems with this machine

 This driver requires images to be loaded in the following device(s):  This machine has no BIOS. This machine has no sound hardware, MAME will produce no sounds, this is expected behaviour.
 This machine requires external artwork files.
 This machine was never completed. It may exhibit strange behavior or missing elements that are not bugs in the emulation.
 Throttle Tickets dispensed: %1$d

 Timer Timing	Imperfect
 Timing	Unimplemented
 Total time Trackball Device Assignment Triple Buffering Type Type name or select: %1$s_ Type name or select: (random) UI Color Settings UI Font UI Fonts Settings UI active Unable to write file
Ensure that cheatpath folder exists Undo last search -- # Uptime: %1$d:%2$02d

 Uptime: %1$d:%2$02d:%3$02d

 Use External Samples Use image as background User Interface Value Vector Vector Flicker Video Mode Video Options Visible Delay WAN	Imperfect
 WAN	Unimplemented
 Wait Vertical Sync Watch Window Mode Write Year	%1$s
 Yes [None]
 [This option is NOT currently mounted in the running system]

Option: %1$s
Device: %2$s

If you select this option, the following items will be enabled:
 [This option is currently mounted in the running system]

Option: %1$s
Device: %2$s

The selected option enables the following items:
 [compatible lists] [create] [empty slot] [empty] [failed] [file manager] [no category INI files] [no groups in INI file] [software list] color-channelAlpha color-channelBlue color-channelGreen color-channelRed color-optionBackground color-optionBorder color-optionClone color-optionDIP switch color-optionGraphics viewer background color-optionMouse down background color color-optionMouse down color color-optionMouse over background color color-optionMouse over color color-optionNormal text color-optionNormal text background color-optionSelected background color color-optionSelected color color-optionSlider color color-optionSubitem color color-optionUnavailable color color-presetBlack color-presetBlue color-presetGray color-presetGreen color-presetOrange color-presetRed color-presetSilver color-presetViolet color-presetWhite color-presetYellow color-sampleClone color-sampleMouse Over color-sampleNormal color-sampleSelected color-sampleSubitem default emulation-featureLAN emulation-featureWAN emulation-featurecamera emulation-featurecolor palette emulation-featurecontrols emulation-featuredisk emulation-featuregraphics emulation-featurekeyboard emulation-featuremicrophone emulation-featuremouse emulation-featureprinter emulation-featureprotection emulation-featuresound emulation-featuretiming input-name1 Player Start input-name2 Players Start input-name3 Players Start input-name4 Players Start input-name5 Players Start input-name6 Players Start input-name7 Players Start input-name8 Players Start input-nameCoin 1 input-nameCoin 10 input-nameCoin 11 input-nameCoin 12 input-nameCoin 2 input-nameCoin 3 input-nameCoin 4 input-nameCoin 5 input-nameCoin 6 input-nameCoin 7 input-nameCoin 8 input-nameCoin 9 input-nameLightgun X input-nameLightgun X 10 input-nameLightgun X 2 input-nameLightgun X 3 input-nameLightgun X 4 input-nameLightgun X 5 input-nameLightgun X 6 input-nameLightgun X 7 input-nameLightgun X 8 input-nameLightgun X 9 input-nameLightgun Y input-nameLightgun Y 10 input-nameLightgun Y 2 input-nameLightgun Y 3 input-nameLightgun Y 4 input-nameLightgun Y 5 input-nameLightgun Y 6 input-nameLightgun Y 7 input-nameLightgun Y 8 input-nameLightgun Y 9 input-nameMouse X input-nameMouse X 10 input-nameMouse X 2 input-nameMouse X 3 input-nameMouse X 4 input-nameMouse X 5 input-nameMouse X 6 input-nameMouse X 7 input-nameMouse X 8 input-nameMouse X 9 input-nameMouse Y input-nameMouse Y 10 input-nameMouse Y 2 input-nameMouse Y 3 input-nameMouse Y 4 input-nameMouse Y 5 input-nameMouse Y 6 input-nameMouse Y 7 input-nameMouse Y 8 input-nameMouse Y 9 input-nameP1 Button 1 input-nameP1 Button 10 input-nameP1 Button 11 input-nameP1 Button 12 input-nameP1 Button 13 input-nameP1 Button 14 input-nameP1 Button 15 input-nameP1 Button 16 input-nameP1 Button 2 input-nameP1 Button 3 input-nameP1 Button 4 input-nameP1 Button 5 input-nameP1 Button 6 input-nameP1 Button 7 input-nameP1 Button 8 input-nameP1 Button 9 input-nameP1 Down input-nameP1 Left input-nameP1 Left Stick/Down input-nameP1 Left Stick/Left input-nameP1 Left Stick/Right input-nameP1 Left Stick/Up input-nameP1 Mahjong A input-nameP1 Mahjong B input-nameP1 Mahjong Bet input-nameP1 Mahjong Big input-nameP1 Mahjong C input-nameP1 Mahjong Chi input-nameP1 Mahjong D input-nameP1 Mahjong Double Up input-nameP1 Mahjong E input-nameP1 Mahjong F input-nameP1 Mahjong Flip Flop input-nameP1 Mahjong G input-nameP1 Mahjong H input-nameP1 Mahjong I input-nameP1 Mahjong J input-nameP1 Mahjong K input-nameP1 Mahjong Kan input-nameP1 Mahjong L input-nameP1 Mahjong Last Chance input-nameP1 Mahjong M input-nameP1 Mahjong N input-nameP1 Mahjong O input-nameP1 Mahjong P input-nameP1 Mahjong Pon input-nameP1 Mahjong Q input-nameP1 Mahjong Reach input-nameP1 Mahjong Ron input-nameP1 Mahjong Small input-nameP1 Mahjong Take Score input-nameP1 Pedal 1 input-nameP1 Pedal 2 input-nameP1 Pedal 3 input-nameP1 Right input-nameP1 Right Stick/Down input-nameP1 Right Stick/Left input-nameP1 Right Stick/Right input-nameP1 Right Stick/Up input-nameP1 Select input-nameP1 Start input-nameP1 Up input-nameP10 Button 1 input-nameP10 Button 10 input-nameP10 Button 11 input-nameP10 Button 12 input-nameP10 Button 13 input-nameP10 Button 14 input-nameP10 Button 15 input-nameP10 Button 16 input-nameP10 Button 2 input-nameP10 Button 3 input-nameP10 Button 4 input-nameP10 Button 5 input-nameP10 Button 6 input-nameP10 Button 7 input-nameP10 Button 8 input-nameP10 Button 9 input-nameP10 Down input-nameP10 Left input-nameP10 Left Stick/Down input-nameP10 Left Stick/Left input-nameP10 Left Stick/Right input-nameP10 Left Stick/Up input-nameP10 Pedal 1 input-nameP10 Pedal 2 input-nameP10 Pedal 3 input-nameP10 Right input-nameP10 Right Stick/Down input-nameP10 Right Stick/Left input-nameP10 Right Stick/Right input-nameP10 Right Stick/Up input-nameP10 Select input-nameP10 Start input-nameP10 Up input-nameP2 Button 1 input-nameP2 Button 10 input-nameP2 Button 11 input-nameP2 Button 12 input-nameP2 Button 13 input-nameP2 Button 14 input-nameP2 Button 15 input-nameP2 Button 16 input-nameP2 Button 2 input-nameP2 Button 3 input-nameP2 Button 4 input-nameP2 Button 5 input-nameP2 Button 6 input-nameP2 Button 7 input-nameP2 Button 8 input-nameP2 Button 9 input-nameP2 Down input-nameP2 Left input-nameP2 Left Stick/Down input-nameP2 Left Stick/Left input-nameP2 Left Stick/Right input-nameP2 Left Stick/Up input-nameP2 Mahjong A input-nameP2 Mahjong B input-nameP2 Mahjong Bet input-nameP2 Mahjong Big input-nameP2 Mahjong C input-nameP2 Mahjong Chi input-nameP2 Mahjong D input-nameP2 Mahjong Double Up input-nameP2 Mahjong E input-nameP2 Mahjong F input-nameP2 Mahjong Flip Flop input-nameP2 Mahjong G input-nameP2 Mahjong H input-nameP2 Mahjong I input-nameP2 Mahjong J input-nameP2 Mahjong K input-nameP2 Mahjong Kan input-nameP2 Mahjong L input-nameP2 Mahjong Last Chance input-nameP2 Mahjong M input-nameP2 Mahjong N input-nameP2 Mahjong O input-nameP2 Mahjong P input-nameP2 Mahjong Pon input-nameP2 Mahjong Q input-nameP2 Mahjong Reach input-nameP2 Mahjong Ron input-nameP2 Mahjong Small input-nameP2 Mahjong Take Score input-nameP2 Pedal 1 input-nameP2 Pedal 2 input-nameP2 Pedal 3 input-nameP2 Right input-nameP2 Right Stick/Down input-nameP2 Right Stick/Left input-nameP2 Right Stick/Right input-nameP2 Right Stick/Up input-nameP2 Select input-nameP2 Start input-nameP2 Up input-nameP3 Button 1 input-nameP3 Button 10 input-nameP3 Button 11 input-nameP3 Button 12 input-nameP3 Button 13 input-nameP3 Button 14 input-nameP3 Button 15 input-nameP3 Button 16 input-nameP3 Button 2 input-nameP3 Button 3 input-nameP3 Button 4 input-nameP3 Button 5 input-nameP3 Button 6 input-nameP3 Button 7 input-nameP3 Button 8 input-nameP3 Button 9 input-nameP3 Down input-nameP3 Left input-nameP3 Left Stick/Down input-nameP3 Left Stick/Left input-nameP3 Left Stick/Right input-nameP3 Left Stick/Up input-nameP3 Pedal 1 input-nameP3 Pedal 2 input-nameP3 Pedal 3 input-nameP3 Right input-nameP3 Right Stick/Down input-nameP3 Right Stick/Left input-nameP3 Right Stick/Right input-nameP3 Right Stick/Up input-nameP3 Select input-nameP3 Start input-nameP3 Up input-nameP4 Button 1 input-nameP4 Button 10 input-nameP4 Button 11 input-nameP4 Button 12 input-nameP4 Button 13 input-nameP4 Button 14 input-nameP4 Button 15 input-nameP4 Button 16 input-nameP4 Button 2 input-nameP4 Button 3 input-nameP4 Button 4 input-nameP4 Button 5 input-nameP4 Button 6 input-nameP4 Button 7 input-nameP4 Button 8 input-nameP4 Button 9 input-nameP4 Down input-nameP4 Left input-nameP4 Left Stick/Down input-nameP4 Left Stick/Left input-nameP4 Left Stick/Right input-nameP4 Left Stick/Up input-nameP4 Pedal 1 input-nameP4 Pedal 2 input-nameP4 Pedal 3 input-nameP4 Right input-nameP4 Right Stick/Down input-nameP4 Right Stick/Left input-nameP4 Right Stick/Right input-nameP4 Right Stick/Up input-nameP4 Select input-nameP4 Start input-nameP4 Up input-nameP5 Button 1 input-nameP5 Button 10 input-nameP5 Button 11 input-nameP5 Button 12 input-nameP5 Button 13 input-nameP5 Button 14 input-nameP5 Button 15 input-nameP5 Button 16 input-nameP5 Button 2 input-nameP5 Button 3 input-nameP5 Button 4 input-nameP5 Button 5 input-nameP5 Button 6 input-nameP5 Button 7 input-nameP5 Button 8 input-nameP5 Button 9 input-nameP5 Down input-nameP5 Left input-nameP5 Left Stick/Down input-nameP5 Left Stick/Left input-nameP5 Left Stick/Right input-nameP5 Left Stick/Up input-nameP5 Pedal 1 input-nameP5 Pedal 2 input-nameP5 Pedal 3 input-nameP5 Right input-nameP5 Right Stick/Down input-nameP5 Right Stick/Left input-nameP5 Right Stick/Right input-nameP5 Right Stick/Up input-nameP5 Select input-nameP5 Start input-nameP5 Up input-nameP6 Button 1 input-nameP6 Button 10 input-nameP6 Button 11 input-nameP6 Button 12 input-nameP6 Button 13 input-nameP6 Button 14 input-nameP6 Button 15 input-nameP6 Button 16 input-nameP6 Button 2 input-nameP6 Button 3 input-nameP6 Button 4 input-nameP6 Button 5 input-nameP6 Button 6 input-nameP6 Button 7 input-nameP6 Button 8 input-nameP6 Button 9 input-nameP6 Down input-nameP6 Left input-nameP6 Left Stick/Down input-nameP6 Left Stick/Left input-nameP6 Left Stick/Right input-nameP6 Left Stick/Up input-nameP6 Pedal 1 input-nameP6 Pedal 2 input-nameP6 Pedal 3 input-nameP6 Right input-nameP6 Right Stick/Down input-nameP6 Right Stick/Left input-nameP6 Right Stick/Right input-nameP6 Right Stick/Up input-nameP6 Select input-nameP6 Start input-nameP6 Up input-nameP7 Button 1 input-nameP7 Button 10 input-nameP7 Button 11 input-nameP7 Button 12 input-nameP7 Button 13 input-nameP7 Button 14 input-nameP7 Button 15 input-nameP7 Button 16 input-nameP7 Button 2 input-nameP7 Button 3 input-nameP7 Button 4 input-nameP7 Button 5 input-nameP7 Button 6 input-nameP7 Button 7 input-nameP7 Button 8 input-nameP7 Button 9 input-nameP7 Down input-nameP7 Left input-nameP7 Left Stick/Down input-nameP7 Left Stick/Left input-nameP7 Left Stick/Right input-nameP7 Left Stick/Up input-nameP7 Pedal 1 input-nameP7 Pedal 2 input-nameP7 Pedal 3 input-nameP7 Right input-nameP7 Right Stick/Down input-nameP7 Right Stick/Left input-nameP7 Right Stick/Right input-nameP7 Right Stick/Up input-nameP7 Select input-nameP7 Start input-nameP7 Up input-nameP8 Button 1 input-nameP8 Button 10 input-nameP8 Button 11 input-nameP8 Button 12 input-nameP8 Button 13 input-nameP8 Button 14 input-nameP8 Button 15 input-nameP8 Button 16 input-nameP8 Button 2 input-nameP8 Button 3 input-nameP8 Button 4 input-nameP8 Button 5 input-nameP8 Button 6 input-nameP8 Button 7 input-nameP8 Button 8 input-nameP8 Button 9 input-nameP8 Down input-nameP8 Left input-nameP8 Left Stick/Down input-nameP8 Left Stick/Left input-nameP8 Left Stick/Right input-nameP8 Left Stick/Up input-nameP8 Pedal 1 input-nameP8 Pedal 2 input-nameP8 Pedal 3 input-nameP8 Right input-nameP8 Right Stick/Down input-nameP8 Right Stick/Left input-nameP8 Right Stick/Right input-nameP8 Right Stick/Up input-nameP8 Select input-nameP8 Start input-nameP8 Up input-nameP9 Button 1 input-nameP9 Button 10 input-nameP9 Button 11 input-nameP9 Button 12 input-nameP9 Button 13 input-nameP9 Button 14 input-nameP9 Button 15 input-nameP9 Button 16 input-nameP9 Button 2 input-nameP9 Button 3 input-nameP9 Button 4 input-nameP9 Button 5 input-nameP9 Button 6 input-nameP9 Button 7 input-nameP9 Button 8 input-nameP9 Button 9 input-nameP9 Down input-nameP9 Left input-nameP9 Left Stick/Down input-nameP9 Left Stick/Left input-nameP9 Left Stick/Right input-nameP9 Left Stick/Up input-nameP9 Pedal 1 input-nameP9 Pedal 2 input-nameP9 Pedal 3 input-nameP9 Right input-nameP9 Right Stick/Down input-nameP9 Right Stick/Left input-nameP9 Right Stick/Right input-nameP9 Right Stick/Up input-nameP9 Select input-nameP9 Start input-nameP9 Up input-nameService input-nameService 1 input-nameService 2 input-nameService 3 input-nameService 4 input-nameUI Cancel input-nameUI Down input-nameUI Left input-nameUI Right input-nameUI Select input-nameUI Up kHz machine-filterAvailable machine-filterCHD Required machine-filterCategory machine-filterClones machine-filterCustom Filter machine-filterFavorites machine-filterHorizontal Screen machine-filterManufacturer machine-filterMechanical machine-filterNo CHD Required machine-filterNot BIOS machine-filterNot Mechanical machine-filterNot Working machine-filterParents machine-filterSave Supported machine-filterSave Unsupported machine-filterUnavailable machine-filterUnfiltered machine-filterVertical Screen machine-filterWorking machine-filterYear path-optionArtwork path-optionArtwork Previews path-optionBosses path-optionCabinets path-optionCategory INIs path-optionCheat Files path-optionControl Panels path-optionCovers path-optionCrosshairs path-optionDATs path-optionFlyers path-optionHowTo path-optionINIs path-optionIcons path-optionLogos path-optionMarquees path-optionPCBs path-optionROMs path-optionScores path-optionSnapshots path-optionSoftware Media path-optionTitle Screens path-optionVersus playing recording selmenu-artworkArtwork Preview selmenu-artworkBosses selmenu-artworkCabinet selmenu-artworkControl Panel selmenu-artworkCovers selmenu-artworkFlyer selmenu-artworkGame Over selmenu-artworkHowTo selmenu-artworkLogo selmenu-artworkMarquee selmenu-artworkPCB selmenu-artworkScores selmenu-artworkSnapshots selmenu-artworkTitle Screen selmenu-artworkVersus software-filterAvailable software-filterClones software-filterCustom Filter software-filterDeveloper software-filterDevice Type software-filterFavorites software-filterParents software-filterPartially Supported software-filterPublisher software-filterRelease Region software-filterSoftware List software-filterSupported software-filterUnavailable software-filterUnfiltered software-filterUnsupported software-filterYear stopped swlist-infoDeveloper swlist-infoRelease Date swlist-infoVersion Project-Id-Version: MAME
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-17 07:08+1100
PO-Revision-Date: 2016-02-23 01:39+0900
Last-Translator: Automatically generated
Language-Team: MAME Language Team
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 1.6.11
 

--- ドライバ情報 ---
ドライバ:  

進めるには、何かキーを押してください。 

このマシンの動作可能なクローンセット：%s 
    設定を保存しました    

 
サウンド：
 
このマシンは動作しません。エミュレーションは未完成です。開発者がエミュレーションを改良するのをお待ち下さい。
 
表示：
   %1$s    [デフォルト：%2$s]
   %1$s    [タグ：%2$s]
   アドジャスター入力    [%1$d 入力]
   アナログ入力    [%1$d 入力]
   ギャンブル入力    [%1$d 入力]
   花札入力    [%1$d 入力]
   キーボード入力    [%1$d 入力]
   キーパッド入力    [%1$d 入力]
   麻雀入力    [%1$d 入力]
   画面 '%1$s'：%2$d × %3$d (H) %4$s Hz
   画面 '%1$s'：%2$d × %3$d (V) %4$s Hz
   画面 '%1$s'：ベクター
   ユーザ入力    [%1$d 入力]
 （デフォルト） （ロック）  色  ペン %1$-.100s %1$.3f %1$1.2f %1$s
%2$s　%3$s
ドライバ：%4$s

CPU：
 %1$s %2$s ( %3$d / %4$d マシン (%5$d BIOS) ) %1$s %2$s （ %3$d / %4$d ソフトウェアパッケージ ） %1$sの輝度 %1$sのコントラスト %1$sのガンマ %1$s 横方向の位置 %1$s 横方向の拡大 %1$sのリフレッシュレート %1$s 縦方向の位置 %1$s 縦方向の拡大 %1$sの音量 %1$s、%2$-.100s %1$s：%2$s
 %1$s：%2$s〜検索：%3$s_ %2$s
 %d個の一致が見つかりました %s 「%s」を
お気に入りに追加しました。 「%s」を
お気に入りから削除しました。 %s [内部] %sを追加しました （再生中） （録音中） * BIOS設定：
  %1$d オプション    [デフォルト：%2$s]
 * 構成設定：
 * DIPスイッチ設定：
 * 入力デバイス：
 * メディア設定：
 * スロット設定：
 * サウンド：
 * ビデオ：
 *** %s.iniの保存エラー *** *** ui.iniの保存エラー *** 、%s 〔フィルタを設定する〕 ARGB設定 実行しました： %s 実行しました： %s = %s %1$sフォルダを追加　〜　検索：%2$s_ フォルダを追加 お気に入りに追加 フィルタを追加 お気に入りに追加または削除 アナログスティックデバイス割り当て 詳細設定 全て 全チート再ロード完了 アナログ操作 アナログコントロール	はい
 全て 終了しますか?

「%1$s」で終了
「%2$s」でエミュレーションに戻る アートワーク設定 存在しないに分類された%1$uマシンのROMを検査します 全%1$uマシンのROMを検査します マシン%2$u／%3$uのROMを検査中...
%1$s 自動 自動フレームスキップ 自動で左に回転 自動で右に回転 連射 自動セーブ／ロード BIOS BIOS選択 BIOS選択： バーコードリーダ バーコードの長さが合いません! ビーム強度比重 ビーム幅（最大） ビーム幅（最小） バイリニアフィルタリング ビットマッププレスケール 太字 ブックキーピング情報 焼き付き CPUまたはRAM カメラ	不完全
 カメラ	未実装
 キャンセル 別のフォルダには保存できません %1$sフォルダを変更　〜　検索：%2$s_ フォルダを変更 この変更は、"新規に検索を開始"が選択されている場合にのみ有効です チート チートのコメント：
%s チートファインダ チート名 チートをcheat.simpleに追加しました チートエンジンが利用できません チートを%sに書き込み、cheat.simpleに追加しました チート パレットから選ぶ 監視を消去 コイン%1$c：%2$d%3$s
 コイン%1$c：NA%3$s
 コインインパルス コインロックアウト 色のプレビュー： 色 コマンドファイル 全くエミュレート出来ていない機能：  フォルダ設定 マシンの設定 オプションの設定 マシン終了時に確認 コントロール	不完全
 コントロール	未実装
 作成 照準のオフセット %1$s 照準のオフセット X %1$1.3f 照準のオフセット Y %1$1.3f 照準設定 照準の大きさ %1$s 照準の大きさ X %1$1.3f 照準の大きさ Y %1$1.3f 現在の%1$sフォルダ 現在の時間 カスタム UIをカスタマイズする DIPスイッチ データフォーマット デフォルト デフォルト名は%s デバイスマッピング ダイヤルデバイス割り当て 無効 無効： %s ディスク	不完全
 ディスク	未実装
 決定 色を変更するには%1$sを押すかダブルクリックしてください %1$sを押すかダブルクリックで選択してください ドライバ BIOSドライバ	いいえ
 BIOSドライバ	はい
 次のクローンのドライバ	%1$s
 親セットのドライバ	
 次のクローンのドライバ：%1$-.100s 親セットのドライバ ドライバ：「%1$s」　ソフトウェアリスト ドライバ：%1$-.100s エミュレート 有効 有効： %s アスペクト比を維持する 右パネルの画像を拡大する コードを入れる %sのアクセスエラー 終了 表示されているリストをファイルに出力 TXT形式でリストを出力（-listfull同様） XML形式でリストを出力（-listxml同様） XML形式でリストを出力（-listxml同様、デバイスを除く） 外部DATビュー 入力名ファイルの保存に失敗しました 早送り ファイル ファイルが存在します－上書きしますか？ ファイルマネージャ フィルタ フィルタ%1$u X方向に反転 Y方向に反転 フォルダ設定 フォント スナップショットの表示で4:3アスペクト比を強制 フレームスキップ GLSL Gameinitファイル 一般情報 共通の入力設定 グラフィック	不完全
 グラフィック	色が不完全
 グラフィック	正常
 グラフィック	未実装
 グラフィック	色の誤り
 グラフィック：不完全　 グラフィック：正常　 グラフィック：未実装　 グループ HLSL 両方を非表示にする フィルタを非表示にする 情報・画像を非表示にする ロム無しのマシンを存在するリストに表示しない ハイスコア ヒストリファイル イメージ形式: イメージ情報 画像 不完全なエミュレート機能：  クローンを含める 情報の自動検査 情報 情報のテキストサイズ 入力（共通） 入力（現在のマシン） 入力設定 入力ポート名ファイルを%sに保存しました 入力ポート イタリック ジョイスティック ジョイスティックデッドゾーン ジョイスティックサチュレーション キーボード	不完全
 キーボード	未実装
 キーボード入力	はい
 キーボードモード ＬＡＮ	不完全
 ＬＡＮ	未実装
 言語 レーザーディスク '%1$s' 横方向の位置 レーザーディスク '%1$s' 横方向の拡大 レーザーディスク '%1$s' 縦方向の位置 レーザーディスク '%1$s' 縦方向の拡大 左と右が一致 右がビットマスクありで左と一致 左が値と一致 左が右より大きく 左が値よりも大きい 左が右より小さく 左が値よりも小さい 左と右が一致せず 右がビットマスクありで左と一致しない 左が値と一致しない ライトガン 光線銃デバイス割り当て 行数 状態をロード MAMEinfoファイル MARPスコア MESSinfoファイル MHz マシン設定 マシン情報 Mamescoreファイル 製造元	%1$s
 マスター音量 一致ブロック 機械仕掛けのマシン	いいえ
 機械仕掛けのマシン	はい
 メニューのプレビュー マクロフォン	不完全
 マクロフォン	未実装
 その他の設定 マウス マウス	不完全
 マウス	未実装
 マウスデバイス割り当て マルチキーボード マルチマウス ROMセット：       名前：
 ナチュラル ナチュラルキーボード ネットワークデバイス 新しいバーコード： 新しいイメージ名: いいえ カテゴリINIファイルがありません カテゴリファイルにグループが見つかりません マシンが見つかりません。%1$s.iniファイルで指定したrompathを確認してください。

もし %2$s の利用が初めてなら、%2$s の設定方法についてdocsフォルダのconfig.txtファイルをご覧ください。 なし なし
 対応していません 画面数 オフ 画面外でのリロード オン このマシン用のROM/CHDが正しくありません。そのため正しく動作しない可能性があります。
 このマシン用のROM/CHDは全て正しく吸い出されていません。

 その他のコントロール 概略	動作不可
 概略	プロテクトが未エミュレート
 概略	動作可
 概略：動作不可 概略：プロテクトが未エミュレート 概略：動作可 サウンド%1$sのオーバークロック CPU %1$sのオーバークロック パドルデバイス割り当て ページ 部分的に対応 一時停止／停止 ペダルデバイス割り当て パフォーマンス設定 再生 遊んだ回数 プレイヤー ファイルの拡張子を入力してください プラグイン設定 プラグイン 位置デバイス割り当て TABキーを押して設定 進めるには、何かキーを押してください。 プリンター	不完全
 プリンター	未実装
 擬似ターミナル ROM検証	無効
サンプル検証	無効
 ROM検証結果	異常
 ROM検証結果	正常
 このイメージから読み取り、別のイメージに書き込む このイメージから読み取り、差分（diff）に書き込む 読み取り専用 読み取り／書き込み 録音 全て再読み込み %1$sフォルダを削除 フォルダを削除 お気に入りから削除 最後のフィルタを削除 アートワークが必要	いいえ
 アートワークが必要	はい
 CHDが必要	いいえ
 CHDが必要	はい
 クリック可のアートワークが必要	いいえ
 クリック可のアートワークが必要	はい
 リセット 全てリセット 元の色に戻す 結果は%1$sに保存されます マシンに戻る 前のメニューに戻る リビジョン：%1$s 巻き戻す 巻き戻し容量 ROMセット	%1$s
 回転 左に回転 右に回転 回転設定 サンプルレート サンプルテキスト - Lorem ipsum dolor sit amet, consectetur adipiscing elit. サンプル検証結果	異常
 サンプル検証結果	不要
 サンプル検証結果	正常
 保存 チートを保存 設定を保存する 状態をセーブ 入力名をファイルに保存 画面 画面 #%d 画面「%1$s」 画面の向き	横
 画面の向き	縦
 カクテルモードでの画面反転表示はサポートされていません。
 検索：%1$s_ 新しいマシンを選択 アクセスモードの選択 カテゴリを選択： ホットキーを設定するチートを選択 カスタムフィルタ選択: イメージ形式の選択 選択リスト - 検索： 設定 ホットキーを設定 設定 全て表示 DATビューを表示 マウスポインタを表示 サイドパネルを表示 対称方向の同時入力 BIOS選択メニューをスキップする 起動時に情報画面をスキップ ソフトウェアパーツ選択メニューをスキップする スリープ スライダ設定 スロットデバイス 次のクローンのソフトウェア：%1$-.100s 親セットのソフトウェア 選択中のソフトウェアパーツ： サウンド サウンド	不完全
 グラフィック	なし
 サウンド	正常
 サウンド	未実装
 サウンド設定 サウンド：不完全 サウンド：なし サウンド：正常 サウンド：未実装 速度 開始時に最大化する 新規に検索を開始 状態／プレイバック設定 カクテルモード対応	いいえ
 セーブ対応	いいえ
 セーブ対応	はい
 対応状況：いいえ 対応状況：部分的 対応状況：はい 項目の並び順の切り替え 並び順を切り替え： 一覧は現在名前順 並び順を切り替え： 一覧は現在ソフト順 リフレッシュを同期 Sysinfoファイル システム：%1$-.100s テープ操作 テスト 選択したゲームに必要な1つ以上のROMまたはCHDが不足しています。別のゲームを選択してください。

進めるには、何かキーを押してください。 このマシンには以下のような問題点があります

 このドライバは以下のデバイスでイメージの読み込みが必要です: このマシンにはBIOSがありません。 このマシンにはサウンドハードウェアがありません。音が出なくても正常な動作です。
 このマシンは別途アートワークファイルが必要です。
 このマシンは未完成です。動作の不具合や不足した要素があってもエミュレーションのバグではありません。
 スロットル チケット発行：%1$d

 タイマー タイミング	不完全
 タイミング	未実装
 合計時間 トラックボールデバイス割り当て トリプルバッファ タイプ 名前を入力もしくは選択： %1$s_ 名前を入力もしくは選択： （ランダム） UIの色設定 UIフォント UIフォント設定 UI有効 ファイルに書き込めません
cheatpathフォルダが存在しているか確認してください 最後の検索を取り消す -- # 稼働時間：%1$d:%2$02d

 稼働時間：%1$d:%2$02d:%3$02d

 外部サンプル使用 画像を背景に使用する ユーザーインターフェイス 値 ベクター ベクターのちらつき ビデオモード 表示設定 表示する長さ ＷＡＮ	不完全
 ＷＡＮ	未実装
 V-Syncを待つ 監視 ウィンドウモード 書き込み 年度	%1$s
 はい [なし]
 ［このオプションは実行中のシステムにマウントされていません］

オプション: %1$s
デバイス　: %2$s

オプションを選択すると、以下の項目が有効になります：
 ［このオプションは実行中のシステムにマウントされています］

オプション: %1$s
デバイス　: %2$s

選択されたオプションは以下の項目で有効：
 【互換リスト】 〔作成〕 〔空のスロット〕 〔空〕 【失敗】 【ファイルマネージャ】 ［カテゴリINIファイルがありません］ ［INIファイルにグループが見つかりません］ 〔ソフトウェアリスト〕 アルファ 青 緑 赤 背景 枠線 クローン DIPスイッチ グラフィックビューアの背景 マウスダウン時の背景色 マウスダウン時の色 マウスオーバー時の背景色 マウスオーバー時の色 通常のテキスト 通常テキストの背景 選択済テキストの背景 選択色 スライダの色 サブ項目の色 動作不可の色 黒 青 灰色 緑 オレンジ 赤 銀色 紫 白 黄 クローン マウスオーバー 通常項目 選択済の項目 サブ項目 デフォルト ＬＡＮ ＷＡＮ カメラ 色パレット コントロール ディスク グラフィック キーボード マイクロフォン マウス プリンター プロテクト サウンド タイミング 1人プレイスタート 2人プレイスタート 3人プレイスタート 4人プレイスタート 5人プレイスタート 6人プレイスタート 7人プレイスタート 8人プレイスタート コイン1 コイン10 コイン11 コイン12 コイン2 コイン3 コイン4 コイン5 コイン6 コイン7 コイン8 コイン9 ライトガンX軸 ライトガンX軸10 ライトガンX軸2 ライトガンX軸3 ライトガンX軸4 ライトガンX軸5 ライトガンX軸6 ライトガンX軸7 ライトガンX軸8 ライトガンX軸9 ライトガンY軸 ライトガンY軸10 ライトガンY軸2 ライトガンY軸3 ライトガンY軸4 ライトガンY軸5 ライトガンY軸6 ライトガンY軸7 ライトガンY軸8 ライトガンY軸9 マウスX軸 マウスX軸10 マウスX軸2 マウスX軸3 マウスX軸4 マウスX軸5 マウスX軸6 マウスX軸7 マウスX軸8 マウスX軸9 マウスY軸 マウスY軸10 マウスY軸2 マウスY軸3 マウスY軸4 マウスY軸5 マウスY軸6 マウスY軸7 マウスY軸8 マウスY軸9 1Pボタン1 1Pボタン10 1Pボタン11 1Pボタン12 1Pボタン13 1Pボタン14 1Pボタン15 1Pボタン16 1Pボタン2 1Pボタン3 1Pボタン4 1Pボタン5 1Pボタン6 1Pボタン7 1Pボタン8 1Pボタン9 1P下 1P左 1P左レバー／下 1P左レバー／左 1P左レバー／右 1P左レバー／上 1P麻雀Ａ 1P麻雀Ｂ 1P麻雀ベット 1P麻雀ビッグ 1P麻雀Ｃ 1P麻雀チー 1P麻雀Ｄ 1P麻雀ダブルアップ 1P麻雀Ｅ 1P麻雀Ｆ 1P麻雀フリップフロップ 1P麻雀Ｇ 1P麻雀Ｈ 1P麻雀Ｉ 1P麻雀Ｊ 1P麻雀Ｋ 1P麻雀カン 1P麻雀Ｌ 1P麻雀ラストチャンス 1P麻雀Ｍ 1P麻雀Ｎ 1P麻雀Ｏ 1P麻雀Ｐ 1P麻雀ポン 1P麻雀Ｑ 1P麻雀リーチ 1P麻雀ロン 1P麻雀スモール 1P麻雀テイクスコアー 1Pペダル1 1Pペダル2 1Pペダル3 1P右 1P右レバー／下 1P右レバー／左 1P右レバー／右 1P右レバー／上 1Pセレクト 1Pスタート 1P上 10Pボタン1 10Pボタン10 10Pボタン11 10Pボタン12 10Pボタン13 10Pボタン14 10Pボタン15 10Pボタン16 10Pボタン2 10Pボタン3 10Pボタン4 10Pボタン5 10Pボタン6 10Pボタン7 10Pボタン8 10Pボタン9 10P下 10P左 10P左レバー／下 10P左レバー／左 10P左レバー／右 10P左レバー／上 10Pペダル1 10Pペダル2 10Pペダル3 10P右 10P右レバー／下 10P右レバー／左 10P右レバー／右 10P右レバー／上 10Pセレクト 10Pスタート 10P上 2Pボタン1 2Pボタン10 2Pボタン11 2Pボタン12 2Pボタン13 2Pボタン14 2Pボタン15 2Pボタン16 2Pボタン2 2Pボタン3 2Pボタン4 2Pボタン5 2Pボタン6 2Pボタン7 2Pボタン8 2Pボタン9 2P下 2P左 2P左レバー／下 2P左レバー／左 2P左レバー／右 2P左レバー／上 2P麻雀Ａ 2P麻雀Ｂ 2P麻雀ベット 2P麻雀ビッグ 2P麻雀Ｃ 2P麻雀チー 2P麻雀Ｄ 2P麻雀ダブルアップ 2P麻雀Ｅ 2P麻雀Ｆ 2P麻雀フリップフロップ 2P麻雀Ｇ 2P麻雀Ｈ 2P麻雀Ｉ 2P麻雀Ｊ 2P麻雀Ｋ 2P麻雀カン 2P麻雀Ｌ 2P麻雀ラストチャンス 2P麻雀Ｍ 2P麻雀Ｎ 2P麻雀Ｏ 2P麻雀Ｐ 2P麻雀ポン 2P麻雀Ｑ 2P麻雀リーチ 2P麻雀ロン 2P麻雀スモール 2P麻雀テイクスコアー 2Pペダル1 2Pペダル2 2Pペダル3 2P右 2P右レバー／下 2P右レバー／左 2P右レバー／右 2P右レバー／上 2Pセレクト 2Pスタート 2P上 3Pボタン1 3Pボタン10 3Pボタン11 3Pボタン12 3Pボタン13 3Pボタン14 3Pボタン15 3Pボタン16 3Pボタン2 3Pボタン3 3Pボタン4 3Pボタン5 3Pボタン6 3Pボタン7 3Pボタン8 3Pボタン9 3P下 3P左 3P左レバー／下 3P左レバー／左 3P左レバー／右 3P左レバー／上 3Pペダル1 3Pペダル2 3Pペダル3 3P右 3P右レバー／下 3P右レバー／左 3P右レバー／右 3P右レバー／上 3Pセレクト 3Pスタート 3P上 4Pボタン1 4Pボタン10 4Pボタン11 4Pボタン12 4Pボタン13 4Pボタン14 4Pボタン15 4Pボタン16 4Pボタン2 4Pボタン3 4Pボタン4 4Pボタン5 4Pボタン6 4Pボタン7 4Pボタン8 4Pボタン9 4P下 4P左 4P左レバー／下 4P左レバー／左 4P左レバー／右 4P左レバー／上 4Pペダル1 4Pペダル2 4Pペダル3 4P右 4P右レバー／下 4P右レバー／左 4P右レバー／右 4P右レバー／上 4Pセレクト 4Pスタート 4P上 5Pボタン1 5Pボタン10 5Pボタン11 5Pボタン12 5Pボタン13 5Pボタン14 5Pボタン15 5Pボタン16 5Pボタン2 5Pボタン3 5Pボタン4 5Pボタン5 5Pボタン6 5Pボタン7 5Pボタン8 5Pボタン9 5P下 5P左 5P左レバー／下 5P左レバー／左 5P左レバー／右 5P左レバー／上 5Pペダル1 5Pペダル2 5Pペダル3 5P右 5P右レバー／下 5P右レバー／左 5P右レバー／右 5P右レバー／上 5Pセレクト 5Pスタート 5P上 6Pボタン1 6Pボタン10 6Pボタン11 6Pボタン12 6Pボタン13 6Pボタン14 6Pボタン15 6Pボタン16 6Pボタン2 6Pボタン3 6Pボタン4 6Pボタン5 6Pボタン6 6Pボタン7 6Pボタン8 6Pボタン9 6P下 6P左 6P左レバー／下 6P左レバー／左 6P左レバー／右 6P左レバー／上 6Pペダル1 6Pペダル2 6Pペダル3 6P右 6P右レバー／下 6P右レバー／左 6P右レバー／右 6P右レバー／上 6Pセレクト 6Pスタート 6P上 7Pボタン1 7Pボタン10 7Pボタン11 7Pボタン12 7Pボタン13 7Pボタン14 7Pボタン15 7Pボタン16 7Pボタン2 7Pボタン3 7Pボタン4 7Pボタン5 7Pボタン6 7Pボタン7 7Pボタン8 7Pボタン9 7P下 7P左 7P左レバー／下 7P左レバー／左 7P左レバー／右 7P左レバー／上 7Pペダル1 7Pペダル2 7Pペダル3 7P右 7P右レバー／下 7P右レバー／左 7P右レバー／右 7P右レバー／上 7Pセレクト 7Pスタート 7P上 8Pボタン1 8Pボタン10 8Pボタン11 8Pボタン12 8Pボタン13 8Pボタン14 8Pボタン15 8Pボタン16 8Pボタン2 8Pボタン3 8Pボタン4 8Pボタン5 8Pボタン6 8Pボタン7 8Pボタン8 8Pボタン9 8P下 8P左 8P左レバー／下 8P左レバー／左 8P左レバー／右 8P左レバー／上 8Pペダル1 8Pペダル2 8Pペダル3 8P右 8P右レバー／下 8P右レバー／左 8P右レバー／右 8P右レバー／上 8Pセレクト 8Pスタート 8P上 9Pボタン1 9Pボタン10 9Pボタン11 9Pボタン12 9Pボタン13 9Pボタン14 9Pボタン15 9Pボタン16 9Pボタン2 9Pボタン3 9Pボタン4 9Pボタン5 9Pボタン6 9Pボタン7 9Pボタン8 9Pボタン9 9P下 9P左 9P左レバー／下 9P左レバー／左 9P左レバー／右 9P左レバー／上 9Pペダル1 9Pペダル2 9Pペダル3 9P右 9P右レバー／下 9P右レバー／左 9P右レバー／右 9P右レバー／上 9Pセレクト 9Pスタート 9P上 サービス サービス1 サービス2 サービス3 サービス4 UIキャンセル UI下 UI左 UI右 UI選択 UI上 kHz 存在する CHDが必要 カテゴリ クローン カスタムフィルタ お気に入り 横画面 製造元 機械仕掛け CHDが不要 BIOS以外 機械仕掛け以外 動作不可 親セット セーブ対応 セーブ非対応 存在しない フィルタ無し 縦画面 動作可 年度 アートワークファイル アートワークのプレビュー ボス画像ファイル 筐体画像ファイル カテゴリ INI チートファイル 操作パネル画像 カバー 照準画像ファイル DATファイル フライヤー画像 HOWTO INIファイル アイコンファイル ロゴ マーキー画像 基板画像 ROMファイル スコア スクリーンショット ソフトウェア メディア タイトル画像 バーサス 再生中 録音中 アートワークのプレビュー ボス画像 筐体画像 操作パネル画像 カバー フライヤー画像 ゲームオーバー HOWTO ロゴ マーキー画像 基板画像 スコア スクリーンショット タイトル画像 バーサス 存在する クローン カスタムフィルタ 開発元 デバイスタイプ お気に入り 親セット 一部対応 発売元 リリース地域 ソフトウェアリスト 対応 存在しない フィルタ無し 非対応 年度 停止 開発元 発売日 バージョン 