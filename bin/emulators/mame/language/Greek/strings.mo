��    �     �-              <[     =[     \[  .   x[     �[  �   �[     p\  �   y\     3]     <]     D]     _]  #   v]  !   �]  #   �]  #   �]  #   ^  !   (^  "   J^  +   m^  +   �^     �^     �^  
   �^  	   	_     _     _  	   !_     +_     2_  	   :_     D_     L_  "   S_  	   v_  .   �_  +   �_     �_     �_     �_     `  
   `     %`     9`     L`     ^`     q`     �`     �`     �`     �`     �`     �`     �`     �`     �`      a  
   4a     ?a     Ma  #   Va  #   za     �a     �a     �a  	   �a     �a  3   �a     b     "b     =b     Ub     ib     {b  	   �b  	   �b     �b     �b     �b     �b     �b     �b     �b     c     c  
   >c     Ic     Zc  
   nc     yc  "   �c     �c     �c     �c     �c     �c  3   d     7d     Dd     Td     id  ^   md     �d  /   �d      e     -e  .   9e     he     me     }e     �e     �e     �e  	   �e     �e  /   �e     f     f     f     /f     >f     Vf     df     zf     �f     �f      �f     �f     �f     �f     �f  
   g     g     !g     7g  :   >g  &   yg     �g     �g     �g  "   �g     h  D   h     [h     ah     sh  
   �h     �h     �h  -   �h     �h     �h     i     i     .i     Ai     Ni     [i     ji     qi     yi     �i      �i  
   �i     �i     �i     j     j     *j     Ej     Yj     qj     xj     �j     �j     �j     �j     �j     k     k     4k     Ak     Hk     Uk     bk     nk     vk  B   �k     �k     �k  
   �k     �k     l     l     #l     7l  *   <l  $   gl     �l     �l     �l     �l     �l     �l     m     m     3m     Em     Zm     cm     km     wm  !   �m  
   �m     �m  $   �m     �m     �m     n  *   "n  )   Mn  >   wn     �n     �n     �n     o     o     o     6o     Co     Jo     Vo     ]o     do     ro  %   xo  
   �o     �o     �o     �o     �o     �o     �o     �o     p     p     +p     Bp     Xp     gp     �p     �p  	   �p     �p     �p  (   �p     �p     �p     �p     �p     �p     q     q     !q     Aq     Pq     `q     fq     vq     |q     �q     �q      �q     �q     �q     �q     �q     r     r     +r     ?r     Wr     lr     zr     �r     �r     �r     �r     �r     s     !s     1s      Es     fs     zs     �s     �s     �s     �s  $   �s     t     )t     2t     Mt  
   St     ^t     jt  	   st     }t     �t     �t     �t     �t     �t     �t     �t     u  	   &u     0u  !   7u     Yu     lu     zu     �u     �u     �u     �u     �u     �u     v     v     6v     Lv     Rv     cv     xv     �v     �v     �v     �v     �v     �v     �v     w     w     w     /w     2w      Nw  �   ow     =x     Nx     cx     wx     |x     �x     �x  '   �x  (   �x     �x  
   �x     y     y  	   y  Y    y  G   zy     �y     �y     �y     �y     z     z     3z     Rz     cz     xz     �z     �z     �z     �z     �z  
   �z  
   �z     �z  !   {  ,   *{  -   W{  $   �{     �{     �{  
   �{     �{     �{  !   �{     |     |     #|     @|     V|     l|     �|     �|     �|     �|     �|     �|  <   }     B}  2   ]}     �}     �}     �}     �}     �}     �}  ,   ~     0~     F~     [~  '   {~     �~  	   �~  
   �~     �~  
   �~     �~     �~     
        �   3  �   �     T�     i�     �     ��     ��     ��     �  	   �     �     �     &�     8�     P�     h�     w�     ~�     ��     ��     ��     ��     ��     ́  F   ؁     �  !   9�     [�     t�  
   y�     ��     ��  
   ��  $   ��     �     ��  
   �     �     �     :�  3   W�     ��     ��     ��     ��     ܃     �     �     �     3�     K�     `�     z�     ~�     ��     ��     ��     ��     ��     τ     �  !   �  "   %�  "   H�     k�     q�     ��     ��     ��     ��     ��     օ     �     �      �     &�     7�  	   C�     M�     b�     p�     ��  	   ��     ��     ��     ��     ��     Ԇ     �  	   ��     �     �     ,�     >�     L�     _�     n�  2   ��  0   ��     �     ��     �     �     #�     0�     5�     J�  �   `�  �   �  ,   f�  E   ��     ى  (   �  ]   �  .   z�  z   ��     $�     -�     G�     M�     _�  
   u�     ��     ��     ��     ��     ŋ     ��     ��     �     *�     2�  	   D�  '   N�  &   v�  8   ��     ֌     �     �     �     4�     L�  e   l�  Z   ҍ     -�     <�     B�     I�  
   X�     c�     q�     �     ��     ��     ��     Ȏ     Ύ     ڎ     ��     �     �     ��     ��  
    �     �     �     +�     7�     E�     Y�     m�     u�  �   ��  �   �     ��  
   ��          Ր     ސ     �     �     ��     �     #�  
   ;�  
   F�     Q�     a�     u�     ��     ��     ��     Ƒ     ڑ     �  '   �  (   -�     V�  (   t�     ��     ��  #   Ԓ  &   ��     �     ;�     U�     p�     ��     ��     ��     Ɠ     ٓ     �     ��     �     &�     9�     M�     `�     x�     ��     ��     ��     ��     Ք     �  "   �     '�      G�     h�     ��     ��     ��     Е     �     �     (�     E�     ]�     w�     ��  %   ��     ז     �     �     �     ,�     F�     a�     |�     ��     ��     ͗     �     �     �     2�     J�     b�     z�     ��     ��          ژ     �     �     !�     9�     Q�     i�     ��     ��     ��     ə     �     ��     �     (�     @�     X�     p�     ��     ��     ��     К     ߚ     �     	�     &�     8�     J�     ]�     p�     ��     ��     ��     ��     ˛     ݛ     �     �     �     *�     :�     J�     ]�     o�     ��     ��     ��     ��     ɜ     ۜ     �     ��     �     (�     <�     P�     d�     x�     ��     ��     ��     ĝ     ޝ     �     �     $�     =�     T�     d�     v�     ��     ��     ��     ��     О     �     ��     	�     �     8�     P�     h�     ��     ��     ��     ȟ     ��     ��     �     '�     ?�     W�     o�     ��     ��     ��     Ϡ     �     ��     �     $�     7�     M�     b�     w�     ��     ��     ��     ˡ     �     ��     �     �     3�     H�     ]�     r�     ��     ��     ��     Ƣ     �     ��     �     *�     B�     Z�     r�     ��     ��     ��     У     �     ��     �     ,�     C�     Z�     m�     ��     ��     ��     ٤     ��     �     *�     E�     _�     z�     ��     ��     ɥ     �     �     �     4�     N�     h�     ��     ��     ��     Ҧ     �     �     "�     :�     R�     j�     ��     ��     ��  !   ̧     �     �     �     6�     N�     h�     ��     ��     ��      Ҩ     �     	�     �     5�     I�     h�     ��     ��     ĩ     ٩     ��     ��     �     /�     H�     a�     z�     ��     ��     Ū     ݪ     ��     �     %�     =�     U�     m�     ��     ��     ��     ̫     �     �     (�     ?�     V�     m�     ��     ��      ¬     �     �     �     ,�     >�     U�     m�     ��     ��     ��     ͭ     �     ��     �     +�     B�     Y�     p�     ��     ��     ��     Ȯ     �     ��     �     4�     O�     j�     ��     ��     ��     կ     �     �     $�     C�     _�     w�     ��     ��     ð     ۰     ��     �     -�     E�     ]�     }�     ��     ��     ű     ݱ     ��     �  !   '�     I�     a�     y�     ��     ��     ò     ۲     ��     �      -�     N�     d�     z�     ��     ��     ó     �     �     �     4�     H�     Y�     p�     ��     ��     ��     д     �      �     �     /�     F�     ]�     t�     ��     ��     ��     е     �     ��     �     2�     Q�     m�     ��     ��     ��     ö     �     �     !�     >�     S�     g�     x�     ��     ��     ��     ׷     �     �     �     7�     N�     e�     |�     ��     ��     ��     ظ     �     �     �     3�     Q�     p�     ��     ��     ��     ι     �     �      �     @�     ]�     r�     ��     ��     ��     ƺ     ޺     ��     �     &�     >�     V�     m�     ��     ��     ��     ɻ     �     ��     �     !�     4�     R�     p�     ��     ��     ��     ׼     ��     �      �     ?�     _�     |�     ��     ��     ��     ͽ     �     ��     �     -�     E�     ]�     u�     ��     ��     ��     Ѿ     �     ��     �     -�     @�     S�     q�     ��     ��     ʿ     �     ��     �      �     ?�     ^�     ~�     ��     ��     ��     ��     ��     �     �     4�     L�     d�     |�     ��     ��     ��     ��     ��     �     �     5�     L�     _�     r�     ��     ��     ��     ��     ��     �     +�     ?�     ^�     }�     ��     ��     ��     ��     ��     �     #�     ;�     S�     k�     ��     ��     ��     ��     ��     ��     �     &�     =�     T�     k�     ~�     ��     ��     ��     ��     �     �     4�     J�     ^�     }�     ��     ��     ��     ��     �     �     *�     B�     Z�     r�     ��     ��     ��     ��     ��      �     �     .�     E�     \�     s�     ��     ��     ��     ��     ��     �     '�     =�     S�     i�     }�     ��     ��     ��     ��     �     !�     2�     D�     Y�     m�     ��     ��     ��     ��     ��     ��     ��     �     $�     :�     P�     f�     |�     ��     ��     ��     ��     ��     �     �     ,�     E�     ]�     u�     ��     ��     ��     ��     ��     �     �     8�     R�     l�     ��     ��     ��     ��     ��     �     �     1�     G�     ]�     v�     ��     ��     ��     ��     ��     �     �      ,�     M�     a�     z�     ��     ��     ��     ��     ��      �     �     -�     A�     Q�     c�     u�     ��     ��     ��     ��     ��     ��     �     �     .�     C�     X�     m�     ��     ��     ��     ��     ��     ��     ��     �     )�     >�      S�     t�  !   ��     ��     ��     ��     ��     �     2�     E�     W�     q�     ��     ��     ��     ��     ��     �     �     4�     M�     j�     ��     ��     ��     ��     ��     ��     �     �     0�  !   E�     g�     k�     ��     ��     ��     ��     ��     ��      �     9�     U�     o�     ��     ��     ��     ��     ��     �     4�     O�     i�     ��     ��  	   ��     ��     ��     ��     �     �     0�     H�     c�     v�     ��     ��     ��     ��     ��     ��     �     �     /�     D�     U�     m�     ��     ��     ��     ��     ��     ��     �     �     5�     Q�     d�  	   l�     v�     ��     ��     ��     ��     ��     �     '�     A�     W�     l�     ��     ��     ��     ��     ��     ��     �     +�     E�     \�     z�     ��     ��     ��     ��  #   ��     "�     =�     W�     v�     ��     ��     ��     ��     �     �     �     :�     M�     h�     ~�     ��  %   ��     ��     ��     ��     �     $�     ;�     T�     n�     ��     ��  w  ��  <   8�  <   u�  j   ��  =   �  b  [�     ��  �  ��     ��     ��  '   ��  !   ��  C   ��  G   .�  V   v�  ;   ��  M   	�  C   W�  :   ��  /   ��  /   �  &   6�  ?   ]�     ��     ��     ��     ��  	   ��     ��      �  	   �     �     �  =   !�  	   _�  :   i�  :   ��     ��     ��      �     �     2�      B�  (   c�  $   ��     ��  "   ��     ��     �     �     !�  &   -�     T�  =   Z�     ��  @   ��  E   ��  
   "�     -�     E�  Q   ]�  Q   ��     �  $   �  
   <�     G�     `�  S   q�     ��  -   ��  3   �  +   D�     p�  %   ��     ��     ��  .   ��  .   �     2�     7�     W�     o�      ��  %   ��  @   ��     �  *   3�  *   ^�     ��  9   ��  q   ��  /   U�  2   ��  !   ��     ��  1   ��  �   �  ,   ��  )   ��  /   ��     �  �   6�     �  `   7�  @   ��     ��  X   ��     L�  ,   ]�  6   ��  0   ��     ��     ��     �  8   $�  W   ]�     ��     ��     ��  A   ��  =   %�  .   c�  (   ��  *   ��  ,   ��     �  =   &�     d�     v�  +   ��     ��  *   ��     ��  )   ��     '�  |   6�  _   ��  T   �      h�  /   ��  <   ��     ��  �   �     ��     ��     ��     ��  3   ��  :   �  U   O�     ��  +   ��  +   ��     �     �     8�  !   T�  ,   v�     ��     ��  &   ��  7   ��  I   �     h�     }�  %   ��  &   ��     ��  E   �      N�  1   o�     ��  .   ��  3   ��  3     %   M  (   s  -   �  -   �  -   �     &    @    U    m    �    � 8   � �   � )   � 6   �    �      $   $    I +   d 
   � b   � T   �    S '   ` '   � 4   � +   � 9    )   K 5   u    � 0   �    �         ( 0   I @   z $   � "   � m       q    ~ -   � 7   � 6    Z   ; '   � 5   � W   �    L	    l	 L   y	 #   �	    �	    �	 '   	
 !   1
    S
    q
 H   �
    �
    �
    �
 -   �
 %   * !   P    r )   �    � +   � )   �         = -   R 
   �    �    �    � 6   � }           � %   �    �    � #   �    
 D    +   ^ 8   �    � 6   �     !   " 2   D #   w `   �    � D       [    h     u    � &   � 5   � 3    /   E    u     �    � ,   � 4   � &    .   > (   m 7   � D   � 7    I   K I   � C   � C   # 3   g K   � 3   �     5   /    e #   t #   �    � 	   �    �    � 5   � -    $   @ 3   e "   � 1   � :   � 	   )    3 Q   J     �    �    � '   � '       C %   X N   ~ ,   �     � /    !   K    m    | +   � 2   � )   �    " ;   B    ~ ,   �    � %   �    � +    !   F    h =   o M   � �  � &   � G   � "       @    M !   [    } F   � N   �    /    3 0   K    |     �   � �   \  (   �  
   ! -   ! @   @! &   �! -   �! @   �! &   " (   >" #   g"    �"    �" .   �"    �" )   �" !   #    A# 0   [# 7   �# F   �# G   $ @   S$ #   �$    �$ #   �$    �$ $   % N   '% #   v%    �% ,   �% >   �% *   & ?   B& +   �& W   �& A   ' _   H' )   �' ?   �' �   ( ;   �( v   �(    Q)     b) 1   �)    �) +   �) :   �) �   :*    �*    �* _   �* O   Z+ b   �+     , #   .,    R, !   a, $   �,    �, /   �, 4   �, O  -- .  }.    �/    �/    �/    0 4    0 4   U0    �0    �0 @   �0 G   �0 *   B1 ;   m1 ;   �1    �1     2 -   2    A2    N2 %   c2    �2 '   �2 )   �2 X   �2 6   T3 G   �3 .   �3    4    4 '   24 L   Z4 )   �4 \   �4 H   .5 
   w5    �5    �5 A   �5 ;   �5 �   !6    �6 (   �6 8   �6 8    7 $   Y7 A   ~7 ;   �7 *   �7 6   '8 9   ^8 2   �8    �8 '   �8    9    9 &   -9 .   T9 ,   �9 %   �9 3   �9 Y   
: h   d: T   �:    ";    B;    X;    d; !   z; 8   �; A   �; /   < A   G< 5   �<    �<    �<    �<    �< '   =    -=    G=    ^=    w= .   �=    �= #   �= /   �= *   > >   C>    �> +   �> 3   �> 3    ? "   4? *   W? "   �? 0   �? z   �? �   Q@ +   �@    �@ #   A    +A    EA    cA    pA +   �A   �A �   �B X   �C �   #D 4   �D Z   �D �   6E W   F   ]F    pG 8   �G    �G     �G 1   �G    /H )   OH    yH 
   �H .   �H B   �H K   I &   SI ,   zI    �I 2   �I    �I `   J \   yJ ~   �J ?   UK ,   �K 3   �K ;   �K +   2L V   ^L �   �L �   �M !   dN    �N    �N -   �N !   �N    �N !   O    0O     BO 9   cO 5   �O    �O '   �O    P    P 
   P    *P    DP 
   FP    QP    `P W   gP    �P    �P >   �P @   4Q    uQ    �Q 6  �Q   �R 2   �S    T    4T    TT    kT 
   �T    �T %   �T 2   �T 1   �T 
   ,U 
   7U !   BU    dU    mU    vU    �U 
   �U    �U    �U    �U ,   �U "   V    .V "   DV    gV    }V .   �V .   �V !   �V    W 8   /W $   hW 
   �W    �W    �W    �W    �W    �W 
   �W    �W 
   �W    X    X 
    X    +X    <X %   QX    wX    �X    �X    �X    �X    �X    �X    Y    'Y    4Y    CY !   \Y    ~Y    �Y    �Y    �Y    �Y    �Y    �Y 8   Z    FZ    OZ 4   bZ    �Z    �Z    �Z    �Z    [    /[    M[    k[    �[    �[    �[    �[    �[    �[    
\    \    2\    F\    Z\    n\    �\    �\    �\    �\    �\    �\    �\    ]    !]    5]    G]    \]    p]    �]    �]    �]    �]    �]    �]    �] 3   ^    E^    X^    w^    �^    �^    �^    �^    �^    �^    �^    �^    �^    �^    _    _ !   %_    G_    X_    q_    �_    �_    �_    �_    �_    `    /`    J`     e` #   �` "   �` "   �` "   �` "   a "   6a "   Ya "   |a "   �a 
   �a    �a    �a    b (   &b (   Ob    xb    �b    �b    �b    �b    �b    �b    �b    c    :c    Sc    hc    ~c    �c    �c    �c    �c    �c    d    'd    ?d    Wd    md    �d    �d    �d    �d    �d    �d    e    .e #   Fe    je !   }e    �e    �e    �e    �e    �e    �e    f    #f    6f    If    \f    mf    �f    �f    �f    �f    �f    �f    �f    g    g    1g    Eg    Zg    og    �g    �g    �g    �g    �g    �g     h    h    (h    <h    Ph    dh    xh    �h    �h    �h    �h    �h    �h    �h    �h    i    i    *i +   >i 3   ji -   �i -   �i    �i    j    j    4j    Lj    Yj    hj #   uj    �j    �j    �j    �j    �j    �j    �j    �j    	k    k .   %k    Tk    ak    nk    {k    �k    �k    �k    �k    �k    �k    �k    l    l    /l %   =l -   cl '   �l '   �l    �l    �l    m    m    &m    <m    Rm    hm    ~m    �m    �m    �m    �m    �m    �m    n    )n    >n    Sn    hn    un ,   �n 4   �n .   �n .   o    Jo    ]o    po    �o &   �o .   �o (   �o (   p    :p    Mp    ^p    mp    �p    �p    �p    �p    �p    �p    �p    q    (q    <q    Pq    dq    xq    �q    �q    �q    �q    �q    �q    �q     r    r     r    0r    @r    Sr    fr +   zr 3   �r -   �r -   s    6s    Cs    Ps    ps    �s    �s    �s #   �s    �s    �s    �s    t    t    t    +t    8t    Et    Tt .   at    �t    �t    �t    �t    �t    �t    �t    �t     u    u    3u    Eu    Wu    iu %   wu -   �u '   �u '   �u    v    -v    =v    Kv    _v    tv    �v    �v    �v    �v    �v    �v    w    w    .w    Bw    Vw    jw    ~w    �w    �w +   �w 3   �w -   x -   @x    nx    �x    �x    �x %   �x -   �x '   y '   .y    Vy    hy    xy    �y    �y    �y    �y    �y    �y    z    z    -z    Az    Uz    iz    }z    �z    �z    �z    �z    �z +   �z 3   { -   M{ -   {{    �{    �{    �{    �{ %   �{ -   | '   A| '   i|    �|    �|    �|    �|    �|    �|    �|    }    )}    >}    S}    h}    |}    �}    �}    �}    �}    �}    �}    ~    ~ +   (~ 3   T~ -   �~ -   �~    �~    �~         %   ( -   N '   | '   �    �    �    �    �    �    %�    :�    O�    d�    y�    ��    ��    ��    ˀ    ߀    �    �    �    /�    C�    O� +   c� 3   �� -   Á -   �    �    1�    C�    U� %   c� -   �� '   �� '   ߂    �    �    )�    7�    K�    `�    u�    ��    ��    ��    Ƀ    ރ    �    �    �    .�    B�    V�    j�    ~�    �� +   �� 3   ʄ -   �� -   ,�    Z�    l�    ~�    �� %   �� -   ą '   � '   �    B�    T�    d�    r�    ��    ��    ��    ņ    چ    �    �    �    -�    A�    U�    i�    }�    ��    ��    ��    Ň +   ه 3   � -   9� -   g�    ��    ��    ��    ˈ %   و -   �� '   -� '   U�    }�    ��    ��    ��    ��    ։    �     �    �    *�    ?�    T�    h�    |�    ��    ��    ��    ̊    ��    �     � +   � 3   @� -   t� -   ��    Ћ    �    �    � %   � -   :� '   h� '   ��    ��    ʌ    ڌ 
   �    �    �    �    �    (�    5�    B�    O�    \�    i�    |�    ��    ��    ��    э    �    ��    �    %� 
   :�    E�    b�    q�    ~�    ��    ��    ��    ��    ʎ    َ    �    ��    �    �    3�    J�    a�    x�    ��    ��    ��    ԏ    �    �    !�    8� )   O� $   y� -   �� )   ̐    ��    �    �    !�    0� D   ?�    ��    �� !   �� 
   ё 1   ܑ #   � #   2� #   V� #   z�    ��    �� 
   ֒    �    �    ��    �    �    ,�    ;�    M�    ^�    o�    ��    ��    ��    ��    ē    Փ    �    ��    �    �    )�    :�    K�    \�    m� +   ~� -   �� 9   ؔ    �    /�    A� &   Y�     ��    ��    ��    �� *   ڕ     � (   &�    O�    _�    s�    ��    �� (   Ė $   � (   �    ;�    I�    a�    s�    ��    ��    ��    ×     � 8   �    <�    @�    S�    X�    q�    �� '   ��    ��    ̘    �    �    � 	   6�    @�    V�    t� +   �� 0   ��    ޙ    ��    �    '�    >�    G�    ]� "   e�    ��    ��    ��        ՚    �    �    �    � +   -�    Y�    p�    |�    ��    ��    ��    �� #   ��    ߛ    �    ��    
�    �    0�    N�    h�    ��    ��    ��    ��    М #   ߜ    �    �    !�    ?�    P�    g� 	   x�    ��    ��    ��    ��    ��    ǝ    ֝    �    �    �    !�    4� '   A� %   i�    ��    ��    ��    ў )   ޞ    �    '� %   6�    \�    |�    ��    �� !   ȟ    �    � %   �    (� .   =� %   l�    ��    �� '   ��    Ҡ    ֠    ��    ��    � +   7� !   c� 6   ��    ��    ء  

--- DRIVER INFO ---
Driver:  

Press any key to continue 

There are working clones of this machine: %s 
    Configuration saved    

 
Elements of this machine cannot be emulated as they require physical interaction or consist of mechanical devices. It is not possible to fully experience this machine.
 
Sound:
 
THIS MACHINE DOESN'T WORK. The emulation for this machine is not yet complete. There is nothing you can do to fix this problem except wait for the developers to improve the emulation.
 
Video:
   %1$s
   %1$s    [default: %2$s]
   %1$s    [tag: %2$s]
   Adjuster inputs    [%1$d inputs]
   Analog inputs    [%1$d inputs]
   Gambling inputs    [%1$d inputs]
   Hanafuda inputs    [%1$d inputs]
   Keyboard inputs    [%1$d inputs]
   Keypad inputs    [%1$d inputs]
   Mahjong inputs    [%1$d inputs]
   Screen '%1$s': %2$d × %3$d (H) %4$s Hz
   Screen '%1$s': %2$d × %3$d (V) %4$s Hz
   Screen '%1$s': Vector
   User inputs    [%1$d inputs]
  (default)  (locked)  COLORS  PENS %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Driver: %4$s

CPU:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d machines (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d software packages ) %1$s (%2$s) - %3$s
 %1$s - %2$s
 %1$s Brightness %1$s Contrast %1$s Gamma %1$s Horiz Position %1$s Horiz Stretch %1$s Refresh Rate %1$s Vert Position %1$s Vert Stretch %1$s Volume %1$s [root%2$s] %1$s, %2$-.100s %1$s: %2$s
 %1$s: %2$s - Search: %3$s_ %2$s
 %d total matches found %s %s
 added to favorites list. %s
 removed from favorites list. %s [%d Hz] %s [internal] %s added %s.txt saved in UI settings folder. %s.xml saved in UI settings folder. (EP)ROM	Imperfect
 (EP)ROM	Unimplemented
 (empty) (playing) (recording) * BIOS settings:
  %1$d options    [default: %2$s]
 * CPU:
 * Configuration settings:
 * DIP switch settings:
 * Input device(s):
 * Media Options:
 * Slot Options:
 * Sound:
 * Video:
 **Error saving %s.ini** **Error saving ui.ini** , %s <set up filters> ARGB Settings About %s Activated: %s Activated: %s = %s Add %1$s Folder - Search: %2$s_ Add Folder Add To Favorites Add autofire button Add filter Add or remove favorite Adjust speed to match refresh rate Adstick Device Assignment Advanced Options Aligned only All All cheats reloaded All slots cleared and current state saved to Slot 1 Allow rewind Analog Controls Analog Controls	Yes
 Any Are you sure you want to quit?

Press ''%1$s'' to quit,
Press ''%2$s'' to return to emulation. Artwork Options Audit ROMs for %1$u machines marked unavailable Audit ROMs for all %1$u machines Audit media Auditing ROMs for machine %2$u of %3$u...
%1$s Auto Auto frame skip Auto rotate left Auto rotate right Autofire Autofire buttons Automatic Automatic save/restore Automatically toggle pause with on-screen menus BIOS BIOS Selection BIOS selection: Barcode Reader Barcode length invalid! Beam Dot Size Beam Intensity Weight Beam Width Maximum Beam Width Minimum Bilinear Filtering Bilinear filtering for snapshots Bitmap Prescaling Bold Bookkeeping Info Burn-in CPU or RAM Camera	Imperfect
 Camera	Unimplemented
 Cancel Cancel audit?

Press %1$s to cancel
Press %2$s to continue Cannot change options while recording! Cannot save over directory Capture	Imperfect
 Capture	Unimplemented
 Change %1$s Folder - Search: %2$s_ Change Folder Changes to this only take effect when "Start new search" is selected Cheat Cheat Comment:
%s Cheat Finder Cheat Name Cheat added to cheat.simple Cheat engine not available Cheat written to %s and added to cheat.simple Cheats Choose from palette Clear Watches Coin %1$c: %2$d%3$s
 Coin %1$c: NA%3$s
 Coin impulse Coin lockout Color preview: Colors Command Communications	Imperfect
 Communications	Unimplemented
 Completely unemulated features:  Compressor Configure Directories Configure Machine Configure Machine: Configure Options Confirm quit from machines Controls	Imperfect
 Controls	Unimplemented
 Create Crosshair Offset %1$s Crosshair Offset X %1$1.3f Crosshair Offset Y %1$1.3f Crosshair Options Crosshair Scale %1$s Crosshair Scale X %1$1.3f Crosshair Scale Y %1$1.3f Current %1$s Folders Current time Custom Customize UI DIP Switches Data Format Default Default name is %s Delete saved state %1$s?
Press %2$s to delete
Press %3$s to cancel Device Mapping Dial Device Assignment Difference Disabled Disabled: %s Disk	Imperfect
 Disk	Unimplemented
 Done Double-click or press %1$s to change color Double-click or press %1$s to select Driver Driver is BIOS	No
 Driver is BIOS	Yes
 Driver is Clone of	%1$s
 Driver is Parent	
 Driver is clone of: %1$-.100s Driver is parent Driver: "%1$s" software list  Driver: %1$-.100s Edit autofire button Emulated Enabled Enabled: %s Enforce Aspect Ratio Enlarge images in the right panel Enter Code Error accessing %s Error removing saved state file %1$s Exit Expand to fit Export displayed list to file Export list in TXT format (like -listfull) Export list in XML format (like -listxml) Export list in XML format (like -listxml, but exclude devices) External DAT View Failed to load autofire menu Failed to save input name file Fast Forward File File Already Exists - Override? File Manager Filter Filter %1$u Flip X Flip Y Folders Setup Fonts Force 4:3 aspect for snapshot display Frame skip GHz GLSL Gameinit General Info General Inputs Graphics	Imperfect
 Graphics	Imperfect Colors
 Graphics	OK
 Graphics	Unimplemented
 Graphics	Wrong Colors
 Graphics: Imperfect,  Graphics: OK,  Graphics: Unimplemented,  Group HLSL Hide Both Hide Filters Hide Info/Image Hide romless machine from available list High Scores History Hotkey Hz Image Format: Image Information Images Imperfectly emulated features:  Include clones Info auto audit Infos Infos text size Input Input (general) Input (this Machine) Input Options Input port name file saved to %s Input ports Invalid sequence entered Italic Joystick Joystick deadzone Joystick saturation Keyboard	Imperfect
 Keyboard	Unimplemented
 Keyboard Inputs	Yes
 Keyboard Mode LAN	Imperfect
 LAN	Unimplemented
 Language Laserdisc '%1$s' Horiz Position Laserdisc '%1$s' Horiz Stretch Laserdisc '%1$s' Vert Position Laserdisc '%1$s' Vert Stretch Last Slot Value Left equal to right Left equal to right with bitmask Left equal to value Left greater than right Left greater than value Left less than right Left less than value Left not equal to right Left not equal to right with bitmask Left not equal to value Lightgun Lightgun Device Assignment Lines Load State Low latency MAMEinfo MARPScore MESSinfo MHz Machine Configuration Machine Information Mag. Drum	Imperfect
 Mag. Drum	Unimplemented
 Mag. Tape	Imperfect
 Mag. Tape	Unimplemented
 Maintain Aspect Ratio Mamescore Manual Manually toggle pause when needed Manufacturer	%1$s
 Master Volume Match block Mechanical Machine	No
 Mechanical Machine	Yes
 Media	Imperfect
 Media	Unimplemented
 Memory state saved to Slot %d Menu Preview Microphone	Imperfect
 Microphone	Unimplemented
 Miscellaneous Options Mouse Mouse	Imperfect
 Mouse	Unimplemented
 Mouse Device Assignment Multi-keyboard Multi-mouse Mute when unthrottled NOT SET Name:             Description:
 Natural Natural keyboard Network Devices New Barcode: New Image Name: No No category INI files found No groups found in category file No machines found. Please check the rompath specified in the %1$s.ini file.

If this is your first time using %2$s, please see the config.txt file in the docs directory for information on configuring %2$s. No plugins found No save states found Non-Integer Scaling None None
 Not supported Number Of Screens Number of frames button will be pressed Number of frames button will be released Off Off frames Offscreen reload On On frames One or more ROMs/CHDs for this machine are incorrect. The machine may not run correctly.
 One or more ROMs/CHDs for this machine have not been correctly dumped.
 Other Controls Other:  Overall	NOT WORKING
 Overall	Unemulated Protection
 Overall	Working
 Overall: NOT WORKING Overall: Unemulated Protection Overall: Working Overclock %1$s sound Overclock CPU %1$s P%d Crosshair P%d Visibility Paddle Device Assignment Page Partially supported Pause Mode Pause/Stop Pedal Device Assignment Perform Compare  :  Slot %d %s %d Perform Compare  :  Slot %d %s Slot %d %s %d Perform Compare  :  Slot %d BITWISE%s Slot %d Perform Compare : Slot %d %s Slot %d Performance Options Play Play Count Player Player %1$d Controls Please enter a file extension too Plugin Options Plugins Positional Device Assignment Press %1$s to append
 Press %1$s to cancel
 Press %1$s to clear
 Press %1$s to delete Press %1$s to restore default
 Press %1$s to set
 Press %s to clear hotkey Press %s to delete Press TAB to set Press a key or joystick button, or select state to overwrite Press any key to continue. Press button for hotkey or wait to leave unchanged Pressed Printer	Imperfect
 Printer	Unimplemented
 Pseudo terminals Punch Tape	Imperfect
 Punch Tape	Unimplemented
 ROM Audit 	Disabled
Samples Audit 	Disabled
 ROM Audit Result	BAD
 ROM Audit Result	OK
 Re-select last machine launched Read this image, write to another image Read this image, write to diff Read-only Read-write Record Reload All Remove %1$s Folder Remove Folder Remove From Favorites Remove last filter Required ROM/disk images for the selected software are missing or incorrect. Please acquire the correct files or select a different software item.

 Required ROM/disk images for the selected system are missing or incorrect. Please acquire the correct files or select a different system.

 Requires Artwork	No
 Requires Artwork	Yes
 Requires CHD	No
 Requires CHD	Yes
 Requires Clickable Artwork	No
 Requires Clickable Artwork	Yes
 Reset Reset All Restore default colors Results will be saved to %1$s Return to Machine Return to Previous Menu Return to previous menu Revision: %1$s Rewind Rewind capacity Romset	%1$s
 Rotate Rotate left Rotate right Rotation Options Sample Rate Sample text - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Samples Audit Result	BAD
 Samples Audit Result	None Needed
 Samples Audit Result	OK
 Save Save Cheat Save Configuration Save Machine Configuration Save State Save current memory state to Slot %d Save input names to file Screen Screen #%d Screen '%1$s' Screen Orientation	Horizontal
 Screen Orientation	Vertical
 Screen flipping in cocktail mode is not supported.
 Search: %1$s_ Select New Machine Select access mode Select an input for autofire Select category: Select cheat to set hotkey Select custom filters: Select image format Select initial contents Select state to load Selection List - Search:  Set Set hotkeys Settings Show All Show DATs view Show mouse pointer Show side panels Simultaneous contradictory Skip BIOS selection menu Skip imperfect emulation warnings Skip information screen at startup Skip software parts selection menu Sleep Slider Controls Slot %d Slot 1 Value Slot Devices Software List Info Software is clone of: %1$-.100s Software is parent Software list/item: %1$s:%2$s Software part selection: Sound Sound	Imperfect
 Sound	None
 Sound	OK
 Sound	Unimplemented
 Sound Options Sound: Imperfect Sound: None Sound: OK Sound: Unimplemented Speed Start Machine Start Out Maximized Start new search State/Playback Options Steadykey Support Cocktail	No
 Support Save	No
 Support Save	Yes
 Supported: No Supported: Partial Supported: Yes Switch Item Ordering Switched Order: entries now ordered by description Switched Order: entries now ordered by shortname Synchronized Refresh Sysinfo System Names System: %1$-.100s Tape Control Test Test Cheat %08X_%02X Test/Write Poke Value The selected game is missing one or more required ROM or CHD images. Please select a different game.

Press any key to continue. The software selected is missing one or more required ROM or CHD images.
Please acquire the correct files or select a different one. There are known problems with this machine

 This driver requires images to be loaded in the following device(s):  This machine has no BIOS. This machine has no configurable inputs. This machine has no sound hardware, MAME will produce no sounds, this is expected behaviour.
 This machine requires external artwork files.
 This machine was never completed. It may exhibit strange behavior or missing elements that are not bugs in the emulation.
 Throttle Tickets dispensed: %1$d

 Timer Timing	Imperfect
 Timing	Unimplemented
 Total time Trackball Device Assignment Triple Buffering Type Type name is empty Type name or select: %1$s_ Type name or select: (random) UI Color Settings UI Customization Settings UI Font UI Fonts Settings UI active UI controls disabled
Use %1$s to toggle UI controls enabled
Use %1$s to toggle Unable to write file
Ensure that cheatpath folder exists Undo last search -- # Uptime: %1$d:%2$02d

 Uptime: %1$d:%2$02d:%3$02d

 Use External Samples Use image as background Use this if you want to poke %s Use this if you want to poke the Last Slot value (eg. You started without an item but finally got it) Use this if you want to poke the Slot 1 value (eg. You started with something but lost it) User Interface Value Vector Vector Flicker Video Mode Video Options Visible Delay WAN	Imperfect
 WAN	Unimplemented
 Wait Vertical Sync Warning Information Watch Window Mode Write X X Only X or Y (Auto) Y Y Only Year	%1$s
 Yes You can enter any type name Zoom = %1$d Zoom = 1/%1$d Zoom to Screen Area Zoom to screen area [None]
 [Start empty] [This option is NOT currently mounted in the running system]

Option: %1$s
Device: %2$s

If you select this option, the following items will be enabled:
 [This option is currently mounted in the running system]

Option: %1$s
Device: %2$s

The selected option enables the following items:
 [Use file manager] [built-in] [compatible lists] [create] [empty slot] [empty] [failed] [file manager] [no category INI files] [no groups in INI file] [root%1$s] [root%2$s] [software list] color-channelAlpha color-channelBlue color-channelGreen color-channelRed color-optionBackground color-optionBorder color-optionClone color-optionDIP switch color-optionGraphics viewer background color-optionMouse down background color color-optionMouse down color color-optionMouse over background color color-optionMouse over color color-optionNormal text color-optionNormal text background color-optionSelected background color color-optionSelected color color-optionSlider color color-optionSubitem color color-optionUnavailable color color-presetBlack color-presetBlue color-presetGray color-presetGreen color-presetOrange color-presetRed color-presetSilver color-presetViolet color-presetWhite color-presetYellow color-sampleClone color-sampleMouse Over color-sampleNormal color-sampleSelected color-sampleSubitem default emulation-featureLAN emulation-featureWAN emulation-featurecamera emulation-featurecapture hardware emulation-featurecolor palette emulation-featurecommunications emulation-featurecontrols emulation-featuredisk emulation-featuregraphics emulation-featurekeyboard emulation-featuremagnetic drum emulation-featuremagnetic tape emulation-featuremedia emulation-featuremicrophone emulation-featuremouse emulation-featureprinter emulation-featureprotection emulation-featurepunch tape emulation-featuresolid state storage emulation-featuresound emulation-featuretiming incorrect checksum incorrect length input-name1 Player Start input-name2 Players Start input-name3 Players Start input-name4 Players Start input-name5 Players Start input-name6 Players Start input-name7 Players Start input-name8 Players Start input-nameAD Stick X input-nameAD Stick X 10 input-nameAD Stick X 2 input-nameAD Stick X 3 input-nameAD Stick X 4 input-nameAD Stick X 5 input-nameAD Stick X 6 input-nameAD Stick X 7 input-nameAD Stick X 8 input-nameAD Stick X 9 input-nameAD Stick Y input-nameAD Stick Y 10 input-nameAD Stick Y 2 input-nameAD Stick Y 3 input-nameAD Stick Y 4 input-nameAD Stick Y 5 input-nameAD Stick Y 6 input-nameAD Stick Y 7 input-nameAD Stick Y 8 input-nameAD Stick Y 9 input-nameAD Stick Z input-nameAD Stick Z 10 input-nameAD Stick Z 2 input-nameAD Stick Z 3 input-nameAD Stick Z 4 input-nameAD Stick Z 5 input-nameAD Stick Z 6 input-nameAD Stick Z 7 input-nameAD Stick Z 8 input-nameAD Stick Z 9 input-nameBet input-nameBill 1 input-nameBook-Keeping input-nameBreak in Debugger input-nameCancel input-nameCoin 1 input-nameCoin 10 input-nameCoin 11 input-nameCoin 12 input-nameCoin 2 input-nameCoin 3 input-nameCoin 4 input-nameCoin 5 input-nameCoin 6 input-nameCoin 7 input-nameCoin 8 input-nameCoin 9 input-nameConfig Menu input-nameDeal input-nameDial input-nameDial 10 input-nameDial 2 input-nameDial 3 input-nameDial 4 input-nameDial 5 input-nameDial 6 input-nameDial 7 input-nameDial 8 input-nameDial 9 input-nameDial V input-nameDial V 10 input-nameDial V 2 input-nameDial V 3 input-nameDial V 4 input-nameDial V 5 input-nameDial V 6 input-nameDial V 7 input-nameDial V 8 input-nameDial V 9 input-nameDoor input-nameDoor Interlock input-nameDouble Up input-nameFast Forward input-nameFrameskip Dec input-nameFrameskip Inc input-nameHalf Gamble input-nameHigh input-nameHold 1 input-nameHold 2 input-nameHold 3 input-nameHold 4 input-nameHold 5 input-nameKey In input-nameKey Out input-nameKeyboard input-nameKeypad input-nameLightgun X input-nameLightgun X 10 input-nameLightgun X 2 input-nameLightgun X 3 input-nameLightgun X 4 input-nameLightgun X 5 input-nameLightgun X 6 input-nameLightgun X 7 input-nameLightgun X 8 input-nameLightgun X 9 input-nameLightgun Y input-nameLightgun Y 10 input-nameLightgun Y 2 input-nameLightgun Y 3 input-nameLightgun Y 4 input-nameLightgun Y 5 input-nameLightgun Y 6 input-nameLightgun Y 7 input-nameLightgun Y 8 input-nameLightgun Y 9 input-nameLoad State input-nameLow input-nameMemory Reset input-nameMouse X input-nameMouse X 10 input-nameMouse X 2 input-nameMouse X 3 input-nameMouse X 4 input-nameMouse X 5 input-nameMouse X 6 input-nameMouse X 7 input-nameMouse X 8 input-nameMouse X 9 input-nameMouse Y input-nameMouse Y 10 input-nameMouse Y 2 input-nameMouse Y 3 input-nameMouse Y 4 input-nameMouse Y 5 input-nameMouse Y 6 input-nameMouse Y 7 input-nameMouse Y 8 input-nameMouse Y 9 input-nameOn Screen Display input-nameP1 Button 1 input-nameP1 Button 10 input-nameP1 Button 11 input-nameP1 Button 12 input-nameP1 Button 13 input-nameP1 Button 14 input-nameP1 Button 15 input-nameP1 Button 16 input-nameP1 Button 2 input-nameP1 Button 3 input-nameP1 Button 4 input-nameP1 Button 5 input-nameP1 Button 6 input-nameP1 Button 7 input-nameP1 Button 8 input-nameP1 Button 9 input-nameP1 Down input-nameP1 Hanafuda A/1 input-nameP1 Hanafuda B/2 input-nameP1 Hanafuda C/3 input-nameP1 Hanafuda D/4 input-nameP1 Hanafuda E/5 input-nameP1 Hanafuda F/6 input-nameP1 Hanafuda G/7 input-nameP1 Hanafuda H/8 input-nameP1 Hanafuda No input-nameP1 Hanafuda Yes input-nameP1 Left input-nameP1 Left Stick/Down input-nameP1 Left Stick/Left input-nameP1 Left Stick/Right input-nameP1 Left Stick/Up input-nameP1 Mahjong A input-nameP1 Mahjong B input-nameP1 Mahjong Bet input-nameP1 Mahjong Big input-nameP1 Mahjong C input-nameP1 Mahjong Chi input-nameP1 Mahjong D input-nameP1 Mahjong Double Up input-nameP1 Mahjong E input-nameP1 Mahjong F input-nameP1 Mahjong Flip Flop input-nameP1 Mahjong G input-nameP1 Mahjong H input-nameP1 Mahjong I input-nameP1 Mahjong J input-nameP1 Mahjong K input-nameP1 Mahjong Kan input-nameP1 Mahjong L input-nameP1 Mahjong Last Chance input-nameP1 Mahjong M input-nameP1 Mahjong N input-nameP1 Mahjong O input-nameP1 Mahjong P input-nameP1 Mahjong Pon input-nameP1 Mahjong Q input-nameP1 Mahjong Reach input-nameP1 Mahjong Ron input-nameP1 Mahjong Small input-nameP1 Mahjong Take Score input-nameP1 Pedal 1 input-nameP1 Pedal 2 input-nameP1 Pedal 3 input-nameP1 Right input-nameP1 Right Stick/Down input-nameP1 Right Stick/Left input-nameP1 Right Stick/Right input-nameP1 Right Stick/Up input-nameP1 Select input-nameP1 Start input-nameP1 Up input-nameP10 Button 1 input-nameP10 Button 10 input-nameP10 Button 11 input-nameP10 Button 12 input-nameP10 Button 13 input-nameP10 Button 14 input-nameP10 Button 15 input-nameP10 Button 16 input-nameP10 Button 2 input-nameP10 Button 3 input-nameP10 Button 4 input-nameP10 Button 5 input-nameP10 Button 6 input-nameP10 Button 7 input-nameP10 Button 8 input-nameP10 Button 9 input-nameP10 Down input-nameP10 Left input-nameP10 Left Stick/Down input-nameP10 Left Stick/Left input-nameP10 Left Stick/Right input-nameP10 Left Stick/Up input-nameP10 Pedal 1 input-nameP10 Pedal 2 input-nameP10 Pedal 3 input-nameP10 Right input-nameP10 Right Stick/Down input-nameP10 Right Stick/Left input-nameP10 Right Stick/Right input-nameP10 Right Stick/Up input-nameP10 Select input-nameP10 Start input-nameP10 Up input-nameP2 Button 1 input-nameP2 Button 10 input-nameP2 Button 11 input-nameP2 Button 12 input-nameP2 Button 13 input-nameP2 Button 14 input-nameP2 Button 15 input-nameP2 Button 16 input-nameP2 Button 2 input-nameP2 Button 3 input-nameP2 Button 4 input-nameP2 Button 5 input-nameP2 Button 6 input-nameP2 Button 7 input-nameP2 Button 8 input-nameP2 Button 9 input-nameP2 Down input-nameP2 Hanafuda A/1 input-nameP2 Hanafuda B/2 input-nameP2 Hanafuda C/3 input-nameP2 Hanafuda D/4 input-nameP2 Hanafuda E/5 input-nameP2 Hanafuda F/6 input-nameP2 Hanafuda G/7 input-nameP2 Hanafuda H/8 input-nameP2 Hanafuda No input-nameP2 Hanafuda Yes input-nameP2 Left input-nameP2 Left Stick/Down input-nameP2 Left Stick/Left input-nameP2 Left Stick/Right input-nameP2 Left Stick/Up input-nameP2 Mahjong A input-nameP2 Mahjong B input-nameP2 Mahjong Bet input-nameP2 Mahjong Big input-nameP2 Mahjong C input-nameP2 Mahjong Chi input-nameP2 Mahjong D input-nameP2 Mahjong Double Up input-nameP2 Mahjong E input-nameP2 Mahjong F input-nameP2 Mahjong Flip Flop input-nameP2 Mahjong G input-nameP2 Mahjong H input-nameP2 Mahjong I input-nameP2 Mahjong J input-nameP2 Mahjong K input-nameP2 Mahjong Kan input-nameP2 Mahjong L input-nameP2 Mahjong Last Chance input-nameP2 Mahjong M input-nameP2 Mahjong N input-nameP2 Mahjong O input-nameP2 Mahjong P input-nameP2 Mahjong Pon input-nameP2 Mahjong Q input-nameP2 Mahjong Reach input-nameP2 Mahjong Ron input-nameP2 Mahjong Small input-nameP2 Mahjong Take Score input-nameP2 Pedal 1 input-nameP2 Pedal 2 input-nameP2 Pedal 3 input-nameP2 Right input-nameP2 Right Stick/Down input-nameP2 Right Stick/Left input-nameP2 Right Stick/Right input-nameP2 Right Stick/Up input-nameP2 Select input-nameP2 Start input-nameP2 Up input-nameP3 Button 1 input-nameP3 Button 10 input-nameP3 Button 11 input-nameP3 Button 12 input-nameP3 Button 13 input-nameP3 Button 14 input-nameP3 Button 15 input-nameP3 Button 16 input-nameP3 Button 2 input-nameP3 Button 3 input-nameP3 Button 4 input-nameP3 Button 5 input-nameP3 Button 6 input-nameP3 Button 7 input-nameP3 Button 8 input-nameP3 Button 9 input-nameP3 Down input-nameP3 Left input-nameP3 Left Stick/Down input-nameP3 Left Stick/Left input-nameP3 Left Stick/Right input-nameP3 Left Stick/Up input-nameP3 Pedal 1 input-nameP3 Pedal 2 input-nameP3 Pedal 3 input-nameP3 Right input-nameP3 Right Stick/Down input-nameP3 Right Stick/Left input-nameP3 Right Stick/Right input-nameP3 Right Stick/Up input-nameP3 Select input-nameP3 Start input-nameP3 Up input-nameP4 Button 1 input-nameP4 Button 10 input-nameP4 Button 11 input-nameP4 Button 12 input-nameP4 Button 13 input-nameP4 Button 14 input-nameP4 Button 15 input-nameP4 Button 16 input-nameP4 Button 2 input-nameP4 Button 3 input-nameP4 Button 4 input-nameP4 Button 5 input-nameP4 Button 6 input-nameP4 Button 7 input-nameP4 Button 8 input-nameP4 Button 9 input-nameP4 Down input-nameP4 Left input-nameP4 Left Stick/Down input-nameP4 Left Stick/Left input-nameP4 Left Stick/Right input-nameP4 Left Stick/Up input-nameP4 Pedal 1 input-nameP4 Pedal 2 input-nameP4 Pedal 3 input-nameP4 Right input-nameP4 Right Stick/Down input-nameP4 Right Stick/Left input-nameP4 Right Stick/Right input-nameP4 Right Stick/Up input-nameP4 Select input-nameP4 Start input-nameP4 Up input-nameP5 Button 1 input-nameP5 Button 10 input-nameP5 Button 11 input-nameP5 Button 12 input-nameP5 Button 13 input-nameP5 Button 14 input-nameP5 Button 15 input-nameP5 Button 16 input-nameP5 Button 2 input-nameP5 Button 3 input-nameP5 Button 4 input-nameP5 Button 5 input-nameP5 Button 6 input-nameP5 Button 7 input-nameP5 Button 8 input-nameP5 Button 9 input-nameP5 Down input-nameP5 Left input-nameP5 Left Stick/Down input-nameP5 Left Stick/Left input-nameP5 Left Stick/Right input-nameP5 Left Stick/Up input-nameP5 Pedal 1 input-nameP5 Pedal 2 input-nameP5 Pedal 3 input-nameP5 Right input-nameP5 Right Stick/Down input-nameP5 Right Stick/Left input-nameP5 Right Stick/Right input-nameP5 Right Stick/Up input-nameP5 Select input-nameP5 Start input-nameP5 Up input-nameP6 Button 1 input-nameP6 Button 10 input-nameP6 Button 11 input-nameP6 Button 12 input-nameP6 Button 13 input-nameP6 Button 14 input-nameP6 Button 15 input-nameP6 Button 16 input-nameP6 Button 2 input-nameP6 Button 3 input-nameP6 Button 4 input-nameP6 Button 5 input-nameP6 Button 6 input-nameP6 Button 7 input-nameP6 Button 8 input-nameP6 Button 9 input-nameP6 Down input-nameP6 Left input-nameP6 Left Stick/Down input-nameP6 Left Stick/Left input-nameP6 Left Stick/Right input-nameP6 Left Stick/Up input-nameP6 Pedal 1 input-nameP6 Pedal 2 input-nameP6 Pedal 3 input-nameP6 Right input-nameP6 Right Stick/Down input-nameP6 Right Stick/Left input-nameP6 Right Stick/Right input-nameP6 Right Stick/Up input-nameP6 Select input-nameP6 Start input-nameP6 Up input-nameP7 Button 1 input-nameP7 Button 10 input-nameP7 Button 11 input-nameP7 Button 12 input-nameP7 Button 13 input-nameP7 Button 14 input-nameP7 Button 15 input-nameP7 Button 16 input-nameP7 Button 2 input-nameP7 Button 3 input-nameP7 Button 4 input-nameP7 Button 5 input-nameP7 Button 6 input-nameP7 Button 7 input-nameP7 Button 8 input-nameP7 Button 9 input-nameP7 Down input-nameP7 Left input-nameP7 Left Stick/Down input-nameP7 Left Stick/Left input-nameP7 Left Stick/Right input-nameP7 Left Stick/Up input-nameP7 Pedal 1 input-nameP7 Pedal 2 input-nameP7 Pedal 3 input-nameP7 Right input-nameP7 Right Stick/Down input-nameP7 Right Stick/Left input-nameP7 Right Stick/Right input-nameP7 Right Stick/Up input-nameP7 Select input-nameP7 Start input-nameP7 Up input-nameP8 Button 1 input-nameP8 Button 10 input-nameP8 Button 11 input-nameP8 Button 12 input-nameP8 Button 13 input-nameP8 Button 14 input-nameP8 Button 15 input-nameP8 Button 16 input-nameP8 Button 2 input-nameP8 Button 3 input-nameP8 Button 4 input-nameP8 Button 5 input-nameP8 Button 6 input-nameP8 Button 7 input-nameP8 Button 8 input-nameP8 Button 9 input-nameP8 Down input-nameP8 Left input-nameP8 Left Stick/Down input-nameP8 Left Stick/Left input-nameP8 Left Stick/Right input-nameP8 Left Stick/Up input-nameP8 Pedal 1 input-nameP8 Pedal 2 input-nameP8 Pedal 3 input-nameP8 Right input-nameP8 Right Stick/Down input-nameP8 Right Stick/Left input-nameP8 Right Stick/Right input-nameP8 Right Stick/Up input-nameP8 Select input-nameP8 Start input-nameP8 Up input-nameP9 Button 1 input-nameP9 Button 10 input-nameP9 Button 11 input-nameP9 Button 12 input-nameP9 Button 13 input-nameP9 Button 14 input-nameP9 Button 15 input-nameP9 Button 16 input-nameP9 Button 2 input-nameP9 Button 3 input-nameP9 Button 4 input-nameP9 Button 5 input-nameP9 Button 6 input-nameP9 Button 7 input-nameP9 Button 8 input-nameP9 Button 9 input-nameP9 Down input-nameP9 Left input-nameP9 Left Stick/Down input-nameP9 Left Stick/Left input-nameP9 Left Stick/Right input-nameP9 Left Stick/Up input-nameP9 Pedal 1 input-nameP9 Pedal 2 input-nameP9 Pedal 3 input-nameP9 Right input-nameP9 Right Stick/Down input-nameP9 Right Stick/Left input-nameP9 Right Stick/Right input-nameP9 Right Stick/Up input-nameP9 Select input-nameP9 Start input-nameP9 Up input-namePaddle input-namePaddle 10 input-namePaddle 2 input-namePaddle 3 input-namePaddle 4 input-namePaddle 5 input-namePaddle 6 input-namePaddle 7 input-namePaddle 8 input-namePaddle 9 input-namePaddle V input-namePaddle V 10 input-namePaddle V 2 input-namePaddle V 3 input-namePaddle V 4 input-namePaddle V 5 input-namePaddle V 6 input-namePaddle V 7 input-namePaddle V 8 input-namePaddle V 9 input-namePause input-namePause - Single Step input-namePayout input-namePositional input-namePositional 10 input-namePositional 2 input-namePositional 3 input-namePositional 4 input-namePositional 5 input-namePositional 6 input-namePositional 7 input-namePositional 8 input-namePositional 9 input-namePositional V input-namePositional V 10 input-namePositional V 2 input-namePositional V 3 input-namePositional V 4 input-namePositional V 5 input-namePositional V 6 input-namePositional V 7 input-namePositional V 8 input-namePositional V 9 input-namePower Off input-namePower On input-nameRecord AVI input-nameRecord MNG input-nameReset Machine input-nameRewind - Single Step input-nameSave Snapshot input-nameSave State input-nameService input-nameService 1 input-nameService 2 input-nameService 3 input-nameService 4 input-nameShow Decoded Graphics input-nameShow FPS input-nameShow Profiler input-nameSoft Reset input-nameStand input-nameStop All Reels input-nameStop Reel 1 input-nameStop Reel 2 input-nameStop Reel 3 input-nameStop Reel 4 input-nameTake Score input-nameThrottle input-nameTilt input-nameTilt 1 input-nameTilt 2 input-nameTilt 3 input-nameTilt 4 input-nameToggle Cheat input-nameTrack X input-nameTrack X 10 input-nameTrack X 2 input-nameTrack X 3 input-nameTrack X 4 input-nameTrack X 5 input-nameTrack X 6 input-nameTrack X 7 input-nameTrack X 8 input-nameTrack X 9 input-nameTrack Y input-nameTrack Y 10 input-nameTrack Y 2 input-nameTrack Y 3 input-nameTrack Y 4 input-nameTrack Y 5 input-nameTrack Y 6 input-nameTrack Y 7 input-nameTrack Y 8 input-nameTrack Y 9 input-nameUI (First) Tape Start input-nameUI (First) Tape Stop input-nameUI Add/Remove favorite input-nameUI Audit Media input-nameUI Cancel input-nameUI Clear input-nameUI Default Zoom input-nameUI Display Comment input-nameUI Down input-nameUI End input-nameUI Export List input-nameUI External DAT View input-nameUI Focus Next input-nameUI Focus Previous input-nameUI Home input-nameUI Left input-nameUI Next Group input-nameUI Page Down input-nameUI Page Up input-nameUI Paste Text input-nameUI Previous Group input-nameUI Release Pointer input-nameUI Right input-nameUI Rotate input-nameUI Select input-nameUI Toggle input-nameUI Up input-nameUI Zoom In input-nameUI Zoom Out input-nameVolume Down input-nameVolume Up input-nameWrite current timecode kHz machine-filterAvailable machine-filterBIOS machine-filterCHD Required machine-filterCategory machine-filterClones machine-filterCustom Filter machine-filterFavorites machine-filterHorizontal Screen machine-filterManufacturer machine-filterMechanical machine-filterNo CHD Required machine-filterNot BIOS machine-filterNot Mechanical machine-filterNot Working machine-filterParents machine-filterSave Supported machine-filterSave Unsupported machine-filterUnavailable machine-filterUnfiltered machine-filterVertical Screen machine-filterWorking machine-filterYear not found path-optionArtwork path-optionArtwork Previews path-optionBosses path-optionCabinets path-optionCategory INIs path-optionCheat Files path-optionControl Panels path-optionCovers path-optionCrosshairs path-optionDATs path-optionFlyers path-optionGame Endings path-optionGame Over Screens path-optionHowTo path-optionINIs path-optionIcons path-optionLogos path-optionMarquees path-optionPCBs path-optionPlugin Data path-optionPlugins path-optionROMs path-optionScores path-optionSelect path-optionSnapshots path-optionSoftware Media path-optionSound Samples path-optionTitle Screens path-optionUI Settings path-optionUI Translations path-optionVersus playing recording selmenu-artworkArtwork Preview selmenu-artworkBosses selmenu-artworkCabinet selmenu-artworkControl Panel selmenu-artworkCovers selmenu-artworkEnding selmenu-artworkFlyer selmenu-artworkGame Over selmenu-artworkHowTo selmenu-artworkLogo selmenu-artworkMarquee selmenu-artworkPCB selmenu-artworkScores selmenu-artworkSelect selmenu-artworkSnapshots selmenu-artworkTitle Screen selmenu-artworkVersus software-filterAuthor software-filterAvailable software-filterClones software-filterCustom Filter software-filterDeveloper software-filterDevice Type software-filterDistributor software-filterFavorites software-filterParents software-filterPartially Supported software-filterProgrammer software-filterPublisher software-filterRelease Region software-filterSoftware List software-filterSupported software-filterUnavailable software-filterUnfiltered software-filterUnsupported software-filterYear stopped swlist-infoAlternate Title swlist-infoAuthor swlist-infoBarcode Number swlist-infoDeveloper swlist-infoDistributor swlist-infoISBN swlist-infoInstallation Instructions swlist-infoOEM swlist-infoOriginal Publisher swlist-infoPCB swlist-infoPart Number swlist-infoProgrammer swlist-infoRelease Date swlist-infoSerial Number swlist-infoSoftware list/item swlist-infoUsage Instructions swlist-infoVersion Project-Id-Version: MAME
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-17 07:08+1100
PO-Revision-Date: 2021-10-22 17:00+0300
Last-Translator: Mame.gr - BraiNKilleR
Language-Team: MAME Language Team
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Virtaal 0.7.1
 

--- ΠΛΗΡΟΦΟΡΙΕΣ ΟΔΗΓΟΥ ---
Οδηγός:  

Πιέστε ένα πλήκτρο για συνέχεια 

Υπάρχουν κλώνοι αυτού του μηχανήματος που λειτουργούν: %s 
    Οι ρυθμίσεις αποθηκεύτηκαν    

 
Στοιχεία αυτού του μηχανήματος δεν μπορούν να εξομοιωθούν, καθώς απαιτούν φυσική αλληλεπίδραση ή αποτελούνται από μηχανικές συσκευές. Δεν είναι δυνατόν να δοκιμάσετε πλήρως αυτό το μηχάνημα.
 
Ήχος:
 
ΑΥΤΟ ΤΟ ΜΗΧΑΝΗΜΑ ΔΕΝ ΛΕΙΤΟΥΡΓΕΙ. Η εξομοίωση για αυτό το μηχάνημα δεν είναι ακόμα πλήρης. Δεν υπάρχει τίποτα που μπορείτε να κάνετε για να διορθώσετε αυτό το πρόβλημα εκτός από την αναμονή για τους προγραμματιστές να βελτιώσουν την εξομοίωση.
 
Βίντεο:
   %1$s
   %1$s    [προεπιλογή: %2$s]
   %1$s    [ετικέτα: %2$s]
   Εισαγωγές ρυθμιστή    [%1$d εισαγωγές]
   Εισαγωγές αναλογικές    [%1$d εισαγωγές]
   Εισαγωγές τυχερών παιχνιδιών    [%1$d εισαγωγές]
   Εισαγωγές Hanafuda    [%1$d εισαγωγές]
   Εισαγωγές πληκτρολογίου    [%1$d εισαγωγές]
   Εισαγωγές πλήκτρων    [%1$d εισαγωγές]
   Εισαγωγές Mahjong    [%1$d εισαγωγές]
   Οθόνη '%1$s': %2$d × %3$d (H) %4$s Hz
   Οθόνη '%1$s': %2$d × %3$d (V) %4$s Hz
   Οθόνη '%1$s': Διάνυσμα
   Εισαγωγές χρήστη    [%1$d εισαγωγές]
  (προεπιλογή)  (κλειδωμένο)  ΧΡΩΜΑΤΑ  ΓΡΑΦΙΔΕΣ %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Οδηγός: %4$s

Επεξεργαστής:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d μηχανήματα (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d πακέτα λογισμικού) %1$s (%2$s) - %3$s
 %1$s - %2$s
 %1$s Φωτεινότητα %1$s Αντίθεση %1$s Γάμμα %1$s Οριζόντια Θέση %1$s Οριζόντια Επέκταση %1$s Ρυθμός Ανανέωσης %1$s Κάθετη Θέση %1$s Κάθετη Επέκταση %1$s Ένταση %1$s [root%2$s] %1$s, %2$-.100s %1$s: %2$s
 %1$s: %2$s - Αναζήτηση: %3$s_ %2$s
 Βρέθηκαν συνολικά %d αντιστοιχίες %s %s
 προστέθηκε στη λίστα αγαπημένων. %s
 αφαιρέθηκε από τη λίστα αγαπημένων. %s [%d Hz] %s [εσωτερικό] %s προστέθηκε %s.txt αποθηκεύτηκε στο φάκελο ρυθμίσεων του UI. %s.xml αποθηκεύτηκε στο φάκελο ρυθμίσεων του UI. (EP)ROM	Ατελής
 (EP)ROM	Μη υλοποιημένη
 (κενό) (αναπαραγωγή) (εγγραφή) * Ρυθμίσεις BIOS:
  %1$d επιλογές    [προεπιλογή: %2$s]
 * Επεξεργαστής:
 * Ρυθμίσεις διαμόρφωσης:
 * Ρυθμίσεις μικροδιακόπτων:
 * Συσκευή(ες) εισαγωγής:
 * Επιλογές Μέσων:
 * Επιλογές Υποδοχής:
 * Ήχος:
 * Βίντεο:
 **Σφάλμα αποθήκευσης %s.ini** **Σφάλμα αποθήκευσης ui.ini** , %s <ρύθμιση φίλτρων> Ρυθμίσεις ARGB Σχετικά με το %s Ενεργοποιήθηκε: %s Ενεργοποιήθηκε: %s = %s Προσθήκη Φακέλου %1$s - Αναζήτηση: %2$s_ Προσθήκη Φακέλου Προσθήκη Στα Αγαπημένα Προσθήκη πλήκτρου autofire Προσθήκη φίλτρου Προσθήκη ή αφαίρεση αγαπημένου Ρύθμιση την ταχύτητας ώστε να ταιριάζει με το ρυθμό ανανέωσης Ανάθεση Συσκευής AD μοχλού Επιλογές για προχωρημένους Ευθυγράμμιση μόνο Όλα Όλα τα Cheats επαναφορτώθηκαν Όλες οι θέσεις αδειάσαν και η τρέχουσα κατάσταση αποθηκεύτηκε στη θέση 1 Επιτρέπεται η επαναφορά Αναλογικά Χειριστήρια Αναλογικός Χειρισμός	Ναι
 Οποιαδήποτε Είστε βέβαιοι ότι θέλετε να εγκαταλείψετε;

Πατήστε ''%1$s'' για να εγκαταλείψετε,
Πατήστε ''%2$s'' για να επιστρέψετε στην εξομοίωση. Επιλογές Artwork Έλεγχος ROMs για %1$u μηχανήματα που δεν είναι διαθέσιμα Έλεγχος ROMs για όλα τα %1$u μηχανήματα Έλεγχος μέσων Γίνεται έλεγχος ROMs για το μηχάνημα %2$u από %3$u...
%1$s Αυτόματο Αυτόματη παράβλεψη καρέ Αυτόματη περιστροφή αριστερά Αυτόματη περιστροφή δεξιά Autofire Πλήκτρα autofire Αυτόματα Αυτόματη αποθήκευση/επαναφορά Αυτόματη εναλλαγή παύσης με τα μενού της οθόνης BIOS Επιλογή BIOS Επιλογή BIOS: Συσκευή Ανάγνωσης Γραμμωτού Κώδικα Μη έγκυρο μήκος γραμμωτού κώδικα! Μέγεθος Κουκίδας Ακτίνας Βάρος Έντασης Ακτίνας Μέγιστο Πλάτος Ακτίνας Ελάχιστο Πλάτος Ακτίνας Bilinear Filtering Bilinear φιλτράρισμα για στιγμιότυπα Bitmap Prescaling Έντονα Λογιστικές Πληροφορίες Burn-in Επεξεργαστής ή μνήμη RAM Κάμερα	Ατελής
 Κάμερα	Μη υλοποιημένη
 Ακύρωση Ακύρωση του ελέγχου?

Πατήστε %1$s για ακύρωση
Πατήστε %2$s για συνέχεια Δεν είναι δυνατή η αλλαγή επιλογών κατά την εγγραφή! Δεν μπορεί να αποθηκευτεί στο τρέχων κατάλογο Καταγραφή	Ατελής
 Καταγραφή	Μη υλοποιημένη
 Αλλαγή Φακέλου %1$s - Αναζήτηση: %2$s_ Αλλαγή Φακέλου Οι αλλαγές σε αυτό ισχύουν μόνο όταν η "Έναρξη νέας αναζήτησης" έχει επιλεγεί Cheat Σχόλιο Cheat:
%s Εύρεση Cheat Όνομα Cheat Το Cheat προστέθηκε στο cheat.simple Η μηχανή Cheat δεν είναι διαθέσιμη Το Cheat γράφτηκε στο %s και προστέθηκε στο cheat.simple Cheats Επιλέξτε από την παλέτα Καθαρισμός Επιτηρήσεων Κέρμα %1$c: %2$d%3$s
 Κέρμα %1$c: NA%3$s
 Ώθηση κερμάτων Κλειδωμα κερμάτων Προεπισκόπηση χρώματος: Χρώματα Εντολή Επικοινωνίες	Ατελής
 Επικοινωνίες	Μη υλοποιημένες
 Εντελώς μη εξομοιωμένα χαρακτηριστικά:  Συμπιεστής Ρύθμιση Φακέλων Ρύθμιση Μηχανήματος Ρύθμιση Μηχανήματος: Ρύθμιση Επιλογών Επιβεβαίωση εγκατάληψης από μηχάνημα Χειρισμός	Ατελής
 Χειρισμός	Μη υλοποιημένος
 Δημιουργία Μετατόπιση Στοχάστρου %1$s Μετατόπιση Στοχάστρου X %1$1.3f Μετατόπιση Στοχάστρου Y %1$1.3f Επιλογές Στόχαστρου Κλίμακα Στόχαστρου %1$s Κλίμακα Στόχαστρου X %1$1.3f Κλίμακα Στόχαστρου Y %1$1.3f Τρέχοντες Φάκελοι για %1$s Τρέχων χρόνος Προσαρμογή Προσαρμογή UI Μικροδιακόπτες Μορφή Δεδομένων Προεπιλογή Το προεπιλεγμένο όνομα είναι %s Διαγραφή αποθηκευμένης κατάστασης %1$s?
Πατήστε %2$s για διαγραφή
Πατήστε %3$s για ακύρωση Χαρτογράφηση Συσκευών Ανάθεση Συσκευής Περιστροφής Διαφορά Απενεργοποιημένο Απενεργοποιημένο: %s Δίσκος	Ατελής
 Δίσκος	Μη υλοποιημένος
 Έγινε Κάντε διπλό κλικ ή πατήστε %1$s για να αλλάξετε το χρώμα Κάντε διπλό κλικ ή πατήστε %1$s για να επιλέξετε Οδηγός Ο οδηγός είναι BIOS	Όχι
 Ο οδηγός είναι BIOS	Ναι
 Ο Οδηγός είναι Κλώνος του	%1$s
 Ο Οδηγός είναι Γονικός	
 Ο Οδηγός είναι κλώνος του: %1$-.100s Ο Οδηγός είναι γονικός Οδηγός: "%1$s" λίστα λογισμικού  Οδηγός: %1$-.100s Επεξεργασία πλήκτρου autofire Εξομοιωμένο Ενεργοποιημένο Ενεργοποιημένο: %s Επιβολή Αναλογίας Εικόνας Μεγέθυνση εικόνων στο δεξιό πίνακα Εισάγετε τον Κωδικό Σφάλμα πρόσβασης %s Πρόβλημα κατά την αφαίρεση της αποθηκευμένης κατάστασης %1$s Έξοδος Μέγιστη Επέκταση Εξαγωγή λίστας σε αρχείο Εξαγωγή σε μορφή TXT (όπως -listfull) Εξαγωγή σε μορφή XML (όπως -listxml) Εξαγωγή σε μορφή XML (όπως -listxml, χωρίς τις συσκευές) Προβολή Εξωτερικού DAT Αποτυχία φόρτωσης μενού autofire Σφάλμα αποθήκευσης ονόματος εισόδου στο αρχείο Γρήγορη Προώθηση Αρχείο Το Αρχείο Υπάρχει Ήδη - Να Αντικατασταθεί; Διαχείριση Αρχείων Φίλτρο Φίλτρο %1$u Οριζόντια περιστροφή Κάθετη περιστροφή Ρύθμιση Φακέλων Γραμματοσειρές Επιβολή 4:3 Εμφάνισης για το στιγμιότυπο Παράβλεψη καρέ GHz GLSL Αρχικοποίηση παιχνιδιού Γενικές Πληροφορίες Γενικός Χειρισμός Γραφικά	Ατελή
 Γραφικά	Ατελή Χρώματα
 Γραφικά	OK
 Γραφικά	Μη υλοποιημένα
 Γραφικά	Λάθος Χρώματα
 Γραφικά: Ατελή,  Γραφικά: OK,  Γραφικά: Μη υλοποιημένα,  Ομάδα HLSL Απόκρυψη Όλων Απόκρυψη Φίλτρων Απόκρυψη Πληροφορίων/Εικόνας Απόκρυψη μηχανημάτων που δεν χρειάζονται roms από τη λίστα διαθέσιμων Υψηλά σκορ Ιστορία Πλήκτρο συντόμευσης Hz Μορφή Εικόνας: Πληροφορίες Τίτλου Εικόνες Μερικώς εξομοιωμένα χαρακτηριστικά:  Περιλαμβάνονται κλώνοι Αυτόματος έλεγχος πληροφοριών Πληροφορίες Μέγεθος κειμένου πληροφοριών Εισαγωγή Χειρισμός (γενικά) Χειρισμός (τρέχων Μηχάνημα) Επιλογές Χειρισμού Το όνομα της θύρας εισόδου αποθηκεύτηκε στο αρχείο %s Θύρες εισόδου Έγινε εισαγωγή μη έγκυρης ακολουθίας Πλάγια Μοχλός Νεκρή ζώνη μοχλού Κορεσμός μοχλού Πληκτρολόγιο	Ατελές
 Πληκτρολόγιο	Μη υλοποιημένο
 Εισαγωγή Πληκτρολογίου	Ναι
 Λειτουργία Πληκτρολογίου LAN	Ατελές
 LAN	Μη υλοποιημένο
 Γλώσσα Laserdisc '%1$s' Οριζόντια Θέση Laserdisc '%1$s' Οριζόντια Επέκταση Laserdisc '%1$s' Κάθετη Θέση Laserdisc '%1$s' Κάθετη Επέκταση Τιμή τελευταίας θέσης Αριστερά είναι ίση με τη δεξιά Αριστερά είναι ίση με τη δεξιά με bitmask Αριστερά είναι ίση με την αξία Αριστερά είναι περισσότερη από τη δεξιά Αριστερά είναι περισσότερη από την αξία Αριστερά είναι λιγότερη από τη δεξιά Αριστερά είναι λιγότερη από την αξία Αριστερά όχι ίση με τη δεξιά Αριστερά δεν είναι ίση με τη δεξιά με bitmask Αριστερά όχι ίση με την αξία Όπλο φωτός Ανάθεση Συσκευής Όπλου φωτός Γραμμές Φόρτωση Κατάστασης Χαμηλή καθυστέρηση MAMEinfo MARPScore MESSinfo MHz Παραμετροποίηση Μηχανήματος Πληροφορίες Μηχανήματος Μαγ. Τύμπανο	Ατελές
 Μαγ. Τύμπανο	Μη υλοποιημένο
 Μαγ. Ταινία	Ατελής
 Μαγ. Ταινία	Μη υλοποιημένη
 Διατήρηση Αναλογίας Διαστάσεων Mamescore Χειροκίνητα Μη αυτόματη εναλλαγή παύσης όταν χρειάζεται Κατασκευαστής	%1$s
 Κύρια Ένταση Ταίριασμα τομέα Μηχανική Συσκευή	Όχι
 Μηχανική Συσκευή	Ναι
 Μέσα	Ατελή
 Μέσα	Μη υλοποιημένα
 Η Κατάσταση Μνήμης αποθηκεύτηκε στη θέση %d Προεπισκόπηση του μενού Μικρόφωνο	Ατελές
 Μικρόφωνο	Μη υλοποιημένο
 Διάφορες Επιλογές Ποντίκι Ποντίκι	Ατελές
 Ποντίκι	Μη υλοποιημένο
 Ανάθεση Συσκευής Ποντικιού Πολλαπλό-πληκτρολόγιο Πολλαπλό-ποντίκι Σίγαση οταν τρεχει σε πληρη ισχυ ΔΕΝ ΡΥΘΜΙΣΤΗΚΕ Όνομα:             Περιγραφή:
 Φυσικό Φυσικό πληκτρολόγιο Συσκευές Δικτύου Νέος Γραμμωτός Κώδικας: Νέο Όνομα Εικόνας: Όχι Δεν βρέθηκαν αρχεία INI κατηγοριών Δεν βρέθηκαν ομάδες στο αρχείο κατηγορίας Δεν βρέθηκαν μηχανήματα . Παρακαλώ ελέγξτε τη διαδρομή roms που καθορίζεται στο αρχείο %1$s.ini.

Αν αυτή είναι η πρώτη φορά που χρησιμοποιείτε %2$s., παρακαλούμε δείτε το αρχείο config.txt στον κατάλογο docs για πληροφορίες σχετικά με τη διαμόρφωση %2$s.. Δε βρέθηκαν πρόσθετα Δε βρέθηκαν αποθηκευμένες καταστάσεις Μη Ακέραια Κλίμακα Κανένα Κανένα
 Δεν υποστηρίζεται Αριθμός οθονών Αριθμός καρέ που θα πατηθεί το πλήκτρο Αριθμός καρέ που θα ελευθερωθεί το πλήκτρο Off Κλειστά καρέ Επαναφόρτωση εκτός οθόνης On Ανοικτά καρέ Μία ή περισσότερες ROMs/CHDs για αυτό το μηχάνημα είναι εσφαλμένες. Το μηχάνημα μπορεί να μην εκτελείται σωστά.
 Μία ή περισσότερες ROMs/CHDs για αυτό το μηχάνημα δεν έχουν γίνει dump σωστά.
 Άλλα Στοιχεία Ελέγχου Άλλο:  Συνολικά	ΔΕΝ ΛΕΙΤΟΥΡΓΕΙ
 Συνολικά	Μη Εξομοιωμένη Προστασία
 Συνολικά	Λειτουργεί
 Συνολικά: ΔΕΝ ΛΕΙΤΟΥΡΓΕΙ Συνολικά: Προστασία Μη Εξομοιωμένη Συνολικά: Λειτουργεί Υπερχρονισμός %1$s ήχου Υπερχρονισμός CPU %1$s Στόχαστρο P%d Ορατότητα P%d Ανάθεση Συσκευής Κουπιού Σελίδα Υποστηρίζεται μερικώς Λειτουργία Παύσης Παύση/Διακοπή Ανάθεση Συσκευής Πεταλιού Εκτέλεση σύγκρισης : Θέση %d %s %d Εκτέλεση σύγκρισης : Θέση %d %s Θέση %d %s %d Εκτέλεση σύγκρισης : Θέση %d BITWISE%s Θέση %d Εκτέλεση σύγκρισης : Θέση %d %s Θέση %d Επιλογές Επιδόσεων Αναπαραγωγή Αριθμός Παιχνιδιών Παίκτης Χειρισμός Παίκτη %1$d Παρακαλούμε εισάγετε μια επέκταση αρχείου Επιλογές Πρόσθετων Πρόσθετα Ανάθεση Συσκευής Θέσεως Πιέστε το πλήκτρο %1$s για προσθήκη
 Πατήστε %1$s για ακύρωση
 Πιέστε το πλήκτρο %1$s για καθαρισμό Πατήστε %1$s για διαγραφή Πιέστε το πλήκτρο %1$s για επαναφορά προεπιλογών
 Πιέστε το πλήκτρο %1$s για να ορίσετε
 Πατήστε το %s για καθαρισμό του πλήκτρου συντόμευσης Πατήστε %s για διαγραφή Πιέστε το πλήκτρο TAB για να ορίσετε Πατήστε ένα πλήκτρο ή κουμπί μοχλού, ή επιλέξτε κατάσταση για αντικατάσταση Πιέστε ένα πλήκτρο για συνέχεια. Πιέστε πλήκτρο για συντόμευση ή περιμένετε για να μείνει ως έχει Πατήθηκε Εκτυπωτής	Ατελής
 Εκτυπωτής	Μη υλοποιημένος
 Ψευδο τερματικά Διάτρητη Ταινία	Ατελής
 Διάτρητη Ταινία	Μη υλοποιημένη
 Έλεγχος ROMs 	Απενεργοποιημένος
Έλεγχος Δειγμάτων Ήχου 	Απενεργοποιημένος
 Έλεγχος ROMs	ΛΑΘΟΣ
 Έλεγχος ROMs	OK
 Επιλογή του τελευταίου μηχανήματος που εκτελέστηκε Ανάγνωση αυτής της εικόνας, εγγραφή σε άλλη Ανάγνωση αυτής της εικόνας, εγγραφή σε αρχείο αλλαγών Μόνο για Ανάγνωση Ανάγνωσης-εγγραφής Εγγραφή Επαναφόρτωση όλων Αφαίρεση Φακέλου %1$s Αφαίρεση Φακέλου Αφαίρεση Από Τα Αγαπημένα Αφαίρεση τελευταίου φίλτρου Οι απαιτούμενες εικόνες ROM / δίσκου για το επιλεγμένο λογισμικό λείπουν ή δεν είναι σωστές.
Παρακαλώ προμηθευτείτε τα σωστά αρχεία ή επιλέξτε ένα διαφορετικό αντικείμενο λογισμικού.

 Οι απαιτούμενες εικόνες ROM / δίσκου για το επιλεγμένο σύστημα λείπουν ή δεν είναι σωστές.
Παρακαλώ προμηθευτείτε τα σωστά αρχεία ή επιλέξτε ένα διαφορετικό σύστημα.

 Απαιτεί Artwork	Όχι
 Απαιτεί Artwork	Ναι
 Απαιτεί CHD	Όχι
 Απαιτεί CHD	Ναι
 Απαιτεί Artwork για επιλογή	Όχι
 Απαιτεί Artwork για επιλογή	Ναι
 Επαναφορά Επαναφορά Όλων Επαναφορά προεπιλεγμένων χρωμάτων Τα αποτελέσματα θα αποθηκευτούν στο %1$s Επιστροφή στο Μηχάνημα Επιστροφή στο Προηγούμενο Μενού Επιστροφή στο προηγούμενο μενού Αναθεώρηση: %1$s Επαναφορά Χωρητικότητα επαναφοράς Romset	%1$s
 Περιστροφή Περιστροφή αριστερά Περιστροφή δεξιά Επιλογές περιστροφής Ρυθμός Δειγματοληψίας Δείγμα κειμένου - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Έλεγχος Δειγμάτων Ήχου	ΛΑΘΟΣ
 Έλεγχος Δειγμάτων Ήχου	Δεν απαιτείται
 Έλεγχος Δειγμάτων Ήχου	OK
 Αποθήκευση Αποθήκευση Cheat Αποθήκευση Ρυθμίσεων Αποθήκευση Παραμετροποίησης Μηχανήματος Αποθήκευση Κατάστασης Αποθήκευση τρέχουσας κατάστασης μνήμης στη Θέση %d Αποθήκευση ονομάτων εισόδου στο αρχείο Οθόνη Οθόνη #%d Οθόνη '%1$s' Προσανατολισμός Οθόνης	Οριζόντιος
 Προσανατολισμός Οθόνης	Κάθετος
 Το αναποδογύρισμα της οθόνης σε κοκτέιλ λειτουργία δεν υποστηρίζεται.
 Αναζήτηση: %1$s_ Επιλέξτε Νέο Μηχάνημα Επιλέξτε λειτουργία πρόσβασης Επιλέξτε μια είσαγωγή για autofire Επιλογή κατηγορίας: Επιλογή cheat για ρύθμιση συντόμευσης Επιλογή προσαρμοσμένων φίλτρων: Επιλέξτε μορφή εικόνας Επιλογή αρχικού περιεχομένου Επιλογή κατάστασης για φόρτωση Λίστα Επιλογής - Αναζήτηση:  Ρύθμιση Ρύθμιση συντομεύσεων Ρυθμίσεις Προβολή Όλων Εμφάνιση προβολής DATs Προβολή δείκτη ποντικιού Προβολή πλευρικών πάνελ Ταυτόχρονη αναίρεση Παράλειψη μενού επιλογής BIOS Παράλειψη προειδοποιησεων ημιτελούς εξομοίωσης Να μην εμφανίζεται η οθόνη πληροφοριών κατά την εκκίνηση Παράλειψη μενού επιλογής τμημάτων λογισμικού Λειτουργία ύπνου Έλεγχος Slider Θέση %d Τιμή θέσης 1 Συσκευές Υποδοχής Πληροφορίες Λίστας λογισμικού Το λογισμικό είναι κλώνος του: %1$-.100s Το λογισμικό είναι γονικό Λίστα λογισμικού/αντικείμενο: %1$s:%2$s Επιλογή τμημάτων λογισμικού: Ήχος Ήχος	Ατελής
 Ήχος	Κανένας
 Ήχος	OK
 Ήχος	Μη υλοποιημένος
 Επιλογές Ήχου Ήχος: Ατελής Ήχος: Κανένας Ήχος: OK Ήχος: Δεν Έχει Υλοποιηθεί Ταχύτητα Έναρξη Μηχανήματος Εκκίνηση μεγιστοποιημένο Έναρξη νέας αναζήτησης Επιλογές Κατάστασης/Αναπαραγωγής Σταθερά πλήκτρα Υποστήριξη Κοκτέιλ	Όχι
 Υποστήριξη Αποθήκευσης	Όχι
 Υποστήριξη Αποθήκευσης	Ναι
 Υποστηρίζεται: Όχι Υποστηρίζεται: Μερικώς Υποστηρίζεται: Ναι Εναλλαγή Σειράς Στοιχείων Εναλλακτική Σειρά: οι εγγραφές είναι ταξινομημένες κατά περιγραφή Εναλλακτική Σειρά: οι εγγραφές είναι ταξινομημένες κατά σύντομο όνομα Συγχρονισμένη Ανανέωση Sysinfo Ονόματα συστήματος Σύστημα: %1$-.100s Έλεγχος Κασέτας Δοκιμή Δοκιμή Cheat %08X_%02X Δοκιμή/Εγγραφή Αξίας Poke Στο επιλεγμένο παιχνίδι λείπει ένα ή περισσότερα απαιτούμενα αρχεία ROM ή CHD. Παρακαλώ επιλέξτε ένα διαφορετικό παιχνίδι.

Πιέστε ένα πλήκτρο για συνέχεια. Στο επιλεγμένο λογισμικό λείπει μία ή περισσότερες ROMs ή εικόνες CHD.
Παρακαλώ προμηθευτείτε τα σωστά αρχεία ή επιλέξτε ένα διαφορετικό. Υπάρχουν γνωστά προβλήματα με αυτό το μηχάνημα

 Αυτός ο οδηγός απαιτεί εικόνες να φορτωθούν στην ακόλουθη συσκευή (ες):  Αυτό το μηχάνημα δεν έχει BIOS. Αυτό το μηχάνημα δεν έχει είσαγωγές προς ρύθμιση. Αυτό το μηχάνημα δεν έχει κανένα υλικό ήχου, το MAME δεν θα παράγει κανένα ήχο, αυτή είναι αναμενόμενη συμπεριφορά.
 Αυτό το μηχάνημα απαιτεί εξωτερικά αρχεία artwork.
 Αυτό το μηχάνημα δεν ολοκληρώθηκε ποτέ. Αυτό μπορεί να εμφανίσει ασυνήθιστη συμπεριφορά ή στοιχεία να λείπουν που δεν είναι σφάλματα στην εξομοίωση.
 Πλήρης ισχύς Εισιτήρια που διανέμονται: %1$d

 Χρονομετρητής Χρονισμός	Ατελής
 Χρονισμός	Μη υλοποιημένος
 Συνολικός χρόνος Ανάθεση Συσκευής Trackball Triple Buffering Τύπος Το όνομα τύπου είναι κενό Πληκτρολογήστε όνομα ή επιλέξτε: %1$s_ Πληκτρολογήστε όνομα ή επιλέξτε: (τυχαίο) Ρυθμίσεις χρωμάτων UI Ρυθμίσεις Προσαρμογής UI Γραμματοσειρά UI Ρυθμίσεις γραμματοσειρών UI UI ενεργοποιημένο Χειρισμός UI απενεργοποιημένος
Χρήση %1$s για εναλλαγή Χειρισμός UI ενεργοποιημένος
Χρήση %1$s για εναλλαγή Αδύνατη η εγγραφή του αρχείου
Ελέγξτε ότι υπάρχει ο φάκελος του cheatpath Αναίρεση τελευταίας αναζήτησης -- # Ώρα λειτουργίας: %1$d:%2$02d

 Ώρα λειτουργίας: %1$d:%2$02d:%3$02d

 Χρήση Εξωτερικών Δειγμάτων Ήχου Χρήση εικόνας ως φόντου Χρησιμοποιήστε αυτό αν θέλετε να κάνετε poke το %s Χρησιμοποιήστε αυτό αν θέλετε να κάνετε poke την αξία της τελευταίας θέσης (πχ. Ξεκίνησες χωρίς κάποιο αντικείμενο αλλά τελικά το βρήκες) Χρησιμοποιήστε αυτό αν θέλετε να κάνετε poke την αξία της θέσης 1 (πχ. Ξεκίνησες με κάτι αλλά το έχασες) Περιβάλλον Χρήστη Αξία Διάνυσμα Τρεμόπαιγμα Διανύσματος Λειτουργία βίντεο Επιλογές Βίντεο Ορατή Καθυστέρηση WAN	Ατελές
 WAN	Μη υλοποιημένο
 Αναμονή για Κάθετο Συγχρονισμό Πληροφορίες Προειδοποιήσεων Επιτήρηση Λειτουργία παραθύρου Εγγραφή X Μόνο X X ή Y (Αυτόματο) Y Μόνο Y Έτος	%1$s
 Ναι Μπορείτε να εισαγάγετε οποιοδήποτε τύπου όνομα Μεγένθυση = %1$d Μεγένθυση = 1/%1$d Μεγέθυνση στην Περιοχή της Οθόνης Μεγέθυνση στην περιοχής της οθόνης [Καμία]
 [Έναρξη άδειο] [Αυτή η επιλογή ΔΕΝ είναι αυτή τη στιγμή τοποθετημένη στο τρέχον σύστημα]

Επιλογή: %1$s
Συσκευή: %2$s

Αν επιλέξετε αυτήν την επιλογή, θα ενεργοποιηθούν τα ακόλουθα στοιχεία:
 [Αυτή η επιλογή είναι αυτή τη στιγμή τοποθετημένη στο τρέχον σύστημα]

Επιλογή: %1$s
Συσκευή: %2$s

Η επιλεγμένη επιλογή ενεργοποιεί τα ακόλουθα στοιχεία:
 [Χρήση διαχείρησης αρχείων] [ενσωματομένο] [συμβατές λίστες] [δημιουργία] [κενή υποδοχή] [κενό] [απέτυχε] [Διαχείριση αρχείων] [χωρίς αρχεία INI κατηγοριών] [χωρίς ομάδες στο αρχείο INI] [root%1$s] [root%2$s] [λίστα λογισμικού] Άλφα Μπλε Πράσινο Κόκκινο Φόντο Περίγραμμα Κλώνος Μικροδιακόπτης Φόντο προβολής γραφικών Χρώμα φόντου Mouse down Χρώμα Mouse down Χρώμα φόντου Mouse over Χρώμα Mouse over Κανονικό κείμενο Φόντο κανονικού κειμένου Χρώμα φόντου επιλεγμένου Χρώμα επιλεγμένου Χρώμα Slider Χρώμα δευτερεύοντος στοιχείου Χρώμα μη διαθέσιμου Μαύρο Μπλε Γκρίζο Πράσινο Πορτοκαλί Κόκκινο Ασημί Βιολετί Λευκό Κίτρινο Κλώνος Mouse Over Κανονικό Επιλεγμένο Δευτερεύον στοιχείο προεπιλογή Δίκτυο LAN Δίκτυο WAN κάμερα υλικό καταγραφής χρωματική παλέτα επικοινωνίες χειρισμός δίσκος γραφικά πληκτρολόγιο μαγνητικό τύμπανο μαγνητική ταινία μέσα μικρόφωνο ποντίκι εκτυπωτής προστασία διάτρητη ταινία αποθήκευση στερεάς κατάστασης ήχος χρονισμός λανθασμένο άθροισμα ελέγχου λανθασμένο μήκος 1 Παίκτης Έναρξη 2 Παίκτες Έναρξη 3 Παίκτες Έναρξη 4 Παίκτες Έναρξη 5 Παίκτες Έναρξη 6 Παίκτες Έναρξη 7 Παίκτες Έναρξη 8 Παίκτες Έναρξη AD Μοχλός X AD Μοχλός X 10 AD Μοχλός X 2 AD Μοχλός X 3 AD Μοχλός X 4 AD Μοχλός X 5 AD Μοχλός X 6 AD Μοχλός X 7 AD Μοχλός X 8 AD Μοχλός X 9 AD Μοχλός Y AD Μοχλός Y 10 AD Μοχλός Y 2 AD Μοχλός Y 3 AD Μοχλός Y 4 AD Μοχλός Y 5 AD Μοχλός Y 6 AD Μοχλός Y 7 AD Μοχλός Y 8 AD Μοχλός Y 9 AD Μοχλός Z AD Μοχλός Z 10 AD Μοχλός Z 2 AD Μοχλός Z 3 AD Μοχλός Z 4 AD Μοχλός Z 5 AD Μοχλός Z 6 AD Μοχλός Z 7 AD Μοχλός Z 8 AD Μοχλός Z 9 Ποντάρισμα Αποδέκτης Χαρτονομισμάτων 1 Λογιστικα Διακοπή στο Debugger Ακύρωση Κέρμα 1 Κέρμα 10 Κέρμα 11 Κέρμα 12 Κέρμα 2 Κέρμα 3 Κέρμα 4 Κέρμα 5 Κέρμα 6 Κέρμα 7 Κέρμα 8 Κέρμα 9 Μενού διαμόρφωσης Μοιρασιά Περιστροφικό Περιστροφικό 10 Περιστροφικό 2 Περιστροφικό 3 Περιστροφικό 4 Περιστροφικό 5 Περιστροφικό 6 Περιστροφικό 7 Περιστροφικό 8 Περιστροφικό 9 Περιστροφικό Κάθ. Περιστροφικό Κάθ. 10 Περιστροφικό Κάθ. 2 Περιστροφικό Κάθ. 3 Περιστροφικό Κάθ. 4 Περιστροφικό Κάθ. 5 Περιστροφικό Κάθ. 6 Περιστροφικό Κάθ. 7 Περιστροφικό Κάθ. 8 Περιστροφικό Κάθ. 9 Πόρτα Κλειδαριά πόρτας Διπλασιασμός Γρήγορη Προώθηση Μείωση Παραληψης καρέ Αύξηση Παραληψης καρέ Μισό στοίχημα Μεγαλύτερο Κράτηση 1 Κράτηση 2 Κράτηση 3 Κράτηση 4 Κράτηση 5 Πίστωση Κλειδιού Χρέωση Κλειδιού Πληκτρολόγιο Pad πλήκτρων Όπλο φωτός X Όπλο φωτός X 10 Όπλο φωτός X 2 Όπλο φωτός X 3 Όπλο φωτός X 4 Όπλο φωτός X 5 Όπλο φωτός X 6 Όπλο φωτός X 7 Όπλο φωτός X 8 Όπλο φωτός X 9 Όπλο φωτός Y Όπλο φωτός Y 10 Όπλο φωτός Y 2 Όπλο φωτός Y 3 Όπλο φωτός Y 4 Όπλο φωτός Y 5 Όπλο φωτός Y 6 Όπλο φωτός Y 7 Όπλο φωτός Y 8 Όπλο φωτός Y 9 Φόρτωση Κατάστασης Μικρότερο Μηδενισμός Μνήμης Ποντίκι X Ποντίκι X 10 Ποντίκι X 2 Ποντίκι X 3 Ποντίκι X 4 Ποντίκι X 5 Ποντίκι X 6 Ποντίκι X 7 Ποντίκι X 8 Ποντίκι X 9 Ποντίκι Y Ποντίκι Y 10 Ποντίκι Y 2 Ποντίκι Y 3 Ποντίκι Y 4 Ποντίκι Y 5 Ποντίκι Y 6 Ποντίκι Y 7 Ποντίκι Y 8 Ποντίκι Y 9 Μενού οθόνης P1 Πλήκτρο 1 P1 Πλήκτρο 10 P1 Πλήκτρο 11 P1 Πλήκτρο 12 P1 Πλήκτρο 13 P1 Πλήκτρο 14 P1 Πλήκτρο 15 P1 Πλήκτρο 16 P1 Πλήκτρο 2 P1 Πλήκτρο 3 P1 Πλήκτρο 4 P1 Πλήκτρο 5 P1 Πλήκτρο 6 P1 Πλήκτρο 7 P1 Πλήκτρο 8 P1 Πλήκτρο 9 P1 Κάτω P1 Hanafuda A/1 P1 Hanafuda B/2 P1 Hanafuda C/3 P1 Hanafuda D/4 P1 Hanafuda E/5 P1 Hanafuda F/6 P1 Hanafuda G/7 P1 Hanafuda H/8 P1 Hanafuda Όχι P1 Hanafuda Ναι P1 Αριστερά P1 Αριστερός μοχλός/Κάτω P1 Αριστερός μοχλός/Αριστερά P1 Αριστερός μοχλός/Δεξιά P1 Αριστερός μοχλός/Επάνω P1 Mahjong A P1 Mahjong B P1 Mahjong Ποντάρισμα P1 Mahjong Μεγάλο P1 Mahjong C P1 Mahjong Chi P1 Mahjong D P1 Mahjong Διπλασιασμός P1 Mahjong E P1 Mahjong F P1 Mahjong Flip Flop P1 Mahjong G P1 Mahjong H P1 Mahjong I P1 Mahjong J P1 Mahjong K P1 Mahjong Kan P1 Mahjong L P1 Mahjong Τελευταία Ευκαιρία P1 Mahjong M P1 Mahjong N P1 Mahjong O P1 Mahjong P P1 Mahjong Pon P1 Mahjong Q P1 Mahjong Reach P1 Mahjong Ron P1 Mahjong Μικρό P1 Mahjong Πάρτε σκορ P1 Πετάλι 1 P1 Πετάλι 2 P1 Πετάλι 3 P1 Δεξιά P1 Δεξιός μοχλός/Κάτω P1 Δεξιός μοχλός/Αριστερά P1 Δεξιός μοχλός/Δεξιά P1 Δεξιός μοχλός/Επάνω P1 Επιλογή P1 Έναρξη P1 Επάνω P10 Πλήκτρο 1 P10 Πλήκτρο 10 P10 Πλήκτρο 11 P10 Πλήκτρο 12 P10 Πλήκτρο 13 P10 Πλήκτρο 14 P10 Πλήκτρο 15 P10 Πλήκτρο 16 P10 Πλήκτρο 2 P10 Πλήκτρο 3 P10 Πλήκτρο 4 P10 Πλήκτρο 5 P10 Πλήκτρο 6 P10 Πλήκτρο 7 P10 Πλήκτρο 8 P10 Πλήκτρο 9 P10 Κάτω P10 Αριστερά P10 Αριστερός μοχλός/Κάτω P10 Αριστερός μοχλός/Αριστερά P10 Αριστερός μοχλός/Δεξιά P10 Αριστερός μοχλός/Επάνω P10 Πετάλι 1 P10 Πετάλι 2 P10 Πετάλι 3 P10 Δεξιά P10 Δεξιός μοχλός/Κάτω P10 Δεξιός μοχλός/Αριστερά P10 Δεξιός μοχλός/Δεξιά P10 Δεξιός μοχλός/Επάνω P10 Επιλογή P10 Έναρξη P10 Επάνω P2 Πλήκτρο 1 P2 Πλήκτρο 10 P2 Πλήκτρο 11 P2 Πλήκτρο 12 P2 Πλήκτρο 13 P2 Πλήκτρο 14 P2 Πλήκτρο 15 P2 Πλήκτρο 16 P2 Πλήκτρο 2 P2 Πλήκτρο 3 P2 Πλήκτρο 4 P2 Πλήκτρο 5 P2 Πλήκτρο 6 P2 Πλήκτρο 7 P2 Πλήκτρο 8 P2 Πλήκτρο 9 P2 Κάτω P2 Hanafuda A/1 P2 Hanafuda B/2 P2 Hanafuda C/3 P2 Hanafuda D/4 P2 Hanafuda E/5 P2 Hanafuda F/6 P2 Hanafuda G/7 P2 Hanafuda H/8 P2 Hanafuda Όχι P2 Hanafuda Ναι P2 Αριστερά P2 Αριστερός μοχλός/Κάτω P2 Αριστερός μοχλός/Αριστερά P2 Αριστερός μοχλός/Δεξιά P2 Αριστερός μοχλός/Επάνω P2 Mahjong A P2 Mahjong B P2 Mahjong Ποντάρισμα P2 Mahjong Μεγάλο P2 Mahjong C P2 Mahjong Chi P2 Mahjong D P2 Mahjong Διπλασιασμός P2 Mahjong E P2 Mahjong F P2 Mahjong Flip Flop P2 Mahjong G P2 Mahjong H P2 Mahjong I P2 Mahjong J P2 Mahjong K P2 Mahjong Kan P2 Mahjong L P2 Mahjong Τελευταία Ευκαιρία P2 Mahjong M P2 Mahjong N P2 Mahjong O P2 Mahjong P P2 Mahjong Pon P2 Mahjong Q P2 Mahjong Reach P2 Mahjong Ron P2 Mahjong Μικρό P2 Mahjong Πάρε σκορ P2 Πετάλι 1 P2 Πετάλι 2 P2 Πετάλι 3 P2 Δεξιά P2 Δεξιός μοχλός/Κάτω P2 Δεξιός μοχλός/Αριστερά P2 Δεξιός μοχλός/Δεξιά P2 Δεξιός μοχλός/Επάνω P2 Επιλογή P2 Έναρξη P2 Επάνω P3 Πλήκτρο 1 P3 Πλήκτρο 10 P3 Πλήκτρο 11 P3 Πλήκτρο 12 P3 Πλήκτρο 13 P3 Πλήκτρο 14 P3 Πλήκτρο 15 P3 Πλήκτρο 16 P3 Πλήκτρο 2 P3 Πλήκτρο 3 P3 Πλήκτρο 4 P3 Πλήκτρο 5 P3 Πλήκτρο 6 P3 Πλήκτρο 7 P3 Πλήκτρο 8 P3 Πλήκτρο 9 P3 Κάτω P3 Αριστερά P3 Αριστερός μοχλός/Κάτω P3 Αριστερός μοχλός/Αριστερά P3 Αριστερός μοχλός/Δεξιά P3 Αριστερός μοχλός/Επάνω P3 Πετάλι 1 P3 Πετάλι 2 P3 Πετάλι 3 P3 Δεξιά P3 Δεξιός μοχλός/Κάτω P3 Δεξιός μοχλός/Αριστερά P3 Δεξιός μοχλός/Δεξιά P3 Δεξιός μοχλός/Επάνω P3 Επιλογή P3 Έναρξη P3 Επάνω P4 Πλήκτρο 1 P4 Πλήκτρο 10 P4 Πλήκτρο 11 P4 Πλήκτρο 12 P4 Πλήκτρο 13 P4 Πλήκτρο 14 P4 Πλήκτρο 15 P4 Πλήκτρο 16 P4 Πλήκτρο 2 P4 Πλήκτρο 3 P4 Πλήκτρο 4 P4 Πλήκτρο 5 P4 Πλήκτρο 6 P4 Πλήκτρο 7 P4 Πλήκτρο 8 P4 Πλήκτρο 9 P4 Κάτω P4 Αριστερά P4 Αριστερός μοχλός/Κάτω P4 Αριστερός μοχλός/Αριστερά P4 Αριστερός μοχλός/Δεξιά P4 Αριστερός μοχλός/Επάνω P4 Πετάλι 1 P4 Πετάλι 2 P4 Πετάλι 3 P4 Δεξιά P4 Δεξιός μοχλός/Κάτω P4 Δεξιός μοχλός/Αριστερά P4 Δεξιός μοχλός/Δεξιά P4 Δεξιός μοχλός/Επάνω P4 Επιλογή P4 Έναρξη P4 Επάνω P5 Πλήκτρο 1 P5 Πλήκτρο 10 P5 Πλήκτρο 11 P5 Πλήκτρο 12 P5 Πλήκτρο 13 P5 Πλήκτρο 14 P5 Πλήκτρο 15 P5 Πλήκτρο 16 P5 Πλήκτρο 2 P5 Πλήκτρο 3 P5 Πλήκτρο 4 P5 Πλήκτρο 5 P5 Πλήκτρο 6 P5 Πλήκτρο 7 P5 Πλήκτρο 8 P5 Πλήκτρο 9 P5 Κάτω P5 Αριστερά P5 Αριστερός μοχλός/Κάτω P5 Αριστερός μοχλός/Αριστερά P5 Αριστερός μοχλός/Δεξιά P5 Αριστερός μοχλός/Επάνω P5 Πετάλι 1 P5 Πετάλι 2 P5 Πετάλι 3 P5 Δεξιά P5 Δεξιός μοχλός/Κάτω P5 Δεξιός μοχλός/Αριστερά P5 Δεξιός μοχλός/Δεξιά P5 Δεξιός μοχλός/Επάνω P5 Επιλογή P5 Έναρξη P5 Επάνω P6 Πλήκτρο 1 P6 Πλήκτρο 10 P6 Πλήκτρο 11 P6 Πλήκτρο 12 P6 Πλήκτρο 13 P6 Πλήκτρο 14 P6 Πλήκτρο 15 P6 Πλήκτρο 16 P6 Πλήκτρο 2 P6 Πλήκτρο 3 P6 Πλήκτρο 4 P6 Πλήκτρο 5 P6 Πλήκτρο 6 P6 Πλήκτρο 7 P6 Πλήκτρο 8 P6 Πλήκτρο 9 P6 Κάτω P6 Αριστερά P6 Αριστερός μοχλός/Κάτω P6 Αριστερός μοχλός/Αριστερά P6 Αριστερός μοχλός/Δεξιά P6 Αριστερός μοχλός/Επάνω P6 Πετάλι 1 P6 Πετάλι 2 P6 Πετάλι 3 P6 Δεξιά P6 Δεξιός μοχλός/Κάτω P6 Δεξιός μοχλός/Αριστερά P6 Δεξιός μοχλός/Δεξιά P6 Δεξιός μοχλός/Επάνω P6 Επιλογή P6 Έναρξη P6 Επάνω P7 Πλήκτρο 1 P7 Πλήκτρο 10 P7 Πλήκτρο 11 P7 Πλήκτρο 12 P7 Πλήκτρο 13 P7 Πλήκτρο 14 P7 Πλήκτρο 15 P7 Πλήκτρο 16 P7 Πλήκτρο 2 P7 Πλήκτρο 3 P7 Πλήκτρο 4 P7 Πλήκτρο 5 P7 Πλήκτρο 6 P7 Πλήκτρο 7 P7 Πλήκτρο 8 P7 Πλήκτρο 9 P7 Κάτω P7 Αριστερά P7 Αριστερός μοχλός/Κάτω P7 Αριστερός μοχλός/Αριστερά P7 Αριστερός μοχλός/Δεξιά P7 Αριστερός μοχλός/Επάνω P7 Πετάλι 1 P7 Πετάλι 2 P7 Πετάλι 3 P7 Δεξιά P7 Δεξιός μοχλός/Κάτω P7 Δεξιός μοχλός/Αριστερά P7 Δεξιός μοχλός/Δεξιά P7 Δεξιός μοχλός/Επάνω P7 Επιλογή P7 Έναρξη P7 Επάνω P8 Πλήκτρο 1 P8 Πλήκτρο 10 P8 Πλήκτρο 11 P8 Πλήκτρο 12 P8 Πλήκτρο 13 P8 Πλήκτρο 14 P8 Πλήκτρο 15 P8 Πλήκτρο 16 P8 Πλήκτρο 2 P8 Πλήκτρο 3 P8 Πλήκτρο 4 P8 Πλήκτρο 5 P8 Πλήκτρο 6 P8 Πλήκτρο 7 P8 Πλήκτρο 8 P8 Πλήκτρο 9 P8 Κάτω P8 Αριστερά P8 Αριστερός μοχλός/Κάτω P8 Αριστερός μοχλός/Αριστερά P8 Αριστερός μοχλός/Δεξιά P8 Αριστερός μοχλός/Επάνω P8 Πετάλι 1 P8 Πετάλι 2 P8 Πετάλι 3 P8 Δεξιά P8 Δεξιός μοχλός/Κάτω P8 Δεξιός μοχλός/Αριστερά P8 Δεξιός μοχλός/Δεξιά P8 Δεξιός μοχλός/Επάνω P8 Επιλογή P8 Έναρξη P8 Επάνω P9 Πλήκτρο 1 P9 Πλήκτρο 10 P9 Πλήκτρο 11 P9 Πλήκτρο 12 P9 Πλήκτρο 13 P9 Πλήκτρο 14 P9 Πλήκτρο 15 P9 Πλήκτρο 16 P9 Πλήκτρο 2 P9 Πλήκτρο 3 P9 Πλήκτρο 4 P9 Πλήκτρο 5 P9 Πλήκτρο 6 P9 Πλήκτρο 7 P9 Πλήκτρο 8 P9 Πλήκτρο 9 P9 Κάτω P9 Αριστερά P9 Αριστερός μοχλός/Κάτω P9 Αριστερός μοχλός/Αριστερά P9 Αριστερός μοχλός/Δεξιά P9 Αριστερός μοχλός/Επάνω P9 Πετάλι 1 P9 Πετάλι 2 P9 Πετάλι 3 P9 Δεξιά P9 Δεξιός μοχλός/Κάτω P9 Δεξιός μοχλός/Αριστερά P9 Δεξιός μοχλός/Δεξιά P9 Δεξιός μοχλός/Επάνω P9 Επιλογή P9 Έναρξη P9 Επάνω Κουπί Κουπί 10 Κουπί 2 Κουπί 3 Κουπί 4 Κουπί 5 Κουπί 6 Κουπί 7 Κουπί 8 Κουπί 9 Κουπί Κάθ. Κουπί Κάθ. 10 Κουπί Κάθ. 2 Κουπί Κάθ. 3 Κουπί Κάθ. 4 Κουπί Κάθ. 5 Κουπί Κάθ. 6 Κουπί Κάθ. 7 Κουπί Κάθ. 8 Κουπί Κάθ. 9 Παύση Παύση - Ένα Βήμα Πληρωμή Θέσεως Θέσεως 10 Θέσεως 2 Θέσεως 3 Θέσεως 4 Θέσεως 5 Θέσεως 6 Θέσεως 7 Θέσεως 8 Θέσεως 9 Θέσεως Κάθ. Θέσεως Κάθ. 10 Θέσεως Κάθ. 2 Θέσεως Κάθ. 3 Θέσεως Κάθ. 4 Θέσεως Κάθ. 5 Θέσεως Κάθ. 6 Θέσεως Κάθ. 7 Θέσεως Κάθ. 8 Θέσεως Κάθ. 9 Απενεργοποίηση Ενεργοποίηση Καταγραφή AVI Καταγραφή MNG Επαναφορά Μηχανήματος Επιστροφή - Ένα Βήμα Αποθήκευση Στιγμιότυπου Αποθήκευση Κατάστασης Σέρβις Σέρβις 1 Σέρβις 2 Σέρβις 3 Σέρβις 4 Προβολή Αποκωδικοποιημένων Γραφικών Προβολή FPS Προβολή Profiler Ελαφριά επαναφορά Στάση Διακοπή Όλων των Κυλίνδρων Διακοπή Κυλίνδρου 1 Διακοπή Κυλίνδρου 2 Διακοπή Κυλίνδρου 3 Διακοπή Κυλίνδρου 4 Πάρτε Βαθμολογία Πλήρης ισχύς Κλίση Κλίση 1 Κλίση 2 Κλίση 3 Κλίση 4 Εναλλαγή Cheat Τροχιά X Τροχιά X 10 Τροχιά X 2 Τροχιά X 3 Τροχιά X 4 Τροχιά X 5 Τροχιά X 6 Τροχιά X 7 Τροχιά X 8 Τροχιά X 9 Τροχιά Y Τροχιά Y 10 Τροχιά Y 2 Τροχιά Y 3 Τροχιά Y 4 Τροχιά Y 5 Τροχιά Y 6 Τροχιά Y 7 Τροχιά Y 8 Τροχιά Y 9 UI (Πρώτο) Έναρξη Κασέτας UI (Πρώτο) Διακοπή Κασέτας UI Προσθήκη/Αφαίρεση αγαπημένου UI Έλεγχος Μέσων UI Ακύρωση UI Καθαρισμός UI Προεπιλεγμένο ζουμ UI Προβολή σχολίου UI Κάτω UI Τέλος UI Εξαγωγή Λίστας UI Προβολή εξωτερικού DAT UI Εστίαση Επόμενο UI Εστίαση Προηγούμενο UI Αρχική UI Αριστερά UI Επόμενη Ομάδα UI Σελίδα Κάτω UI Σελίδα Επάνω UI Επικόλληση κειμένου UI Προηγούμενη Ομάδα UI Απελευθέρωση δείκτη UI Δεξιά UI Περιστροφή UI Επιλογή UI Εναλλαγή UI Επάνω UI Μεγένθυση UI Σιμικρυνση Ένταση Ήχου Κάτω Ένταση Ήχου Επάνω Εγγραφή τρέχοντος χρονοκώδικα kHz Διαθέσιμα BIOS Απαιτείται CHD Κατηγορία Κλώνοι Προσαρμοσμένο Φίλτρο Αγαπημένα Οριζόντια οθόνη Κατασκευαστής Μηχανικά Δεν Απαιτείται CHD Μη BIOS Μη Μηχανικά Δεν Λειτουργούν Γονικά Υποστήριξη Αποθήκευσης Μη Υποστήριξη Αποθήκευσης Μη Διαθέσιμα Αφιλτράριστα Κάθετη οθόνη Λειτουργούν Έτος δεν βρέθηκε Artwork Προεπισκόπηση Artwork Αρχηγοί Καμπίνες INIs Κατηγοριών Αρχεία Cheat Πίνακες ελέγχου Εξώφυλλα Στόχαστρα DATs Φυλλάδια Τερματισμοί Παιχνιδιών Οθονες Game Over Πως να INIs Εικονίδια Λογότυπα Μαρκίζες PCBs Δεδομένα Πρόσθετων Πρόσθετα ROMs Βαθμολογία Επιλογή Στιγμιότυπα Μέσα Λογισμικού Δείγματα ήχου Οθόνες Τίτλου Ρυθμίσεις UI Μεταφράσεις UI Versus αναπαραγωγή εγγραφή Προεπισκόπηση Artworks Αρχηγοί Καμπίνα Πίνακας Ελέγχου Εξώφυλλα Τερματισμός Φυλλάδιο Game Over Πως να Λογότυπο Μαρκίζα PCB Βαθμολογία Επιλογή Στιγμιότυπα Οθόνη Τίτλου Versus Συγγραφέας Διαθέσιμα Κλώνοι Προσαρμοσμένο Φίλτρο Υπεύθυνος ανάπτυξης Τύπος συσκευής Διανομέας Αγαπημένα Γονικά Υποστηρίζεται Μερικώς Προγραμματιστής Εκδότης Περιοχή Κυκλοφορίας Λίστα Λογισμικού Υποστηρίζεται Μη Διαθέσιμα Αφιλτράριστα Δεν Υποστηρίζεται Έτος διακοπή Εναλλακτικός Τίτλος Συγγραφέας Αριθμός Γραμμωτου Κώδικα Υπεύθυνος ανάπτυξης Διανομέας ISBN Οδηγίες Εγκατάστασης OEM Αρχικός Εκδότης PCB Αριθμός παρτίδας Προγραμματιστής Ημερομηνία κυκλοφορίας Σειριακός Αριθμός Λίστα λογισμικού/αντικείμενο Οδηγίες Χρήσης Έκδοση 