��    �     �              �     �  .   �     �        �         �   
   �   	   �      �      �   	   �       !     !  	   !     !     !!  "   (!  	   K!  .   U!  +   �!     �!     �!  
   �!     �!     �!      "     "     %"     7"     C"     S"     _"     z"     �"     �"      �"     �"  	   �"     �"     �"     �"     #     #     +#     9#  
   Y#     d#  
   u#     �#     �#     �#     �#     �#  ^   �#     C$  .   S$     �$     �$     �$     �$     �$     �$     �$     �$     �$     %     %     2%     E%     X%     k%     }%     �%     �%     �%     �%     �%  "   �%     &     &     &     '&     .&     B&     W&     j&     w&     �&     �&     �&      �&     �&     �&     �&     �&     '     ,'     D'     K'     a'     |'     �'     �'     �'     �'     �'     (     (     !(     0(     G(     P(     `(  $   t(     �(     �(     �(     �(     �(     �(     )     ")     *)  !   ?)  
   a)     l)     q)  *   �)  )   �)  >   �)     #*     5*     B*     G*     g*     t*     {*     �*     �*     �*     �*  %   �*  
   �*     �*     �*     �*     �*     +     +     3+     @+     X+     o+     �+     �+     �+     �+  	   �+     �+     �+  (   �+     	,     ,     ,     +,     =,     D,     d,     s,     �,     �,     �,     �,     �,     �,     �,     �,     �,     -     -     .-     C-     Q-     `-     s-     |-     �-     �-     �-     �-     .     .  
   ".     -.  	   6.     @.     I.     M.     c.  	   w.     �.     �.     �.     �.     �.     �.     �.     �.     /     !/     9/     H/     T/     t/     |/     �/     �/     �/     �/     �/      �/  �   �/     �0     �0     �0     �0     �0     �0     1  Y   1  G   e1     �1     �1     �1     �1     2     2     52     F2     [2     n2     �2  
   �2     �2     �2     �2  !   �2     �2     3     3     -3     >3     Y3     l3     �3  '   �3     �3  	   �3  
   �3     �3  
   �3     4     4     #4     94     L4  	   R4     \4     z4     �4     �4     �4     �4     �4     �4     �4     �4     �4  F   5     K5  
   ^5     i5  
   p5     {5     �5     �5  3   �5     �5     6     6     -6     >6     U6     i6     �6     �6     �6     �6     �6     �6     �6  "   �6  "   7     B7     H7     X7     e7     �7     �7     �7     �7     �7  	   �7     �7     �7     8     8  	   8     (8     =8     C8     W8  	   n8     x8     �8     �8     �8  2   �8  0   �8     !9     69     >9     P9  �   ]9  ,   �9  E   :     Q:  ]   k:  .   �:  z   �:     s;     |;     �;     �;     �;     �;     �;     <     $<     6<     ><  	   P<     Z<     p<     �<     �<     �<     �<     �<  
   �<     �<     �<     =     =     (=     ;=     G=     I=  
   K=     V=     Z=     m=     v=     �=     �=     �=     �=     �=     �=     �=     �=     
>     >     0>     H>     \>     o>  (   �>     �>  (   �>     �>     ?  #   .?  &   R?     y?     �?     �?     �?     �?     �?     @      @     3@     G@     X@     l@     �@     �@     �@     �@     �@     �@     �@     A     A     /A     EA     ^A     ~A     �A     �A     �A     �A     B     B     5B     RB     jB     �B     �B     �B     �B     �B     �B     �B     C      4C     UC     qC     �C     �C     �C     �C     �C     D     0D     PD     kD     �D     �D     �D     �D     �D      E     E     (E     BE     ]E     pE     �E     �E     �E     �E     �E     �E     �E     F     F     )F     <F     RF     mF     �F  	   �F     �F     �F     �F     �F     �F     G     ,G     FG     \G     qG     �G     �G     �G     �G     �G     �G     H     8H     RH  #   jH     �H     �H     �H     �H     �H     I     6I     RI     gI  ^  oI  .   �J  4   �J     2K     QK  �   YK     %L     .L     :L     CL     LL  	   TL     ^L     eL  	   mL     wL     L  %   �L  	   �L  .   �L  '   �L     M     M  
   +M     6M     PM     oM     �M     �M  
   �M     �M     �M     �M     �M     N     N  "   "N     EN     QN     ^N  $   jN  $   �N     �N     �N     �N  %   �N     O     O     *O     <O     XO     nO     �O     �O  {   �O     )P  3   CP  
   wP  !   �P     �P     �P     �P     �P     �P  	   Q     Q  !   $Q     FQ     aQ     Q     �Q     �Q     �Q     �Q     �Q     �Q     �Q     R  !   #R     ER     TR     YR     lR     qR     �R     �R     �R     �R     �R     �R     �R     �R     S     "S     6S  "   OS     rS     �S     �S     �S     �S     �S     �S     
T     T     7T     PT     iT     xT     �T     �T     �T     �T     �T  -   �T  	   U     U     9U  "   UU     xU      �U     �U  	   �U     �U  '   �U     V     V     V  ,   0V  '   ]V  9   �V     �V     �V     �V     �V     W     W     W     #W     *W     1W     FW  9   OW     �W     �W  	   �W     �W     �W     �W     �W  
   X     X     +X     ?X     VX     cX     ~X     �X  
   �X     �X     �X  2   �X     �X     �X     �X     Y     Y  #   $Y     HY  !   YY     {Y     �Y     �Y     �Y     �Y     �Y     �Y     �Y     �Y     Z     Z     ;Z     KZ     \Z     nZ     �Z  %   �Z  *   �Z  "   �Z  '   �Z  
   '[     2[     I[     P[     \[  	   e[     o[     x[     |[     �[  	   �[     �[     �[     �[     �[     �[     \     &\     *\     <\     R\     g\  	   y\     �\  	   �\     �\     �\     �\     �\     �\     �\      ]  �   @]     ^     ^  	   ^     &^     5^     8^     N^  a   R^  N   �^     _     _      (_     I_     Z_      n_     �_     �_     �_     �_     �_  
   �_     �_     `     /`     5`     T`     e`     n`     �`  -   �`     �`     �`     a  4   a  9   Ha     �a     �a     �a     �a     �a     �a     �a     �a     	b     b  #   &b     Jb     db     �b     �b     �b     �b     �b     �b     �b     �b  C   c     Hc     \c     hc  
   oc     zc     �c     �c  -   �c     �c     �c     d     d     .d     @d     Xd     md  	   |d     �d     �d     �d     �d     �d  +   �d     e     "e     'e     :e  %   Ge     me     �e     �e     �e     �e     �e     �e     �e     �e     �e     
f     f  	   *f     4f     Ef     df  
   sf     ~f  	   �f     �f  /   �f  ,   �f     g  
   g     &g     8g  �   Eg  +   �g  N   �g     Hh  o   gh  5   �h  �   i  	   �i     �i     �i     �i     j     j  !   'j  *   Ij     tj     �j     �j  	   �j     �j     �j     �j     k     )k     >k     Ek     Tk     `k     pk     �k     �k  "   �k     �k     �k     �k  	   �k     �k     �k     l     l     l     l     ,l     ;l     Vl     ol     l     �l     �l     �l     �l     �l     �l  
   �l     �l     �l     �l     �l     	m     m     )m  
   >m     Im     Wm     em     {m     �m     �m     �m     �m     �m     �m     �m     �m     �m     �m  
   �m     �m     �m     �m     �m     �m     �m     �m     �m  
   �m     n     	n     n     n     %n     )n     2n     An     Fn     Mn     Qn     _n     dn     zn  
   �n     �n  	   �n     �n     �n  	   �n     �n     �n     �n     �n  
   
o     o     #o     4o  
   Eo     Po     `o     io     mo     |o     �o     �o     �o     �o     �o     �o     �o     �o     �o     �o     �o  	   p  	   p     p     p     p     &p     2p     Ap  
   Ep  	   Pp      Zp     {p     �p     �p     �p     �p     �p     �p     �p     �p     �p     �p     �p     �p  
   �p     �p     
q  	   q  
   #q     .q     >q     Gq     Yq     gq     nq  
   q  
   �q     �q     �q   

Press any key to continue 

There are working clones of this machine: %s 
    Configuration saved    

 
Sound:
 
THIS MACHINE DOESN'T WORK. The emulation for this machine is not yet complete. There is nothing you can do to fix this problem except wait for the developers to improve the emulation.
 
Video:
  (default)  (locked)  COLORS  PENS %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Driver: %4$s

CPU:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d machines (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d software packages ) %1$s Brightness %1$s Contrast %1$s Gamma %1$s Horiz Position %1$s Horiz Stretch %1$s Refresh Rate %1$s Vert Position %1$s Vert Stretch %1$s Volume %1$s, %2$-.100s %1$s: %2$s
 %1$s: %2$s - Search: %3$s_ %2$s
 %s %s
 added to favorites list. %s
 removed from favorites list. %s [internal] (playing) (recording) **Error saving %s.ini** **Error saving ui.ini** , %s <set up filters> ARGB Settings Add %1$s Folder - Search: %2$s_ Add Folder Add To Favorites Add filter Adstick Device Assignment Advanced Options All cheats reloaded Analog Controls Analog Controls	Yes
 Are you sure you want to quit?

Press ''%1$s'' to quit,
Press ''%2$s'' to return to emulation. Artwork Options Auditing ROMs for machine %2$u of %3$u...
%1$s Auto Auto frame skip Auto rotate left Auto rotate right Automatic save/restore BIOS BIOS Selection BIOS selection: Barcode Reader Barcode length invalid! Beam Intensity Weight Beam Width Maximum Beam Width Minimum Bilinear Filtering Bitmap Prescaling Bold Bookkeeping Info Burn-in Camera	Imperfect
 Camera	Unimplemented
 Cannot save over directory Change %1$s Folder - Search: %2$s_ Change Folder Cheat Cheat Comment:
%s Cheats Choose from palette Coin %1$c: %2$d%3$s
 Coin %1$c: NA%3$s
 Coin impulse Coin lockout Color preview: Colors Command Completely unemulated features:  Configure Directories Configure Machine Configure Options Confirm quit from machines Controls	Imperfect
 Controls	Unimplemented
 Create Crosshair Offset %1$s Crosshair Offset X %1$1.3f Crosshair Offset Y %1$1.3f Crosshair Options Crosshair Scale %1$s Crosshair Scale X %1$1.3f Crosshair Scale Y %1$1.3f Current %1$s Folders Customize UI DIP Switches Device Mapping Dial Device Assignment Disabled Disk	Imperfect
 Disk	Unimplemented
 Double-click or press %1$s to select Driver Driver is Clone of	%1$s
 Driver is Parent	
 Driver is clone of: %1$-.100s Driver is parent Driver: "%1$s" software list  Emulated Enabled Enforce Aspect Ratio Enlarge images in the right panel Enter Code Exit Export displayed list to file Export list in TXT format (like -listfull) Export list in XML format (like -listxml) Export list in XML format (like -listxml, but exclude devices) External DAT View Fast Forward File File Already Exists - Override? File Manager Filter Filter %1$u Flip X Flip Y Folders Setup Fonts Force 4:3 aspect for snapshot display Frame skip GLSL Gameinit General Info General Inputs Graphics	Imperfect
 Graphics	Imperfect Colors
 Graphics	OK
 Graphics	Unimplemented
 Graphics	Wrong Colors
 Graphics: Imperfect,  Graphics: OK,  Graphics: Unimplemented,  Group HLSL Hide Both Hide Filters Hide Info/Image Hide romless machine from available list High Scores History Image Format: Image Information Images Imperfectly emulated features:  Include clones Info auto audit Infos Infos text size Input (general) Input (this Machine) Input Options Italic Joystick Joystick deadzone Joystick saturation Keyboard	Imperfect
 Keyboard	Unimplemented
 Keyboard Inputs	Yes
 Keyboard Mode LAN	Imperfect
 LAN	Unimplemented
 Language Laserdisc '%1$s' Horiz Position Laserdisc '%1$s' Horiz Stretch Laserdisc '%1$s' Vert Position Laserdisc '%1$s' Vert Stretch Lightgun Lightgun Device Assignment Lines Load State MAMEinfo MARPScore MESSinfo MHz Machine Configuration Machine Information Mamescore Manufacturer	%1$s
 Master Volume Menu Preview Microphone	Imperfect
 Microphone	Unimplemented
 Miscellaneous Options Mouse Mouse	Imperfect
 Mouse	Unimplemented
 Mouse Device Assignment Multi-keyboard Multi-mouse Name:             Description:
 Natural Natural keyboard Network Devices New Barcode: New Image Name: No No category INI files found No groups found in category file No machines found. Please check the rompath specified in the %1$s.ini file.

If this is your first time using %2$s, please see the config.txt file in the docs directory for information on configuring %2$s. None None
 Not supported Number Of Screens Off Offscreen reload On One or more ROMs/CHDs for this machine are incorrect. The machine may not run correctly.
 One or more ROMs/CHDs for this machine have not been correctly dumped.
 Other Controls Overall	NOT WORKING
 Overall	Unemulated Protection
 Overall	Working
 Overall: NOT WORKING Overall: Unemulated Protection Overall: Working Overclock %1$s sound Overclock CPU %1$s Paddle Device Assignment Partially supported Pause/Stop Pedal Device Assignment Performance Options Play Please enter a file extension too Plugin Options Plugins Positional Device Assignment Press TAB to set Press any key to continue. Printer	Imperfect
 Printer	Unimplemented
 Pseudo terminals Read this image, write to another image Read this image, write to diff Read-only Read-write Record Reload All Remove %1$s Folder Remove Folder Remove From Favorites Remove last filter Reset Reset All Results will be saved to %1$s Return to Machine Return to Previous Menu Revision: %1$s Rewind Romset	%1$s
 Rotate Rotate left Rotate right Rotation Options Sample Rate Sample text - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Save Configuration Save State Screen Screen #%d Screen '%1$s' Screen Orientation	Horizontal
 Screen Orientation	Vertical
 Screen flipping in cocktail mode is not supported.
 Search: %1$s_ Select New Machine Select access mode Select category: Select custom filters: Select image format Selection List - Search:  Settings Show All Show DATs view Show mouse pointer Show side panels Simultaneous contradictory Skip BIOS selection menu Skip information screen at startup Skip software parts selection menu Sleep Slider Controls Slot Devices Software is clone of: %1$-.100s Software is parent Software part selection: Sound Sound	Imperfect
 Sound	None
 Sound	OK
 Sound	Unimplemented
 Sound Options Sound: Imperfect Sound: None Sound: OK Sound: Unimplemented Speed Start Out Maximized State/Playback Options Steadykey Supported: No Supported: Partial Supported: Yes Switch Item Ordering Switched Order: entries now ordered by description Switched Order: entries now ordered by shortname Synchronized Refresh Sysinfo System: %1$-.100s Tape Control The selected game is missing one or more required ROM or CHD images. Please select a different game.

Press any key to continue. There are known problems with this machine

 This driver requires images to be loaded in the following device(s):  This machine has no BIOS. This machine has no sound hardware, MAME will produce no sounds, this is expected behaviour.
 This machine requires external artwork files.
 This machine was never completed. It may exhibit strange behavior or missing elements that are not bugs in the emulation.
 Throttle Tickets dispensed: %1$d

 Timing	Imperfect
 Timing	Unimplemented
 Trackball Device Assignment Triple Buffering Type name or select: %1$s_ Type name or select: (random) UI Color Settings UI Font UI Fonts Settings UI active Uptime: %1$d:%2$02d

 Uptime: %1$d:%2$02d:%3$02d

 Use External Samples Use image as background User Interface Vector Vector Flicker Video Mode Video Options Visible Delay WAN	Imperfect
 WAN	Unimplemented
 Wait Vertical Sync Window Mode X Y Year	%1$s
 Yes [compatible lists] [create] [empty slot] [empty] [failed] [file manager] [no category INI files] [no groups in INI file] [software list] color-channelAlpha color-channelBlue color-channelGreen color-channelRed color-optionBackground color-optionBorder color-optionClone color-optionDIP switch color-optionMouse down background color color-optionMouse down color color-optionMouse over background color color-optionMouse over color color-optionNormal text color-optionNormal text background color-optionSelected background color color-optionSelected color color-optionSlider color color-optionSubitem color color-optionUnavailable color color-presetBlack color-presetBlue color-presetGray color-presetGreen color-presetOrange color-presetRed color-presetSilver color-presetViolet color-presetWhite color-presetYellow color-sampleClone color-sampleMouse Over color-sampleNormal color-sampleSelected color-sampleSubitem default emulation-featureLAN emulation-featureWAN emulation-featurecamera emulation-featurecolor palette emulation-featurecontrols emulation-featuredisk emulation-featuregraphics emulation-featurekeyboard emulation-featuremicrophone emulation-featuremouse emulation-featureprinter emulation-featureprotection emulation-featuresound emulation-featuretiming kHz machine-filterAvailable machine-filterBIOS machine-filterCHD Required machine-filterCategory machine-filterClones machine-filterCustom Filter machine-filterFavorites machine-filterHorizontal Screen machine-filterManufacturer machine-filterMechanical machine-filterNo CHD Required machine-filterNot BIOS machine-filterNot Mechanical machine-filterNot Working machine-filterParents machine-filterSave Supported machine-filterSave Unsupported machine-filterUnavailable machine-filterUnfiltered machine-filterVertical Screen machine-filterWorking machine-filterYear path-optionArtwork path-optionArtwork Previews path-optionBosses path-optionCabinets path-optionCategory INIs path-optionControl Panels path-optionCovers path-optionCrosshairs path-optionDATs path-optionFlyers path-optionHowTo path-optionINIs path-optionIcons path-optionLogos path-optionMarquees path-optionPCBs path-optionROMs path-optionScores path-optionSnapshots path-optionSoftware Media path-optionVersus playing recording selmenu-artworkArtwork Preview selmenu-artworkBosses selmenu-artworkCabinet selmenu-artworkControl Panel selmenu-artworkCovers selmenu-artworkFlyer selmenu-artworkGame Over selmenu-artworkHowTo selmenu-artworkLogo selmenu-artworkPCB selmenu-artworkScores selmenu-artworkSnapshots selmenu-artworkVersus software-filterAvailable software-filterClones software-filterCustom Filter software-filterDevice Type software-filterFavorites software-filterParents software-filterPartially Supported software-filterPublisher software-filterRelease Region software-filterSoftware List software-filterSupported software-filterUnavailable software-filterUnfiltered software-filterUnsupported software-filterYear stopped Project-Id-Version: MAME
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-17 07:08+1100
PO-Revision-Date: 2016-02-20 18:03+0100
Last-Translator: Automatically generated
Language-Team: MAME Language Team
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 

Tryck på valfri tangent för att fortsätta 

Det finns fungerande varianter av denna maskin: %s 
    Konfiguration sparad   

 
Ljud:
 
DENNA MASKIN FUNGERAR EJ. Emuleringen för denna masin är ännu ej komplett. Det finns inget du kan göra för att fixa detta problem, förutom att vänta på att utvecklarna förbättrar emuleringen.
 
Video:
  (standard)  (låst)  FÄRGER  PENNOR %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Emulering: %4$s

CPU:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d maskiner (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d mjukvarupaket ) %1$s Ljusstyrka %1$s Kontrast %1$s Gamma %1$s Horisontell Position %1$s Horisontell utsträckning %1$s Uppdateringsfrekvens %1$s Vertikal Position %1$s Vertikal utsträckning %1$s Volym %1$s, %2$-.100s %1$s: %2$s
 %1$s: %2$s - Sök: %3$s_ %2$s
 %s %s
 tillagd i favoritlistan. %s
 borttagen från favoritlistan. %s [intern] (spelar upp) (spelar in) **Fel uppstod när %s.ini sparades** **Fel uppstod när ui.ini sparades** , %s <skapa filter> ARGB inställningar Lägg till %1$s Katalog - Sök: %2$s_ Lägg Till Katalog Lägg Till Favoriter Lägg till filter Reklamlappenhetstilldelning Avancerade Alternativ Alla fusk uppdaterade Analog Kontroller Analoga kontroller	Ja
 Är du säker på att du vill avsluta?

Tryck ''%1$s'' för att avsluta,
Tryck ''%2$s'' för att återgå till emuleringen. Grafisk Layout Alternativ Granskning av ROMs för maskin %2$u av %3$u...
%1$s Automatisk Automatisk skippning av bildrutor Automatisk vänsterrotation Automatisk högerrotation Automtisk Spara/Återställning BIOS BIOS val BIOS val: Streckkodsläsare Streckkodens längd är felaktig! Elektronstråle Intensitet Elektronstråle Bredd Maximum Stråle Bredd Minimum Bilinjär Filtrering Bitmap Prescaling Fet Bokföringsstatus Burn-in Kamera	Bristfällig
 Kamera	Ej implementerad
 Kan inte spara på katalog Ändra %1$s Katalog - Sök: %2$s_ Ändra Katalog Fusk Fusk Kommentar:
%s Fusk Välj från palette Mynt %1$c: %2$d%3$s
 Mynt %1$c: NA%3$s
 Mynt impuls Mynt lockout Förhandsvisning Färg: Färger Kommando Helt oemulerade funktioner:  Konfigurera Kataloger Maskinkonfiguration Konfigurationsalternativ Konfirmera avsluta maskinhantering Kontroller	Bristfällig
 Kontroller	Ej implementerad
 Skapa Hårkors Offset %1$s Hårkors Offset X %1$1.3f Hårkors Offset Y %1$1.3f Hårkorsalternativ Hårkors Skala %1$s Hårkors Skala X %1$1.3f Hårkors Skala Y %1$1.3f Nuvarande %1$s Kataloger Skräddarsy UI DIP Switchar Enhetsmappning Telefonenhetstilldelning Inaktiverad Disk	Bristfällig
 Disk	Ej implementerad
 Dubbelklicka eller tryck %1$s för att välja Drivrutin Drivrutinen är klon av	%1$s
 Drivrutinen är oberoende	
 Drivrutinen är klon av: %1$-.100s Drivrutinen har kloner Drivrutin: "%1$s" mjukvarulista  Emulerat Aktiverad Tvinga bildförhållande Förstora bilderna i den högra panelen Ange kod Hoppa ur Exportera listan till fil Exportera lista i TXT format (som -listfull) Exportlista i XML format (som -listxml) Exportlista i XML format (som -listxml, men utan enheter) Extern DAT vy Snabbspola framåt Fil Fil Finns Redan - Skriv Över? Filhanterare Filter Filter %1$u Flip X Flip Y Katalogkonfiguration Typsnitt Tvinga 4:3 bildförhållande för skärmavbildningsskärm Skippning av bildrutor GLSL Spelstart Generell Information Generell Indata Grafik	Bristfällig
 Grafik	Bristfälliga färger
 Grafik	OK
 Grafik	Ej implementerad
 Grafik	Fel Färger
 Grafik: Bristfällig,  Grafik: OK,  Grafik: Ej implementerad,  Grupp HLSL Göm Båda Göm Filter Göm Info/Bild Göm ROM lösa maskiner från tillgängliga-listan Ranking Historia Avbildningsformat: Avbildningsinformation Bilder Bristfälligt emulerade funtioner:  Inkludera kloner Information automatisk granskning Information Info Textstorlek Input (generell) Input (denna Maskin) Input Alternativ Kursiv Joystick Joystick dödzon Joystick mättnad Tangentbord	Bristfälligt
 Tangentbord	Ej implementerat
 Tangentbord	Ja
 Tangenbordsläge LAN	Bristfällig
 LAN	Ej implementerad
 Språk Laserdisk '%1$s' Horisontell Position Laserdisk '%1$s' Horisontell utsträckning Laserdisk '%1$s' Vertikal Position Laserdisk '%1$s' Vertikal utsträckning Ljuspistol Ljuspistolstilldelning Linjer Ladda läge MAMEinfo MARPScore MESSinfo MHz Maskin Konfiguration Maskininformation Mamescore Tillverkare	%1$s
 Huvud Volym Förhandsvisning Meny Mikrofon	Bristfällig
 Mikrofon	Ej implememterad
 Diverse Alternativ Mus Mus	Bristfällig
 Mus	Ej implementerad
 Musenhetstilldelning Multi-tangentbord Multi-mus Namn:             Beskrivning:
 Naturligt Naturligt tangentbord Nätverksenheter Ny streckkod Nytt Namn på Avbildning: Nej Inga kategorier INI filer funna Inga grupper funna i kategorifil Inga maskiner funna. Kontrollera sökvägar till ROMar i %1$s.ini filen.

Om det här är första gången du använder %2$s, läs config.txt filen i docs katalogen för information hur man konfigurerar %2$s. Ingen None
 Stöds Ej Antal skärmar Av Skärmlös omladdning På En eller flera ROMs/CHDs för denna maskin är felaktiga. Maskinen kan uppvisa fel vid körning.
 En eller flera ROMs/CHDs för denna maskin har inte blivit avbildade korrekt.
 Andra kontroller Status	FUNGERAR EJ
 Status	Oemulerad skyddsmekanism
 Status	Fungerar
 Status: FUNGERAR EJ Status: Oemulerad skyddsmekanism Status: Fungerar Överklocka %1$s ljud Överklocka CPU %1$s Paddlingsenhetstilldelning Stöds Delvis Paus/Stopp Pedalanordningstilldelning Prestanda Alternativ Spela Skriv in en filändelse också Pluginalternativ Tillägg Positionsenhetstilldelning Tryck TAB för att välja Tryck på valfri tangent för att fortsätta. Printer	Bristfällig
 Printer	Ej implementerad
 Låtsasterminaler Läs denna skivavbildning, skriv kopia till en annan Läs denna skivavbildning, skriv skillnader till en annan Enbart läsa Läsa och skriva Spela in Ladda Om Alla Tabort %1$s Katalog Tabort Katalog Tabort Från Favoriter Ta bort senaste filtret Återställ Återställ Alla Resultat kommer att sparas som %1$s Tillbaka till Emuleringen Tillbaka till Föregående Meny Revision: %1$s Spola tillbaka Romdistribution	%1$s
 Rotera Rotera vänster Rotera höger Rotationsalternativ Samplingshastighet Provtext - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Spara Konfiguration Spara läge Skärm Skärm #%d Skärm '%1$s' Skärm Orientering	Horisontal
 Skärm Orientering	Vertikal
 Skärm flippning i cocktail läge stöds ej.
 Sök: %1$s_ Välj Ny Maskin Välj accesstyp Välj kategori: Välj eget filter Välj avbildningsformat Urvalslista - Sök:  Inställningar Visa Alla Visa DATs vy Visa muspekare Visa sidopaneler Motsägelsefullt Hoppa över BIOS menyn Hoppa över informationsdialog vid uppstart Hoppa över mjukvaru menyn Sova Reglage Kontroller Slitsenheter Mjukvaran är klonad från: %1$-.100s Mjukvaran har kloner Val av mjukvarudel: Ljud Ljud	Bristfälligt
 Ljud	Inget
 Ljud	OK
 Ljud	Ej implementerat
 Ljudalternativ Ljud: Bristfälligt Ljud: Inget Ljud: OK Ljud: Ej implementerat Hastighet Starta Maximerad Status/Uppspelnings Alternativ Stadig tangent Stöd: Nej Stöd: Delvis Stöd: Ja Byt Sortering Bytt Sortering: poster sorteras på beskrivning Bytt Sortering: poster sorteras på kortnamn Synkroniserad Uppdatering Systeminfo System: %1$-.100s Bandkontroll Det valda spelet saknar en eller flera ROM eller CHS avbildningar. Välj ett annat spel.

Tryck på valfri tangent för att fortsätta. Det finns kända problem med denna maskin

 Den här emuleringen kräver att avbildningar laddas i följande apparat(er):  Den här maskinen saknar BIOS. Den här maskinen har ingen ljudhårdvara, MAME producerar därför inget ljud och detta är korrekt beteende.
 Denna maskin kräver extra filer med grafisk layout.
 Emulering av den här maskinen fullbordades aldrig. Den kan därför uppträda konstigt eller sakna delar som därför inte är felprogrammering i emuleringen.
 Strypning Biljetter fördelade: %1$d

 Timing	Bristfällig
 Timing	Ej implementerad
 Styrkuletilldening Tripplebuffrande Knappa in namn eller välj; %1$s_ Knappa in namn eller välj: (slumpmässig) UI färginställningar UI Typsnitt UI Typsnitts Inställningar Aktivt UI Drifttid: %1$d:%2$02d

 Drifttid: %1$d:%2$02d:%3$02d

 Använd Externa Samplingar Använd bild som bakgrund Användergränssnitt Vektor Vektor Flimmer Video läge Videoalternativ Synlig Fördröjning WAN	Bristfälligt
 WAN	Ej implementerad
 Vänta på Vertikal Synkronisering Fönster läge X Y År	%1$s
 Ja [kompatibla listor] [skapa] [tom plats] [tom] [misslyckad] [filhanterare] [ingen kategori INI filer] [inga grupper i INI fil] [mjukvarulista] Alfa Blå Grön Röd Bakgrund Kant Klon DIP switch Mouse down bakgrundsfärg Mouse down färg Mouse over bakgrunds färg Mouse over färg Normal text Normal textbakgrund Vald backgrundsfärg Vald färg Reglage färg Delpost färg Ej tillgänglig färg Svart Blå Grå Grön Orange Röd Silver Lila Vit Gul Klon Mouse Over Normal Vald Delpost standard LAN WAN kamera färgpalett kontroller disk grafik tangentbord mikrofon mus skrivare skyddsmekanism ljud timing kHz Tillgängliga BIOS CHD avbildning krävs Kategori Är Kloner Eget filter Favoriter Horisontell skärm Tillverkare Mekaniska Ingen CHD avbildning krävs Ej BIOS Ej Mekaniska Fungerar ej Har Kloner Spara Stödda Spara Ej Stödda Ej tillgängliga Ofiltrerat Vertikal skärm Fungerar År Grafisk Layout Förhandsvisning Grafisk Layout Bossar Kabinett Kategori INIs Kontrollpaneler Omslagsbilder Hårkors DATs Flygblad HowTo INIs Ikoner Logotyper Draperier PCBs ROMs Poäng Skärmklipp Mjukvaru Media Mot spelar upp spelar in Förhandsvisining Grafisk Layout Bossar Kabinett Kontrollpanel Omslagsbilder Flygblad Spelet Slut HowTo Logotyp PCB Poäng Skärmklipp Mot Tillgängliga Är Kloner Eget filter Typ av apparat Favoriter Har Kloner Partiellt Stöd Utgivare Utgivningsområde Mjukvarulista Stödd Ej tillgängliga Ofiltrerat Ej Stödda År stoppad 