��    t     �              \     ]  .   y     �     �  �   �     �  
   �  	   �     �     �  	   �     �     �  	   �     �     �  "   �  .     +   :     f     v  
   �     �     �     �     �     �     �     �     	          0     6      S     t  	   �     �     �     �     �     �  
   �       
             1  ^   A     �     �     �     �     �     �     �               ,     1     B  "   ]     �     �     �     �     �     �     �     �     �     �               /     A     H     ^     y     �     �     �     �     �                 $   '     L     S     l          �     �     �     �     �  
   �     �     �  *     )   H  >   r     �     �     �     �     �           	                  &      3      B      V      q      ~      �      �      �   	   �      �      �      �      !     !     $!     +!     1!     A!     Q!     f!     m!     �!     �!     �!     �!     �!     �!     "     ,"     2"     ;"     D"     H"     ^"  	   r"     |"     �"     �"     �"     �"     �"     �"     �"     �"     #     #     +#  �   .#     �#     $     $     $  Y   $  G   q$     �$     �$     �$     �$     %     "%     A%     R%     e%  
   y%     �%  !   �%     �%     �%     �%     �%     �%  '   �%     '&  	   F&  
   P&     [&  
   b&     m&     �&     �&     �&     �&  	   �&     �&     �&     �&     '     '     %'     2'     9'  F   E'     �'     �'  
   �'     �'     �'     �'  3   �'     /(     =(     P(     c(     z(     �(     �(     �(     �(     �(     �(     �(     �(     )     *)     C)     I)  	   Z)     d)     y)     �)  	   �)     �)     �)     �)     �)     �)     �)     *     *  ,   #*  E   P*  ]   �*  .   �*  z   #+     �+     �+     �+     �+     ,     ,     ,     3,     P,     e,     t,     {,     �,     �,     �,     �,  
   �,     �,     �,     �,     �,     �,     �,     �,     -     -     &-     9-     M-     _-     w-     �-     �-  '   �-  (   �-     .  (   %.     N.     l.  #   �.  &   �.     �.     �.     /     !/     @/     S/     e/     w/     �/     �/     �/     �/     �/     �/     �/     0     )0     =0     S0     h0     p0     �0     �0     �0     �0     �0     1     +1     E1     b1     z1     ~1     �1     �1      �1     �1     2     2     :2     U2     t2     �2     �2     �2     �2     �2     �2     3     &3     =3     N3     a3     z3     �3     �3     �3     �3     �3     �3     �3     4     4     44     N4     a4  	   i4     s4     �4     �4     �4     �4     �4     	5     5     25     I5     `5     z5     �5     �5     �5     �5     �5     6     +6  �  36  K   �7  I   B8  6   �8     �8    �8     �9     �9     :  	   :     ":  	   /:     9:     @:  	   H:     R:     Z:  (   a:  2   �:  =   �:     �:     ;     -;  .   ;;  3   j;  0   �;  *   �;  /   �;     *<     G<     W<  $   c<     �<  0   �<  8   �<     �<     =     '=  ;   8=  7   t=     �=  B   �=  #   	>  )   ->     W>  /   o>  !   �>  �   �>     �?     �?     �?     �?     �?  9   @     ;@  .   [@  *   �@     �@  ?   �@  E   A  F   NA  '   �A     �A  &   �A     �A  "   �A     !B     <B  #   UB     yB     �B  5   �B  '   �B  '   �B     C      &C  %   GC  %   mC     �C      �C  %   �C  %   �C  .   D  :   ID     �D     �D  P   �D     E  '   E  "   :E  ,   ]E  "   �E  1   �E     �E     �E     F     F  
   ;F  <   FF  F   �F  E   �F  c   G  #   tG      �G     �G  0   �G  %   �G     H  /   &H     VH     eH  !   �H  !   �H  $   �H  -   �H     I  5   0I  &   fI     �I  7   �I     �I     �I      J     1J     BJ  $   ]J     �J     �J  5   �J  8   �J  ;   K     UK  (   fK  9   �K     �K     �K  :   �K  ?   +L  6   kL  ;   �L     �L     �L     	M     'M  '   +M  &   SM  	   zM     �M  $   �M  &   �M     �M     �M  +   N     9N     YN     jN     �N     �N     �N  �  �N     ;P     KP     eP     xP  �   �P  {   0Q     �Q     �Q  ;   �Q     &R     AR  <   aR     �R  3   �R  #   �R     S  
   %S  1   0S     bS     �S  .   �S  J   �S     	T  4   )T  5   ^T     �T     �T  
   �T      �T  *   �T  %   U  /   AU  *   qU     �U     �U     �U  ;   �U  /   V     KV     bV     sV     �V  )   �V  T   �V  )   W  
   AW     LW     [W  ?   mW  ;   �W  R   �W     <X  $   TX  &   yX  0   �X     �X  -   �X     Y     0Y     FY  &   UY     |Y     �Y  ,   �Y      �Y  %   Z     (Z     1Z     NZ  -   [Z     �Z     �Z     �Z  -   �Z     �Z  $   [     :[  !   Q[  )   s[     �[     �[  K   �[     \  �   �\  T   9]  �   �]     s^  :   �^  T   �^  I   #_  4   m_  P   �_  "   �_  )   `  .   @`  '   o`     �`  !   �`     �`     �`     �`     �`      a     a  %   a     >a     Oa     ga     va  '   �a     �a     �a  
   �a     �a     �a     �a     b     b     0b  .   Eb  :   tb  )   �b  :   �b  )   c     >c  0   Zc  <   �c     �c     �c     �c  "   d     ;d  
   Dd     Od     Xd     ed     zd     �d     �d     �d     �d     �d     �d     �d     �d     e     e     /e     <e     Ee     Te     ge     �e     �e     �e     �e     �e     �e     �e     �e     �e  #   f     *f     ?f     Rf     jf     xf     �f     �f     �f  2   �f  #   �f     g  !   -g  
   Og     Zg     gg  
   zg  (   �g     �g     �g  
   �g  
   �g  *   �g  %   h     9h  !   Gh     ih     �h     �h     �h     �h     �h     �h  #   i     =i  
   Li  "   Wi     zi     �i  %   �i  !   �i     �i     �i     j     ,j     ;j     Qj     mj     |j     �j     �j   

Press any key to continue 

There are working clones of this machine: %s 
    Configuration saved    

 
Sound:
 
THIS MACHINE DOESN'T WORK. The emulation for this machine is not yet complete. There is nothing you can do to fix this problem except wait for the developers to improve the emulation.
 
Video:
  (default)  (locked)  COLORS  PENS %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Driver: %4$s

CPU:
 %1$s %2$s ( %3$d / %4$d machines (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d software packages ) %1$s Brightness %1$s Contrast %1$s Gamma %1$s Horiz Position %1$s Horiz Stretch %1$s Refresh Rate %1$s Vert Position %1$s Vert Stretch %1$s Volume %1$s, %2$-.100s %1$s: %2$s
 %1$s: %2$s - Search: %3$s_ %2$s
 %s
 added to favorites list. %s
 removed from favorites list. %s [internal] (playing) (recording) **Error saving %s.ini** **Error saving ui.ini** ARGB Settings Add %1$s Folder - Search: %2$s_ Add Folder Add To Favorites Add filter All cheats reloaded Analog Controls Are you sure you want to quit?

Press ''%1$s'' to quit,
Press ''%2$s'' to return to emulation. Auto BIOS BIOS Selection BIOS selection: Barcode Reader Barcode length invalid! Beam Intensity Weight Beam Width Maximum Beam Width Minimum Bold Bookkeeping Info Cannot save over directory Change %1$s Folder - Search: %2$s_ Change Folder Cheat Cheat Comment:
%s Cheats Choose from palette Coin %1$c: %2$d%3$s
 Coin %1$c: NA%3$s
 Color preview: Colors Command Configure Directories Configure Machine Configure Options Create Crosshair Offset %1$s Crosshair Offset X %1$1.3f Crosshair Offset Y %1$1.3f Crosshair Options Crosshair Scale %1$s Crosshair Scale X %1$1.3f Crosshair Scale Y %1$1.3f Current %1$s Folders Customize UI DIP Switches Disabled Double-click or press %1$s to select Driver Driver is Clone of	%1$s
 Driver is Parent	
 Driver is clone of: %1$-.100s Driver is parent Driver: "%1$s" software list  Driver: %1$-.100s Emulated Enabled Enter Code Exit Export displayed list to file Export list in TXT format (like -listfull) Export list in XML format (like -listxml) Export list in XML format (like -listxml, but exclude devices) External DAT View Fast Forward File File Already Exists - Override? File Manager Filter Folders Setup Fonts Gameinit General Info General Inputs Graphics	Imperfect
 Graphics	Imperfect Colors
 Graphics	OK
 Graphics	Unimplemented
 Graphics: Imperfect,  Graphics: OK,  Graphics: Unimplemented,  Hide Both Hide Filters Hide Info/Image History Image Format: Image Information Images Infos Infos text size Input (general) Input (this Machine) Italic Keyboard	Imperfect
 Keyboard	Unimplemented
 Keyboard Mode Language Laserdisc '%1$s' Horiz Position Laserdisc '%1$s' Horiz Stretch Laserdisc '%1$s' Vert Position Laserdisc '%1$s' Vert Stretch Lines MAMEinfo MESSinfo MHz Machine Configuration Machine Information Mamescore Manufacturer	%1$s
 Master Volume Menu Preview Mouse Mouse	Imperfect
 Mouse	Unimplemented
 Name:             Description:
 Natural Network Devices New Barcode: New Image Name: No No machines found. Please check the rompath specified in the %1$s.ini file.

If this is your first time using %2$s, please see the config.txt file in the docs directory for information on configuring %2$s. None
 Not supported Off On One or more ROMs/CHDs for this machine are incorrect. The machine may not run correctly.
 One or more ROMs/CHDs for this machine have not been correctly dumped.
 Other Controls Overall	NOT WORKING
 Overall	Unemulated Protection
 Overall	Working
 Overall: NOT WORKING Overall: Unemulated Protection Overall: Working Overclock CPU %1$s Partially supported Pause/Stop Play Please enter a file extension too Plugin Options Plugins Press TAB to set Press any key to continue. Pseudo terminals Read this image, write to another image Read this image, write to diff Read-only Read-write Record Reload All Remove %1$s Folder Remove Folder Remove From Favorites Remove last filter Reset Reset All Results will be saved to %1$s Return to Machine Return to Previous Menu Revision: %1$s Rewind Romset	%1$s
 Rotate Sample Rate Sample text - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Save Configuration Screen Screen #%d Screen '%1$s' Screen Orientation	Horizontal
 Screen Orientation	Vertical
 Screen flipping in cocktail mode is not supported.
 Search: %1$s_ Select New Machine Select access mode Select custom filters: Select image format Selection List - Search:  Settings Show All Show DATs view Show side panels Slider Controls Slot Devices Software is clone of: %1$-.100s Software is parent Software part selection: Sound Sound	Imperfect
 Sound	OK
 Sound	Unimplemented
 Sound Options Sound: Imperfect Sound: OK Sound: Unimplemented Supported: No Supported: Partial Supported: Yes Switch Item Ordering Sysinfo System: %1$-.100s Tape Control There are known problems with this machine

 This driver requires images to be loaded in the following device(s):  This machine has no sound hardware, MAME will produce no sounds, this is expected behaviour.
 This machine requires external artwork files.
 This machine was never completed. It may exhibit strange behavior or missing elements that are not bugs in the emulation.
 Tickets dispensed: %1$d

 Type name or select: %1$s_ Type name or select: (random) UI Color Settings UI Font UI Fonts Settings Uptime: %1$d:%2$02d

 Uptime: %1$d:%2$02d:%3$02d

 Use External Samples User Interface Vector Vector Flicker Video Options Visible Delay X Y Year	%1$s
 Yes [compatible lists] [create] [empty slot] [empty] [failed] [file manager] [software list] color-channelAlpha color-channelBlue color-channelGreen color-channelRed color-optionBackground color-optionBorder color-optionClone color-optionDIP switch color-optionGraphics viewer background color-optionMouse down background color color-optionMouse down color color-optionMouse over background color color-optionMouse over color color-optionNormal text color-optionNormal text background color-optionSelected background color color-optionSelected color color-optionSlider color color-optionSubitem color color-optionUnavailable color color-presetBlack color-presetBlue color-presetGray color-presetGreen color-presetOrange color-presetRed color-presetSilver color-presetViolet color-presetWhite color-presetYellow color-sampleClone color-sampleMouse Over color-sampleNormal color-sampleSelected color-sampleSubitem default emulation-featurecamera emulation-featuredisk emulation-featuregraphics emulation-featurekeyboard emulation-featuremagnetic tape emulation-featuremicrophone emulation-featuremouse emulation-featureprinter emulation-featureprotection emulation-featuresound kHz machine-filterCategory machine-filterClones machine-filterFavorites machine-filterHorizontal Screen machine-filterManufacturer machine-filterMechanical machine-filterNot Mechanical machine-filterNot Working machine-filterVertical Screen machine-filterWorking machine-filterYear path-optionArtwork path-optionArtwork Previews path-optionBosses path-optionCabinets path-optionControl Panels path-optionCovers path-optionCrosshairs path-optionDATs path-optionFlyers path-optionGame Endings path-optionHowTo path-optionINIs path-optionIcons path-optionLogos path-optionMarquees path-optionPCBs path-optionROMs path-optionScores path-optionSelect path-optionSnapshots path-optionTitle Screens path-optionVersus playing recording selmenu-artworkArtwork Preview selmenu-artworkBosses selmenu-artworkCabinet selmenu-artworkCovers selmenu-artworkGame Over selmenu-artworkHowTo selmenu-artworkLogo selmenu-artworkPCB selmenu-artworkScores selmenu-artworkSelect selmenu-artworkSnapshots selmenu-artworkVersus software-filterClones software-filterDevice Type software-filterFavorites software-filterPublisher software-filterSoftware List software-filterYear stopped Project-Id-Version: MAME
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-17 07:08+1100
PO-Revision-Date: 2016-02-23 12:43+0100
Last-Translator: Automatically generated
Language-Team: MAME Language Team
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.8.7
 

Притисните било који тастер за наставак 

Постоји клонови ове машине који раде: %s 
    Конфигурација сачувана    

 
Звук:
 
ОВА МАШИНА НЕ РАДИ. Емулација ове машине још није потпуна. Не можете ништа урадити у вези с тим, осим да сачекате програмере да унапреде емулацију.
 
Видео:
  (уобичајено)  (закључано)  БОЈЕ ОЛОВКЕ %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Драjвeр: %4$s

CPU:
 %1$s %2$s ( %3$d / %4$d машине (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d програмских пакета ) %1$s осветљеност %1$s контраст %1$s гама %1$s хоризонтална позиција %1$s растезање по хоризонтали %1$s фреквенција освежавања %1$s вертикална позиција %1$s растезање по вертикали %1$s јачина звука %1$s, %2$-.100s %1$s: %2$s
 %1$s: %2$s - Претрага: %3$s_ %2$s
 %s
 додато у листу омиљених. %s
 избрисано из листе омиљених. %s [интерно] (репродукција) (снимање) **Грешка прликом записивања %s.ini** **Грешка приликом снимања ui.ini** ARGB подешавања Додај %1$s директоријум - Претрага: %2$s_ Додај директоријум Додај у листу омиљених Додај филтер Сва варања поново учитана Аналогне контроле Да ли сте сигурни да желите да напустите програм?

Притисните ''%s'' за напуштање програма,
Притисните ''%s'' за наставак емулације. Аутоматски BIOS Избор BIOS-а Избор BIOS-а: Баркод читач Неодговарајућа дужина баркода! Интензитет снопа Максимална шириниа снопа Минимална ширина снопа Подебљано Информације о књиговодству машине Немогуће снимити преко директоријума Промени %1$s директоријум - Претрага: %2$s_ Промени директоријум Варање Коментар за варање:
%s Варања Изабрати из палете Жетон %1$c: %2$d%3$s
 Жетон %1$c: NA%3$s
 Пробни приказ боја: Боје Команда Конфигурација директоријума Конфигурација машине Конфигурација опција Креирај Померај нишана %1$s Померај нишана X %1$1.3f Померај нишана Y %1$1.3f Опције нишана Размера нишана %1$s Размера нишана X %1$1.3f Размера нишана Y %1$1.3f Тренутни %1$s директоријум Прилагоди кориснички интерфејс DIP прекидачи Онемогућено Дупли клик или притисните %1$s за селектовање Драјвер Драјвер је клон од	%1$s
 Драјвер је предак	
 Драјвер је клон од: %1$-.100s Драјвер је основни Драjвeр: "%1$s" листа програма  Драјвер: %1$-.100s Емулирани Омогућено Унесите баркод Излаз Експортуј приказану листу у фајл Експортуј листу у TXT формату (као -listfull) Експортуј листу у XML формату (као -listxml) Експортуј листу у XML формату (као -listxml, али без уређаја) Приказ спољашњег DAT Премотај у напред Фајл Фајл већ постоји - препиши? Управљање фајловима Филтер Подешавање директоријума Фонтови Иницијализација Опште информације Глобалне контроле Графика	несавршена
 Графика	несавршене боје
 Графика	ОК
 Графика	није имплементирана
 Графика: несавршена,  Графика: ОК,  Графика: није имплементирана,  Сакриј обоје Сакриј филтере Сакриј Инфо/Слику Историја Формат записа: Информације о слици Програми Информације Висина текста за информације Подешавање контрола (глобално) Подешавање контрола (ова машина) Искошено Тастатура	несавршена
 Тастатура	није имплементирана
 Мод тастатуре Језици Laserdisc '%1$s' хоризонтална позиција Laserdisc '%1$s' растезање по хоризонтали Laserdisc '%1$s' вертикална позиција Laserdisc '%1$s' растезање по вертикали Линије MAME - информације MESS - информације MHz Конфигурација машине Информације о машини Mamescore Произвођач	%1$s
 Главна јачина звука Пробни приказ менија Миш Миш	несавршен
 Миш	није имплементиран
 Име:              Опис:
 Природни Мрежни уређаји Нови баркод: Ново име: Не Нису проанађене машине. Молимо Вас, проверите путању до ROM фајлова дефинисану у %1$s.ini фајлу

Ако је ово Ваш први пут да користите %2$s, молимо Вас погледајте config.txt фајл у docs директоријуму за информације о подешавању %2$s. Ниједан
 Није подржано Искључено Укључено Један или више ROM/CHD фаЈлова за ову машину су неисправни. Машина можда неће исправно радити.
 Један или више ROM/CHD фајлова за ову машину нису исправно направљени.
 Остале контроле Уопштено	НЕ РАДИ
 Уопштено	Заштита није емулирана Уопштено	ради
 Уопштено: НЕ РАДИ Уопштено: Заштита није емулирана Уопштено: ради Убрзање основног такта CPU %1$s Делимично подржано Пауза/Стоп Пусти Молим унесите и екстензију Опције додатака Додатци Притисните TAB да подесите Притисните било који тастер за наставак. Псеудо терминали Читај овај фајл, пиши у други Читај овај фајл, пиши разлике Само читање Читање-писање Сними Поново учитај све Уклони %1$s директоријум Уклони директоријум Избриши из листе омиљених Уклони последњи филтер Ресетуј Ресетуји све Нормални текст Подешавање контрола (ова машина) Повратак у претходни мени Ревизија: %1$s Премотај РОМ скуп	%1$s
 Ротирај Учесталост узорковања Пример текста - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Сачувај конфигурацију Екран Екран #%d Екран '%1$s' Оријентација екрана	Хоризонтално
 Оријентација екрана	Вертикално
 Обртање екрана у "коктел" моду није подржано.
 Претрага: %1$s_ Изабери нову машину Одабери мод приступа Избор произвољних филтера Одабери формат Изборна листа - претрага: Подешавања Прикажи све Прижи DAT Прикажи бочне панеле Контроле клизача Слот уређаји Софтвер је клон од: %1$-.100s Софтвер је предак Избор дела софтвера: Звук Звук	несавршен
 Звук	OK
 Звук	није имплементиран
 Опције за звук Звук: несавршен Звук: ОК Звук: није имплементиран Подржано: не Подржано: делимично Подржано: да Промени сортирање Системске информације Систем: %1$-.100s Контрола траке Постоје познати проблеми с овом машином

 За овај драјвер је неопходно да слике буду учитане у следеће уређаје:  Машина не поседује уређај за звук, МАМЕ неће пуштати звук, ово је очекивано понашање.
 Машина захтева додатне илустрационе фајлове.
 Машсина није никад завршена. Може се јавити чудно понашање или недостатак елемената сто не представља проблем с емулацијом.
 Издате карте: %1$d

 Откуцајте име или изаберите: %1$s_ Откуцајте име или изаберите: (насумичан избор) Подешавање боја корисничког интерфејса Фонт корисничког интерфејса Подешавање фонтова за кориснички интерфејс Време рада: %1$d:%2$02d

 Време рада: %1$d:%2$02d:%3$02d

 Користи додатне семплове Кориснички интерфејс Вектор Треперење вектора Видео опције Видљиво кашњење X Y Година	%1$s
 Да [компатибилне листе] [креирај] [празан слот] [празно] [неуспечно] [управљање фајловима] [листа програма] Алфа Плава Зелена Црвена Позадина Граница Клонирај DIP прекидач Графички приказ позадине Боја позадине при притиску миша Боја при притиску миша Боја позадине при преласку миша Боја при преласку миша Боја изабраног Позадина нормалног текста Боја позадине за изабрану ставку Боја изабраног Боја клизача Боја подставке Боја за недоступно Црна Плава Сива Зелена Наранџаста Црвена Сребрна Љубичаста Бела Жута Клонирај Прелаз мишем Нормално Изабрано Под-ставка уобичајено камера диск графика тастатура магнетна трака микрофон миш штампач заштита звук kHz Категорија Клонови Листа омиљених Хоризонтални екран Произвођач Механички Не механички Не ради Вертикални екран Ради Година Илустрације Умањени приказ илустрација Финални противници Кабинети Управљачки панели Маске Нишани DAT фајлови Летци Екрани завршетка игре Упутство INI фајлови Иконе Логои Постери изнад кабинета PCB (штампане плочице) РОМ-ови Најбољи резултати Изборни екрани Снимци екрана Насловни екрани Версус екрани репродукција снимање Преглед artwork-а Финални противници Кабинет Маске Завршни екран игре Упутство Лого PCB (штампана плочица) Најбољи резултати Изборни екрани Снимци екрана Версус екрани Клонови Тип уређаја Листа омиљених Издавач Листа програма Година стопирано 