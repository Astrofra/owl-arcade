��    9     �              �#     �#     �#  .   �#     $     6$  �   ?$     �$  
   %  	   %     %     %  	   %%     /%     6%  	   >%     H%     P%  "   W%  	   z%  .   �%  +   �%     �%     �%  
   �%     &     &     /&     A&     T&     f&     r&     �&     �&     �&     �&     �&      �&     �&     �&  	   '     '     '     5'     M'     R'     c'     q'     '     �'  
   �'     �'  
   �'     �'     �'     (     (     (     ,(     A(  ^   E(     �(  /   �(      �(  .   )     4)     9)     I)     Z)     l)     �)     �)     �)     �)     �)     �)     �)     �)     
*     *     /*     4*     E*  
   M*     X*     j*     �*     �*  "   �*     �*  D   �*     +     +     0+  
   =+     H+     d+  -   +     �+     �+     �+     �+     �+     �+     ,     ,     ',     .,      6,     W,     m,     ,     �,     �,     �,     �,     �,     �,     -     +-     =-     R-     l-     �-     �-     �-     �-     �-     �-     �-     �-     �-     	.     .     .     /.     C.  $   H.     m.     t.     �.     �.     �.     �.     �.     �.     /     &/     //     7/     C/  !   X/  
   z/     �/     �/  *   �/  )   �/  >   �/     <0     N0     [0     `0     �0     �0     �0     �0     �0     �0     �0  %   �0  
   �0     �0     �0     1     1     1     11     L1     Y1     q1     �1     �1     �1     �1     �1  	   �1     �1     �1  (   �1     "2     .2     62     D2     V2     ]2     }2     �2     �2     �2     �2     �2     �2     �2     �2     �2     3     3     /3     G3     \3     j3     y3     �3     �3     �3     �3     �3     4      %4     F4     Z4     r4  $   �4     �4     �4     �4     �4  
   �4     �4  	   5     5     5     5     /5  	   C5     M5     `5     n5     z5     �5     �5     �5     �5     �5     �5     6     6     (6     @6     O6     [6     {6     �6     �6     �6     �6     �6     �6      �6  �   7     �7     �7     �7     �7     �7     �7     8  Y   8  G   l8     �8     �8     �8     �8     9     9     <9     M9     b9     u9     �9     �9  
   �9     �9     �9     �9  
   �9     �9  !   �9     :     &:     .:     K:     \:     w:     �:     �:     �:     �:  '   �:     ;  	   $;  
   .;     9;  
   @;     K;     ^;     l;     �;     �;     �;     �;     �;     �;     <     "<  	   (<     2<     P<     b<     z<     �<     �<     �<     �<     �<     �<     �<  F   �<     !=  !   ;=     ]=     v=  
   {=     �=  
   �=     �=  
   �=     �=     �=     �=  3    >     4>     B>     U>     h>     y>     �>     �>     �>     �>     �>     �>     �>     �>     
?     ?     .?     I?  "   b?  "   �?     �?     �?     �?     �?     �?     �?     @     @     .@  	   :@     D@     Y@     g@     x@  	   �@     �@     �@     �@     �@     �@  	   �@     �@     A     A     'A     5A     HA     WA  2   lA  0   �A     �A     �A     �A     �A     B  �   B  ,   �B  E   �B     C  ]   C  .   }C  z   �C     'D     0D     JD     PD     bD  
   xD     �D     �D     �D     �D     �D     �D      E     E  	   E  8   $E     ]E     sE     �E     �E     �E     �E     �E     �E     �E  
   �E     	F     F     %F     4F     GF     ZF     `F     lF     rF     tF  
   vF     �F     �F     �F     �F     �F     �F     �F     �F     �F     �F     G     "G     5G     IG     [G     sG     �G  '   �G  (   �G     �G  (   	H     2H     PH  #   iH  &   �H     �H     �H     �H     I     $I     7I     II     [I     nI     �I     �I     �I     �I     �I     �I     �I     J     !J     7J     LJ     TJ     jJ     �J     �J     �J     �J     �J     K     !K     >K     VK     pK     �K     �K     �K     �K     �K     �K     L     #L     9L     VL      oL     �L     �L     �L     �L     �L     M     6M     MM     kM     �M     �M     �M     �M     �M     
N     N     2N     LN     gN     zN     �N     �N     �N     �N     �N     �N     �N     O     "O     3O     FO     \O     wO     �O  	   �O     �O     �O     �O     �O      P     P     0P     HP     \P     sP     �P     �P     �P     �P     �P     Q     )Q  #   AQ     eQ     Q     �Q     �Q     �Q     �Q     R     )R     >R  M  FR  !   �S  )   �S  .   �S  !   T     1T  �   8T     �T  	   U  
   U     U      U  	   )U     3U     :U  	   BU     LU     TU  "   [U  	   ~U  /   �U  -   �U     �U     �U     V     V     1V     NV     lV     �V     �V     �V     �V     �V     �V     �V  %   �V  #   W     9W     FW     TW  
   bW     mW     �W     �W     �W     �W     �W     �W  %   �W     X     %X     =X  $   NX     sX     �X     �X     �X     �X     �X  k   �X     PY  ;   dY  '   �Y  /   �Y     �Y     Z     #Z     :Z     PZ     nZ     sZ     �Z     �Z  '   �Z     �Z     �Z     [     ;[     O[     e[     m[     �[  
   �[     �[     �[     �[     �[  !   �[     \  X   \     t\     z\     �\     �\     �\      �\  /   �\     ]     &]     8]     P]     f]     z]     �]     �]     �]     �]     �]     �]     �]     ^     #^     B^     Y^     w^     }^     �^     �^     �^     �^     �^     _     _     ._     ;_     I_     `_     q_     y_     �_  +   �_     �_     �_     �_     �_  	   `  3   "`     V`     ]`     r`     �`     �`     �`     �`  !   �`     a     a     &a  
   -a     8a  "   Ha     ka     a  %   �a  .   �a  -   �a  I   b     Qb     ob     b  "   �b     �b     �b     �b  
   �b  
   �b     �b     �b  .   c     1c     Dc     Ic  
   Rc     ]c     kc     �c     �c     �c     �c     �c     �c     d     -d     3d     8d     Gd     Xd  4   md     �d  
   �d     �d     �d     �d  .   �d     e  '   &e     Ne     Te     oe     }e     �e     �e     �e     �e     �e     �e     �e     f     .f     >f     Uf     sf  (   zf  (   �f  &   �f  &   �f     g  $   3g     Xg     og     �g  )   �g     �g     �g  *   �g     !h     (h     8h  	   Ah     Kh     Th     Xh     sh  	   �h     �h     �h     �h     �h     �h     �h     i     )i     Fi     Xi     ]i     ni      �i     �i     �i     �i     �i     �i      j     j     -j     Bj  1   Gj  6   yj  �   �j     �k     �k     �k     �k  	   �k     �k     �k  l    l  P   ml     �l     �l     �l     m     m     .m     Nm     `m     sm  "   �m     �m     �m     �m  !   �m     �m     n  
   n     n  6   &n     ]n     qn  &   yn     �n  '   �n     �n     �n     o      )o     Jo  +   ho  #   �o     �o     �o     �o     �o     �o     p     p     'p     >p     Sp     gp     xp     �p     �p  	   �p     �p  %   �p     q     q     /q  	   >q     Hq     Uq     [q     qq     �q     �q  K   �q  %   r  3   (r  "   \r     r     �r     �r     �r     �r  	   �r     �r  !   �r     �r  7   s     Ns     ]s     ws     �s  %   �s  "   �s     �s     t     .t     4t     Et     Ut     bt     �t     �t     �t  !   �t  '   �t  -   u     Fu     Mu     cu  !   xu     �u     �u     �u     �u     �u     �u     �u     v     !v     1v     >v     Fv  
   ]v     hv     |v     �v  	   �v     �v     �v     �v     w     w     'w     6w  8   Nw  6   �w     �w     �w     �w     �w     x  �   x  0   �x  N   �x     y  i   ;y  4   �y  ~   �y     Yz     pz     �z     �z  !   �z     �z  %   �z     {     {  !   {  (   ={  %   f{     �{     �{     �{  J   �{     |  "   3|  )   V|     �|     �|     �|     �|     �|     �|     �|     �|     }     }     .}     E}     `}     l}     {}     �}     �}  	   �}     �}     �}     �}     �}     �}     �}     �}      �}     
~     '~  	   ;~     E~     J~     P~     Y~     _~     e~  "   k~     �~     �~     �~     �~     �~     �~          *     <     Z     k     ~     �     �     �     �     �     �     �     �     �     �  
   �     �     �     �     �  
   �     
�     �     �  	   &�     0�  	   6�     @�  	   H�     R�  
   W�     b�     n�     r�     ��     ��     ��  
   ��  	   ��     ��     ��  	   ǀ     р  
   �  	   �     ��  	   �     �      �  	   /�     9�     N�     h�     y�     ��  	   ��     ��     ��     ��     ��     ρ     �     �     ��     ��  
   ��     	�     �     �  	   �     &�     2�     7�  	   @�     J�     `�     g�     s�     |�     ��     ��     ��     ��  
   ��     ǂ  
   Ђ     ۂ  	   �     �     ��     �     �     �  	   1�  	   ;�     E�     \�     c�     z�  	   ��     ��     ��     ��     ă     ȃ   

--- DRIVER INFO ---
Driver:  

Press any key to continue 

There are working clones of this machine: %s 
    Configuration saved    

 
Sound:
 
THIS MACHINE DOESN'T WORK. The emulation for this machine is not yet complete. There is nothing you can do to fix this problem except wait for the developers to improve the emulation.
 
Video:
  (default)  (locked)  COLORS  PENS %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Driver: %4$s

CPU:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d machines (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d software packages ) %1$s Brightness %1$s Contrast %1$s Gamma %1$s Horiz Position %1$s Horiz Stretch %1$s Refresh Rate %1$s Vert Position %1$s Vert Stretch %1$s Volume %1$s, %2$-.100s %1$s: %2$s
 %1$s: %2$s - Search: %3$s_ %2$s
 %s %s
 added to favorites list. %s
 removed from favorites list. %s [internal] %s added (playing) (recording) **Error saving %s.ini** **Error saving ui.ini** , %s <set up filters> ARGB Settings Activated: %s Activated: %s = %s Add %1$s Folder - Search: %2$s_ Add Folder Add To Favorites Add filter Adstick Device Assignment Advanced Options All All cheats reloaded Analog Controls Analog Controls	Yes
 Any Are you sure you want to quit?

Press ''%1$s'' to quit,
Press ''%2$s'' to return to emulation. Artwork Options Audit ROMs for %1$u machines marked unavailable Audit ROMs for all %1$u machines Auditing ROMs for machine %2$u of %3$u...
%1$s Auto Auto frame skip Auto rotate left Auto rotate right Automatic save/restore BIOS BIOS Selection BIOS selection: Barcode Reader Barcode length invalid! Beam Intensity Weight Beam Width Maximum Beam Width Minimum Bilinear Filtering Bitmap Prescaling Bold Bookkeeping Info Burn-in CPU or RAM Camera	Imperfect
 Camera	Unimplemented
 Cancel Cannot save over directory Change %1$s Folder - Search: %2$s_ Change Folder Changes to this only take effect when "Start new search" is selected Cheat Cheat Comment:
%s Cheat Finder Cheat Name Cheat added to cheat.simple Cheat engine not available Cheat written to %s and added to cheat.simple Cheats Choose from palette Clear Watches Coin %1$c: %2$d%3$s
 Coin %1$c: NA%3$s
 Coin impulse Coin lockout Color preview: Colors Command Completely unemulated features:  Configure Directories Configure Machine Configure Options Confirm quit from machines Controls	Imperfect
 Controls	Unimplemented
 Create Crosshair Offset %1$s Crosshair Offset X %1$1.3f Crosshair Offset Y %1$1.3f Crosshair Options Crosshair Scale %1$s Crosshair Scale X %1$1.3f Crosshair Scale Y %1$1.3f Current %1$s Folders Current time Custom Customize UI Data Format Default Default name is %s Device Mapping Dial Device Assignment Disabled Disabled: %s Disk	Imperfect
 Disk	Unimplemented
 Done Double-click or press %1$s to select Driver Driver is BIOS	No
 Driver is BIOS	Yes
 Driver is Clone of	%1$s
 Driver is Parent	
 Driver is clone of: %1$-.100s Driver is parent Driver: "%1$s" software list  Driver: %1$-.100s Emulated Enabled Enabled: %s Enforce Aspect Ratio Enlarge images in the right panel Enter Code Exit Export displayed list to file Export list in TXT format (like -listfull) Export list in XML format (like -listxml) Export list in XML format (like -listxml, but exclude devices) External DAT View Fast Forward File File Already Exists - Override? File Manager Filter Filter %1$u Flip X Flip Y Folders Setup Fonts Force 4:3 aspect for snapshot display Frame skip GLSL Gameinit General Info General Inputs Graphics	Imperfect
 Graphics	Imperfect Colors
 Graphics	OK
 Graphics	Unimplemented
 Graphics	Wrong Colors
 Graphics: Imperfect,  Graphics: OK,  Graphics: Unimplemented,  Group HLSL Hide Both Hide Filters Hide Info/Image Hide romless machine from available list High Scores History Image Format: Image Information Images Imperfectly emulated features:  Include clones Info auto audit Infos Infos text size Input (general) Input (this Machine) Input Options Italic Joystick Joystick deadzone Joystick saturation Keyboard	Imperfect
 Keyboard	Unimplemented
 Keyboard Inputs	Yes
 Keyboard Mode LAN	Imperfect
 LAN	Unimplemented
 Language Laserdisc '%1$s' Horiz Position Laserdisc '%1$s' Horiz Stretch Laserdisc '%1$s' Vert Position Laserdisc '%1$s' Vert Stretch Left equal to right Left equal to right with bitmask Left equal to value Left greater than value Left less than value Left not equal to right with bitmask Left not equal to value Lightgun Lightgun Device Assignment Lines Load State MAMEinfo MARPScore MESSinfo MHz Machine Configuration Machine Information Mamescore Manufacturer	%1$s
 Master Volume Match block Mechanical Machine	No
 Mechanical Machine	Yes
 Menu Preview Microphone	Imperfect
 Microphone	Unimplemented
 Miscellaneous Options Mouse Mouse	Imperfect
 Mouse	Unimplemented
 Mouse Device Assignment Multi-keyboard Multi-mouse Name:             Description:
 Natural Natural keyboard Network Devices New Barcode: New Image Name: No No category INI files found No groups found in category file No machines found. Please check the rompath specified in the %1$s.ini file.

If this is your first time using %2$s, please see the config.txt file in the docs directory for information on configuring %2$s. None None
 Not supported Number Of Screens Off Offscreen reload On One or more ROMs/CHDs for this machine are incorrect. The machine may not run correctly.
 One or more ROMs/CHDs for this machine have not been correctly dumped.
 Other Controls Overall	NOT WORKING
 Overall	Unemulated Protection
 Overall	Working
 Overall: NOT WORKING Overall: Unemulated Protection Overall: Working Overclock %1$s sound Overclock CPU %1$s Paddle Device Assignment Page Partially supported Pause/Stop Pedal Device Assignment Performance Options Play Play Count Player Please enter a file extension too Plugin Options Plugins Positional Device Assignment Press TAB to set Press any key to continue. Printer	Imperfect
 Printer	Unimplemented
 Pseudo terminals ROM Audit Result	BAD
 ROM Audit Result	OK
 Read this image, write to another image Read this image, write to diff Read-only Read-write Record Reload All Remove %1$s Folder Remove Folder Remove From Favorites Remove last filter Requires Artwork	No
 Requires Artwork	Yes
 Requires CHD	No
 Requires CHD	Yes
 Requires Clickable Artwork	No
 Requires Clickable Artwork	Yes
 Reset Reset All Results will be saved to %1$s Return to Machine Return to Previous Menu Revision: %1$s Rewind Romset	%1$s
 Rotate Rotate left Rotate right Rotation Options Sample Rate Sample text - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Samples Audit Result	BAD
 Samples Audit Result	None Needed
 Samples Audit Result	OK
 Save Save Cheat Save Configuration Save State Screen Screen #%d Screen '%1$s' Screen Orientation	Horizontal
 Screen Orientation	Vertical
 Screen flipping in cocktail mode is not supported.
 Search: %1$s_ Select New Machine Select access mode Select category: Select cheat to set hotkey Select custom filters: Select image format Selection List - Search:  Set Set hotkeys Settings Show All Show DATs view Show mouse pointer Show side panels Simultaneous contradictory Skip BIOS selection menu Skip information screen at startup Skip software parts selection menu Sleep Slider Controls Slot Devices Software is clone of: %1$-.100s Software is parent Software part selection: Sound Sound	Imperfect
 Sound	None
 Sound	OK
 Sound	Unimplemented
 Sound Options Sound: Imperfect Sound: None Sound: OK Sound: Unimplemented Speed Start Out Maximized Start new search State/Playback Options Steadykey Support Cocktail	No
 Support Save	No
 Support Save	Yes
 Supported: No Supported: Partial Supported: Yes Switch Item Ordering Switched Order: entries now ordered by description Switched Order: entries now ordered by shortname Synchronized Refresh Sysinfo System: %1$-.100s Tape Control Test The selected game is missing one or more required ROM or CHD images. Please select a different game.

Press any key to continue. There are known problems with this machine

 This driver requires images to be loaded in the following device(s):  This machine has no BIOS. This machine has no sound hardware, MAME will produce no sounds, this is expected behaviour.
 This machine requires external artwork files.
 This machine was never completed. It may exhibit strange behavior or missing elements that are not bugs in the emulation.
 Throttle Tickets dispensed: %1$d

 Timer Timing	Imperfect
 Timing	Unimplemented
 Total time Trackball Device Assignment Triple Buffering Type Type name or select: %1$s_ Type name or select: (random) UI Color Settings UI Font UI Fonts Settings UI active Unable to write file
Ensure that cheatpath folder exists Undo last search -- # Uptime: %1$d:%2$02d

 Uptime: %1$d:%2$02d:%3$02d

 Use External Samples Use image as background User Interface Value Vector Vector Flicker Video Mode Video Options Visible Delay WAN	Imperfect
 WAN	Unimplemented
 Wait Vertical Sync Watch Window Mode Write X Y Year	%1$s
 Yes [compatible lists] [create] [empty slot] [empty] [failed] [file manager] [no category INI files] [no groups in INI file] [software list] color-channelAlpha color-channelBlue color-channelGreen color-channelRed color-optionBackground color-optionBorder color-optionClone color-optionGraphics viewer background color-optionMouse down background color color-optionMouse down color color-optionMouse over background color color-optionMouse over color color-optionNormal text color-optionNormal text background color-optionSelected background color color-optionSelected color color-optionSlider color color-optionSubitem color color-optionUnavailable color color-presetBlack color-presetBlue color-presetGray color-presetGreen color-presetOrange color-presetRed color-presetSilver color-presetViolet color-presetWhite color-presetYellow color-sampleClone color-sampleMouse Over color-sampleNormal color-sampleSelected color-sampleSubitem default emulation-featureLAN emulation-featureWAN emulation-featurecamera emulation-featurecolor palette emulation-featurecontrols emulation-featuredisk emulation-featuregraphics emulation-featurekeyboard emulation-featuremicrophone emulation-featuremouse emulation-featureprinter emulation-featureprotection emulation-featuresound emulation-featuretiming kHz machine-filterAvailable machine-filterBIOS machine-filterCHD Required machine-filterCategory machine-filterClones machine-filterCustom Filter machine-filterFavorites machine-filterHorizontal Screen machine-filterManufacturer machine-filterMechanical machine-filterNo CHD Required machine-filterNot BIOS machine-filterNot Mechanical machine-filterNot Working machine-filterParents machine-filterSave Supported machine-filterSave Unsupported machine-filterUnavailable machine-filterUnfiltered machine-filterVertical Screen machine-filterWorking machine-filterYear path-optionBosses path-optionCabinets path-optionCategory INIs path-optionControl Panels path-optionCovers path-optionCrosshairs path-optionDATs path-optionFlyers path-optionHowTo path-optionINIs path-optionIcons path-optionLogos path-optionMarquees path-optionPCBs path-optionROMs path-optionScores path-optionSnapshots path-optionSoftware Media path-optionVersus playing recording selmenu-artworkArtwork Preview selmenu-artworkBosses selmenu-artworkCovers selmenu-artworkFlyer selmenu-artworkGame Over selmenu-artworkHowTo selmenu-artworkMarquee selmenu-artworkPCB selmenu-artworkScores selmenu-artworkSnapshots selmenu-artworkVersus software-filterAvailable software-filterClones software-filterCustom Filter software-filterDevice Type software-filterFavorites software-filterParents software-filterPartially Supported software-filterPublisher software-filterRelease Region software-filterSoftware List software-filterSupported software-filterUnavailable software-filterUnfiltered software-filterUnsupported software-filterYear stopped Project-Id-Version: MAME
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-17 07:08+1100
PO-Revision-Date: 2017-10-26 22:51+0100
Last-Translator: pmos69
Language-Team: MAME Language Team
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 

--- INFO de DRIVER ---
Driver:  

Pressione qualquer tecla para continuar 

Existem clones funcionais desta máquina: %s 
    Configuração gravada    

 
Som:
 ESTA MÁQUINA NÃO FUNCIONA. A emulação desta máquina ainda não está completa. Não há nada que possa fazer para corrigir isto excepto aguardar que os programadores melhorem a emulação.
 
Video:
 (padrão)  (travado)  CORES  CANETAS %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Driver: %4$s

CPU:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d máquinas (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d pacotes de software ) Brilho do %1$s Contraste do %1$s Gama do %1$s Posição Horizontal do %1$s Extensão Horizontal do %1$s Taxa de refrescamento do %1$s Posição Vertical do %1$s Extensão Vertical do %1$s %1$s Volume %1$s, %2$-.100s %1$s: %2$s
 %1$s: %2$s - Procura: %3$s_ %2$s
 %s %s
 adicionado à lista de favoritos. %s
 removido da lista de favoritos. %s [interna] %s adicionado (funcionando) (gravando) **Erro ao gravar %s.ini** **Erro ao gravar ui.ini** , %s <configurar filtros> Configurações ARGB Activado: %s Activado: %s = %s Adicionar Pasta %1$s - Procura: %2$s_ Adicionar Pasta Adicionar aos Favoritos Adicionar filtro Atribuição do Dispositivo AD Stick Opções Avançadas Todos Todos os cheats recarregados Controlos Analógicos Controlos Analógicos	Sim
 Qualquer Tem a certeza que deseja sair?

Pressione ''%1$s'' para sair,
Pressione ''%2$s'' para voltar à emulação. Opções de Artwork Auditar ROMs de %1$u máquinas marcadas como indisponíveis Auditar ROMs de todas as %1$u máquinas Auditando ROMs da máquina %2$u de %3$u...
%1$s Automático Descarte automático de Frames Auto rodar à esquerda Auto-rodar à direita Guardar/Restaurar automático BIOS Selecção de BIOS Selecção de BIOS: Leitor de Código de Barras Tamanho de código de barras inválido! Peso da Intensidade do Feixe Largura Máxixa do Feixe de Luz Largura Mínima do Feixe de Luz Filtragem Bi-linear Prescaling de Bitmaps Negrito Info de Contabilidade Burn-in CPU ou RAM Câmara	Imperfeita
 Câmara	Não Implementada
 Cancelar Não pode gravar sobre a pasta Mudar Pasta %1$s - Procura: %2$s_ Mudar Pasta Alterações a isto apenas têm efeito quando "Começar nova procura" está seleccionada Cheat Comentário de Cheat:
%s Cheat Finder Nome da Cheat Cheat adicionada a cheat.simple Motor de cheats não disponível Cheat gravada em %s e adicionada a cheat.simple Cheats Escolha da paleta Limpar Monitorizações Moeda %1$c: %2$d%3$s
 Moeda %1$c: NA%3$s
 Impulso da Moeda Bloqueio de Moedas Pré-Visualização de côr: Cores Comando Funcionalidades não emuladas:  Configurar Pastas Configurar Máquina Configurar Opções Confirmar saída das máquinas Controlos	Imperfeitos
 Controlos	Não Implementados
 Criar Ajuste da Mira %1$s Ajuste X da Mira %1$1.3f Ajuste Y da Mira %1$1.3f Opções de Miras Escala da Mira %1$s Escala X da Mira %1$1.3f Escala Y da Mira %1$1.3f Pastas %1$s Atuais Tempo Actual Personalizado Personalizar Interface Formato de Dados Padrão Nome padrão é %s Mapeamento de Dispositivos Atribuição do Dispositivo Botão Rotativo Desactivado Desligado: %s Disco	Imperfeito
 Disco	Não Implementado
 Efectuado Clique duas vezes ou pressione %1$s para selecionar Driver Driver é BIOS	Não
 Driver é BIOS	Sim
 Driver é Clone de	%1$s
 Driver é Principal	
 O Driver é clone de: %1$-.100s O Driver é original Driver: "%1$s" lista de software  Driver: %1$-.100s Emulado Activo Ligado: %s Forçar Aspecto Aumentar imagens no painel direito Introduza o Código Sair Exportar lista visível para ficheiro Exportar lista em formato TXT (como -listfull) Exportar lista em formato XML (como -listxml) Exportar lista em formato XML (como -listxml, mas excluindo dispositivos) Visualização de DAT Externa Avanço Rápido Ficheiro Ficheiro Já Existente - Sobrepor? Gestôr de Ficheiros Filtro Filtro %1$u Inverter X Inverter Y Configurar Pastas Fontes Forçar aspecto 4:3 na exibição de snapshots Descarte de Frames GLSL Gameinit Info Geral Inputs Gerais Gráficos	Imperfeitos
 Gráficos	Cores Imperfeitas
 Gráficos	OK
 Gráficos	Não Implementados
 Gráficos	Cores Erradas
 Gráficos: Imperfeitos,  Gráficos: OK,  Gráficos: Não Implementados,  Grupo HLSL Esconder Ambos Esconder Filtros Esconder Info/Imagem Esconder máquinas sem ROMs da lista de disponíveis Recordes Histórico Formato da Imagem: Informação da Imagem Imagens Funcionalidades emuladas de forma imperfeita:  Incluir clones Auditoria automática no painel de Info Infos Tamanho do texto das Infos Input (geral) Input (esta Máquina) Opções de Input Itálico Joystick Zona morta do Joystick Saturação do Joystick Teclado	Imperfeito
 Teclado	Não Implementado
 Entradas de Teclado	Sim
 Modo do Teclado Rede Local	Imperfeita
 Rede Local	Não Implementada
 Idioma Posição Horizontal do Laserdisc '%1$s' Extensão Horizontal do Laserdisc '%1$s' Posição Vertical do Laserdisc '%1$s' Extensão Vertical do Laserdisc '%1$s' Esquerda igual a direita Esquerda igual a direita com bitmask Esquerda igual a valor Esquerda maior que valor Esquerda menor que valor Esquerda diferente da direita com bitmask Esquerda diferente de valor Pistola de Luz Atribuição do Dispositivo Pistola de Luz Linhas Carregar Estado MAMEinfo MARPScore MESSinfo MHz Configuração da Máquina Informação da Máquina MAMEscore Fabricante	%1$-.100s
 Volume Global Match bloco Máquina Mecânica	Não
 Máquina Mecânica	Sim
 Pré-Visualização do Menu Microfone	Imperfeito
 Microfone	Não Implementado
 Opções Diversas Rato Rato	Imperfeito
 Rato	Não Implementado
 Atribuição do Dispositivo Rato Teclado Múltiplo Rato Múltiplo Nome:             Descrição:
 Natural Teclado Natural Dispositivos de Rede Novo Código de Barras: Novo Nome da Imagem: Não Não foram encontrados ficheiros INI da categoria Não foram encontrados grupos no ficheiro da categoria Nenhuma máquina encontrada. Por favor verifique a rompath especificada no ficheiro %1$s.ini .

Se esta é a sua primeira vez que usa %2$s, por favor veja o ficheiro config.txt no diretório docs para informação sobre a configuração de %2$s. Nenhum Nenhum
 Não suportado Número de Ecrãs Desligado Recarregar fora de ecrã Ligado Uma ou mais ROMs/CHDs para esta máquina estão incorretas. A máquina poderá não funcionar corretamente.
 Uma ou mais ROMs/CHDs para esta máquina não foram corretamente descarregadas.
 Outros Controlos Global	NÃO FUNCIONAL
 Global	Proteção não Emulada
 Global	Funcional
 Global: NÃO FUNCIONA Global: Proteção Não Emulada Global: Funcional Overclock %1$s som Overclock CPU %1$s Atribuição do Dispositivo Paddle Página Parcialmente suportado Pausa/Parar Atribuição do Dispositivo Pedal Opções de Performance Play Play Count Jogador Por favor digite também uma extensão para o ficheiro Opções de Plugins Plugins Atribuição do Dispositivo Posicional Pressione TAB para definir Pressione qualquer tecla para continuar Impressora	Imperfeita
 Impressora	Não Implementada
 Pseudo terminais Resultado da Audit à ROM	FALHA
 Resultado da Audit à ROM	OK
 Ler esta imagem, escrever para outra imagem Ler esta imagem, escrever para diff Apenas-leitura Leitura-e-escrita Gravar Recarregar Tudo Remover Pasta %1$s Remover Pasta Remover dos Favoritos Remover último filtro Requer Artwork	Não
 Requer Artwork	Sim
 Requer CHD	Não
 Requer CHD	Sim
 Requer Artwork Clicável	Não
 Requer Artwork Clicável	Sim
 Reiniciar Reiniciar Tudo Os resultados serão gravados em %1$s Voltar à Máquina Voltar ao Menu Anterior Revisão: %1$s Rebobinar Romset	%1$s
 Rodar Rodar para a esquerda Rodar para a direita Opções de Rotação Frequência de Amostragem Exemplo de texto - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Resultado da Audit às Samples	FALHA
 Resultado da Audit às Samples	Nenhuma Necessária
 Resultado da Audit às Samples	OK
 Gravar Gravar Cheat Gravar Configuração Gravar Estado Ecrã Ecrã #%d Ecrã '%1$s' Orientação de Ecrã	Horizontal
 Orientação de Ecrã	Vertical
 Inversão de ecrã não é suportada em modo cocktail.
 Procura: %1$s_ Seleccionar Nova Máquina Seleccionar modo de acesso Selecione categoria Seleccione cheat para atribuir hotkey Selecionar filtros personalizados: Seleccione o formato da imagem Lista de Seleção - Procurar:  Fixar Atribuir hotkeys Configurações Mostrar Tudo Exibir visualização de DATs Mostrar ponteiro do rato Mostrar painéis laterais Contraditório Simultâneo Saltar menu de selecção de BIOS Saltar ecrã de informação ao iniciar Saltar menu de selecção de item de software Dormir Controlos Deslizantes Dispositivos de Slot O Software é clone de: %1$-.100s O Software é original Selecção de Item de Software: Som Som	Imperfeito
 Som	Sem Som
 Som	OK
 Som	Não Implementado
 Opções de Som Som: Imperfeito Som: Sem Som Som: OK Som: Não Implementado Velocidade Começar Maximizado Começar nova procura Opções de Estado/Reprodução Steadykey Suporta Cocktail	Não
 Suporta Gravação	Não
 Suporta Gravação	Sim
 Suportado: Não Suportado: Parcial Suportado: Sim Alterar Ordem dos Items Ordem Alterada: Entradas agora ordenadas por descrição Ordem Alterada: Entradas agora ordenadas por nom curto Refrescamento Sincronizado Sysinfo Sistema: %1$-.100s Controle da Cassete Teste O jogo selecionado tem uma ou mais ROM ou imagem CHD em falta. Por favor selecione um jogo diferente.

Pressione qualquer tecla para continuar. Existem problemas conhecidos com esta máquina

 Este driver necessita de imagens carregadas no(s) seguinte(s) dispositivo(s):  Esta máquina não tem BIOS. Esta máquina não tem hardware de som. O MAME não irá produzir som. Isto é o comportamento esperado.
 Esta máquina requer ficheiros de artwork externos.
 Esta máquina não foi acabada. Poderá ter um comportamento estranho ou elementos em falta que não são bugs na emulação.
 Controlo de Velocidade Tickets emitidos: %1$d

 Temporizador Temporização	Imperfeita
 Temporização	Não Implementada
 Tempo Total Atribuição do Dispositivo Trackball Triple Buffering Tipo Digite o nome ou selecione: %1$s_ Digite o nome ou selecione: (aleatório) Configurações de Cores da Interface Fonte da interface Configurações das Fontes Interface activa Não foi possivel escrever ficheiro
Assegurar que a pasta cheatpath existe Undo da última procura -- # Tempo de execução: %1$d:%2$02d

 Tempo de execução: %1$d:%2$02d:%3$02d

 Utilizar Samples Externas Usar imagem como fundo Interface do Utilizador Valor Vetorial Cintilação Vetorial Modo de Video Opções de Video Atraso Visível WAN	Imperfeita
 WAN	Não Implementada
 Esperar pelo Sync Vertical Monitorizar Modo de Janela Escrever X Y Ano	%1$s
 Sim [listas compatíveis] [criar] [slot vazio] [vazio] [falhou] [gestôr de ficheiros] [sem ficheiros INI de categoria] [sem grupos no ficheiro INI] [lista de software] Opacidade Azul Verde Vermelho Fundo Borda Clone Fundo do visualizador de gráficos Cor de fundo do rato-premido Cor do rato-premido Cor de fundo do rato-sobre Cor do rato-sobre Texto normal Fundo do texto normal Côr de fundo seleccionada Côr seleccionada Cor dos controlos deslizantes Côr do sub-item Côr indisponível Preto Azul Cinzento Verde Laranja Vermelho Prateado Violeta Branco Amarelo Clone Rato-Sobre Normal Seleccionado Sub-Item padrão rede local WAN câmara paleta de cores controlos disco gráficos teclado microfone rato impressora protecção som temporização kHz Disponível BIOS Requer CHD Categoria Clones Filtro Personalizado Favoritos Ecrã Horizontal Fabricante Mecânico Não Requer CHD Não BIOS Não Mecânico Não Funcional Originais Gravação Suportada Gravação Não Suportada Não disponível Não filtrado Ecrã Vertical Funcional Ano Bosses Armários Arcade INIs de Categorias Painéis de Controlo Capas Miras DATs Folhetos Como Fazer INIs Ícones Logos Tabuletas Placas Mãe ROMs Placares Snapshots Ficheiros de Software Versus funcionando gravando Pré-Visualização da Artwork Bosses Capas Folheto Fim de Jogo Como Fazer Tabuleta Placa Mãe Placares Snapshots Versus Disponível Clones Filtro Personalizado Tipo de Dispositivo Favoritos Originais Parcialmente Suportado Editor Região do Lançamento Lista de Software Suportado Não disponível Não filtrado Não Suportado Ano parado 