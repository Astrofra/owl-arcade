��    �     t              �     �  .   �          7  �   @     �  
     	                	   &     0     7  	   ?     I     Q  "   X  	   {  .   �  +   �     �     �  
   �     	          0     B     U     g     s     �     �     �      �     �  	   �     �     �          '     5  
   U     `  
   q     |     �     �     �  ^   �     *     :     ?     O     `     r     �     �     �     �     �     �     �                %     6     >  "   Y     |     �     �     �     �     �     �     �     �     �                    3     E     W     r     y     �     �     �     �     �                  5      B      Q      h   $   q      �      �      �      �      �      �      !     (!     1!     9!  !   N!  
   p!     {!     �!  *   �!  )   �!  >   �!     2"     D"     Q"     V"     v"     �"     �"     �"     �"     �"  %   �"  
   �"     �"     �"     �"     �"     #     #     6#     C#     Y#     h#  	   m#     w#     �#  (   �#     �#     �#     �#     �#     �#     �#     $     $     "$     7$     E$     L$     U$     g$     {$     �$     �$     �$     �$     �$     %     %     2%     8%     <%     R%  	   f%     p%     �%     �%     �%     �%     �%     �%     �%     �%     &     &     &&     6&     C&     S&  �   V&     $'     *'     8'     J'     N'     _'  Y   b'  G   �'     (     (     ((     G(     X(     m(     �(     �(     �(     �(  
   �(     �(      )     )  !   )     ;)     J)     R)     o)     �)  '   �)     �)  	   �)  
   �)     �)  
   �)     �)     *      *     6*     I*  	   O*     Y*     k*     �*     �*     �*     �*     �*     �*     �*     �*  F   �*     *+     =+  
   D+     O+     ]+     |+  3   �+     �+     �+     �+     
,     ,     8,     A,     J,     Y,     l,     },     �,  "   �,  "   �,     �,     �,     -     -     :-     M-     f-     l-  	   }-     �-     �-     �-  	   �-     �-     �-     �-     �-  	   .     .     #.     6.     E.     Z.     o.     w.     �.  ,   �.  E   �.  ]   	/  z   g/     �/     �/     0     !0     20     M0     k0     }0     �0  	   �0     �0     �0     �0     �0     1     1     1  
   &1     11     ?1     M1     `1     l1     n1  
   p1     {1     1     �1     �1     �1     �1     �1     �1     �1     �1     �1     2     %2     =2     Q2  '   d2  (   �2     �2  (   �2     �2     3  #   33  &   W3     ~3     �3     �3     �3     �3     4     4     %4     84     L4     ]4     q4     �4     �4     �4     �4     �4     �4     5     5     5     "5     :5     V5     j5     ~5     �5     �5     �5     �5     �5     �5     6     !6     26     D6     V6     k6     |6     �6     �6     �6     �6  	   �6     �6     �6     7     )7     ?7     Y7     o7     �7     �7     �7     �7     �7     �7     8     78     L8  �  T8  "   :  /   1:      a:     �:  �   �:     F;     N;     V;     b;     k;  	   s;     };     �;  	   �;     �;     �;  3   �;  	   �;  *   �;  )   <     8<     G<  
   V<     a<     z<     �<     �<     �<     �<     �<     �<     
=  "   =  )   3=     ]=     i=     w=  *   �=  *   �=     �=  *   �=     >     .>     G>     \>     y>     �>     �>  y   �>     >?     \?     e?     �?     �?  !   �?     �?     �?      @     @     :@     X@     v@     �@     �@     �@     �@     �@  (   �@     A     &A     .A  	   FA     PA     kA     �A     �A     �A     �A     �A     �A     �A     �A     B  /   B     NB     [B  $   {B  $   �B     �B     �B     �B     C     2C     IC     bC     qC     �C  9   �C     �C  $   �C     	D  3   #D     WD  (   oD     �D  	   �D     �D     �D  !   �D     E  	   E  ,    E  4   ME  3   �E  T   �E     F     F     3F  !   9F     [F     iF     qF     �F     �F     �F  ?   �F     �F     �F     G     G  %    G     FG     VG     nG     {G     �G     �G     �G     �G     �G  6   �G  
   H     %H     5H     HH     OH     lH     sH     �H     �H     �H     �H     �H     �H     �H     I     #I  (   )I  (   RI  )   {I  )   �I     �I  #   �I     J     	J     J     !J  	   4J     >J     MJ     ZJ     kJ     �J     �J     �J     �J     �J     �J     �J     K     #K     2K     AK    EK     [L     bL     rL     �L     �L     �L  n   �L  F   M     ^M     rM  "   �M     �M     �M  "   �M     N     N     8N     VN     nN     |N     �N  	   �N     �N     �N     �N  !   O      )O     JO  1   _O  (   �O     �O     �O  	   �O     �O     �O     P     -P     LP     lP     ~P     �P     �P     �P     �P     �P  	   �P     �P     Q     Q     /Q  H   @Q     �Q  
   �Q     �Q     �Q     �Q      �Q  2   R     AR     VR     vR     �R  !   �R     �R     �R     �R     S     S     6S  "   OS  3   rS  -   �S     �S     �S     �S  +   
T     6T     FT     bT     gT  	   tT     ~T     �T     �T  	   �T     �T  	   �T     �T  &   �T     %U     5U     FU     ^U     pU     �U     �U     �U     �U  +   �U  [   
V  e   fV  �   �V  
   VW     aW  "   zW     �W  (   �W  4   �W  !   X     6X  )   NX     xX     �X  !   �X     �X      �X     Y     Y     !Y  	   2Y     <Y     PY     bY     tY     �Y     �Y  	   �Y     �Y     �Y     �Y     �Y     �Y     �Y     �Y     �Y     Z     Z     Z     Z     Z     'Z     -Z     3Z     PZ     iZ     yZ     �Z     �Z     �Z     �Z     �Z     �Z     [     [     3[     :[     ?[     G[     M[     U[     ][     d[     k[     r[     y[  	   [     �[     �[     �[     �[     �[  
   �[     �[     �[     �[     �[     �[     �[  	   �[     \     \  	   \     !\     (\     4\     ;\  	   B\     L\     R\     V\     b\     q\     w\     �\     �\     �\  	   �\     �\     �\     �\     �\     �\     �\     �\     �\     ]     ]     ]     *]     .]   

Press any key to continue 

There are working clones of this machine: %s 
    Configuration saved    

 
Sound:
 
THIS MACHINE DOESN'T WORK. The emulation for this machine is not yet complete. There is nothing you can do to fix this problem except wait for the developers to improve the emulation.
 
Video:
  (default)  (locked)  COLORS  PENS %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Driver: %4$s

CPU:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d machines (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d software packages ) %1$s Brightness %1$s Contrast %1$s Gamma %1$s Horiz Position %1$s Horiz Stretch %1$s Refresh Rate %1$s Vert Position %1$s Vert Stretch %1$s Volume %1$s, %2$-.100s %1$s: %2$s
 %2$s
 %s
 added to favorites list. %s
 removed from favorites list. %s [internal] (playing) (recording) **Error saving %s.ini** **Error saving ui.ini** ARGB Settings Add %1$s Folder - Search: %2$s_ Add Folder Add To Favorites Add filter Adstick Device Assignment Advanced Options All cheats reloaded Analog Controls Are you sure you want to quit?

Press ''%1$s'' to quit,
Press ''%2$s'' to return to emulation. Artwork Options Auto Auto frame skip Auto rotate left Auto rotate right Automatic save/restore BIOS Selection Barcode Reader Barcode length invalid! Beam Intensity Weight Beam Width Maximum Beam Width Minimum Bilinear Filtering Bitmap Prescaling Bold Bookkeeping Info Burn-in Cannot save over directory Change %1$s Folder - Search: %2$s_ Change Folder Cheat Cheat Comment:
%s Cheats Choose from palette Coin %1$c: %2$d%3$s
 Coin %1$c: NA%3$s
 Coin impulse Coin lockout Color preview: Colors Command Configure Directories Configure Machine Configure Options Confirm quit from machines Create Crosshair Offset %1$s Crosshair Offset X %1$1.3f Crosshair Offset Y %1$1.3f Crosshair Options Crosshair Scale %1$s Crosshair Scale X %1$1.3f Crosshair Scale Y %1$1.3f Current %1$s Folders Customize UI Device Mapping Dial Device Assignment Disabled Double-click or press %1$s to select Driver Driver is Clone of	%1$s
 Driver is Parent	
 Driver is clone of: %1$-.100s Driver is parent Driver: "%1$s" software list  Driver: %1$-.100s Emulated Enabled Enforce Aspect Ratio Enlarge images in the right panel Enter Code Exit Export displayed list to file Export list in TXT format (like -listfull) Export list in XML format (like -listxml) Export list in XML format (like -listxml, but exclude devices) External DAT View Fast Forward File File Already Exists - Override? File Manager Filter Flip X Flip Y Folders Setup Fonts Force 4:3 aspect for snapshot display Frame skip GLSL Gameinit General Info General Inputs Graphics	Imperfect
 Graphics	Imperfect Colors
 Graphics	OK
 Graphics: Imperfect,  Graphics: OK,  HLSL Hide Both Hide Filters Hide Info/Image Hide romless machine from available list History Image Format: Image Information Images Info auto audit Infos Infos text size Input (general) Input (this Machine) Input Options Italic Joystick Joystick deadzone Joystick saturation Keyboard Mode Language Laserdisc '%1$s' Horiz Position Laserdisc '%1$s' Horiz Stretch Laserdisc '%1$s' Vert Position Laserdisc '%1$s' Vert Stretch Lightgun Lightgun Device Assignment Lines MHz Machine Configuration Machine Information Mamescore Manufacturer	%1$s
 Master Volume Menu Preview Miscellaneous Options Mouse Mouse Device Assignment Multi-keyboard Multi-mouse Name:             Description:
 Natural Natural keyboard Network Devices New Barcode: New Image Name: No No machines found. Please check the rompath specified in the %1$s.ini file.

If this is your first time using %2$s, please see the config.txt file in the docs directory for information on configuring %2$s. None
 Not supported Number Of Screens Off Offscreen reload On One or more ROMs/CHDs for this machine are incorrect. The machine may not run correctly.
 One or more ROMs/CHDs for this machine have not been correctly dumped.
 Other Controls Overall	NOT WORKING
 Overall	Unemulated Protection
 Overall	Working
 Overall: NOT WORKING Overall: Unemulated Protection Overall: Working Overclock CPU %1$s Paddle Device Assignment Partially supported Pause/Stop Pedal Device Assignment Performance Options Play Please enter a file extension too Plugin Options Plugins Positional Device Assignment Press TAB to set Pseudo terminals Read this image, write to another image Read this image, write to diff Read-only Read-write Record Reload All Remove %1$s Folder Remove Folder Remove From Favorites Remove last filter Reset Reset All Return to Machine Return to Previous Menu Revision: %1$s Rewind Romset	%1$s
 Rotate Rotate left Rotate right Rotation Options Sample Rate Sample text - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Save Configuration Screen Screen #%d Screen '%1$s' Screen Orientation	Horizontal
 Screen Orientation	Vertical
 Screen flipping in cocktail mode is not supported.
 Select New Machine Select access mode Select custom filters: Select image format Selection List - Search:  Settings Show All Show DATs view Show mouse pointer Show side panels Simultaneous contradictory Skip BIOS selection menu Skip information screen at startup Skip software parts selection menu Sleep Slider Controls Slot Devices Software is clone of: %1$-.100s Software is parent Software part selection: Sound Sound	Imperfect
 Sound	OK
 Sound	Unimplemented
 Sound Options Sound: Imperfect Sound: OK Sound: Unimplemented Speed Start Out Maximized State/Playback Options Steadykey Supported: No Supported: Partial Supported: Yes Switch Item Ordering Synchronized Refresh Sysinfo System: %1$-.100s Tape Control There are known problems with this machine

 This driver requires images to be loaded in the following device(s):  This machine has no sound hardware, MAME will produce no sounds, this is expected behaviour.
 This machine was never completed. It may exhibit strange behavior or missing elements that are not bugs in the emulation.
 Throttle Tickets dispensed: %1$d

 Trackball Device Assignment Triple Buffering Type name or select: %1$s_ Type name or select: (random) UI Color Settings UI Font UI Fonts Settings UI active Uptime: %1$d:%2$02d

 Uptime: %1$d:%2$02d:%3$02d

 Use External Samples Use image as background User Interface Vector Vector Flicker Video Mode Video Options Visible Delay Wait Vertical Sync Window Mode X Y Year	%1$s
 Yes [compatible lists] [create] [empty slot] [empty] [failed] [file manager] [software list] color-channelAlpha color-channelBlue color-channelGreen color-channelRed color-optionBackground color-optionBorder color-optionClone color-optionGraphics viewer background color-optionMouse down background color color-optionMouse down color color-optionMouse over background color color-optionMouse over color color-optionNormal text color-optionNormal text background color-optionSelected background color color-optionSelected color color-optionSlider color color-optionSubitem color color-optionUnavailable color color-presetBlack color-presetBlue color-presetGray color-presetGreen color-presetOrange color-presetRed color-presetSilver color-presetViolet color-presetWhite color-presetYellow color-sampleClone color-sampleMouse Over color-sampleNormal color-sampleSelected color-sampleSubitem default kHz machine-filterCategory machine-filterManufacturer machine-filterYear path-optionArtwork path-optionBosses path-optionCabinets path-optionControl Panels path-optionCovers path-optionCrosshairs path-optionDATs path-optionFlyers path-optionHowTo path-optionINIs path-optionIcons path-optionLogos path-optionMarquees path-optionPCBs path-optionROMs path-optionScores path-optionSnapshots path-optionVersus playing recording selmenu-artworkArtwork Preview selmenu-artworkBosses selmenu-artworkCovers selmenu-artworkFlyer selmenu-artworkGame Over selmenu-artworkHowTo selmenu-artworkMarquee selmenu-artworkPCB selmenu-artworkScores selmenu-artworkSnapshots selmenu-artworkVersus software-filterDevice Type software-filterPublisher software-filterSoftware List software-filterYear stopped Project-Id-Version: MAME
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-17 07:08+1100
PO-Revision-Date: 2017-05-20 09:18+0200
Last-Translator: Delirious <zsoltn71@gmail.com>
Language-Team: MAME Language Team
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Loco-Source-Locale: hu_HU
X-Generator: Poedit 1.8.11
X-Loco-Parser: loco_parse_mo
 

Nyomj egy gombot a folytatáshoz 

Ennek a gépnek vannak működő klónjai: %s 
    Beállítás elmentve    

 
Hang:
 
EZ A GÉP NEM MŰKÖDIK. A gép emulációja nem teljes még. A hiba javítása érdekében nem lehet semmit tenni, csupán várni, hogy a fejlesztők tökéletesítsék az emulációt.
 
Kép:
  (alap)  (zárolva)  SZÍNEK  TOLLAK %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Vezérlőprogram: %4$s

Processzor:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d gép (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d szoftver csomag ) %1$s Fényerő %1$s Kontraszt %1$s Gamma %1$s Vízszintes helyzet %1$s Vízszintes nyújtás %1$s Képfrissítés %1$s Függőleges helyzet %1$s Függőleges nyújtás %1$s Hangerő %1$s, %2$-.100s %1$s: %2$s
 %2$s
 %s
 felvéve a Kedvencek listára. %s
 eltávolítva a Kedvencek listáról. %s [belső] (lejátszás) (rögzítés) **%s.ini mentése közben hiba történt** **ui.ini mentése közben hiba történt** ARGB beállítások %1$s mappa létrehozása - Keresés: %2$s_ Mappa létrehozása Felvétel a Kedvencekhez Szűrő beállítás ADStick eszköz beállítás Haladó beállítások Minden csalás újra betöltve Analóg irányítók Biztos ki akarsz lépni?

Nyomj ''%1$s'' gombot a kilépéshez,
Nyomj ''%2$s'' gombot, hogy visszatérj az emulációhoz. Illusztráció beállítások Automata Automata képkocka kihagyás Automatikus forgatás balra Automatikus forgatás jobbra Automata mentés/visszaállítás BIOS választás Vonalkód olvasó Érvénytelen vonalkód hossz! Sugár erősség mértéke Maximális sugár szélesség Minimális sugár szélesség Bilineáris szűrés Bittérkép lágyítása Félkövér Nyilvántartási infó Égés Nem menthető el a könyvtárba %1$s mappára váltás - Keresés: %2$s_ Mappa váltás Csalás Csalás megjegyzés:
%s Csalások Választás a palettából Érme %1$c: %2$d%3$s
 Érme %1$c: NA%3$s
 Érme hatás Érme zárolás Szín előnézet: Színek Parancs Könyvtár beállítások Gép beállítások Beállítási lehetőségek Gépekből történő kilépés megerősítése Létrehozás Célkereszt kiegyenlítés %1$s Célkereszt kiegyenlítés X %1$1.3f Célkereszt kiegyenlítés Y %1$1.3f Célkereszt beállítások Célkereszt skála %1$s Célkereszt skála X %1$1.3f Célkereszt skála Y %1$1.3f Jelenlegi %1$s mappák Felület testre szabása Gomb kiosztás Dial eszköz gombkiosztása Kikapcsolva Dupla kattintás vagy %1$s megnyomása a kiválasztáshoz Vezérlőprogram Vezérlőprogram klónja ennek	%1$s
 A vezérlőprogram Alap	
 A vezérlőprogram a következő klónja: %1$-.100s A vezérlőprogram alap Vezérlőprogram: "%1$s" szoftver lista  Illesztőprogram: %1$-.100s Emulálva Bekapcsolva Kényszerített képarány Képek nagyítása a jobb panelen Kód megadása Kilépés A megjelenített lista exportálása fájlba Lista exportálása TXT formátumba (mint -listfull) Lista exportálása XML formátumba (mint -listxml) Lista exportálása XML formátumba (mint -listxml, de belefoglalva az eszközöket) Külső DAT nézet Gyors előretekerés Fájl Fájl már lézeik - Felülírod? Fájl kezelő Szűrő X elfordítás Y elfordítás Mappa beállítások Betűk Kényszerített 4:3 képarány pillanatkép megjelenítéséhez Képkocka kihagyás GLSL Gameinit Általános infók Általános irányítás beállítás Grafika	Hibás
 Grafika	Hibás színek
 Grafika	Jó
 Grafika: Hibás,  Grafika: Jó,  HLSL Mindkettő elrejtése Szűrők elrejtése Infó / kép elrejtése Rom nélküli gépek elrejtése a meglévő listából Történet Kép formátum: Kép információk Képek Automata ellenőrzési infó Infók Infó szöveg mérete Irányítás (általános) Irányítás (ez a gép) Irányítás beállítások Dőlt Joystick Joystick holtsáv Joystick szaturáció Billenytűzet mód Nyelv Lézerlemez '%1$s' vízszintes pozíció Lézerlemez '%1$s' vízszintes nyújtás Lézerlemez '%1$s' függőleges pozíció Lézerlemez '%1$s' függőleges nyújtás Fénypisztoly Fénypisztoly eszköz gombkiosztás Vonalak MHz Gép beállítások Gép információk Mamescore Gyártó	%1$s
 Fő hangerő Menü előnézet Egyéb beállítások Egér Egér eszköz gombkiosztása Osztott billentyűzet Osztott egér Név:             Leírás:
 Természetes Természetes billentyűzet Hálózati eszközök Új vonalkód: Új kép neve: Nem Gép nem található. %1$s.ini fájlban megadott rom elérési út ellenőrzése szükséges.

%2$s első alkalommal történő használata esetén, tekintsd meg a docs könvtárban található config.txt fájlt %2$s beállításokkal kapcsolatos bővebb információk végett. Nincs
 Nem támogatott Képernyők száma Ki Újratöltés képen kívül Be Egy vagy több ROM/CHD nem megfelelő ehhez a géphez. A gép valószínűleg nem fog megfelelően működni.
 Egy vagy több ROM/CHD letárolása nem megfelelő ennél a gépnél.
 Egyéb irányítók Összegezve	NEM MŰKÖDIK
 Összegezve	Emulálatlan védelem
 Összegezve	Működik
 Összegezve: NEM MŰKÖDIK Összegezve: Emulálatlan védelem Összegezve: Működik Processzor túlhajtás %1$s Paddle eszköz gombkiosztása Részlegesen támogatva Szünet/Állj Pedál eszköz gombkiosztása Teljesítmény beállítások Indítás Adj meg fájlkiterjesztést is Beépülő beállítások Beépülők Positional eszköz gombkiosztása Beállítás a TAB billentyűvel Pszeudó terminálok A kép beolvasása és mentése egy másik képbe A kép beolvasása és mentése másikba Csak olvasható Olvasható-írható Felvétel Mind újratöltése %1$s mappa eltávolítása Mappa eltávolítása Eltávolítás a kedvencekből Utolsó szűrő eltávolítása Alapra állítás Mindet alapra Vissza a géphez Vissza az előző menübe Revízió: %1$s Visszatekerés Romkészlet	%1$s
 Forgatás Forgatás balra Forgatás jobbra Elforgatási beállítások Mintavételezés Minta szöveg - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Beállítások mentése Képernyő Képernyő #%d Képernyő '%1$s' Kép igazítottság	Vízszintes Kép igazítottság	Függőleges Képtükrözés a vegyes módban nem támogatott.
 Új gép választás Hozzáférési mód választás Egyéni szűrő választás: Kép formátum választás Kiválasztási lista - Keresés:  Beállítások Mindet mutat DAT nézet mutatása Egérmutató megjelenítése Oldal panelek mutatása Folyamatos ellentmondás BIOS választási menü kihagyása Indításkori tájékoztató képernyő kihagyása  Szoftver elemek választási menü kihagyása Alvás Csúszka irányítók Behelyezhető eszközök A szoftver a következő klónja: %1$-.100s A szoftver alap Szoftver rész választás: Hang Hang	Hibás
 Hang	Jó
 Hang	nincs beépítve
 Hang beállítások Hang: Hibás Hang: Jó Hang: nincs beépítve Sebesség Indítás teljes képernyőn Mentés/visszajátszás beállítások Gyorsbillentyű Támogatás: Nem Támogatás: Részleges Támogatás: Igen Elem sorrend beállítása Szinkronizált frissítés Rendszer infó Rendszer: %1$-.100s Kazetta irányítók A géppel kapcsolatos hibák közismertek

 Az illesztő számára szükségesek a következő eszköz(ök) által betöltött képek:  A gép nem rendelkezik hang hardverrel, MAME nem fog hangot szolgáltatni, ez az elvárt működés.
 Ez a gép nem volt teljesen befejezve. Furcsa működést produkálhat vagy összetevők hiányozhatnak, amelyek nem emulációs hibák.
 Lassítás Kiosztott jegyek: %1$d

 Hanyattegér eszköz beállítása Háromszoros pufferelés Név beírása vagy kiválasztás: %1$s_ Név beírása vagy kiválasztás: (véletlenszerű) Felület színeinek beállítása Felület betűkészlete Felület betűkészleteinek beállítása Felület aktív Játékidő: %1$d:%2$02d

 Játékidő: %1$d:%2$02d:%3$02d

 Külső minták használata Kép beállítása háttérként Felhasználói felület Vektor Vektor villogás Kép mód Kép beállítások Látható késés V-sync-re várás Ablakban futtatás X Y Év	%1$s
 Igen [kompatibilitási lista] [létrehozás] [üres hely] [üres] [sikertelen] [fájl kezelő] [szoftver lista] Alpha Kék Zöld Vörös Háttér Keret Klón Grafikai betekintő háttere Mousedown háttér szín Mousedown szín Mouseover háttér szín Mouseover szín Normál szöveg Normál szöveg háttér Választott háttérszín Választott szín Csúszka színe Segédeszköz szín Nem használható szín Fekete Kék Szürke Zöld Narancs Vörös Ezüst Ibolya Fehér Sárga Klón Mouseover Normál Kiválasztva Segédeszköz alap kHz Kategória Gyártó Év Illusztrációk Boss Játékgép Kezelő pult Borítók Célkeresztek DAT Plakátok Hogyan INI fájlok Ikonok Logók Matricák PCB-k ROM Eredmények Pillanatképek Ellen játszva felvéve Illusztráció előnézet Boss Borítók Plakát A játék végetért Hogyan Matrica PCB Eredmények Pillanatképek Ellen Eszköz fajtája Kiadó Szoftver lista Év leállítva 