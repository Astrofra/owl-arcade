��    �     �-              <[     =[     \[  .   x[     �[  �   �[     p\  �   y\     3]     <]     D]     _]  #   v]  !   �]  #   �]  #   �]  #   ^  !   (^  "   J^  +   m^  +   �^     �^     �^  
   �^  	   	_     _     _  	   !_     +_     2_  	   :_     D_     L_  "   S_  	   v_  .   �_  +   �_     �_     �_     �_     `  
   `     %`     9`     L`     ^`     q`     �`     �`     �`     �`     �`     �`     �`     �`     �`      a  
   4a     ?a     Ma  #   Va  #   za     �a     �a     �a  	   �a     �a  3   �a     b     "b     =b     Ub     ib     {b  	   �b  	   �b     �b     �b     �b     �b     �b     �b     �b     c     c  
   >c     Ic     Zc  
   nc     yc  "   �c     �c     �c     �c     �c     �c  3   d     7d     Dd     Td     id  ^   md     �d  /   �d      e     -e  .   9e     he     me     }e     �e     �e     �e  	   �e     �e  /   �e     f     f     f     /f     >f     Vf     df     zf     �f     �f      �f     �f     �f     �f     �f  
   g     g     !g     7g  :   >g  &   yg     �g     �g     �g  "   �g     h  D   h     [h     ah     sh  
   �h     �h     �h  -   �h     �h     �h     i     i     .i     Ai     Ni     [i     ji     qi     yi     �i      �i  
   �i     �i     �i     j     j     *j     Ej     Yj     qj     xj     �j     �j     �j     �j     �j     k     k     4k     Ak     Hk     Uk     bk     nk     vk  B   �k     �k     �k  
   �k     �k     l     l     #l     7l  *   <l  $   gl     �l     �l     �l     �l     �l     �l     m     m     3m     Em     Zm     cm     km     wm  !   �m  
   �m     �m  $   �m     �m     �m     n  *   "n  )   Mn  >   wn     �n     �n     �n     o     o     o     6o     Co     Jo     Vo     ]o     do     ro  %   xo  
   �o     �o     �o     �o     �o     �o     �o     �o     p     p     +p     Bp     Xp     gp     �p     �p  	   �p     �p     �p  (   �p     �p     �p     �p     �p     �p     q     q     !q     Aq     Pq     `q     fq     vq     |q     �q     �q      �q     �q     �q     �q     �q     r     r     +r     ?r     Wr     lr     zr     �r     �r     �r     �r     �r     s     !s     1s      Es     fs     zs     �s     �s     �s     �s  $   �s     t     )t     2t     Mt  
   St     ^t     jt  	   st     }t     �t     �t     �t     �t     �t     �t     �t     u  	   &u     0u  !   7u     Yu     lu     zu     �u     �u     �u     �u     �u     �u     v     v     6v     Lv     Rv     cv     xv     �v     �v     �v     �v     �v     �v     �v     w     w     w     /w     2w      Nw  �   ow     =x     Nx     cx     wx     |x     �x     �x  '   �x  (   �x     �x  
   �x     y     y  	   y  Y    y  G   zy     �y     �y     �y     �y     z     z     3z     Rz     cz     xz     �z     �z     �z     �z     �z  
   �z  
   �z     �z  !   {  ,   *{  -   W{  $   �{     �{     �{  
   �{     �{     �{  !   �{     |     |     #|     @|     V|     l|     �|     �|     �|     �|     �|     �|  <   }     B}  2   ]}     �}     �}     �}     �}     �}     �}  ,   ~     0~     F~     [~  '   {~     �~  	   �~  
   �~     �~  
   �~     �~     �~     
        �   3  �   �     T�     i�     �     ��     ��     ��     �  	   �     �     �     &�     8�     P�     h�     w�     ~�     ��     ��     ��     ��     ��     ́  F   ؁     �  !   9�     [�     t�  
   y�     ��     ��  
   ��  $   ��     �     ��  
   �     �     �     :�  3   W�     ��     ��     ��     ��     ܃     �     �     �     3�     K�     `�     z�     ~�     ��     ��     ��     ��     ��     τ     �  !   �  "   %�  "   H�     k�     q�     ��     ��     ��     ��     ��     օ     �     �      �     &�     7�  	   C�     M�     b�     p�     ��  	   ��     ��     ��     ��     ��     Ԇ     �  	   ��     �     �     ,�     >�     L�     _�     n�  2   ��  0   ��     �     ��     �     �     #�     0�     5�     J�  �   `�  �   �  ,   f�  E   ��     ى  (   �  ]   �  .   z�  z   ��     $�     -�     G�     M�     _�  
   u�     ��     ��     ��     ��     ŋ     ��     ��     �     *�     2�  	   D�  '   N�  &   v�  8   ��     ֌     �     �     �     4�     L�  e   l�  Z   ҍ     -�     <�     B�     I�  
   X�     c�     q�     �     ��     ��     ��     Ȏ     Ύ     ڎ     ��     �     �     ��     ��  
    �     �     �     +�     7�     E�     Y�     m�     u�  �   ��  �   �     ��  
   ��          Ր     ސ     �     �     ��     �     #�  
   ;�  
   F�     Q�     a�     u�     ��     ��     ��     Ƒ     ڑ     �  '   �  (   -�     V�  (   t�     ��     ��  #   Ԓ  &   ��     �     ;�     U�     p�     ��     ��     ��     Ɠ     ٓ     �     ��     �     &�     9�     M�     `�     x�     ��     ��     ��     ��     Ք     �  "   �     '�      G�     h�     ��     ��     ��     Е     �     �     (�     E�     ]�     w�     ��  %   ��     ז     �     �     �     ,�     F�     a�     |�     ��     ��     ͗     �     �     �     2�     J�     b�     z�     ��     ��          ژ     �     �     !�     9�     Q�     i�     ��     ��     ��     ə     �     ��     �     (�     @�     X�     p�     ��     ��     ��     К     ߚ     �     	�     &�     8�     J�     ]�     p�     ��     ��     ��     ��     ˛     ݛ     �     �     �     *�     :�     J�     ]�     o�     ��     ��     ��     ��     ɜ     ۜ     �     ��     �     (�     <�     P�     d�     x�     ��     ��     ��     ĝ     ޝ     �     �     $�     =�     T�     d�     v�     ��     ��     ��     ��     О     �     ��     	�     �     8�     P�     h�     ��     ��     ��     ȟ     ��     ��     �     '�     ?�     W�     o�     ��     ��     ��     Ϡ     �     ��     �     $�     7�     M�     b�     w�     ��     ��     ��     ˡ     �     ��     �     �     3�     H�     ]�     r�     ��     ��     ��     Ƣ     �     ��     �     *�     B�     Z�     r�     ��     ��     ��     У     �     ��     �     ,�     C�     Z�     m�     ��     ��     ��     ٤     ��     �     *�     E�     _�     z�     ��     ��     ɥ     �     �     �     4�     N�     h�     ��     ��     ��     Ҧ     �     �     "�     :�     R�     j�     ��     ��     ��  !   ̧     �     �     �     6�     N�     h�     ��     ��     ��      Ҩ     �     	�     �     5�     I�     h�     ��     ��     ĩ     ٩     ��     ��     �     /�     H�     a�     z�     ��     ��     Ū     ݪ     ��     �     %�     =�     U�     m�     ��     ��     ��     ̫     �     �     (�     ?�     V�     m�     ��     ��      ¬     �     �     �     ,�     >�     U�     m�     ��     ��     ��     ͭ     �     ��     �     +�     B�     Y�     p�     ��     ��     ��     Ȯ     �     ��     �     4�     O�     j�     ��     ��     ��     կ     �     �     $�     C�     _�     w�     ��     ��     ð     ۰     ��     �     -�     E�     ]�     }�     ��     ��     ű     ݱ     ��     �  !   '�     I�     a�     y�     ��     ��     ò     ۲     ��     �      -�     N�     d�     z�     ��     ��     ó     �     �     �     4�     H�     Y�     p�     ��     ��     ��     д     �      �     �     /�     F�     ]�     t�     ��     ��     ��     е     �     ��     �     2�     Q�     m�     ��     ��     ��     ö     �     �     !�     >�     S�     g�     x�     ��     ��     ��     ׷     �     �     �     7�     N�     e�     |�     ��     ��     ��     ظ     �     �     �     3�     Q�     p�     ��     ��     ��     ι     �     �      �     @�     ]�     r�     ��     ��     ��     ƺ     ޺     ��     �     &�     >�     V�     m�     ��     ��     ��     ɻ     �     ��     �     !�     4�     R�     p�     ��     ��     ��     ׼     ��     �      �     ?�     _�     |�     ��     ��     ��     ͽ     �     ��     �     -�     E�     ]�     u�     ��     ��     ��     Ѿ     �     ��     �     -�     @�     S�     q�     ��     ��     ʿ     �     ��     �      �     ?�     ^�     ~�     ��     ��     ��     ��     ��     �     �     4�     L�     d�     |�     ��     ��     ��     ��     ��     �     �     5�     L�     _�     r�     ��     ��     ��     ��     ��     �     +�     ?�     ^�     }�     ��     ��     ��     ��     ��     �     #�     ;�     S�     k�     ��     ��     ��     ��     ��     ��     �     &�     =�     T�     k�     ~�     ��     ��     ��     ��     �     �     4�     J�     ^�     }�     ��     ��     ��     ��     �     �     *�     B�     Z�     r�     ��     ��     ��     ��     ��      �     �     .�     E�     \�     s�     ��     ��     ��     ��     ��     �     '�     =�     S�     i�     }�     ��     ��     ��     ��     �     !�     2�     D�     Y�     m�     ��     ��     ��     ��     ��     ��     ��     �     $�     :�     P�     f�     |�     ��     ��     ��     ��     ��     �     �     ,�     E�     ]�     u�     ��     ��     ��     ��     ��     �     �     8�     R�     l�     ��     ��     ��     ��     ��     �     �     1�     G�     ]�     v�     ��     ��     ��     ��     ��     �     �      ,�     M�     a�     z�     ��     ��     ��     ��     ��      �     �     -�     A�     Q�     c�     u�     ��     ��     ��     ��     ��     ��     �     �     .�     C�     X�     m�     ��     ��     ��     ��     ��     ��     ��     �     )�     >�      S�     t�  !   ��     ��     ��     ��     ��     �     2�     E�     W�     q�     ��     ��     ��     ��     ��     �     �     4�     M�     j�     ��     ��     ��     ��     ��     ��     �     �     0�  !   E�     g�     k�     ��     ��     ��     ��     ��     ��      �     9�     U�     o�     ��     ��     ��     ��     ��     �     4�     O�     i�     ��     ��  	   ��     ��     ��     ��     �     �     0�     H�     c�     v�     ��     ��     ��     ��     ��     ��     �     �     /�     D�     U�     m�     ��     ��     ��     ��     ��     ��     �     �     5�     Q�     d�  	   l�     v�     ��     ��     ��     ��     ��     �     '�     A�     W�     l�     ��     ��     ��     ��     ��     ��     �     +�     E�     \�     z�     ��     ��     ��     ��  #   ��     "�     =�     W�     v�     ��     ��     ��     ��     �     �     �     :�     M�     h�     ~�     ��  %   ��     ��     ��     ��     �     $�     ;�     T�     n�     ��     ��  %  ��  +   ��     �  ,   '�  "   T�  �   w�     �  �   �     ��     ��     ��     ��  #   ��      �      5�      V�      w�  &   ��      ��  +   ��  -   �     :�  #   U�     y�     ��     ��     ��  	   ��     ��     ��  	   ��     ��     ��  ,   ��  	   �  0   �  &   @�     g�     {�     ��     ��     ��     ��     ��     ��     ��     ��     �     �     "�     2�     >�     ]�     c�     z�     }�     ��  
   ��     ��     ��  &   ��  &   �     /�     B�  	   X�     b�     u�  6   ��     ��     ��     ��     ��     �     �  
   +�     6�  !   C�  !   e�     ��     ��     ��  	   ��     ��     ��  '   ��     ��     
�     �     -�     :�     V�     u�     ��  	   ��     ��     ��  8   ��     �     �     �     -�  J   4�     �  /   ��  #   ��     ��  3   ��     $�     +�     >�     N�     ^�     e�     r�     y�  '   ��     ��     ��     ��     ��     ��     ��     �     �     +�     >�     K�     j�     }�     ��     ��     ��     ��     ��     ��  6   ��  $   �     4�     J�     \�  '   q�     ��  1   ��     ��     ��     ��     �  "   !�     D�  2   Z�     ��     ��     ��     ��     ��     ��     ��     �     �  	   �     (�     :�     O�     g�     n�     {�     ��     ��     ��     ��     ��     ��     ��     ��     �     8�     K�     c�     ��     ��     ��     ��  	   ��     ��     ��     ��     ��  G   �     S�     `�     s�  	   z�     ��     ��     ��     ��  !   ��     ��     �     �     (�     B�     ^�      v�     ��  $   ��     ��     ��     ��  	   �  
   �     �     '�     C�     P�  0   `�     ��     ��     ��  +   ��  *   ��  ?   �     ^�     q�  !   ��     ��     ��  #   ��     ��     ��     ��     �     �     �     &�     -�     M�     Z�     ^�     c�     l�     y�     ��     ��  
   ��     ��     ��     ��     ��     �     %�     ,�     1�     >�     K�  &   _�  	   ��     ��     ��     ��     ��     ��     ��     ��     ��     ��     �     
�     �     $�     >�     [�  !   h�  	   ��     ��     ��     ��     ��     ��     ��     ��     �     �     !�     9�     T�     [�     z�     ��     ��     ��     ��  !   ��     �     2�     E�     X�     k�     ~�  $   ��     ��  	   ��     ��     ��     ��  	    �     
�  	   �     �     &�     *�     7�     D�     V�     k�     }�     ��  	   ��     ��     ��     ��  	   ��     ��     ��     �     &�     8�  $   M�     r�     �     ��     ��     ��     ��     ��     ��     ��     �     �  	   0�     :�     Z�     a�     n�     {�     ��     ��     ��  !   ��  �   ��     ��     ��     ��     ��     ��  	   ��  	   ��  !   ��  !   �     &�     *�     7�     M�     Q�  ^   ^�  B   ��      �  
   �     �     -�     E�     Z�     q�     ��     ��     ��     ��     ��     ��     ��     ��               %      8  /   Y  0   �  %   �     �     �     �                 4    A    H    [    o    �    �    �    �    �    �     9       S '   i    �    �    � 	   �    �    � +   �    (    A !   V -   x $   �    �    �    �    �    �    	        ) �   < |   �    <    Q    f    v     �     �    �    �    �    �    
        *    =    L    S    `    m 	   t 	   ~    �    � Q   �    �         3    M    T    d    q    � *   �    �    � 
   �    �    �     3   "    V    f    v    �    � !   �    �    �    	    	    2	    L	    S	    `	    g	    t	    �	    �	    �	    �	 $   �	    
     
    ?
    F
 	   S
    ]
    m
    z
    �
    �
    �
    �
    �
    �
     
           4    A    U    c    p    �    �    �    �    � 	   �    �    �        $    5    I    Z -   m 0   �    �    �    �    �                5 t   L w   � &   9 7   `    � !   � X   � $   + a   P    �    � 	   �    �    � 	   
        *    7    >    Q !   p    �    � 	   �    � 	   �     �     � 9       R    j $   �    �    � !   � c   � N   b    �    �    �    �    �    �    �            9    L    Y    `    m    t 	   v    �    � 	   �    �    �    �    �    �    �            ' �   8 z   �    =    W    `    q    z    � 
   �    �    �    � 
   � 
   �    � 	                       "    )    6    I    _    x    �    �    �    �    �    �    �            -    1    5    9    =    A    E    I    M    Q    U    b    o 	   | 	   �    �    �    �    �    �    �    �    �    �    �    �    �    �    � 	        	        	   &    0    =    D    K    a    t    �    �    �    �    �    �    �    �                 +    9    G    U    c    q        �    �    �    �    �    �    �    �    �    
        %    3    A    O    ]    k    y    �    �    �    �    �    �    �    �    �    �    �                 ,    8    D    P    \    i 	   p    z    �    �    �    �    �    �    �    �    �    �                ,    :    H    V    d    r    y    �    �    �    �    �    �    �    �    �    �    �    �    �      	               ,    :    H    V    d    r    �    �    �    �    �    �    �    �    �    �            '    4    8    H    Q 
   ] 
   h 
   s 
   ~ 
   � 
   � 
   � 
   �    �    � 
   � 
   � 
   � 
   � 
   � 
    
    
       "    /    ;    H    U    b    o    |    �    �    �    �    �    �    �    �    �    �    �              '     5     C     Q     _     m     {     �     �     �     �     �     �     �     �     �     !    !    %!    1!    B!    N!    Z!    n!    z!    �!    �!    �!    �!    �!    �!    �!    �!    �!    �!    "    "    %"    3"    A"    O"    `"    l"    x"    �"    �"    �"    �"    �" 	   �" 	   �"    �"    �"    �"    #    #    !#    /#    =#    K#    Y#    f#    s#    �#    �#    �#    �#    �#    �#    �#    �#    �#    �#    $    $    &$    3$    @$    H$    Z$    l$    ~$ 
   �$ 
   �$    �$    �$    �$    �$    �$    �$    �$    �$    %    %    !%    -%    9%    E%    Q%    ]%    i%    u%    |%    �%    �%    �%    �%    �%    �%    �%    �%    �%    &    &     &    1&    B&    S&    _&    k&    |&    �&    �&    �&    �&    �&    �&    �&    �&    �&    '    '    '    )'    7'    C'    Z'    f'    r'    ~'    �'    �'    �'    �'    �'    �'    �'    �'    �'    (    
(    (    ,(    =( 	   N( 	   X(    b(    i(    u(    �(    �(    �(    �(    �(    �(    �(    �(    �(    �(     )    )    )    $)    0)    7)    >)    O)    `)    q)    �)    �)    �)    �)    �)    �)    �)    �) 	   �) 	   �)    *    *    *    %*    2*    ?*    L*    Y*    f*    s*    *    �*    �*    �*    �*    �*    �*    �*    �*    �*    �*    +    +    %+    1+    =+    I+    P+    a+    r+    �+ 	   �+ 	   �+    �+    �+    �+    �+    �+    �+    �+    �+    	,    ,    ",    .,    :,    F,    R,    ^,    j,    v,    },    �,    �,    �,    �,    �,    �,    �,    �,    �,    -    -    &- 	   7- 	   A-    K-    R-    ^-    k-    x-    �-    �-    �-    �-    �-    �-    �-    �-    �-    �-    .    .    .     .    '.    8.    I.    Z.    k.    w.    �.    �.    �.    �.    �.    �. 	   �. 	   �.    �.    �.    /    /    /    (/    5/    B/    O/    \/    h/    t/    �/    �/    �/    �/    �/    �/    �/    �/    �/    �/    �/    0    0    &0    20    90    J0    [0    l0 	   }0 	   �0    �0    �0    �0    �0    �0    �0    �0    �0    �0    �0    1    1    #1    /1    ;1    G1    S1    _1    f1    m1    ~1    �1    �1    �1    �1    �1    �1    �1    �1    �1    2 	    2 	   *2    42    ;2    G2    T2    a2    n2    {2    �2    �2    �2    �2    �2    �2    �2    �2    �2    �2    3    	3    3    !3    23    C3    T3    `3    l3    x3    3    �3    �3    �3 	   �3 	   �3    �3 	   �3    �3    �3    4    4    4    %4    14    =4    I4    U4    a4    p4    ~4    �4    �4    �4    �4    �4    �4    �4    �4    �4 	   �4    5    5    !5    -5    95    E5    Q5    ]5    i5    u5    �5    �5    �5    �5    �5    �5    �5    �5    �5     6    6 
   6 
   6    $6    76    G6    T6    a6    n6    }6    �6    �6    �6 
   �6    �6    �6    �6    �6    7    7    &7    57    D7    K7    X7    e7    t7    �7    �7    �7    �7    �7    �7    �7    �7    �7    8    8     8    .8    <8    H8    W8    e8    s8    �8    �8    �8    �8    �8    �8    �8     �8    9    .9    ?9    P9    d9    x9    �9    �9    �9    �9    �9    �9    �9    :    :    .:    ?:    S:    d:    x:    �:    �:    �:    �:    �:    �:    �:    �:    �:    ;    ;    !; 
   &;    1; 	   8;    B;    O;    V; 	   c; 	   m;    w;    �;    �;    �;    �;    �;    �; 	   �; 	   �;    �;    �;    	< 	   < 	   <    $<    4< 	   ;< 
   E< 	   P< 	   Z<    d<    k<    x< 	   <    �<    �< 	   �<    �<    �< 	   �< 	   �<    �<    �<    �<    �<    �<    �<     =    =    =    $= 	   1= 	   ;=    E=    L=    Y=    f=    v= 	   }= 	   �=    �=    �= 	   �=    �= 	   �= 	   �= 	   �=    �=    �=    �=    �=    �=    �=    >    > 	   >    > 	   *>    4> 	   A>    K>    R>    Y>    f> 	   s>    }>    �> 	   �> 	   �> 	   �> 	   �>    �> 	   �> 	   �>    �>    �> 	   �> 	   �>    ?    ?    ?    ?    %?    )?    6? 	   C?    M?    T?    h?    u?  

--- DRIVER INFO ---
Driver:  

Press any key to continue 

There are working clones of this machine: %s 
    Configuration saved    

 
Elements of this machine cannot be emulated as they require physical interaction or consist of mechanical devices. It is not possible to fully experience this machine.
 
Sound:
 
THIS MACHINE DOESN'T WORK. The emulation for this machine is not yet complete. There is nothing you can do to fix this problem except wait for the developers to improve the emulation.
 
Video:
   %1$s
   %1$s    [default: %2$s]
   %1$s    [tag: %2$s]
   Adjuster inputs    [%1$d inputs]
   Analog inputs    [%1$d inputs]
   Gambling inputs    [%1$d inputs]
   Hanafuda inputs    [%1$d inputs]
   Keyboard inputs    [%1$d inputs]
   Keypad inputs    [%1$d inputs]
   Mahjong inputs    [%1$d inputs]
   Screen '%1$s': %2$d × %3$d (H) %4$s Hz
   Screen '%1$s': %2$d × %3$d (V) %4$s Hz
   Screen '%1$s': Vector
   User inputs    [%1$d inputs]
  (default)  (locked)  COLORS  PENS %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Driver: %4$s

CPU:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d machines (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d software packages ) %1$s (%2$s) - %3$s
 %1$s - %2$s
 %1$s Brightness %1$s Contrast %1$s Gamma %1$s Horiz Position %1$s Horiz Stretch %1$s Refresh Rate %1$s Vert Position %1$s Vert Stretch %1$s Volume %1$s [root%2$s] %1$s, %2$-.100s %1$s: %2$s
 %1$s: %2$s - Search: %3$s_ %2$s
 %d total matches found %s %s
 added to favorites list. %s
 removed from favorites list. %s [%d Hz] %s [internal] %s added %s.txt saved in UI settings folder. %s.xml saved in UI settings folder. (EP)ROM	Imperfect
 (EP)ROM	Unimplemented
 (empty) (playing) (recording) * BIOS settings:
  %1$d options    [default: %2$s]
 * CPU:
 * Configuration settings:
 * DIP switch settings:
 * Input device(s):
 * Media Options:
 * Slot Options:
 * Sound:
 * Video:
 **Error saving %s.ini** **Error saving ui.ini** , %s <set up filters> ARGB Settings About %s Activated: %s Activated: %s = %s Add %1$s Folder - Search: %2$s_ Add Folder Add To Favorites Add autofire button Add filter Add or remove favorite Adjust speed to match refresh rate Adstick Device Assignment Advanced Options Aligned only All All cheats reloaded All slots cleared and current state saved to Slot 1 Allow rewind Analog Controls Analog Controls	Yes
 Any Are you sure you want to quit?

Press ''%1$s'' to quit,
Press ''%2$s'' to return to emulation. Artwork Options Audit ROMs for %1$u machines marked unavailable Audit ROMs for all %1$u machines Audit media Auditing ROMs for machine %2$u of %3$u...
%1$s Auto Auto frame skip Auto rotate left Auto rotate right Autofire Autofire buttons Automatic Automatic save/restore Automatically toggle pause with on-screen menus BIOS BIOS Selection BIOS selection: Barcode Reader Barcode length invalid! Beam Dot Size Beam Intensity Weight Beam Width Maximum Beam Width Minimum Bilinear Filtering Bilinear filtering for snapshots Bitmap Prescaling Bold Bookkeeping Info Burn-in CPU or RAM Camera	Imperfect
 Camera	Unimplemented
 Cancel Cancel audit?

Press %1$s to cancel
Press %2$s to continue Cannot change options while recording! Cannot save over directory Capture	Imperfect
 Capture	Unimplemented
 Change %1$s Folder - Search: %2$s_ Change Folder Changes to this only take effect when "Start new search" is selected Cheat Cheat Comment:
%s Cheat Finder Cheat Name Cheat added to cheat.simple Cheat engine not available Cheat written to %s and added to cheat.simple Cheats Choose from palette Clear Watches Coin %1$c: %2$d%3$s
 Coin %1$c: NA%3$s
 Coin impulse Coin lockout Color preview: Colors Command Communications	Imperfect
 Communications	Unimplemented
 Completely unemulated features:  Compressor Configure Directories Configure Machine Configure Machine: Configure Options Confirm quit from machines Controls	Imperfect
 Controls	Unimplemented
 Create Crosshair Offset %1$s Crosshair Offset X %1$1.3f Crosshair Offset Y %1$1.3f Crosshair Options Crosshair Scale %1$s Crosshair Scale X %1$1.3f Crosshair Scale Y %1$1.3f Current %1$s Folders Current time Custom Customize UI DIP Switches Data Format Default Default name is %s Delete saved state %1$s?
Press %2$s to delete
Press %3$s to cancel Device Mapping Dial Device Assignment Difference Disabled Disabled: %s Disk	Imperfect
 Disk	Unimplemented
 Done Double-click or press %1$s to change color Double-click or press %1$s to select Driver Driver is BIOS	No
 Driver is BIOS	Yes
 Driver is Clone of	%1$s
 Driver is Parent	
 Driver is clone of: %1$-.100s Driver is parent Driver: "%1$s" software list  Driver: %1$-.100s Edit autofire button Emulated Enabled Enabled: %s Enforce Aspect Ratio Enlarge images in the right panel Enter Code Error accessing %s Error removing saved state file %1$s Exit Expand to fit Export displayed list to file Export list in TXT format (like -listfull) Export list in XML format (like -listxml) Export list in XML format (like -listxml, but exclude devices) External DAT View Failed to load autofire menu Failed to save input name file Fast Forward File File Already Exists - Override? File Manager Filter Filter %1$u Flip X Flip Y Folders Setup Fonts Force 4:3 aspect for snapshot display Frame skip GHz GLSL Gameinit General Info General Inputs Graphics	Imperfect
 Graphics	Imperfect Colors
 Graphics	OK
 Graphics	Unimplemented
 Graphics	Wrong Colors
 Graphics: Imperfect,  Graphics: OK,  Graphics: Unimplemented,  Group HLSL Hide Both Hide Filters Hide Info/Image Hide romless machine from available list High Scores History Hotkey Hz Image Format: Image Information Images Imperfectly emulated features:  Include clones Info auto audit Infos Infos text size Input Input (general) Input (this Machine) Input Options Input port name file saved to %s Input ports Invalid sequence entered Italic Joystick Joystick deadzone Joystick saturation Keyboard	Imperfect
 Keyboard	Unimplemented
 Keyboard Inputs	Yes
 Keyboard Mode LAN	Imperfect
 LAN	Unimplemented
 Language Laserdisc '%1$s' Horiz Position Laserdisc '%1$s' Horiz Stretch Laserdisc '%1$s' Vert Position Laserdisc '%1$s' Vert Stretch Last Slot Value Left equal to right Left equal to right with bitmask Left equal to value Left greater than right Left greater than value Left less than right Left less than value Left not equal to right Left not equal to right with bitmask Left not equal to value Lightgun Lightgun Device Assignment Lines Load State Low latency MAMEinfo MARPScore MESSinfo MHz Machine Configuration Machine Information Mag. Drum	Imperfect
 Mag. Drum	Unimplemented
 Mag. Tape	Imperfect
 Mag. Tape	Unimplemented
 Maintain Aspect Ratio Mamescore Manual Manually toggle pause when needed Manufacturer	%1$s
 Master Volume Match block Mechanical Machine	No
 Mechanical Machine	Yes
 Media	Imperfect
 Media	Unimplemented
 Memory state saved to Slot %d Menu Preview Microphone	Imperfect
 Microphone	Unimplemented
 Miscellaneous Options Mouse Mouse	Imperfect
 Mouse	Unimplemented
 Mouse Device Assignment Multi-keyboard Multi-mouse Mute when unthrottled NOT SET Name:             Description:
 Natural Natural keyboard Network Devices New Barcode: New Image Name: No No category INI files found No groups found in category file No machines found. Please check the rompath specified in the %1$s.ini file.

If this is your first time using %2$s, please see the config.txt file in the docs directory for information on configuring %2$s. No plugins found No save states found Non-Integer Scaling None None
 Not supported Number Of Screens Number of frames button will be pressed Number of frames button will be released Off Off frames Offscreen reload On On frames One or more ROMs/CHDs for this machine are incorrect. The machine may not run correctly.
 One or more ROMs/CHDs for this machine have not been correctly dumped.
 Other Controls Other:  Overall	NOT WORKING
 Overall	Unemulated Protection
 Overall	Working
 Overall: NOT WORKING Overall: Unemulated Protection Overall: Working Overclock %1$s sound Overclock CPU %1$s P%d Crosshair P%d Visibility Paddle Device Assignment Page Partially supported Pause Mode Pause/Stop Pedal Device Assignment Perform Compare  :  Slot %d %s %d Perform Compare  :  Slot %d %s Slot %d %s %d Perform Compare  :  Slot %d BITWISE%s Slot %d Perform Compare : Slot %d %s Slot %d Performance Options Play Play Count Player Player %1$d Controls Please enter a file extension too Plugin Options Plugins Positional Device Assignment Press %1$s to append
 Press %1$s to cancel
 Press %1$s to clear
 Press %1$s to delete Press %1$s to restore default
 Press %1$s to set
 Press %s to clear hotkey Press %s to delete Press TAB to set Press a key or joystick button, or select state to overwrite Press any key to continue. Press button for hotkey or wait to leave unchanged Pressed Printer	Imperfect
 Printer	Unimplemented
 Pseudo terminals Punch Tape	Imperfect
 Punch Tape	Unimplemented
 ROM Audit 	Disabled
Samples Audit 	Disabled
 ROM Audit Result	BAD
 ROM Audit Result	OK
 Re-select last machine launched Read this image, write to another image Read this image, write to diff Read-only Read-write Record Reload All Remove %1$s Folder Remove Folder Remove From Favorites Remove last filter Required ROM/disk images for the selected software are missing or incorrect. Please acquire the correct files or select a different software item.

 Required ROM/disk images for the selected system are missing or incorrect. Please acquire the correct files or select a different system.

 Requires Artwork	No
 Requires Artwork	Yes
 Requires CHD	No
 Requires CHD	Yes
 Requires Clickable Artwork	No
 Requires Clickable Artwork	Yes
 Reset Reset All Restore default colors Results will be saved to %1$s Return to Machine Return to Previous Menu Return to previous menu Revision: %1$s Rewind Rewind capacity Romset	%1$s
 Rotate Rotate left Rotate right Rotation Options Sample Rate Sample text - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Samples Audit Result	BAD
 Samples Audit Result	None Needed
 Samples Audit Result	OK
 Save Save Cheat Save Configuration Save Machine Configuration Save State Save current memory state to Slot %d Save input names to file Screen Screen #%d Screen '%1$s' Screen Orientation	Horizontal
 Screen Orientation	Vertical
 Screen flipping in cocktail mode is not supported.
 Search: %1$s_ Select New Machine Select access mode Select an input for autofire Select category: Select cheat to set hotkey Select custom filters: Select image format Select initial contents Select state to load Selection List - Search:  Set Set hotkeys Settings Show All Show DATs view Show mouse pointer Show side panels Simultaneous contradictory Skip BIOS selection menu Skip imperfect emulation warnings Skip information screen at startup Skip software parts selection menu Sleep Slider Controls Slot %d Slot 1 Value Slot Devices Software List Info Software is clone of: %1$-.100s Software is parent Software list/item: %1$s:%2$s Software part selection: Sound Sound	Imperfect
 Sound	None
 Sound	OK
 Sound	Unimplemented
 Sound Options Sound: Imperfect Sound: None Sound: OK Sound: Unimplemented Speed Start Machine Start Out Maximized Start new search State/Playback Options Steadykey Support Cocktail	No
 Support Save	No
 Support Save	Yes
 Supported: No Supported: Partial Supported: Yes Switch Item Ordering Switched Order: entries now ordered by description Switched Order: entries now ordered by shortname Synchronized Refresh Sysinfo System Names System: %1$-.100s Tape Control Test Test Cheat %08X_%02X Test/Write Poke Value The selected game is missing one or more required ROM or CHD images. Please select a different game.

Press any key to continue. The software selected is missing one or more required ROM or CHD images.
Please acquire the correct files or select a different one. There are known problems with this machine

 This driver requires images to be loaded in the following device(s):  This machine has no BIOS. This machine has no configurable inputs. This machine has no sound hardware, MAME will produce no sounds, this is expected behaviour.
 This machine requires external artwork files.
 This machine was never completed. It may exhibit strange behavior or missing elements that are not bugs in the emulation.
 Throttle Tickets dispensed: %1$d

 Timer Timing	Imperfect
 Timing	Unimplemented
 Total time Trackball Device Assignment Triple Buffering Type Type name is empty Type name or select: %1$s_ Type name or select: (random) UI Color Settings UI Customization Settings UI Font UI Fonts Settings UI active UI controls disabled
Use %1$s to toggle UI controls enabled
Use %1$s to toggle Unable to write file
Ensure that cheatpath folder exists Undo last search -- # Uptime: %1$d:%2$02d

 Uptime: %1$d:%2$02d:%3$02d

 Use External Samples Use image as background Use this if you want to poke %s Use this if you want to poke the Last Slot value (eg. You started without an item but finally got it) Use this if you want to poke the Slot 1 value (eg. You started with something but lost it) User Interface Value Vector Vector Flicker Video Mode Video Options Visible Delay WAN	Imperfect
 WAN	Unimplemented
 Wait Vertical Sync Warning Information Watch Window Mode Write X X Only X or Y (Auto) Y Y Only Year	%1$s
 Yes You can enter any type name Zoom = %1$d Zoom = 1/%1$d Zoom to Screen Area Zoom to screen area [None]
 [Start empty] [This option is NOT currently mounted in the running system]

Option: %1$s
Device: %2$s

If you select this option, the following items will be enabled:
 [This option is currently mounted in the running system]

Option: %1$s
Device: %2$s

The selected option enables the following items:
 [Use file manager] [built-in] [compatible lists] [create] [empty slot] [empty] [failed] [file manager] [no category INI files] [no groups in INI file] [root%1$s] [root%2$s] [software list] color-channelAlpha color-channelBlue color-channelGreen color-channelRed color-optionBackground color-optionBorder color-optionClone color-optionDIP switch color-optionGraphics viewer background color-optionMouse down background color color-optionMouse down color color-optionMouse over background color color-optionMouse over color color-optionNormal text color-optionNormal text background color-optionSelected background color color-optionSelected color color-optionSlider color color-optionSubitem color color-optionUnavailable color color-presetBlack color-presetBlue color-presetGray color-presetGreen color-presetOrange color-presetRed color-presetSilver color-presetViolet color-presetWhite color-presetYellow color-sampleClone color-sampleMouse Over color-sampleNormal color-sampleSelected color-sampleSubitem default emulation-featureLAN emulation-featureWAN emulation-featurecamera emulation-featurecapture hardware emulation-featurecolor palette emulation-featurecommunications emulation-featurecontrols emulation-featuredisk emulation-featuregraphics emulation-featurekeyboard emulation-featuremagnetic drum emulation-featuremagnetic tape emulation-featuremedia emulation-featuremicrophone emulation-featuremouse emulation-featureprinter emulation-featureprotection emulation-featurepunch tape emulation-featuresolid state storage emulation-featuresound emulation-featuretiming incorrect checksum incorrect length input-name1 Player Start input-name2 Players Start input-name3 Players Start input-name4 Players Start input-name5 Players Start input-name6 Players Start input-name7 Players Start input-name8 Players Start input-nameAD Stick X input-nameAD Stick X 10 input-nameAD Stick X 2 input-nameAD Stick X 3 input-nameAD Stick X 4 input-nameAD Stick X 5 input-nameAD Stick X 6 input-nameAD Stick X 7 input-nameAD Stick X 8 input-nameAD Stick X 9 input-nameAD Stick Y input-nameAD Stick Y 10 input-nameAD Stick Y 2 input-nameAD Stick Y 3 input-nameAD Stick Y 4 input-nameAD Stick Y 5 input-nameAD Stick Y 6 input-nameAD Stick Y 7 input-nameAD Stick Y 8 input-nameAD Stick Y 9 input-nameAD Stick Z input-nameAD Stick Z 10 input-nameAD Stick Z 2 input-nameAD Stick Z 3 input-nameAD Stick Z 4 input-nameAD Stick Z 5 input-nameAD Stick Z 6 input-nameAD Stick Z 7 input-nameAD Stick Z 8 input-nameAD Stick Z 9 input-nameBet input-nameBill 1 input-nameBook-Keeping input-nameBreak in Debugger input-nameCancel input-nameCoin 1 input-nameCoin 10 input-nameCoin 11 input-nameCoin 12 input-nameCoin 2 input-nameCoin 3 input-nameCoin 4 input-nameCoin 5 input-nameCoin 6 input-nameCoin 7 input-nameCoin 8 input-nameCoin 9 input-nameConfig Menu input-nameDeal input-nameDial input-nameDial 10 input-nameDial 2 input-nameDial 3 input-nameDial 4 input-nameDial 5 input-nameDial 6 input-nameDial 7 input-nameDial 8 input-nameDial 9 input-nameDial V input-nameDial V 10 input-nameDial V 2 input-nameDial V 3 input-nameDial V 4 input-nameDial V 5 input-nameDial V 6 input-nameDial V 7 input-nameDial V 8 input-nameDial V 9 input-nameDoor input-nameDoor Interlock input-nameDouble Up input-nameFast Forward input-nameFrameskip Dec input-nameFrameskip Inc input-nameHalf Gamble input-nameHigh input-nameHold 1 input-nameHold 2 input-nameHold 3 input-nameHold 4 input-nameHold 5 input-nameKey In input-nameKey Out input-nameKeyboard input-nameKeypad input-nameLightgun X input-nameLightgun X 10 input-nameLightgun X 2 input-nameLightgun X 3 input-nameLightgun X 4 input-nameLightgun X 5 input-nameLightgun X 6 input-nameLightgun X 7 input-nameLightgun X 8 input-nameLightgun X 9 input-nameLightgun Y input-nameLightgun Y 10 input-nameLightgun Y 2 input-nameLightgun Y 3 input-nameLightgun Y 4 input-nameLightgun Y 5 input-nameLightgun Y 6 input-nameLightgun Y 7 input-nameLightgun Y 8 input-nameLightgun Y 9 input-nameLoad State input-nameLow input-nameMemory Reset input-nameMouse X input-nameMouse X 10 input-nameMouse X 2 input-nameMouse X 3 input-nameMouse X 4 input-nameMouse X 5 input-nameMouse X 6 input-nameMouse X 7 input-nameMouse X 8 input-nameMouse X 9 input-nameMouse Y input-nameMouse Y 10 input-nameMouse Y 2 input-nameMouse Y 3 input-nameMouse Y 4 input-nameMouse Y 5 input-nameMouse Y 6 input-nameMouse Y 7 input-nameMouse Y 8 input-nameMouse Y 9 input-nameOn Screen Display input-nameP1 Button 1 input-nameP1 Button 10 input-nameP1 Button 11 input-nameP1 Button 12 input-nameP1 Button 13 input-nameP1 Button 14 input-nameP1 Button 15 input-nameP1 Button 16 input-nameP1 Button 2 input-nameP1 Button 3 input-nameP1 Button 4 input-nameP1 Button 5 input-nameP1 Button 6 input-nameP1 Button 7 input-nameP1 Button 8 input-nameP1 Button 9 input-nameP1 Down input-nameP1 Hanafuda A/1 input-nameP1 Hanafuda B/2 input-nameP1 Hanafuda C/3 input-nameP1 Hanafuda D/4 input-nameP1 Hanafuda E/5 input-nameP1 Hanafuda F/6 input-nameP1 Hanafuda G/7 input-nameP1 Hanafuda H/8 input-nameP1 Hanafuda No input-nameP1 Hanafuda Yes input-nameP1 Left input-nameP1 Left Stick/Down input-nameP1 Left Stick/Left input-nameP1 Left Stick/Right input-nameP1 Left Stick/Up input-nameP1 Mahjong A input-nameP1 Mahjong B input-nameP1 Mahjong Bet input-nameP1 Mahjong Big input-nameP1 Mahjong C input-nameP1 Mahjong Chi input-nameP1 Mahjong D input-nameP1 Mahjong Double Up input-nameP1 Mahjong E input-nameP1 Mahjong F input-nameP1 Mahjong Flip Flop input-nameP1 Mahjong G input-nameP1 Mahjong H input-nameP1 Mahjong I input-nameP1 Mahjong J input-nameP1 Mahjong K input-nameP1 Mahjong Kan input-nameP1 Mahjong L input-nameP1 Mahjong Last Chance input-nameP1 Mahjong M input-nameP1 Mahjong N input-nameP1 Mahjong O input-nameP1 Mahjong P input-nameP1 Mahjong Pon input-nameP1 Mahjong Q input-nameP1 Mahjong Reach input-nameP1 Mahjong Ron input-nameP1 Mahjong Small input-nameP1 Mahjong Take Score input-nameP1 Pedal 1 input-nameP1 Pedal 2 input-nameP1 Pedal 3 input-nameP1 Right input-nameP1 Right Stick/Down input-nameP1 Right Stick/Left input-nameP1 Right Stick/Right input-nameP1 Right Stick/Up input-nameP1 Select input-nameP1 Start input-nameP1 Up input-nameP10 Button 1 input-nameP10 Button 10 input-nameP10 Button 11 input-nameP10 Button 12 input-nameP10 Button 13 input-nameP10 Button 14 input-nameP10 Button 15 input-nameP10 Button 16 input-nameP10 Button 2 input-nameP10 Button 3 input-nameP10 Button 4 input-nameP10 Button 5 input-nameP10 Button 6 input-nameP10 Button 7 input-nameP10 Button 8 input-nameP10 Button 9 input-nameP10 Down input-nameP10 Left input-nameP10 Left Stick/Down input-nameP10 Left Stick/Left input-nameP10 Left Stick/Right input-nameP10 Left Stick/Up input-nameP10 Pedal 1 input-nameP10 Pedal 2 input-nameP10 Pedal 3 input-nameP10 Right input-nameP10 Right Stick/Down input-nameP10 Right Stick/Left input-nameP10 Right Stick/Right input-nameP10 Right Stick/Up input-nameP10 Select input-nameP10 Start input-nameP10 Up input-nameP2 Button 1 input-nameP2 Button 10 input-nameP2 Button 11 input-nameP2 Button 12 input-nameP2 Button 13 input-nameP2 Button 14 input-nameP2 Button 15 input-nameP2 Button 16 input-nameP2 Button 2 input-nameP2 Button 3 input-nameP2 Button 4 input-nameP2 Button 5 input-nameP2 Button 6 input-nameP2 Button 7 input-nameP2 Button 8 input-nameP2 Button 9 input-nameP2 Down input-nameP2 Hanafuda A/1 input-nameP2 Hanafuda B/2 input-nameP2 Hanafuda C/3 input-nameP2 Hanafuda D/4 input-nameP2 Hanafuda E/5 input-nameP2 Hanafuda F/6 input-nameP2 Hanafuda G/7 input-nameP2 Hanafuda H/8 input-nameP2 Hanafuda No input-nameP2 Hanafuda Yes input-nameP2 Left input-nameP2 Left Stick/Down input-nameP2 Left Stick/Left input-nameP2 Left Stick/Right input-nameP2 Left Stick/Up input-nameP2 Mahjong A input-nameP2 Mahjong B input-nameP2 Mahjong Bet input-nameP2 Mahjong Big input-nameP2 Mahjong C input-nameP2 Mahjong Chi input-nameP2 Mahjong D input-nameP2 Mahjong Double Up input-nameP2 Mahjong E input-nameP2 Mahjong F input-nameP2 Mahjong Flip Flop input-nameP2 Mahjong G input-nameP2 Mahjong H input-nameP2 Mahjong I input-nameP2 Mahjong J input-nameP2 Mahjong K input-nameP2 Mahjong Kan input-nameP2 Mahjong L input-nameP2 Mahjong Last Chance input-nameP2 Mahjong M input-nameP2 Mahjong N input-nameP2 Mahjong O input-nameP2 Mahjong P input-nameP2 Mahjong Pon input-nameP2 Mahjong Q input-nameP2 Mahjong Reach input-nameP2 Mahjong Ron input-nameP2 Mahjong Small input-nameP2 Mahjong Take Score input-nameP2 Pedal 1 input-nameP2 Pedal 2 input-nameP2 Pedal 3 input-nameP2 Right input-nameP2 Right Stick/Down input-nameP2 Right Stick/Left input-nameP2 Right Stick/Right input-nameP2 Right Stick/Up input-nameP2 Select input-nameP2 Start input-nameP2 Up input-nameP3 Button 1 input-nameP3 Button 10 input-nameP3 Button 11 input-nameP3 Button 12 input-nameP3 Button 13 input-nameP3 Button 14 input-nameP3 Button 15 input-nameP3 Button 16 input-nameP3 Button 2 input-nameP3 Button 3 input-nameP3 Button 4 input-nameP3 Button 5 input-nameP3 Button 6 input-nameP3 Button 7 input-nameP3 Button 8 input-nameP3 Button 9 input-nameP3 Down input-nameP3 Left input-nameP3 Left Stick/Down input-nameP3 Left Stick/Left input-nameP3 Left Stick/Right input-nameP3 Left Stick/Up input-nameP3 Pedal 1 input-nameP3 Pedal 2 input-nameP3 Pedal 3 input-nameP3 Right input-nameP3 Right Stick/Down input-nameP3 Right Stick/Left input-nameP3 Right Stick/Right input-nameP3 Right Stick/Up input-nameP3 Select input-nameP3 Start input-nameP3 Up input-nameP4 Button 1 input-nameP4 Button 10 input-nameP4 Button 11 input-nameP4 Button 12 input-nameP4 Button 13 input-nameP4 Button 14 input-nameP4 Button 15 input-nameP4 Button 16 input-nameP4 Button 2 input-nameP4 Button 3 input-nameP4 Button 4 input-nameP4 Button 5 input-nameP4 Button 6 input-nameP4 Button 7 input-nameP4 Button 8 input-nameP4 Button 9 input-nameP4 Down input-nameP4 Left input-nameP4 Left Stick/Down input-nameP4 Left Stick/Left input-nameP4 Left Stick/Right input-nameP4 Left Stick/Up input-nameP4 Pedal 1 input-nameP4 Pedal 2 input-nameP4 Pedal 3 input-nameP4 Right input-nameP4 Right Stick/Down input-nameP4 Right Stick/Left input-nameP4 Right Stick/Right input-nameP4 Right Stick/Up input-nameP4 Select input-nameP4 Start input-nameP4 Up input-nameP5 Button 1 input-nameP5 Button 10 input-nameP5 Button 11 input-nameP5 Button 12 input-nameP5 Button 13 input-nameP5 Button 14 input-nameP5 Button 15 input-nameP5 Button 16 input-nameP5 Button 2 input-nameP5 Button 3 input-nameP5 Button 4 input-nameP5 Button 5 input-nameP5 Button 6 input-nameP5 Button 7 input-nameP5 Button 8 input-nameP5 Button 9 input-nameP5 Down input-nameP5 Left input-nameP5 Left Stick/Down input-nameP5 Left Stick/Left input-nameP5 Left Stick/Right input-nameP5 Left Stick/Up input-nameP5 Pedal 1 input-nameP5 Pedal 2 input-nameP5 Pedal 3 input-nameP5 Right input-nameP5 Right Stick/Down input-nameP5 Right Stick/Left input-nameP5 Right Stick/Right input-nameP5 Right Stick/Up input-nameP5 Select input-nameP5 Start input-nameP5 Up input-nameP6 Button 1 input-nameP6 Button 10 input-nameP6 Button 11 input-nameP6 Button 12 input-nameP6 Button 13 input-nameP6 Button 14 input-nameP6 Button 15 input-nameP6 Button 16 input-nameP6 Button 2 input-nameP6 Button 3 input-nameP6 Button 4 input-nameP6 Button 5 input-nameP6 Button 6 input-nameP6 Button 7 input-nameP6 Button 8 input-nameP6 Button 9 input-nameP6 Down input-nameP6 Left input-nameP6 Left Stick/Down input-nameP6 Left Stick/Left input-nameP6 Left Stick/Right input-nameP6 Left Stick/Up input-nameP6 Pedal 1 input-nameP6 Pedal 2 input-nameP6 Pedal 3 input-nameP6 Right input-nameP6 Right Stick/Down input-nameP6 Right Stick/Left input-nameP6 Right Stick/Right input-nameP6 Right Stick/Up input-nameP6 Select input-nameP6 Start input-nameP6 Up input-nameP7 Button 1 input-nameP7 Button 10 input-nameP7 Button 11 input-nameP7 Button 12 input-nameP7 Button 13 input-nameP7 Button 14 input-nameP7 Button 15 input-nameP7 Button 16 input-nameP7 Button 2 input-nameP7 Button 3 input-nameP7 Button 4 input-nameP7 Button 5 input-nameP7 Button 6 input-nameP7 Button 7 input-nameP7 Button 8 input-nameP7 Button 9 input-nameP7 Down input-nameP7 Left input-nameP7 Left Stick/Down input-nameP7 Left Stick/Left input-nameP7 Left Stick/Right input-nameP7 Left Stick/Up input-nameP7 Pedal 1 input-nameP7 Pedal 2 input-nameP7 Pedal 3 input-nameP7 Right input-nameP7 Right Stick/Down input-nameP7 Right Stick/Left input-nameP7 Right Stick/Right input-nameP7 Right Stick/Up input-nameP7 Select input-nameP7 Start input-nameP7 Up input-nameP8 Button 1 input-nameP8 Button 10 input-nameP8 Button 11 input-nameP8 Button 12 input-nameP8 Button 13 input-nameP8 Button 14 input-nameP8 Button 15 input-nameP8 Button 16 input-nameP8 Button 2 input-nameP8 Button 3 input-nameP8 Button 4 input-nameP8 Button 5 input-nameP8 Button 6 input-nameP8 Button 7 input-nameP8 Button 8 input-nameP8 Button 9 input-nameP8 Down input-nameP8 Left input-nameP8 Left Stick/Down input-nameP8 Left Stick/Left input-nameP8 Left Stick/Right input-nameP8 Left Stick/Up input-nameP8 Pedal 1 input-nameP8 Pedal 2 input-nameP8 Pedal 3 input-nameP8 Right input-nameP8 Right Stick/Down input-nameP8 Right Stick/Left input-nameP8 Right Stick/Right input-nameP8 Right Stick/Up input-nameP8 Select input-nameP8 Start input-nameP8 Up input-nameP9 Button 1 input-nameP9 Button 10 input-nameP9 Button 11 input-nameP9 Button 12 input-nameP9 Button 13 input-nameP9 Button 14 input-nameP9 Button 15 input-nameP9 Button 16 input-nameP9 Button 2 input-nameP9 Button 3 input-nameP9 Button 4 input-nameP9 Button 5 input-nameP9 Button 6 input-nameP9 Button 7 input-nameP9 Button 8 input-nameP9 Button 9 input-nameP9 Down input-nameP9 Left input-nameP9 Left Stick/Down input-nameP9 Left Stick/Left input-nameP9 Left Stick/Right input-nameP9 Left Stick/Up input-nameP9 Pedal 1 input-nameP9 Pedal 2 input-nameP9 Pedal 3 input-nameP9 Right input-nameP9 Right Stick/Down input-nameP9 Right Stick/Left input-nameP9 Right Stick/Right input-nameP9 Right Stick/Up input-nameP9 Select input-nameP9 Start input-nameP9 Up input-namePaddle input-namePaddle 10 input-namePaddle 2 input-namePaddle 3 input-namePaddle 4 input-namePaddle 5 input-namePaddle 6 input-namePaddle 7 input-namePaddle 8 input-namePaddle 9 input-namePaddle V input-namePaddle V 10 input-namePaddle V 2 input-namePaddle V 3 input-namePaddle V 4 input-namePaddle V 5 input-namePaddle V 6 input-namePaddle V 7 input-namePaddle V 8 input-namePaddle V 9 input-namePause input-namePause - Single Step input-namePayout input-namePositional input-namePositional 10 input-namePositional 2 input-namePositional 3 input-namePositional 4 input-namePositional 5 input-namePositional 6 input-namePositional 7 input-namePositional 8 input-namePositional 9 input-namePositional V input-namePositional V 10 input-namePositional V 2 input-namePositional V 3 input-namePositional V 4 input-namePositional V 5 input-namePositional V 6 input-namePositional V 7 input-namePositional V 8 input-namePositional V 9 input-namePower Off input-namePower On input-nameRecord AVI input-nameRecord MNG input-nameReset Machine input-nameRewind - Single Step input-nameSave Snapshot input-nameSave State input-nameService input-nameService 1 input-nameService 2 input-nameService 3 input-nameService 4 input-nameShow Decoded Graphics input-nameShow FPS input-nameShow Profiler input-nameSoft Reset input-nameStand input-nameStop All Reels input-nameStop Reel 1 input-nameStop Reel 2 input-nameStop Reel 3 input-nameStop Reel 4 input-nameTake Score input-nameThrottle input-nameTilt input-nameTilt 1 input-nameTilt 2 input-nameTilt 3 input-nameTilt 4 input-nameToggle Cheat input-nameTrack X input-nameTrack X 10 input-nameTrack X 2 input-nameTrack X 3 input-nameTrack X 4 input-nameTrack X 5 input-nameTrack X 6 input-nameTrack X 7 input-nameTrack X 8 input-nameTrack X 9 input-nameTrack Y input-nameTrack Y 10 input-nameTrack Y 2 input-nameTrack Y 3 input-nameTrack Y 4 input-nameTrack Y 5 input-nameTrack Y 6 input-nameTrack Y 7 input-nameTrack Y 8 input-nameTrack Y 9 input-nameUI (First) Tape Start input-nameUI (First) Tape Stop input-nameUI Add/Remove favorite input-nameUI Audit Media input-nameUI Cancel input-nameUI Clear input-nameUI Default Zoom input-nameUI Display Comment input-nameUI Down input-nameUI End input-nameUI Export List input-nameUI External DAT View input-nameUI Focus Next input-nameUI Focus Previous input-nameUI Home input-nameUI Left input-nameUI Next Group input-nameUI Page Down input-nameUI Page Up input-nameUI Paste Text input-nameUI Previous Group input-nameUI Release Pointer input-nameUI Right input-nameUI Rotate input-nameUI Select input-nameUI Toggle input-nameUI Up input-nameUI Zoom In input-nameUI Zoom Out input-nameVolume Down input-nameVolume Up input-nameWrite current timecode kHz machine-filterAvailable machine-filterBIOS machine-filterCHD Required machine-filterCategory machine-filterClones machine-filterCustom Filter machine-filterFavorites machine-filterHorizontal Screen machine-filterManufacturer machine-filterMechanical machine-filterNo CHD Required machine-filterNot BIOS machine-filterNot Mechanical machine-filterNot Working machine-filterParents machine-filterSave Supported machine-filterSave Unsupported machine-filterUnavailable machine-filterUnfiltered machine-filterVertical Screen machine-filterWorking machine-filterYear not found path-optionArtwork path-optionArtwork Previews path-optionBosses path-optionCabinets path-optionCategory INIs path-optionCheat Files path-optionControl Panels path-optionCovers path-optionCrosshairs path-optionDATs path-optionFlyers path-optionGame Endings path-optionGame Over Screens path-optionHowTo path-optionINIs path-optionIcons path-optionLogos path-optionMarquees path-optionPCBs path-optionPlugin Data path-optionPlugins path-optionROMs path-optionScores path-optionSelect path-optionSnapshots path-optionSoftware Media path-optionSound Samples path-optionTitle Screens path-optionUI Settings path-optionUI Translations path-optionVersus playing recording selmenu-artworkArtwork Preview selmenu-artworkBosses selmenu-artworkCabinet selmenu-artworkControl Panel selmenu-artworkCovers selmenu-artworkEnding selmenu-artworkFlyer selmenu-artworkGame Over selmenu-artworkHowTo selmenu-artworkLogo selmenu-artworkMarquee selmenu-artworkPCB selmenu-artworkScores selmenu-artworkSelect selmenu-artworkSnapshots selmenu-artworkTitle Screen selmenu-artworkVersus software-filterAuthor software-filterAvailable software-filterClones software-filterCustom Filter software-filterDeveloper software-filterDevice Type software-filterDistributor software-filterFavorites software-filterParents software-filterPartially Supported software-filterProgrammer software-filterPublisher software-filterRelease Region software-filterSoftware List software-filterSupported software-filterUnavailable software-filterUnfiltered software-filterUnsupported software-filterYear stopped swlist-infoAlternate Title swlist-infoAuthor swlist-infoBarcode Number swlist-infoDeveloper swlist-infoDistributor swlist-infoISBN swlist-infoInstallation Instructions swlist-infoOEM swlist-infoOriginal Publisher swlist-infoPCB swlist-infoPart Number swlist-infoProgrammer swlist-infoRelease Date swlist-infoSerial Number swlist-infoSoftware list/item swlist-infoUsage Instructions swlist-infoVersion Project-Id-Version: MAME
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-17 07:08+1100
PO-Revision-Date: 2021-10-16 12:55+0800
Last-Translator: YuiFAN
Language-Team: MAME Language Team
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 

--- 驅動程式資訊 ---
驅動程式:  

按任意鍵繼續 

本機台尚有可執行的仿製版： %s 
        設定已儲存        

 
此機台的部分元件無法模擬，因其需要實際活動的互動或機械裝置的組合。 故無法達成此機台的完整體驗。
 
聲音：
 
此機台無法執行，機台的模擬還不完全。除了等待開發人員改良模擬之外，沒有方法可以解決這個問題。
 
視訊：
   %1$s
   %1$s    [預設值： %2$s]
   %1$s    [標籤： %2$s]
   調節器輸入    [%1$d 輸入]
   類比輸入    [%1$d 輸入]
   博奕輸入    [%1$d 輸入]
   花牌輸入    [%1$d 輸入]
   鍵盤輸入    [%1$d 輸入]
   數字鍵盤輸入    [%1$d 輸入]
   麻將輸入    [%1$d 輸入]
   畫面 '%1$s': %2$d × %3$d (H) %4$s Hz
   畫面 '%1$s'： %2$d × %3$d (V) %4$s Hz
   畫面 '%1$s'： 向量
   使用者輸入    [%1$d 輸入]
  （預設）  （已鎖定）  色彩  PENS %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
驅動程式： %4$s

CPU：
 %1$s %2$s %1$s %2$s （ %3$d / %4$d 機台 (%5$d BIOS) ） %1$s %2$s ( %3$d / %4$d 軟體套件 ) %1$s (%2$s) - %3$s
 %1$s - %2$s
 %1$s 亮度 %1$s 對比 %1$s Gamma 值 %1$s 水平位置 %1$s 水平延展 %1$s 更新率 %1$s 垂直位置 %1$s 垂直延展 %1$s 音量 %1$s [root%2$s] %1$s, %2$-.100s %1$s: %2$s
 %1$s： %2$s - 搜尋： %3$s_ %2$s
 總計發現 %d 符合 %s %s
 已新增至最愛清單。 %s
 已從最愛清單移除。 %s [%d Hz] %s [內部] %s 已加入 %s.txt 已儲存於 UI 設定資料夾 %s.xml 已儲存於 UI 設定資料夾 (EP)ROM	不完美
 (EP)ROM	無法執行
 （空） （正在執行） （正在錄製） * BIOS 設定：
  %1$d 選項    [預設值： %2$s]
 * CPU:
 * 配置設定：
 * 指撥開關設定：
 * 輸入裝置： * 媒體選項：
 * 插槽選項：
 * 聲音:
 * 視訊：
 **儲存 %s.ini 時發生錯誤** **儲存 ui.ini 時發生錯誤** , %s <設定篩選> ARGB 設定 關於 %s 已啟動: %s 已啟動: %s = %s 新增 %1$s 資料夾 - 搜尋： %2$s_ 新增資料夾 新增至最愛 新增連射按鈕 新增篩選 新增或移除最愛項目 調整速度以符合更新率 分配 Adstick 裝置 進階選項 僅對齊 所有 全部作弊碼已重新載入 全部插槽已清除且目前狀態已儲存至插槽 1 允許倒帶 類比控制 類比控制	是
 任意 確定要退出嗎？

按『%1$s』退出，
按『%2$s』繼續模擬。 裝飾圖選項 驗證標記為不可用的 %1$u 機台 ROM 檔 驗證所有 %1$u 機台的 ROM 檔 驗證媒體 正在驗證供機台 %2$u 的 %3$u ROM 檔...
%1$s 自動 自動省略畫格 自動左旋轉 自動右旋轉 連射 連射按鈕 自動 自動儲存/還原 螢幕選單出現時自動切換暫停 BIOS 選擇 BIOS 選擇 BIOS： 條碼讀取器 條碼長度無效！ 光束點大小 光束強度 最大光束寬度 最小光束寬度 雙線過濾 供截圖使用的雙線過濾 點陣圖預縮放 粗體 收入資訊 烙印擷圖 CPU 或 RAM 相機	不完美
 相機	無法執行
 取消 取消驗證？

按下 %1$s 取消
按下 %2$s 繼續 正在錄製時無法變更選項！ 無法跨目錄儲存 擷取	不完美
 擷取	無法執行
 變更 %1$s 資料夾 - 搜尋： %2$s_ 變更資料夾 此變更僅當選定 "開始新搜尋" 時生效 作弊引擎 作弊碼註解：
%s 作弊碼搜尋器 作弊碼名稱 作弊碼已加入至 cheat.simple 作弊引擎不可用 作弊碼已寫入至 %s 且已加入 cheat.simple 作弊引擎 從色盤挑選 清除監視 投幣數 %1$c： %2$d%3$s
 投幣數 %1$c： NA%3$s
 投幣脈衝 投幣鎖定 顏色預覽： 顏色 指令集 通訊	不完美
 通訊	無法執行
 未完整模擬功能:  壓縮 設定目錄 設定機台 設定機台： 設定選項 確認離開機台 控制	不完美
 控制	無法執行
 建立 十字準星偏移 %1$s 十字準星偏移 X %1$1.3f 十字準星偏移 Y %1$1.3f 十字準星選項 十字準星尺度 %1$s 十字準星尺度 X %1$1.3f 十字準星尺度 Y %1$1.3f 目前 %1$s 資料夾 目前時數 自訂 自訂 UI 機台設定開關 資料格式 預設 預設名稱為 %s 刪除已儲存的狀態檔 %1$s?
按下 %2$s 刪除
按下 %3$s 取消 裝置映對 分配轉盤裝置 差異 已停用 已停用: %s 磁碟	不完美
 磁碟	無法執行
 完成 雙擊或按下 %1$s 改變顏色 雙擊或按下 %1$s 以選擇 驅動程式 驅動程式為 BIOS	否
 驅動程式為 BIOS	是
 驅動程式相容於	%1$s
 驅動程式為主檔	
 驅動程式相容於： %-.100s 驅動程式為主檔 驅動程式： "%1$s" 軟體清單  驅動程式： %-.100s
 編輯連射按鈕 模擬 已啟用 啟用: %s 強制比例 放大右側面版的圖片 輸入條碼 錯誤存取 %s 移除已儲存的狀態檔 %1$s 時發生錯誤 結束 展開以符合 匯出顯示的清單至檔案 匯出 TXT 格式清單 （同 -listfull） 匯出 XML 格式清單 （同 -listxml） 匯出 TXT 格式清單 （同 -listxml，但不包含裝置） 查閱外部文件 載入連射選單時失敗 儲存輸入名稱檔案時失敗 快進 檔案 檔案已存在 — 是否覆蓋？ 檔案管理員 篩選 篩選 %1$u 翻轉 X 翻轉 Y 資料夾設定 字體 強制擷圖顯示比例為 4:3 省略畫格 GHz GLSL Gameinit 一般資訊 一般輸入 圖形	不完美
 圖形	色彩不完美
 圖形	OK
 圖形	無法執行
 圖形	色彩錯誤
 圖形： 不完美，  圖形： OK，  圖形： 不完整， 群組 HLSL 隱藏兩者 隱藏篩選 隱藏資訊/圖片 在可用清單中隱藏無 ROM 機台 高分檔 歷史 熱鍵 Hz 映像檔格式 映像檔資訊 圖片 未完美模擬功能:  包含仿製版 資訊自動驗證 資訊 資訊字體大小 輸入 輸入設定 （一般） 輸入設定 （本機台） 輸入選項 輸入埠名稱檔案儲存至 %s 輸入埠 輸入了無效序列 斜體 搖桿 搖桿無反應區 搖桿飽和值 鍵盤	不完美
 鍵盤	無法執行
 鍵盤輸入	是
 鍵盤模式 區域網路	不完美
 區域網路	無法執行
 語言 雷射影碟 %1$s 水平位置 雷射影碟 %1$s 水平延展 雷射影碟 %1$s 垂直位置 雷射影碟 %1$s 垂直延展 最後插槽的值 左值等於右值 位元遮罩下左值等於右值 左值等於原值 左值多於右值 左值多於原值 左值少於右值 左值少於原值 左值不等於右值 位元遮罩下左值不等於右值 左值不等於原值 光線槍 分配光線槍裝置 行 載入狀態 低延遲 MAMEinfo MARPScore MESSinfo MHz 機台設定 機台資訊 磁鼓	不完美
 磁鼓	無法執行
 磁帶	不完美
 磁帶	無法執行
 維持長寬比例 Mamescore 手動 當需要時手動切換暫停 製造商	%1$s
 主音量 符合區塊 機械式機台	否
 機械式機台	是
 媒體	不完美
 媒體	無法執行
 記憶體狀態已儲存至插槽 %d 選單預覽 麥克風	不完美
 麥克風	無法執行
 其他選項 滑鼠 滑鼠	不完美
 滑鼠	無法執行
 分配滑鼠裝置 多重鍵盤 多重滑鼠 當未限制速度時靜音 未設定 名稱：            描述：
 自然 自然鍵盤 網路裝置 新條碼： 新映像檔名稱： 否 沒有找到類別 INI 檔案 類別檔案中沒有找到群組 找不到機台，請檢查 %1$s.ini 的 ROM 目錄設定

如果是首次使用 %2$s，請參閱 docs 目錄中的 config.txt 以取得設定 %2$s 的相關資訊。 未找到外掛 未找到已儲存的狀態 非整數縮放 無 無
 不支援 畫面數 經過畫格數後按鈕將按下 經過畫格數後按鈕將放開 關 放開畫格 畫面外重新裝填 開 按下畫格 本機台有一個或更多的 ROM/CHD 是不正確的。此機台可能無法正確執行。
 本機台有一個或更多的 ROM/CHD 尚未正確地被 DUMP。
 其他控制 其他：  整體	不可執行
 整體	未模擬保護
 整體	可以執行
 整體： 不可執行 整體： 未模擬保護 整體： 可以執行 超頻 %1$s 聲音 超頻 CPU %1$s P%d 十字準星 P$d 可見度 分配划槳裝置 頁 部分支援 暫停模式 暫停／停止 分配踏板裝置 執行比較  :  插槽 %d %s %d 執行比較  ：  插槽 %d %s 插槽 %d %s %d 執行比較  ：  插槽 %d BITWISE%s 插槽 %d 執行比較：插槽 %d %s 插槽 %d 效能選項 執行 遊玩次數 玩家 玩家 %1$d 控制 請一併輸入副檔名 外掛選項 外掛 分配指向裝置 按下 %1$s 加入
 按下 %1$s 取消
 按下 %1$s 清除
 按下  %1$s 刪除 按下 %1$s 還原預設值
 按下 %1$s 設定
 按下 %s 清除熱鍵 按下 %s 刪除 按下 TAB 鍵設定 按下鍵盤按鍵、搖桿按鈕或選擇狀態以覆寫 按任意鍵繼續。 按下熱鍵按鍵或稍後保持不變 按住 印表機	不完美
 印表機	無法執行
 偽終端 打孔帶	不完美
 打孔帶	無法執行
 ROM 驗證 	停用
樣本檔驗證 	停用
 ROM 驗證結果	錯誤
 ROM 驗證結果	OK
 重新選擇上次執行的機台 讀取此映像檔，寫入至其他映像檔 讀取此映像檔，寫入差異檔 唯讀 讀寫 記錄 全部重新載入 移除 %1$s 資料夾 移除資料夾 從最愛移除 移除最後篩選 選定軟體所需要的 ROM / 磁碟映像檔為缺少或不正確。 請取得正確的檔案或選擇不同的軟體項目。

 選定系統所需要的 ROM / 磁碟映像檔為缺少或不正確。 請取得正確的檔案或選擇不同的系統。

 需要裝飾圖	否
 需要裝飾圖	是
 需要 CHD	否
 需要 CHD	是
 需要可點擊的裝飾圖	否
 需要可點擊的裝飾圖	是
 重設 全部重設 還原至預設顏色 結果將儲存至 %1$s 回到機台 回上一層選單 回上一層選單 版本： %1$s 倒帶 倒帶容量 ROM組	%1$s
 旋轉 左旋轉 右旋轉 選轉選項 取樣頻率 文字樣本～永東國酬愛鬱靈鷹袋、南去經三國，東來過五湖。 樣本檔驗證結果	錯誤
 樣本檔驗證結果	不需要
 樣本檔驗證結果	OK
 儲存 儲存作弊碼 儲存設定 儲存機台設定 儲存狀態 儲存現有的記憶體狀態至插槽 %d 儲存輸入名稱至檔案 畫面 畫面 #%d 畫面 '%1$s' 畫面方向	水平
 畫面方向	垂直
 尚未支援檯面型筐體模式的畫面翻轉。 搜尋： %1$s_ 選擇新機台 選擇存取模式 選擇供連射用的按鍵 選擇類別： 選擇要設定熱鍵的作弊碼 選擇自訂篩選： 選擇映像檔格式 選擇初始化內容 選擇要載入的狀態 選項清單 - 搜尋：  設定 設定熱鍵 設定 顯示全部 顯示文件檢視 顯示滑鼠指標 顯示側面版 同時發生的對立狀況 略過 BIOS 選擇選單 略過未完整模擬的警示訊息 略過起始的資訊畫面 略過軟體部分選擇選單 休眠 參數調整 插槽 %d 插槽 1 的值 插槽裝置 軟體列表資訊 軟體相容於： %1$-.100s 軟體為主檔 軟體列表/項目: %1$s:%2$s 軟體部分選項： 聲音 聲音	不完美
 聲音	無
 聲音	OK
 聲音	無法執行
 聲音選項 聲音： 不完美 聲音： 無 聲音： OK 聲音： 無法執行 速度 啟動機台 最大化開始 開始新搜尋 狀態/播放選項 穩定鍵 支援檯面型筐體	否
 支援即時存檔	否
 支援即時存檔	是
 已支援： 否 已支援： 部分 已支援： 是 切換項目順序 切換順序：目前項目依照描述排序 切換順序：目前項目依照短檔名排序 同步更新 系統資訊 系統名稱 系統： %1$-.100s 磁帶控制 測試 測試作弊碼 %08X_%02X 測試/寫入塞入值 選定的遊戲缺少一個至多個必要的 ROM 或 CHD 映像檔，請選擇其他遊戲。
按任意鍵繼續。 選定的軟體缺少一個或多個必要的 ROM 或 CHD 映像檔
請取得正確的檔案或選擇不同的檔案。 本機台已知具有下列的問題

 此驅動需要額外載入下列裝置的映像檔：  此機台沒有 BIOS。 此機台沒有可設定的輸入 本機台無聲音硬體，故 MAME 將不會產生聲音，這是可預期的行為。
 此機台需要額外的裝飾圖檔 本機台無法完成，並非模擬時的錯誤而可能呈現怪異的行為或缺少元件。
 限制速度 彩票分配數： %1$d

 計時器 計時	不完美
 計時	無法執行
 總時數 分配軌跡球裝置 三重緩衝 類型 類型名稱為空 輸入名稱或選擇： %1$s_ 輸入名稱或選擇： (隨機) UI 色彩設定 UI 自訂設置 UI 字體 UI 字體設定 UI 啟動 停用 UI 控制
以 %1$s 切換 啟用 UI 控制
以 %1$s 切換 無法寫入檔案
確認 cheatpath 資料夾是否存在 撤銷前次搜尋 -- # 執行時間： %1$d:%2$02d

 執行時間： %1$d:%2$02d:%3$02d

 使用外部樣本檔 使用圖片作為背景 如果你想塞入 %s，用這個 如果你想塞入最後的插槽值（如你開始時忘了某項但終於找到了），用這個 如果你想塞入插槽 1 的值（如你開始某事但忘了），用這個 使用者介面 值 向量 向量閃爍度 視訊模式 視訊選項 可見度延遲 外部網路	不完美
 外部網路	無法執行
 等待垂直同步 警示訊息 監視 視窗模式 寫入 X 僅 X 軸 X 軸或 Y 週（自動） Y 僅 Y 軸 年代	%1$s
 是 可輸入任意類型名稱 縮放 = %1$d 縮放 = 1/%1$d 縮放至畫面區域 縮放至畫面區域 [ 無 ]
 [ 無卡啟動 ] [ 此選項目前「尚未」掛載於執行中的系統 ]

選項： %1$s
裝置： %2$s

如選用此選項，下列項目將啟用：
 [ 此選項目前已掛載於執行中的系統 ]

選項： %1$s
裝置： %2$s

已選用的選項啟用下列項目：
 [ 使用檔案管理員 ] [內建] [ 完整清單 ] [建立] [空插槽] [ 空 ] [ 錯誤 ] [ 檔案管理員 ] [沒有類別 INI 檔案] [INI 檔案中無群組] [root%1$s] [root%2$s] [軟體清單] 透明度 藍 綠 紅 背景 邊框 相容版本 機台設定開關 圖形檢視器背景 滑鼠按下背景顏色 滑鼠按下顏色 滑鼠經過背景顏色 滑鼠經過顏色 一般文字 一般文字背景 已選擇背景顏色 選擇色彩 參數調整 子項目顏色 未擁有遊戲顏色 黑 藍 灰 綠 橘 紅 銀 紫 白 黃 相容版本 滑鼠經過 一般項目 已選擇 子項目 預設 區域網路 外部網路 相機 擷取硬體 色盤 通訊 控制 磁碟 圖形 鍵盤 磁鼓 磁帶 媒體 麥克風 滑鼠 印表機 保護 打孔帶 固態儲存 聲音 計時 不正確的檢查碼 不正確的長度 玩家 1 開始 玩家 2 開始 玩家 3 開始 玩家 4 開始 玩家 5 開始 玩家 6 開始 玩家 7 開始 玩家 8 開始 AD 搖桿 X AD 搖桿 X 10 AD 搖桿 X 2 AD 搖桿 X 3 AD 搖桿 X 4 AD 搖桿 X 5 AD 搖桿 X 6 AD 搖桿 X 7 AD 搖桿 X 8 AD 搖桿 X 9 AD 搖桿 Y AD 搖桿 Y 10 AD 搖桿 Y 2 AD 搖桿 Y 3 AD 搖桿 Y 4 AD 搖桿 Y 5 AD 搖桿 Y 6 AD 搖桿 Y 7 AD 搖桿 Y 8 AD 搖桿 Y 9 AD 搖桿 Z AD 搖桿 Z 10 AD 搖桿 Z 2 AD 搖桿 Z 3 AD 搖桿 Z 4 AD 搖桿 Z 5 AD 搖桿 Z 6 AD 搖桿 Z 7 AD 搖桿 Z 8 AD 搖桿 Z 9 押分 吸鈔口 1 記帳 除錯器內中斷 取消 投幣孔 1 投幣孔 10 投幣孔 11 投幣孔 12 投幣孔 2 投幣孔 3 投幣孔 4 投幣孔 5 投幣孔 6 投幣孔 7 投幣孔 8 投幣孔 9 組態選單 發牌 方向盤 方向盤 10 方向盤 2 方向盤 3 方向盤 4 方向盤 5 方向盤 6 方向盤 7 方向盤 8 方向盤 9 方向盤 V 方向盤 V 10 方向盤 V 2 方向盤 V 3 方向盤 V 4 方向盤 V 5 方向盤 V 6 方向盤 V 7 方向盤 V 8 方向盤 V 9 機門 機門連鎖 比倍 快轉 減少省略畫格 增加省略畫格 半押 大 保留 1 保留 2 保留 3 保留 4 保留 5 開分 洗分 鍵盤 小鍵盤 光線槍 X 光線槍 X 10 光線槍 X 2 光線槍 X 3 光線槍 X 4 光線槍 X 5 光線槍 X 6 光線槍 X 7 光線槍 X 8 光線槍 X 9 光線槍 Y 光線槍 Y 10 光線槍 Y 2 光線槍 Y 3 光線槍 Y 4 光線槍 Y 5 光線槍 Y 6 光線槍 Y 7 光線槍 Y 8 光線槍 Y 9 載入狀態 小 記憶體重置 滑鼠 X 滑鼠 X 10 滑鼠 X 2 滑鼠 X 3 滑鼠 X 4 滑鼠 X 5 滑鼠 X 6 滑鼠 X 7 滑鼠 X 8 滑鼠 X 9 滑鼠 Y 滑鼠 Y 10 滑鼠 Y 2 滑鼠 Y 3 滑鼠 Y 4 滑鼠 Y 5 滑鼠 Y 6 滑鼠 Y 7 滑鼠 Y 8 滑鼠 Y 9 參數調整 P1 按鈕 1 P1 按鈕 10 P1 按鈕 11 P1 按鈕 12 P1 按鈕 13 P1 按鈕 14 P1 按鈕 15 P1 按鈕 16 P1 按鈕 2 P1 按鈕 3 P1 按鈕 4 P1 按鈕 5 P1 按鈕 6 P1 按鈕 7 P1 按鈕 8 P1 按鈕 9 P1 下 P1 花牌 A/1 P1 花牌 B/2 P1 花牌 C/3 P1 花牌 D/4 P1 花牌 E/5 P1 花牌 F/6 P1 花牌 G/7 P1 花牌 H/8 P1 花牌 否 P1 花牌 是 P1 左 P1 左搖桿/下 P1 左搖桿/左 P1 左搖桿/右 P1 左搖桿/上 P1 麻將 A P1 麻將 B P1 麻將 押分 P1 麻將 大 P1 麻將 C P1 麻將 吃 P1 麻將 D P1 麻將 加倍 P1 麻將 E P1 麻將 F P1 麻將 交換牌 P1 麻將 G P1 麻將 H P1 麻將 I P1 麻將 J P1 麻將 K P1 麻將 槓 P1 麻將 L P1 麻將 海底機會 P1 麻將 M P1 麻將 N P1 麻將 O P1 麻將 P P1 麻將 碰 P1 麻將 Q P1 麻將 聽 P1 麻將 胡 P1 麻將 小 P1 麻將 得分 P1 踏板 1 P1 踏板 2 P1 踏板 3 P1 右 P1 右搖桿/下 P1 右搖桿/左 P1 右搖桿/右 P1 右搖桿/上 P1 選擇 P1 開始 P1 上 P10 按鈕 1 P10 按鈕 10 P10 按鈕 11 P10 按鈕 12 P10 按鈕 13 P10 按鈕 14 P10 按鈕 15 P10 按鈕 16 P10 按鈕 2 P10 按鈕 3 P10 按鈕 4 P10 按鈕 5 P10 按鈕 6 P10 按鈕 7 P10 按鈕 8 P10 按鈕 9 P10 下 P10 左 P10 左搖桿/下 P10 左搖桿/左 P10 左搖桿/右 P10 左搖桿/上 P10 踏板 1 P10 踏板 2 P10 踏板 3 P10 右 P10 右搖桿/下 P10 右搖桿/左 P10 右搖桿/右 P10 右搖桿/上 P10 選擇 P10 開始 P10 上 P2 按鈕 1 P2 按鈕 10 P2 按鈕 11 P2 按鈕 12 P2 按鈕 13 P2 按鈕 14 P2 按鈕 15 P2 按鈕 16 P2 按鈕 2 P2 按鈕 3 P2 按鈕 4 P2 按鈕 5 P2 按鈕 6 P2 按鈕 7 P2 按鈕 8 P2 按鈕 9 P2 下 P2 花牌 A/1 P2 花牌 B/2 P2 花牌 C/3 P2 花牌 D/4 P2 花牌 E/5 P2 花牌 F/6 P2 花牌 G/7 P2 花牌 H/8 P2 花牌 否 P2 花牌 是 P2 左 P2 左搖桿/下 P2 左搖桿/左 P2 左搖桿/右 P2 左搖桿/上 P2 麻將 A P2 麻將 B P2 麻將 押分 P2 麻將 大 P2 麻將 C P2 麻將 吃 P2 麻將 D P2 麻將 加倍 P2 麻將 E P2 麻將 F P2 麻將 交換牌 P2 麻將 G P2 麻將 H P2 麻將 I P2 麻將 J P2 麻將 K P2 麻將 槓 P2 麻將 L P2 麻將 海底機會 P2 麻將 M P2 麻將 N P2 麻將 O P2 麻將 P P2 麻將 碰 P2 麻將 Q P2 麻將 聽 P2 麻將 胡 P2 麻將 小 P2 麻將 得分 P2 踏板 1 P2 踏板 2 P2 踏板 3 P2 右 P2 右搖桿/下 P2 右搖桿/左 P2 右搖桿/右 P2 右搖桿/上 P2 選擇 P2 開始 P2 上 P3 按鈕 1 P3 按鈕 10 P3 按鈕 11 P3 按鈕 12 P3 按鈕 13 P3 按鈕 14 P3 按鈕 15 P3 按鈕 16 P3 按鈕 2 P3 按鈕 3 P3 按鈕 4 P3 按鈕 5 P3 按鈕 6 P3 按鈕 7 P3 按鈕 8 P3 按鈕 9 P3 下 P3 左 P3 左搖桿/下 P3 左搖桿/左 P3 左搖桿/右 P3 左搖桿/上 P3 踏板 1 P3 踏板 2 P3 踏板 3 P3 右 P3 右搖桿/下 P3 右搖桿/左 P3 右搖桿/右 P3 右搖桿/上 P3 選擇 P3 開始 P3 上 P4 按鈕 1 P4 按鈕 10 P4 按鈕 11 P4 按鈕 12 P4 按鈕 13 P4 按鈕 14 P4 按鈕 15 P4 按鈕 16 P4 按鈕 2 P4 按鈕 3 P4 按鈕 4 P4 按鈕 5 P4 按鈕 6 P4 按鈕 7 P4 按鈕 8 P4 按鈕 9 P4 下 P4 左 P4 左搖桿/下 P4 左搖桿/左 P4 左搖桿/右 P4 左搖桿/上 P4 踏板 1 P4 踏板 2 P4 踏板 3 P4 右 P4 右搖桿/下 P4 右搖桿/左 P4 右搖桿/右 P4 右搖桿/上 P4 選擇 P4 開始 P4 上 P5 按鈕 1 P5 按鈕 10 P5 按鈕 11 P5 按鈕 12 P5 按鈕 13 P5 按鈕 14 P5 按鈕 15 P5 按鈕 16 P5 按鈕 2 P5 按鈕 3 P5 按鈕 4 P5 按鈕 5 P5 按鈕 6 P5 按鈕 7 P5 按鈕 8 P5 按鈕 9 P5 下 P5 左 P5 左搖桿/下 P5 左搖桿/左 P5 左搖桿/右 P5 左搖桿/上 P5 踏板 1 P5 踏板 2 P5 踏板 3 P5 右 P5 右搖桿/下 P5 右搖桿/左 P5 右搖桿/右 P5 右搖桿/上 P5 選擇 P5 開始 P5 上 P6 按鈕 1 P6 按鈕 10 P6 按鈕 11 P6 按鈕 12 P6 按鈕 13 P6 按鈕 14 P6 按鈕 15 P6 按鈕 16 P6 按鈕 2 P6 按鈕 3 P6 按鈕 4 P6 按鈕 5 P6 按鈕 6 P6 按鈕 7 P6 按鈕 8 P6 按鈕 9 P6 下 P6 左 P6 左搖桿/下 P6 左搖桿/左 P6 左搖桿/右 P6 左搖桿/上 P6 踏板 1 P6 踏板 2 P6 踏板 3 P6 右 P6 右搖桿/下 P6 右搖桿/左 P6 右搖桿/右 P6 右搖桿/上 P6 選擇 P6 開始 P6 上 P7 按鈕 1 P7 按鈕 10 P7 按鈕 11 P7 按鈕 12 P7 按鈕 13 P7 按鈕 14 P7 按鈕 15 P7 按鈕 16 P7 按鈕 2 P7 按鈕 3 P7 按鈕 4 P7 按鈕 5 P7 按鈕 6 P7 按鈕 7 P7 按鈕 8 P7 按鈕 9 P7 下 P7 左 P7 左搖桿/下 P7 左搖桿/左 P7 左搖桿/右 P7 左搖桿/上 P7 踏板 1 P7 踏板 2 P7 踏板 3 P7 右 P7 右搖桿/下 P7 右搖桿/左 P7 右搖桿/右 P7 右搖桿/上 P7 選擇 P7 開始 P7 上 P8 按鈕 1 P8 按鈕 10 P8 按鈕 11 P8 按鈕 12 P8 按鈕 13 P8 按鈕 14 P8 按鈕 15 P8 按鈕 16 P8 按鈕 2 P8 按鈕 3 P8 按鈕 4 P8 按鈕 5 P8 按鈕 6 P8 按鈕 7 P8 按鈕 8 P8 按鈕 9 P8 下 P8 左 P8 左搖桿/下 P8 左搖桿/左 P8 左搖桿/右 P8 左搖桿/上 P8 踏板 1 P8 踏板 2 P8 踏板 3 P8 右 P8 右搖桿/下 P8 右搖桿/左 P8 右搖桿/右 P8 右搖桿/上 P8 選擇 P8 開始 P8 上 P9 按鈕 1 P9 按鈕 10 P9 按鈕 11 P9 按鈕 12 P9 按鈕 13 P9 按鈕 14 P9 按鈕 15 P9 按鈕 16 P9 按鈕 2 P9 按鈕 3 P9 按鈕 4 P9 按鈕 5 P9 按鈕 6 P9 按鈕 7 P9 按鈕 8 P9 按鈕 9 P9 下 P9 左 P9 左搖桿/下 P9 左搖桿/左 P9 左搖桿/右 P9 左搖桿/上 P9 踏板 1 P9 踏板 2 P9 踏板 3 P9 右 P9 右搖桿/下 P9 右搖桿/左 P9 右搖桿/右 P9 右搖桿/上 P9 選擇 P9 開始 P9 上 操縱桿 操縱桿 10 操縱桿 2 操縱桿 3 操縱桿 4 操縱桿 5 操縱桿 6 操縱桿 7 操縱桿 8 操縱桿 9 操縱桿 V 操縱桿 V 10 操縱桿 V 2 操縱桿 V 3 操縱桿 V 4 操縱桿 V 5 操縱桿 V 6 操縱桿 V 7 操縱桿 V 8 操縱桿 V 9 暫停 暫停 - 單步 退幣 定位器 定位器 10 定位器 2 定位器 3 定位器 4 定位器 5 定位器 6 定位器 7 定位器 8 定位器 9 定位器 V 定位器 V 10 定位器 V 2 定位器 V 3 定位器 V 4 定位器 V 5 定位器 V 6 定位器 V 7 定位器 V 8 定位器 V 9 關機 開機 錄製 AVI 錄製 MNG 重新啟動機台 倒帶 - 單步 儲存擷圖 儲存狀態 業務模式 業務開關 1 業務開關 2 業務開關 3 業務開關 4 顯示解碼圖形 顯示 FPS 顯示效能分析 軟體重新啟動 停牌 停止所有捲軸 停止捲軸 1 停止捲軸 2 停止捲軸 3 停止捲軸 4 得分 限制速度 碰撞機台 碰撞機台 1 碰撞機台 2 碰撞機台 3 碰撞機台 4 切換作弊碼 軌跡球 X 軌跡球 X 10 軌跡球 X 2 軌跡球 X 3 軌跡球 X 4 軌跡球 X 5 軌跡球 X 6 軌跡球 X 7 軌跡球 X 8 軌跡球 X 9 軌跡球 Y 軌跡球 Y 10 軌跡球 Y 2 軌跡球 Y 3 軌跡球 Y 4 軌跡球 Y 5 軌跡球 Y 6 軌跡球 Y 7 軌跡球 Y 8 軌跡球 Y 9 選單 (首)磁帶開始 選單 (首)磁帶停止 選單 新增/移除最愛項目 選單 驗證媒體 選單 取消鍵 選單 清除鍵 選單 預設縮放 選單 顯示註解 UI 向下鍵 選單 最下鍵 選單 匯出清單 選單 檢視外部 DAT 選單 下一焦點 選單 上一焦點 選單 最上鍵 選單 向左鍵 選單 下一組 選單 下一頁 選單 上一頁 選單 貼上文字 選單 上一組 選單 釋放指標 選單 向右鍵 選單 旋轉 選單 選定鍵 選單 切換 選單 向上鍵 選單 放大 選單 縮小 音量降低 音量提升 寫入目前的時間碼 kHz 可用 BIOS 需要 CHD 類別 仿製版 自訂篩選 最愛 水平螢幕 製造商 機械式 不需要 CHD 非 BIOS 非機械式 不可執行 母檔 已支援即時存檔 未支援即時存檔 不可用 未篩選 垂直螢幕 可以執行 年代 未找到 裝飾圖 裝飾圖預覽 首領 筐體圖 類別 INI 作弊檔 操作檯 封面 十字準星 文件 廣告圖 遊戲結局 遊戲結束畫面 說明圖 INI 圖示 標題圖 標籤圖 PCB 圖 外掛資料 外掛 ROM 得分 精選 擷圖 軟體媒體 聲音樣本檔 標題畫面 UI 設定 UI 翻譯 對戰 正在執行 正在錄製 裝飾圖預覽 首領 筐體圖 操作檯 封面 結局 廣告圖 遊戲結束 說明圖 標題圖 標籤圖 PCB 圖 得分 精選 擷圖 標題畫面 對戰 作者 可用 仿製版 自訂篩選 開發商 裝置類別 經銷商 最愛 母檔 部分支援 程式設計 出版商 釋出區域 軟體清單 已支援 不可用 未篩選 不支援 年代 已停止 別題名 作者 條碼編號 開發商 經銷商 ISBN 安裝說明 OEM 原出版商 PCB 配件編號 程式設計 發行日 序號 軟體列表/項目 使用說明 版本 