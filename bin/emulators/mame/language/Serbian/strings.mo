��    t     �              \     ]  .   y     �     �  �   �     �  
   �  	   �     �     �  	   �     �     �  	   �     �     �  "   �  	     .     +   D     p     �  
   �     �     �     �     �     �     �                    :     @      ]     ~  	   �     �     �     �     �     �  
           
        '     ;  ^   K     �     �     �     �     �     �     �          #     6     ;     L  "   g     �     �     �     �     �     �     �     �          	          '     9     K     R     h     �     �     �     �     �     �               (  $   1     V     ]     v     �     �     �     �     �     �  
   �          	  *   '  )   R  >   |     �     �     �     �     �                 !      '      0      =      L      `      {      �      �      �      �   	   �      �      �      !     !     !     .!     5!     ;!     K!     [!     p!     w!     �!     �!     �!     �!     �!     �!     "     6"     <"     E"     N"     R"     h"  	   |"     �"     �"     �"     �"     �"     �"     �"      #     #     #     %#     5#  �   8#     $     $     $     $  Y   !$  G   {$     �$     �$     �$     %     %     ,%     K%     \%     o%  
   �%     �%  !   �%     �%     �%     �%     �%     �%  '   	&     1&  	   P&  
   Z&     e&  
   l&     w&     �&     �&     �&     �&  	   �&     �&     �&     �&     
'     '     '     %'  F   1'     x'     �'  
   �'     �'     �'     �'  3   �'     (     )(     <(     O(     f(     z(     �(     �(     �(     �(     �(     �(     �(     )     )     /)     5)  	   F)     P)     e)     s)  	   �)     �)     �)     �)     �)     �)     �)     �)     *  ,   *  E   <*  ]   �*  .   �*  z   +     �+     �+     �+     �+     �+     �+     	,     ,     <,     Q,     `,     g,     v,     �,     �,     �,  
   �,     �,     �,     �,     �,     �,     �,     �,     �,     �,     -     %-     9-     K-     c-     w-     �-  '   �-  (   �-     �-  (   .     :.     X.  #   q.  &   �.     �.     �.     �.     /     ,/     ?/     Q/     c/     v/     �/     �/     �/     �/     �/     �/     �/     0     )0     ?0     T0     \0     u0     �0     �0     �0     �0     �0     1     11     N1     f1     j1     �1     �1      �1     �1     �1     2     &2     A2     `2     w2     �2     �2     �2     �2     �2     �2     3     )3     :3     M3     f3     x3     �3     �3     �3     �3     �3     �3     �3     
4      4     :4     M4  	   U4     _4     4     �4     �4     �4     �4     �4     
5     5     55     L5     f5     }5     �5     �5     �5     �5     6     6  �  6  (   �7  +   8  "   78     Z8  �   b8     �8     9     9     %9     +9  	   39     =9     D9  	   L9     V9     ^9  #   e9  	   �9  -   �9  ,   �9     �9      :  	   :     :     3:     R:     p:     �:     �:     �:     �:     �:     �:     �:  !   ;     7;     D;  
   S;  &   ^;  $   �;     �;  )   �;     �;     �;     <     <     :<  �   L<  
   �<     �<     �<     �<     �<      =     /=     @=     Z=  
   r=      }=  %   �=  +   �=     �=     >     >     %>     ->     @>     W>     l>     y>     ~>     �>     �>     �>     �>     �>     �>     ?     ?     -?     B?     \?     v?     �?     �?     �?  .   �?     �?     @     @     1@     O@     b@     �@  	   �@  
   �@     �@     �@      �@  -   �@  ,   A  >   ;A     zA     �A     �A     �A     �A     �A     �A     �A     B     B     *B     <B     QB     kB     wB     �B     �B     �B     �B     �B     �B     C     C     C     5C     >C     JC      gC  #   �C  	   �C     �C     �C     �C     �C  &   D  *   (D  $   SD  (   xD     �D     �D     �D     �D     �D     �D  	   E     E      E     5E     JE     OE     `E     yE     �E     �E     �E  	   �E     �E  �   �E     �F     �F     �F  
   �F  d   �F  J   IG     �G     �G  "   �G     �G     �G  "   �G      H      0H     QH  
   fH     qH     wH     �H     �H     �H  (   �H     �H     �H     I     ?I     MI     ^I     dI     wI     �I     �I     �I     �I     �I  #   �I     J     +J     :J     CJ     RJ     ZJ  H   rJ     �J     �J  	   �J     �J  !   �J     K  0   1K     bK     rK     �K     �K     �K     �K     �K     �K     �K     L     L     0L     >L     \L     nL     �L     �L     �L     �L     �L     �L     �L     �L     �L     M     !M     /M     BM     XM     jM  *   yM  I   �M  ]   �M  .   LN  �   {N     �N  "   O  /   7O  )   gO     �O  -   �O     �O      �O     P     0P     FP     MP     `P     mP     �P     �P     �P     �P     �P  	   �P     �P     �P     �P     �P     �P     Q     Q     Q     Q     Q     &Q     .Q     7Q     EQ      _Q     �Q      �Q     �Q     �Q     �Q      �Q     R     *R     8R     HR     [R     `R     gR     lR     tR     �R     �R     �R     �R     �R     �R     �R     �R     �R  
   �R     �R     �R     �R     �R  	   �R     S     S     S  	   "S     ,S     5S     :S  
   >S     IS     QS     aS     tS  
   �S     �S     �S     �S     �S     �S     �S     �S     �S     �S     T     T     T     %T     1T     7T     NT     WT     cT     iT     oT     �T     �T     �T     �T     �T     �T     �T     �T     U     U     U     0U     8U     >U     RU     [U     `U     yU     �U     �U     �U     �U     �U     �U     �U     �U     �U  	   �U   

Press any key to continue 

There are working clones of this machine: %s 
    Configuration saved    

 
Sound:
 
THIS MACHINE DOESN'T WORK. The emulation for this machine is not yet complete. There is nothing you can do to fix this problem except wait for the developers to improve the emulation.
 
Video:
  (default)  (locked)  COLORS  PENS %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Driver: %4$s

CPU:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d machines (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d software packages ) %1$s Brightness %1$s Contrast %1$s Gamma %1$s Horiz Position %1$s Horiz Stretch %1$s Refresh Rate %1$s Vert Position %1$s Vert Stretch %1$s Volume %1$s, %2$-.100s %1$s: %2$s
 %1$s: %2$s - Search: %3$s_ %2$s
 %s
 added to favorites list. %s
 removed from favorites list. %s [internal] (playing) (recording) **Error saving %s.ini** **Error saving ui.ini** ARGB Settings Add %1$s Folder - Search: %2$s_ Add Folder Add To Favorites Add filter All cheats reloaded Analog Controls Are you sure you want to quit?

Press ''%1$s'' to quit,
Press ''%2$s'' to return to emulation. Auto BIOS BIOS Selection BIOS selection: Barcode Reader Barcode length invalid! Beam Intensity Weight Beam Width Maximum Beam Width Minimum Bold Bookkeeping Info Cannot save over directory Change %1$s Folder - Search: %2$s_ Change Folder Cheat Cheat Comment:
%s Cheats Choose from palette Coin %1$c: %2$d%3$s
 Coin %1$c: NA%3$s
 Color preview: Colors Command Configure Directories Configure Machine Configure Options Create Crosshair Offset %1$s Crosshair Offset X %1$1.3f Crosshair Offset Y %1$1.3f Crosshair Options Crosshair Scale %1$s Crosshair Scale X %1$1.3f Crosshair Scale Y %1$1.3f Current %1$s Folders Customize UI DIP Switches Disabled Double-click or press %1$s to select Driver Driver is Clone of	%1$s
 Driver is Parent	
 Driver is clone of: %1$-.100s Driver is parent Driver: "%1$s" software list  Driver: %1$-.100s Emulated Enabled Enter Code Exit Export displayed list to file Export list in TXT format (like -listfull) Export list in XML format (like -listxml) Export list in XML format (like -listxml, but exclude devices) External DAT View Fast Forward File File Already Exists - Override? File Manager Filter Folders Setup Fonts Gameinit General Info General Inputs Graphics	Imperfect
 Graphics	Imperfect Colors
 Graphics	OK
 Graphics	Unimplemented
 Graphics: Imperfect,  Graphics: OK,  Graphics: Unimplemented,  Hide Both Hide Filters Hide Info/Image History Image Format: Image Information Images Infos Infos text size Input (general) Input (this Machine) Italic Keyboard	Imperfect
 Keyboard	Unimplemented
 Keyboard Mode Language Laserdisc '%1$s' Horiz Position Laserdisc '%1$s' Horiz Stretch Laserdisc '%1$s' Vert Position Laserdisc '%1$s' Vert Stretch Lines MAMEinfo MESSinfo MHz Machine Configuration Machine Information Mamescore Manufacturer	%1$s
 Master Volume Menu Preview Mouse Mouse	Imperfect
 Mouse	Unimplemented
 Name:             Description:
 Natural Network Devices New Barcode: New Image Name: No No machines found. Please check the rompath specified in the %1$s.ini file.

If this is your first time using %2$s, please see the config.txt file in the docs directory for information on configuring %2$s. None
 Not supported Off On One or more ROMs/CHDs for this machine are incorrect. The machine may not run correctly.
 One or more ROMs/CHDs for this machine have not been correctly dumped.
 Other Controls Overall	NOT WORKING
 Overall	Unemulated Protection
 Overall	Working
 Overall: NOT WORKING Overall: Unemulated Protection Overall: Working Overclock CPU %1$s Partially supported Pause/Stop Play Please enter a file extension too Plugin Options Plugins Press TAB to set Press any key to continue. Pseudo terminals Read this image, write to another image Read this image, write to diff Read-only Read-write Record Reload All Remove %1$s Folder Remove Folder Remove From Favorites Remove last filter Reset Reset All Return to Machine Return to Previous Menu Revision: %1$s Rewind Romset	%1$s
 Rotate Sample Rate Sample text - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Save Configuration Screen Screen #%d Screen '%1$s' Screen Orientation	Horizontal
 Screen Orientation	Vertical
 Screen flipping in cocktail mode is not supported.
 Search: %1$s_ Select New Machine Select access mode Select custom filters: Select image format Selection List - Search:  Settings Show All Show DATs view Show side panels Slider Controls Slot Devices Software is clone of: %1$-.100s Software is parent Software part selection: Sound Sound	Imperfect
 Sound	OK
 Sound	Unimplemented
 Sound Options Sound: Imperfect Sound: OK Sound: Unimplemented Supported: No Supported: Partial Supported: Yes Switch Item Ordering Sysinfo System: %1$-.100s Tape Control There are known problems with this machine

 This driver requires images to be loaded in the following device(s):  This machine has no sound hardware, MAME will produce no sounds, this is expected behaviour.
 This machine requires external artwork files.
 This machine was never completed. It may exhibit strange behavior or missing elements that are not bugs in the emulation.
 Tickets dispensed: %1$d

 Type name or select: %1$s_ Type name or select: (random) UI Color Settings UI Font UI Fonts Settings Uptime: %1$d:%2$02d

 Uptime: %1$d:%2$02d:%3$02d

 Use External Samples User Interface Vector Vector Flicker Video Options Visible Delay X Y Year	%1$s
 Yes [compatible lists] [create] [empty slot] [empty] [failed] [file manager] [software list] color-channelAlpha color-channelBlue color-channelGreen color-channelRed color-optionBackground color-optionBorder color-optionClone color-optionDIP switch color-optionGraphics viewer background color-optionMouse down background color color-optionMouse down color color-optionMouse over background color color-optionMouse over color color-optionNormal text color-optionNormal text background color-optionSelected background color color-optionSelected color color-optionSlider color color-optionSubitem color color-optionUnavailable color color-presetBlack color-presetBlue color-presetGray color-presetGreen color-presetOrange color-presetRed color-presetSilver color-presetViolet color-presetWhite color-presetYellow color-sampleClone color-sampleMouse Over color-sampleNormal color-sampleSelected color-sampleSubitem default emulation-featurecamera emulation-featuredisk emulation-featuregraphics emulation-featurekeyboard emulation-featuremagnetic tape emulation-featuremicrophone emulation-featuremouse emulation-featureprinter emulation-featureprotection emulation-featuresound kHz machine-filterCategory machine-filterClones machine-filterFavorites machine-filterHorizontal Screen machine-filterManufacturer machine-filterMechanical machine-filterNot Mechanical machine-filterNot Working machine-filterVertical Screen machine-filterWorking machine-filterYear path-optionArtwork path-optionArtwork Previews path-optionBosses path-optionCabinets path-optionControl Panels path-optionCovers path-optionCrosshairs path-optionDATs path-optionFlyers path-optionGame Endings path-optionHowTo path-optionINIs path-optionIcons path-optionLogos path-optionMarquees path-optionPCBs path-optionROMs path-optionScores path-optionSelect path-optionSnapshots path-optionTitle Screens path-optionVersus playing recording selmenu-artworkArtwork Preview selmenu-artworkBosses selmenu-artworkCabinet selmenu-artworkCovers selmenu-artworkGame Over selmenu-artworkHowTo selmenu-artworkLogo selmenu-artworkPCB selmenu-artworkScores selmenu-artworkSelect selmenu-artworkSnapshots selmenu-artworkVersus software-filterClones software-filterDevice Type software-filterFavorites software-filterPublisher software-filterSoftware List software-filterYear stopped Project-Id-Version: MAME
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-17 07:08+1100
PO-Revision-Date: 2016-03-02 21:46+0100
Last-Translator: Automatically generated
Language-Team: MAME Language Team
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.8.7
 

Pritsnite bilo koji taster za nastavak 

Postoji klonovi ove mašine koji rade: %s 
    Konfiguracija sačuvana    

 
Zvuk:
 
OVA MAŠINA NE RADI. Emulacija ove mašine još nije potpuna. Ne možete ništa uraditi u vezi s tim, osim da sačekate programere da unaprede emulaciju.
 
Video:
  (uobičajeno)  (zakǉučano)  BOJE  OLOVKE %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Drajver: %4$s

CPU:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d mašine (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d programskih paketa ) %1$s osvetǉenost %1$s kontrast %1$s gama %1$s horizontalna pozicija %1$s rastezaǌe po horizontali %1$s frekvencija osvežavaǌa %1$s vertikalna pozicija %1$s rastezaǌe po vertikali %1$s jačina zvuka %1$s, %2$-.100s %1$s: %2$s
 %1$s: %2$s - Pretraga: %3$s_ %2$s
 %s
 dodato u listu omiǉenih %s
 izbrisano iz liste omiǉenih. %s [interno] (reprodukcija) (snimaǌe) **Greška prlikom zapisivaǌa %s.ini** **Greška prilikom snimaǌa ui.ini** ARGB podešavaǌa Dodaj %1$s direktorijum - Pretraga: %2$s_ Dodaj direktorijum Dodaj u listu omiǉenih Dodaj filter Sva varaǌa ponovo učitana Analogne kontrole Da li ste sigurni da želite da napustite program?

Pritisnite ''%s'' za napuštaǌe programa,
Pritisnite ''%s'' za nastavak emulacije. Automatski BIOS Izbor BIOS-a Izbor BIOS-a: Barkod čitač Neodgovarajuća dužina barkoda! Intenzitet snopa Maksimalna širinia snopa Minimalna širina snopa Podebǉano Informacije o statistici mašine Nemoguće snimiti preko direktorijuma Promeni %1$s direktorijum - Pretraga: %2$s_ Promeni direktorijum Varaǌe Komentar za varaǌe:
%s Varaǌa Izabrati iz palete Žeton %1$c: %2$d%3$s
 Žeton %1$c: NA%3$s
 Prikaz boja: Boje Komanda Konfiguracija direktorijuma Konfiguracija mašine Konfiguracija opcija Kreiraj Pomeraj nišana %1$s Pomeraj nišana X %1$1.3f Pomeraj nišana Y %1$1.3f Opcije nišana Razmera nišana %1$s Razmera nišana X %1$1.3f Razmera nišana Y %1$1.3f Trenutni %1$s direktorijum Prilagodi korisnički interfejs DIP prekidači Onemogućeno Dupli klik ili pritisnite %1$s za selektovaǌe Drajver Drajver je klon od	%1$s
 Drajver je predak	
 Drajver je klon od: %1$-.100s Drajver je osnovni Drajver: "%1$s" lista programa  Drajver: %1$-.100s Emulirani Omogućeno Unesite barkod Izlaz Eksportuj prikazanu listu u fajl Eksportuj listu u TXT formatu (kao -listfull) Eksportuj listu u XML formatu (kao -listxml) Eksportuj listu u XML formatu (kao -listxml, ali bez uredjaja) Prikaz spoǉašǌeg DAT Premotaj u napred Fajl Fajl već postoji - prepiši? Upravǉaǌe fajlovima Filter Podešavaǌe direktorijuma Fontovi Inicijalizacija Opšte informacije Globalne kontrole Grafika	nesavršena
 Grafika	nesavršene boje
 Grafika	OK
 Grafika	nije implementirana
 Grafika: nesavršena,  Grafika: OK,  Grafika: nije implementirana,  Sakrij oboje Sakrij filtere Sakrij Info/Sliku Istorija Format zapisa: Informacije o programu Programi Informacije Visina teksta za informacije Podešavaǌe kontrola (globalno) Podešavaǌe kontrola (ova mašina) Iskošeno Tastatura	nesavršena
 Tastatura	nije implementirana
 Mod tastature Jezici Laserdisc '%1$s' horizontalna pozicija Laserdisc '%1$s' rastezaǌe po horizontali Laserdisc '%1$s' vertikalna pozicija Laserdisc '%1$s' rastezaǌe po vertikali Linije МАМЕ - informacije МESS - informacije MHz Konfiguracija mašine Informacije o mašini Mamescore Proizvođač	%1$s
 Glavna jačina zvuka Probni prikaz menija Miš Miš	nesavršen
 Miš	nije implementiran
 Ime:              Opis:
 Prirodni Mrežni uređaji Novi barkod: Novo ime: Ne Nisu proanađene mašine. Molimo Vas, proverite putaǌu do ROM fajlova definisanu u %1$s.ini fajlu

Ako je ovo Vaš prvi put da koristite %2$s, molimo Vas pogledajte config.txt fajl u docs direktorijumu za informacije o podešavaǌu %2$s. Nijedan
 Nije podržano Iskǉučeno Ukǉučeno Jedan ili više ROM/CHD fajlova za ovu mašinu su neispravni. Mašina možda neće ispravno raditi.
 Jedan ili više ROM/CHD fajlova za ovu mašinu nisu ispravno napravǉeni.
 Ostale kontrole Uopšteno	NE RADI
 Uopšteno	Zaštita nije emulirana
 Uopšteno	radi
 Uopšteno: NE RADI Uopšteno: Zaštita nije emulirana Uopšteno: radi Ubrzaǌe osnovnog takta CPU %1$s Delimično podržano Pauza/Stop Pusti Molim unesite i ekstenziju Opcije dodataka Dodatci Pritisnite TAB da podesite Pritisnite bilo koji taster za nastavak. Pseudo terminali Čitaj ovaj fajl, piši u drugi Čitaj ovaj fajl, piši razlike Samo čitaǌe Čitaǌe-pisaǌe Snimi Ponovo učitaj sve Ukloni %1$s direktorijum Ukloni direktorijum Izbriši iz liste omiǉenih Ukloni posledǌi filter Resetuj Resetuji sve Podešavaǌe kontrola (ova mašina) Povratak u prethodni meni Revizija: %1$s Premotaj ROM skup	%1$s
 Rotiraj Učestalost uzorkovaǌa Primer teksta - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sačuvaj konfiguraciju Ekran Ekran #%d Ekran '%1$s' Orijentacija ekrana	Horizontalno
 Orijentacija ekrana	Vertikalno
 Obrtaǌe ekrana u "koktel" modu nije podržano.
 Pretraga: %1$s_ Izaberi novu mašinu Odaberi mod pristupa Izbor proizvoǉnih filtera Odaberi format Izborna lista - pretraga: Podešavaǌa Prikaži sve Prikaži DAT Prikaži bočne panele Kontrole klizača Slot uređaji Softver je klon od: %1$-.100s Softver je predak Izbor dela softvera: Zvuk Zvuk	nesavršen
 Zvuk	OK
 Zvuk	nije implementiran
 Opcije za zvuk Zvuk: nesavršen Zvuk: OK Zvuk: nije implementiran Podržano: ne Podržano: delimično Podržano: da Promeni sortiraǌe Systemske informacije Sistem: %1$-.100s Kontrola trake Postoje poznati problemi s ovom mašinom

 Za ovaj drajver je neophodno da slike budu učitane u sledeće uređaje:  Mašina ne poseduje uređaj za zvuk, MAME neće puštati zvuk, ovo je očekivano ponašaǌe.
 Mašina zahteva dodatne ilustracione fajlove.
 Mašsina nije nikad završena. Može se javiti čudno ponašaǌe ili nedostatak elemenata sto ne predstavǉa problem s emulacijom.
 Izdate karte: %1$d

 Otkucajte ime ili izaberite: %1$s_ Otkucajte ime ili izaberite: (nasumičan izbor) Podešavaǌe boja korisničkog interfejsa Font korisničkog interfejsa Podešavaǌe fontova za korisnički interfejs Vreme rada: %1$d:%2$02d

 Vreme rada: %1$d:%2$02d:%3$02d

 Koristi dodatne semplove Korisnički interfejs Vektor Trepereǌe vektora Video opcije Vidǉivo kašǌeǌe X Y Godina	%1$s
 Da [kompatibilne liste] [kreiraj] [prazan slot] [prazno] [neuspešno] [upravǉaǌe fajlovima] [lista programa] Alfa Plavа Zelenа Crvenа Pozadina Granica Kloniraj DIP prekidač Grafički prikaz pozadine Boja pozadine pri pritisku miša Boja pri pritisku miša Boja pozadine pri prelasku miša Boja pri prelasku miša Normalni tekst Pozadina normalnog teksta Boja pozadine za izabranu stavku Boja izabranog Boja klizača Boja pod-stavke Boja za nedostupno Crna Plavа Siva Zelenа Naranǆasta Crvenа Srebrna ǈubičasta Bela Žuta Kloniraj Prelay mišem Normalno Izabrano Pod-stavka uobičajeno kamera disk grafika tastatura magnetna traka mikrofon miš štampač zaštita zvuk kHz Kategorija Klonovi Lista omiǉenih Horizontalni ekran Proizvođač Mehanički Ne mehanički Ne radi Vertikalni ekran Radi Godina Ilustracije Umaǌeni prikaz ilustracija Finalni protivnici Kabineti Upravǉački paneli Maske Nišani DAT fajlovi Letci Ekrani završetka igre Uputstvo INI fajlovi Ikone Logoi Posteri iznad kabineta PCB (štampane pločice) ROM-ovi Najboǉi rezultata Izborni ekrani Snimci ekrana Naslovni ekrani Versus ekrani reprodukcija snimaǌe Pregled artwork-a Finalni protivnici Kabinet Maske Završni ekran igre Uputstvo Logo PCB (štampana pločica) Najboǉi rezultata Izborni ekrani Snimci ekrana Versus ekrani Klonovi Tip uredjaja Lista omiǉenih Izdavač Lista programa Godina stopirano 