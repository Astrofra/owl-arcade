��                       (     (     ,(  .   H(     w(     �(  �   �(     Y)     b)     j)     �)  #   �)  !   �)  #   �)  #   *  #   **  !   N*  "   p*  +   �*  +   �*     �*     +  
   $+  	   /+     9+     A+  	   G+     Q+     X+  	   `+     j+     r+  "   y+  	   �+  .   �+  +   �+     ,     ,  
   ,     *,     >,     Q,     c,     v,     �,     �,     �,     �,     �,     �,     �,     �,      -     )-     7-     @-     S-  	   j-     t-  3   �-     �-     �-     �-     �-     .     .  	   &.  	   0.     :.     R.     j.     o.     �.     �.     �.     �.  
   �.     �.  
   �.     �.     /     '/     8/     </  3   P/     �/     �/     �/  ^   �/     0  /   0      L0  .   m0     �0     �0     �0     �0     �0     �0     �0     �0     1     1     61     L1     _1     r1     �1     �1     �1     �1  
   �1     �1     �1     �1     �1     
2     2  "   42     W2  D   e2     �2     �2     �2  
   �2     �2     �2  -   3     ?3     F3     Z3     h3     }3     �3     �3     �3     �3     �3     �3     �3       4     !4     74     I4     [4     v4     �4     �4     �4     �4     �4     �4     5     5     65     P5     e5     r5     y5     �5     �5     �5     �5     �5     �5  
   �5     �5     �5     6     6     %6  $   *6     O6     V6     i6     }6     �6     �6     �6     �6     �6     7     7     7     %7  !   :7  
   \7     g7     z7     7  *   �7  )   �7  >   �7     18     C8     b8     o8     t8     �8     �8     �8     �8     �8     �8     �8  %   �8  
   �8     9     9     9     "9     19     E9     `9     m9     �9     �9     �9     �9     �9     �9  	   �9     �9     �9  (   :     6:     B:     J:     X:     j:     q:     �:     �:     �:     �:     �:     �:     �:      �:     ;     &;     -;     6;     H;     \;     p;     �;     �;     �;     �;     �;     �;     �;     <     4<     R<      f<     �<     �<     �<     �<     �<     �<  $   =     2=     J=     S=     n=  
   t=     =  	   �=     �=     �=     �=     �=     �=     �=     �=     >  	   %>     />     B>     P>     \>     s>     �>     �>     �>     �>     �>     �>     ?     
?     ?     0?     H?     W?     c?     �?     �?     �?     �?     �?     �?     �?      �?  �   	@     �@     �@     �@     �@     A     A     A  Y   A  G   tA     �A     �A     �A     �A     B     %B     DB     UB     jB     }B     �B     �B  
   �B  
   �B     �B     �B     �B  
   �B     C  !   C     *C     9C     AC     ^C     oC     �C     �C     �C     �C     �C  ,   �C     "D     8D  '   MD     uD  	   �D  
   �D     �D  
   �D     �D     �D     �D     �D     E     E     0E     AE     SE     rE     �E  	   �E     �E     �E     �E     �E     F     F     F     'F     4F     ;F     GF     TF     eF  F   qF     �F  !   �F     �F     G  
   G     G  
   0G     ;G     TG  
   [G     fG     tG     �G  3   �G     �G     �G     H     H     )H     DH     [H     oH     �H     �H     �H     �H     �H     �H     �H     �H     �H  "   I  "   5I     XI     ^I     nI     {I     �I     �I     �I     �I     �I  	   �I     �I     	J     J     (J  	   4J     >J     SJ     YJ     mJ     ~J  	   �J     �J     �J     �J     �J     �J     �J     K  2   K  0   OK     �K     �K     �K     �K     �K     �K  �   �K  ,   XL  E   �L     �L  ]   �L  .   CM  z   rM     �M     �M     N     N     (N  
   >N     IN     eN     vN     {N     �N     �N     �N     �N  	   �N  8   �N     #O     9O     OO     lO     �O  e   �O  Z   �O     ZP     iP     oP     vP  
   �P     �P     �P     �P     �P     �P     �P     �P     �P     �P     �P  
   �P     Q     Q  �   Q  �   �Q     5R     HR     QR     ^R     fR     oR     ~R     �R     �R     �R     �R     �R     �R     S     #S     7S     JS  (   bS     �S  (   �S     �S     �S  #   	T  &   -T     TT     pT     �T     �T     �T     �T     �T     �T     U     "U     3U     GU     [U     nU     �U     �U     �U     �U     �U     �U     �U     
V      V  "   9V     \V      |V     �V     �V     �V     �V     W     %W     EW     ]W     zW     �W     �W     �W  %   �W     X     $X     =X     AX     ZX     nX     �X     �X     �X     �X      �X     Y     +Y     EY     dY     |Y     �Y     �Y     �Y     �Y     
Z     %Z     ?Z     ^Z     uZ     �Z     �Z     �Z     �Z     �Z     �Z     [     $[     5[     H[     Z[     k[     }[     �[     �[     �[     �[     �[     �[     
\     \  	   %\     /\     O\     f\     ~\     �\     �\     �\     �\     �\     ]     "]     9]     S]     j]     �]     �]     �]     �]     �]  #   ^     +^     E^     d^     �^     �^     �^     �^     �^     _  o  _     |`  #   �`  1   �`     �`  	   a  �   a     �a     �a     �a     b  '   )b  $   Qb     vb  $   �b  '   �b  '   �b  #   c  +   /c  +   [c     �c  &   �c     �c     �c     �c     �c  	   �c     �c     d  	   d     d     d  (   &d  	   Od  6   Yd  +   �d     �d     �d  
   �d     �d     �d     e     4e     Ke     ee     qe     �e     �e     �e  !   �e     �e  #   �e  #   �e     f     %f     3f     Gf     ff  
   rf  8   }f     �f     �f     �f     �f     g     $g  
   4g  	   ?g  #   Ig  #   mg     �g     �g     �g     �g     �g     �g     h     h     (h     9h     Uh     gh     {h     �h  8   �h     �h     �h     �h  v   i     i  <   �i  '   �i  3   �i     ,j     8j     Hj     [j     oj     �j     �j     �j     �j     �j     �j     �j     k     k     1k     Ek     Ik  
   _k  
   jk     uk     �k     �k     �k     �k     �k     �k  
   l  ^    l     l     �l     �l  	   �l  !   �l     �l  7   �l     (m     /m     >m     Nm     cm  
   zm     �m     �m     �m     �m     �m  #   �m  '   �m     n     -n     An     Tn     pn      �n     �n     �n     �n     �n     �n      o     o     *o     Bo     Vo     co     io     �o     �o  	   �o     �o     �o     �o     �o     p     p     !p     4p     Rp  +   Xp     �p     �p     �p     �p     �p  $   �p     q     #q     Aq     Sq     _q     lq     }q  +   �q     �q     �q  	   �q  %   �q  0   r  /   Br  F   rr     �r  %   �r     �r     s  #   s     0s     ?s     Fs  
   Rs  
   ]s     hs     xs  4   �s  
   �s     �s     �s     �s     �s     �s     t  	   #t     -t     Jt     ct     zt     �t     �t     �t     �t     �t     �t  -   �t     u     u     *u     ;u     Nu  '   [u     �u     �u  
   �u     �u     �u     �u     �u  '   v     ,v     :v     Bv     Kv     ]v     rv  "   �v     �v     �v     �v     �v     �v     w  !   "w     Dw     bw     �w  %   �w     �w     �w     �w     x     !x     :x  *   Wx     �x     �x     �x     �x  
   �x     �x  	   �x     �x     �x     �x     y     y  #   0y     Ty      jy  	   �y     �y     �y  	   �y     �y     �y     �y     �y     z     .z      Dz     ez     tz     yz     �z     �z     �z     �z      �z  
   �z     {     {     0{     @{     U{  %   Y{  )   {  �   �{     �|     �|     �|     �|     �|     �|     �|  l   �|  D   =}     �}     �}  "   �}     �}     �}  "   �}     ~     #~     8~     J~     [~     b~     {~     �~     �~     �~     �~     �~     �~     �~     �~          	     #  "   :     ]     q     �     �     �  :   �     �     /�  0   J�  #   {�     ��     ��     ��     Ā     Ӏ     �     ��     �     *�     ?�     S�     d�     t�     ��     ��     ��     ȁ  &   �     	�     �     2�     @�     M�     b�     o�     v�     ��     ��     ��  I   ��      ��  $   �     D�     a�     i�     v�     ��     ��     ��  
   ��     Ƀ     ׃     ��  9   �     M�     Y�     r�     ��  %   ��     ń     ؄     �     �     �     �  
   ,�     7�     F�     W�     g�     ��  !   ��  )   ��     �     ��     �  &   �     ;�     U�     q�     x�     ��  
   ��     ��     ��     φ     �  
   �     ��     �     $�     :�     T�  	   o�     y�     ��     ��     ć     Շ     �     ��  C   �  A   \�     ��     ��     È     ֈ     �     �  �   �  +   ��  A   ��     ��  b   �  /   }�  s   ��     !�     *�     F�     L�     _�     }�     ��     ��     ��     ��  %   ֋  %   ��     "�  +   A�     m�  N   ��  ,   ׌     �      �     ?�  "   W�  c   z�  X   ލ     7�     K�     R�     Y�  
   g�     r�     �     ��     ��     ��     Ԏ     ڎ     �     �     �  
   �     ��     �  �   	�  �   ��     -�  
   C�     N�     Z�  	   a�     k�     |�     ��     ��     ɐ     ϐ     Ր     ې     ��     �     �     ��     �     #�     3�     N�     ^�     l�     ��     ��     ��     đ     ё     �     �     �     ��     ��     �     	�     �     �     �      �  	   (�     2�     :�  	   G�  	   Q�     [�     _�     c�     j�  
   y�     ��  	   ��     ��     ��     ��     ��     Ē     В  	   ֒     ��     �     �     ��     �     �     �     #�     '�     3�     8�  	   E�  	   O�     Y�  
   f�     q�  	   ��  
   ��     ��  	   ��     ��     œ     ғ     ��     ��     �     �     '�     8�     @�     E�  	   M�     W�     ^�     m�     �     ��     ��     ��     ��     ��     ��     ��     ��     Ȕ     Ԕ     ٔ     ��     �     �  	   �     �     �  	   0�     :�     ?�     O�     W�  	   ]�     g�     x�  
   }�     ��     ��     ��     ��  	   ��     ��     ˕  
   ؕ     �     �     
�     �      �     .�     :�     H�     T�     e�     j�   

--- DRIVER INFO ---
Driver:  

Press any key to continue 

There are working clones of this machine: %s 
    Configuration saved    

 
Sound:
 
THIS MACHINE DOESN'T WORK. The emulation for this machine is not yet complete. There is nothing you can do to fix this problem except wait for the developers to improve the emulation.
 
Video:
   %1$s
   %1$s    [default: %2$s]
   %1$s    [tag: %2$s]
   Adjuster inputs    [%1$d inputs]
   Analog inputs    [%1$d inputs]
   Gambling inputs    [%1$d inputs]
   Hanafuda inputs    [%1$d inputs]
   Keyboard inputs    [%1$d inputs]
   Keypad inputs    [%1$d inputs]
   Mahjong inputs    [%1$d inputs]
   Screen '%1$s': %2$d × %3$d (H) %4$s Hz
   Screen '%1$s': %2$d × %3$d (V) %4$s Hz
   Screen '%1$s': Vector
   User inputs    [%1$d inputs]
  (default)  (locked)  COLORS  PENS %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Driver: %4$s

CPU:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d machines (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d software packages ) %1$s Brightness %1$s Contrast %1$s Gamma %1$s Horiz Position %1$s Horiz Stretch %1$s Refresh Rate %1$s Vert Position %1$s Vert Stretch %1$s Volume %1$s, %2$-.100s %1$s: %2$s
 %1$s: %2$s - Search: %3$s_ %2$s
 %d total matches found %s %s
 added to favorites list. %s
 removed from favorites list. %s [internal] %s added (EP)ROM	Imperfect
 (EP)ROM	Unimplemented
 (playing) (recording) * BIOS settings:
  %1$d options    [default: %2$s]
 * CPU:
 * Configuration settings:
 * DIP switch settings:
 * Input device(s):
 * Media Options:
 * Slot Options:
 * Sound:
 * Video:
 **Error saving %s.ini** **Error saving ui.ini** , %s <set up filters> ARGB Settings Activated: %s Activated: %s = %s Add %1$s Folder - Search: %2$s_ Add Folder Add To Favorites Add filter Add or remove favorite Adstick Device Assignment Advanced Options All All cheats reloaded All slots cleared and current state saved to Slot 1 Analog Controls Analog Controls	Yes
 Any Are you sure you want to quit?

Press ''%1$s'' to quit,
Press ''%2$s'' to return to emulation. Artwork Options Audit ROMs for %1$u machines marked unavailable Audit ROMs for all %1$u machines Auditing ROMs for machine %2$u of %3$u...
%1$s Auto Auto frame skip Auto rotate left Auto rotate right Automatic save/restore BIOS BIOS Selection BIOS selection: Barcode Reader Barcode length invalid! Beam Intensity Weight Beam Width Maximum Beam Width Minimum Bilinear Filtering Bitmap Prescaling Bold Bookkeeping Info Burn-in CPU or RAM Camera	Imperfect
 Camera	Unimplemented
 Cancel Cannot save over directory Capture	Imperfect
 Capture	Unimplemented
 Change %1$s Folder - Search: %2$s_ Change Folder Changes to this only take effect when "Start new search" is selected Cheat Cheat Comment:
%s Cheat Finder Cheat Name Cheat added to cheat.simple Cheat engine not available Cheat written to %s and added to cheat.simple Cheats Choose from palette Clear Watches Coin %1$c: %2$d%3$s
 Coin %1$c: NA%3$s
 Coin impulse Coin lockout Color preview: Colors Command Communications	Imperfect
 Communications	Unimplemented
 Completely unemulated features:  Configure Directories Configure Machine Configure Options Confirm quit from machines Controls	Imperfect
 Controls	Unimplemented
 Create Crosshair Offset %1$s Crosshair Offset X %1$1.3f Crosshair Offset Y %1$1.3f Crosshair Options Crosshair Scale %1$s Crosshair Scale X %1$1.3f Crosshair Scale Y %1$1.3f Current %1$s Folders Current time Custom Customize UI DIP Switches Data Format Default Default name is %s Device Mapping Dial Device Assignment Difference Disabled Disabled: %s Disk	Imperfect
 Disk	Unimplemented
 Done Double-click or press %1$s to select Driver Driver is BIOS	No
 Driver is BIOS	Yes
 Driver is Clone of	%1$s
 Driver is Parent	
 Driver is clone of: %1$-.100s Driver is parent Driver: "%1$s" software list  Driver: %1$-.100s Emulated Enabled Enabled: %s Enforce Aspect Ratio Enlarge images in the right panel Enter Code Error accessing %s Exit Export displayed list to file Export list in TXT format (like -listfull) Export list in XML format (like -listxml) Export list in XML format (like -listxml, but exclude devices) External DAT View Failed to save input name file Fast Forward File File Already Exists - Override? File Manager Filter Filter %1$u Flip X Flip Y Folders Setup Fonts Force 4:3 aspect for snapshot display Frame skip GLSL Gameinit General Info General Inputs Graphics	Imperfect
 Graphics	Imperfect Colors
 Graphics	OK
 Graphics	Unimplemented
 Graphics	Wrong Colors
 Graphics: Imperfect,  Graphics: OK,  Graphics: Unimplemented,  Group HLSL Hide Both Hide Filters Hide Info/Image Hide romless machine from available list High Scores History Image Format: Image Information Images Imperfectly emulated features:  Include clones Info auto audit Infos Infos text size Input (general) Input (this Machine) Input Options Input port name file saved to %s Input ports Italic Joystick Joystick deadzone Joystick saturation Keyboard	Imperfect
 Keyboard	Unimplemented
 Keyboard Inputs	Yes
 Keyboard Mode LAN	Imperfect
 LAN	Unimplemented
 Language Laserdisc '%1$s' Horiz Position Laserdisc '%1$s' Horiz Stretch Laserdisc '%1$s' Vert Position Laserdisc '%1$s' Vert Stretch Left equal to right Left equal to right with bitmask Left equal to value Left greater than right Left greater than value Left less than right Left less than value Left not equal to right Left not equal to right with bitmask Left not equal to value Lightgun Lightgun Device Assignment Lines Load State MAMEinfo MARPScore MESSinfo MHz Machine Configuration Machine Information Mag. Drum	Imperfect
 Mag. Drum	Unimplemented
 Mag. Tape	Imperfect
 Mag. Tape	Unimplemented
 Mamescore Manufacturer	%1$s
 Master Volume Match block Mechanical Machine	No
 Mechanical Machine	Yes
 Media	Imperfect
 Media	Unimplemented
 Menu Preview Microphone	Imperfect
 Microphone	Unimplemented
 Miscellaneous Options Mouse Mouse	Imperfect
 Mouse	Unimplemented
 Mouse Device Assignment Multi-keyboard Multi-mouse Name:             Description:
 Natural Natural keyboard Network Devices New Barcode: New Image Name: No No category INI files found No groups found in category file No machines found. Please check the rompath specified in the %1$s.ini file.

If this is your first time using %2$s, please see the config.txt file in the docs directory for information on configuring %2$s. None None
 Not supported Number Of Screens Off Offscreen reload On One or more ROMs/CHDs for this machine are incorrect. The machine may not run correctly.
 One or more ROMs/CHDs for this machine have not been correctly dumped.
 Other Controls Overall	NOT WORKING
 Overall	Unemulated Protection
 Overall	Working
 Overall: NOT WORKING Overall: Unemulated Protection Overall: Working Overclock %1$s sound Overclock CPU %1$s Paddle Device Assignment Page Partially supported Pause Mode Pause/Stop Pedal Device Assignment Performance Options Play Play Count Player Please enter a file extension too Plugin Options Plugins Positional Device Assignment Press TAB to set Press any key to continue. Printer	Imperfect
 Printer	Unimplemented
 Pseudo terminals Punch Tape	Imperfect
 Punch Tape	Unimplemented
 ROM Audit 	Disabled
Samples Audit 	Disabled
 ROM Audit Result	BAD
 ROM Audit Result	OK
 Read this image, write to another image Read this image, write to diff Read-only Read-write Record Reload All Remove %1$s Folder Remove Folder Remove From Favorites Remove last filter Requires Artwork	No
 Requires Artwork	Yes
 Requires CHD	No
 Requires CHD	Yes
 Requires Clickable Artwork	No
 Requires Clickable Artwork	Yes
 Reset Reset All Restore default colors Results will be saved to %1$s Return to Machine Return to Previous Menu Revision: %1$s Rewind Rewind capacity Romset	%1$s
 Rotate Rotate left Rotate right Rotation Options Sample Rate Sample text - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Samples Audit Result	BAD
 Samples Audit Result	None Needed
 Samples Audit Result	OK
 Save Save Cheat Save Configuration Save State Save input names to file Screen Screen #%d Screen '%1$s' Screen Orientation	Horizontal
 Screen Orientation	Vertical
 Screen flipping in cocktail mode is not supported.
 Search: %1$s_ Select New Machine Select access mode Select category: Select cheat to set hotkey Select custom filters: Select image format Selection List - Search:  Set Set hotkeys Settings Show All Show DATs view Show mouse pointer Show side panels Simultaneous contradictory Skip BIOS selection menu Skip information screen at startup Skip software parts selection menu Sleep Slider Controls Slot Devices Software is clone of: %1$-.100s Software is parent Software part selection: Sound Sound	Imperfect
 Sound	None
 Sound	OK
 Sound	Unimplemented
 Sound Options Sound: Imperfect Sound: None Sound: OK Sound: Unimplemented Speed Start Out Maximized Start new search State/Playback Options Steadykey Support Cocktail	No
 Support Save	No
 Support Save	Yes
 Supported: No Supported: Partial Supported: Yes Switch Item Ordering Switched Order: entries now ordered by description Switched Order: entries now ordered by shortname Synchronized Refresh Sysinfo System: %1$-.100s Tape Control Test Test/Write Poke Value The selected game is missing one or more required ROM or CHD images. Please select a different game.

Press any key to continue. There are known problems with this machine

 This driver requires images to be loaded in the following device(s):  This machine has no BIOS. This machine has no sound hardware, MAME will produce no sounds, this is expected behaviour.
 This machine requires external artwork files.
 This machine was never completed. It may exhibit strange behavior or missing elements that are not bugs in the emulation.
 Throttle Tickets dispensed: %1$d

 Timer Timing	Imperfect
 Timing	Unimplemented
 Total time Trackball Device Assignment Triple Buffering Type Type name or select: %1$s_ Type name or select: (random) UI Color Settings UI Font UI Fonts Settings UI active Unable to write file
Ensure that cheatpath folder exists Undo last search -- # Uptime: %1$d:%2$02d

 Uptime: %1$d:%2$02d:%3$02d

 Use External Samples Use image as background Use this if you want to poke the Last Slot value (eg. You started without an item but finally got it) Use this if you want to poke the Slot 1 value (eg. You started with something but lost it) User Interface Value Vector Vector Flicker Video Mode Video Options Visible Delay WAN	Imperfect
 WAN	Unimplemented
 Wait Vertical Sync Watch Window Mode Write X Y Year	%1$s
 Yes [None]
 [This option is NOT currently mounted in the running system]

Option: %1$s
Device: %2$s

If you select this option, the following items will be enabled:
 [This option is currently mounted in the running system]

Option: %1$s
Device: %2$s

The selected option enables the following items:
 [compatible lists] [create] [empty slot] [empty] [failed] [file manager] [no category INI files] [no groups in INI file] [software list] color-channelAlpha color-channelBlue color-channelGreen color-channelRed color-optionBackground color-optionBorder color-optionClone color-optionDIP switch color-optionMouse down background color color-optionMouse down color color-optionMouse over background color color-optionMouse over color color-optionNormal text color-optionNormal text background color-optionSelected background color color-optionSelected color color-optionSlider color color-optionSubitem color color-optionUnavailable color color-presetBlack color-presetBlue color-presetGray color-presetGreen color-presetOrange color-presetRed color-presetSilver color-presetViolet color-presetWhite color-presetYellow color-sampleClone color-sampleMouse Over color-sampleNormal color-sampleSelected color-sampleSubitem default emulation-featureLAN emulation-featureWAN emulation-featurecamera emulation-featurecapture hardware emulation-featurecolor palette emulation-featurecommunications emulation-featurecontrols emulation-featuredisk emulation-featuregraphics emulation-featurekeyboard emulation-featuremagnetic drum emulation-featuremagnetic tape emulation-featuremedia emulation-featuremicrophone emulation-featuremouse emulation-featureprinter emulation-featureprotection emulation-featurepunch tape emulation-featuresolid state storage emulation-featuresound emulation-featuretiming kHz machine-filterAvailable machine-filterBIOS machine-filterCHD Required machine-filterCategory machine-filterClones machine-filterCustom Filter machine-filterFavorites machine-filterHorizontal Screen machine-filterManufacturer machine-filterMechanical machine-filterNo CHD Required machine-filterNot BIOS machine-filterNot Mechanical machine-filterNot Working machine-filterParents machine-filterSave Supported machine-filterSave Unsupported machine-filterUnavailable machine-filterUnfiltered machine-filterVertical Screen machine-filterWorking machine-filterYear path-optionArtwork path-optionBosses path-optionCabinets path-optionCategory INIs path-optionControl Panels path-optionCovers path-optionCrosshairs path-optionDATs path-optionFlyers path-optionHowTo path-optionINIs path-optionIcons path-optionLogos path-optionMarquees path-optionPCBs path-optionROMs path-optionScores path-optionSnapshots path-optionSoftware Media path-optionVersus playing recording selmenu-artworkArtwork Preview selmenu-artworkBosses selmenu-artworkCabinet selmenu-artworkControl Panel selmenu-artworkCovers selmenu-artworkFlyer selmenu-artworkGame Over selmenu-artworkHowTo selmenu-artworkLogo selmenu-artworkPCB selmenu-artworkScores selmenu-artworkSnapshots selmenu-artworkVersus software-filterAvailable software-filterClones software-filterCustom Filter software-filterDevice Type software-filterFavorites software-filterParents software-filterPartially Supported software-filterPublisher software-filterRelease Region software-filterSoftware List software-filterSupported software-filterUnavailable software-filterUnfiltered software-filterUnsupported software-filterYear stopped Project-Id-Version: MAME
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-17 07:08+1100
PO-Revision-Date: 2021-04-04 13:08+0200
Last-Translator: Jos van Mourik
Language-Team: MAME Language Team
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.4.2
 

--- DRIVER INFO ---
Driver:  

Druk op een toets om door te gaan 

Er zijn werkende varianten van deze machine: %s 
    Configuratie opgeslagen

 
Geluid:
 
DEZE MACHINE WERKT NIET. De emulatie van deze machine is nog niet compleet. Er is niets wat je kunt doen om dit probleem te verhelpen, afgezien van wachten tot ontwikkelaars de emulatie verbeteren.
 
Beeld:
   %1$s
   %1$s    [standaard: %2$s]
   %1$s    [label: %2$s]
   Aanpassingsinvoer    [%1$d invoeren]
   Analoge invoer    [%1$d invoeren]
   Gokinvoer    [%1$d invoeren]
   Hanafudainvoer    [%1$d invoeren]
   Toetsenbordinvoer    [%1$d invoeren]
   Toetsenblokinvoer    [%1$d invoeren]
   Mahjonginvoer    [%1$d invoeren]
   Scherm '%1$s': %2$d × %3$d (H) %4$s Hz
   Scherm '%1$s': %2$d × %3$d (V) %4$s Hz
   Scherm '%1$s': Vector
   Gebruikersinvoer    [%1$d invoeren]
  (standaard)  (afgeschermd)  KLEUREN  PENNEN %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Driver: %4$s

Processor:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d machines (waarvan %5$d BIOS) ) %1$s %2$s ( %3$d / %4$d softwarepakketten ) %1$s Helderheid %1$s Contrast %1$s Gamma %1$s Horizontale positie %1$s Horizontale uitrekking %1$s Verversingfrequentie %1$s Verticale positie %1$s Verticale uitrekking %1$s volume %1$s, %2$-.100s %1$s: %2$s
 %1$s: %2$s - Zoek: %3$s_ %2$s
 %d totale overeenkomsten gevonden %s %s
 toegevoegd aan favorietenlijst. %s
 verwijderd uit favorietenlijst. %s [intern] %s toegevoegd (EP)ROM	Onvolledig
 (EP)ROM	Niet geïmplementeerd
 (speelt af) (neemt op) * BIOS-instellingen:
  %1$d opties    [standaard: %2$s]
 * Processor:
 * Configuratieinstellingen:
 DIP-schakelaarinstellingen:
 * Invoerapparatuur:
 * Media opties:
 * Plek opties:
 * Geluid:
 * Video:
 **Fout bij het opslaan van %s.ini** **Fout bij het opslaan van ui.ini** , %s <filters instellen> ARGB-instellingen Geactiveerd: %s Geactiveerd: %s = %s Voeg %1$s map toe - Zoek: %2$s_ Map toevoegen Toevoegen aan favorieten Filter toevoegen Naar of uit favorietenlijst Adsticktoewijzing Geavanceerde opties Alle Alle cheats zijn herladen Alle slots geleegd en huidige state opgeslagen in slot 1 Analoge besturing Analoge besturing	Ja
 Willekeurige Weet u zeker dat u wilt stoppen?

Druk op ''%1$s'' om te stoppen,
druk op ''%2$s'' om terug te keren naar de emulatie. Artworkinstellingen Controleer ROMs voor %1$u onbeschikbaar gemarkeerde machines Controleer ROMs voor alle %1$u machines ROMs controleren voor machine %2$u van %3$u...
%1$s Automatisch Auto frame skip Auto rotatie links Auto rotatie rechts Automatisch opslaan/hervatten BIOS BIOS-selectie BIOS-selectie: Barcodelezer Barcodelengte ongeldig! Gewicht straalintensiteit Maximale straalbreedte Minimale straalbreedte Bilineaire filtering Bitmap voorschaling Vet Boekhoudinginformatie Inbranding CPU of RAM Camera	Onvolledig
 Camera	Niet geïmplementeerd
 Annuleer Kan geen map vervangen Opname	Onvolledig
 Opname	Niet geïmplementeerd
 Wijzig %1$s map - Zoek: %2$s_ Wijzig map Veranderingen hieraan hebben alleen effect wanneer "Start nieuwe zoekopdracht" is geselecteerd Cheat Cheat commentaar:
%s Cheatzoeker Cheatnaam Cheat toegevoegd aan cheat.simple Cheat engine niet beschikbaar Cheat geschreven naar %s en toegevoegd aan cheat.simple Cheats Kies uit palet Herstel watches Munt %1$c: %2$d%3$s
 Munt %1$c: n.v.t.%3$s
 Muntimpuls Munt lockout Kleur voorbeeld: Kleuren Commando Communicatie	Onvolledig
 Communicatie	Niet geïmplementeerd
 Volledig ongeëmuleerde eigenschappen:  Mappen configureren Configureer machine Configureer opties Bevestig afsluiten machines Besturing	Onvolledig
 Besturing	Niet geïmplementeerd
 Aanmaken Vizieroffset %1$s Vizier X-offset %1$1.3f Vizier Y-offset %1$1.3f Vizier opties Vizierschaal %1$s Vizier X-schaal %1$1.3f Vizier Y-schaal %1$1.3f Huidige %1$s mappen Huidige tijd Eigen Gebruikersinterface aanpassen DIP-schakelaars Dataformaat Standaard Standaardnaam is %s Invoerapparaat in kaart brengen Dialapparaattoewijzing Verschil Uitgeschakeld Uitgeschakeld: %s Schijf	Onvolledig
 Schijf	Niet geïmplementeerd
 Klaar Dubbelklik of druk op %1$s om te selecteren Driver Driver is BIOS	Nee
 Driver is BIOS	Ja
 Driver is variant van	%1$s
 Driver is hoofdsoftware	
 Driver is een variant van: %1$-.100s Driver is hoofddriver Driver: "%1$s" softwarelijst  Driver: %1$-.100s Geëmuleerd Ingeschakeld Ingeschakeld: %s Forceer aspectratio Vergroot afbeeldingen in het rechter paneel Vul code in Fout bij toegang tot %s Afsluiten Exporteer getoonde lijst naar bestand Exporteer lijst in XML-formaat (zoals -listfull) Exporteer lijst in XML-formaat (zoals -listxml) Exporteer lijst in XML-formaat (zoals -listxml, maar zonder apparaten) Externe DAT-overzicht Opslaan van invoernaambestand mislukt Vooruitspoelen Bestand Bestand bestaat al - Overschrijven? Bestandsbeheer Filter Filter %1$u Draai X om Draai Y om Mapinstellingen Lettertypes Forceer 4:3 verhouding voor schermafbeeldingweergave Frame skip GLSL Spelinit Algemene informatie Algemene invoer Beeld	Onvolledig
 Beeld	Onvolledige kleuren
 Beeld	OK
 Beeld	Niet geïmplementeerd
 Beeld	Verkeerde kleuren
 Beeld: Niet volledig,  Beeld: OK,  Beeld: Niet geïmplementeerd,  Groep HLSL Verberg beide Verberg filters Verberg info/plaatje Verberg romloze machine uit beschikbare lijst High Scores Geschiedenis Bestandsformaat: Bestandsinformatie Afbeeldingen Onvolledig geëmuleerde eigenschappen:  Varianten insluiten Info auto controle Informatie Tekstgrootte info's Invoer (algemeen) Invoer (deze machine) Invoerinstellingen Invoerpoortnaambestand opgeslagen in %s Invoerpoorten Cursief Joystick Joystick doodzone Joystick verzadiging Toetsenbord	Onvolledig
 Toetsenbord	Niet geïmplementeerd
 Toetensenbordinvoer	Ja
 Keyboardmodus LAN	Onvolledig
 LAN	Niet geïmplementeerd
 Taal Laserdisc '%1$s' Horiz positie Laserdisc '%1$s' Horiz uitrekking Laserdisc '%1$s' Vert positie Laserdic '%1$s' Vert uitrekking Links gelijk aan rechts Links gelijk aan rechts met bitmasker Links gelijk aan waarde Links groter dan rechts Links groter dan waarde Links kleiner dan rechts Links kleiner dan waarde Links niet gelijk aan rechts Links niet gelijk aan rechts met bitmasker Links niet gelijk aan waarde Lichtpistool Lightguntoewijzing Lijnen Laad state MAMEinfo MARPScore MESSinfo MHz Machineconfiguratie Machineinformatie Mag. trommel	Onvolledig
 Mag. trommel	Niet geïmplementeerd
 Mag. Band	Onvolledig
 Mag. Band	Niet geïmplementeerd
 Mamescore Fabrikant	%1$s
 Hoofdvolume Paar blok Mechanische machine	Nee
 Mechanische machine	Ja
 Media	Onvolledig
 Media	Niet geïmplementeerd
 Menu voorvertoning Microfoon	Onvolledig
 Microfoon	Niet geïmplementeerd
 Overige opties Muis Muis	Onvolledig
 Muis	Niet geïmplementeerd
 Muistoewijzing Meerdere toetsenborden Meerdere muizen Naam:             Omschrijving:
 Natuurlijk Natuurlijk toetsenbord Netwerkapparaten Nieuwe barcode: Nieuwe bestandsnaam: Nee Geen categorie INI-bestanden gevonden Geen groepen gevonden in categoriebestand Geen machines gevonden. Controleer de ROM-mapinstellingen in het bestand %1$s.ini.

Als dit de eerste keer is dat je %2$s gebruikt, lees dan het bestand config.txt in de docs-map voor informatie om %2$s te configureren. Geen Geen
 Niet ondersteund Aantal schermen Uit Buiten scherm herladen Aan Eén of meer ROMs/CHDs voor deze machine zijn niet correct. De machine kan mogelijk niet goed functioneren.
 Eén of meer ROMs/CHDs voor deze machine zijn niet correct gedumpt.
 Andere besturing Status	NIET WERKEND
 Status	Ongeëmuleerde beveiliging
 Status	Werkend
 Status: NIET WERKEND Status: Ongeëmuleerde beveiliging Status: Werkend Overklok %1$s geluid Overklok CPU %1$s Paddletoewijzing Pagina Gedeeltelijk ondersteund Pauzeermodus Pauzeer/Stop Pedaaltoewijzing Prestatie opties Speel af Speelteller Speler Vul ook een bestandsextensie in Plugin opties Plugins Positieapparaattoewijzing Druk op TAB voor keuze Druk op een toets om door te gaan. Printer	Onvolledig
 Printer	Niet geïmplementeerd
 Pseudoterminals Ponsband	Onvolledig
 Ponsband	Niet geïmplementeerd
 ROM-controle 	Uitgeschakeld
Samplecontrole 	Uitgeschakeld
 ROM-controle Resultaat	SLECHT
 ROM-controle Resultaat	OK
 Lees dit bestand, schrijf naar een ander bestand Lees dit bestand, schrijf naar diff Alleen-lezen Lezen-schrijven Neem op Alles herladen Verwijder %1$s map Map verwijderen Verwijderen uit favorieten Verwijder laatste filter Vereist artwork	Nee
 Vereist artwork	Ja
 Vereist CHD	Nee
 Vereist CHD	Ja
 Vereist klikbare artwork	Nee
 Vereist klikbare artwork	Ja
 Herstel Alles herstellen Herstel originele kleuren Resultaten worden opgeslagen naar %1$s Terug naar machine Terug naar vorig menu Revisie: %1$s Terugspoelen Terugspoelcapaciteit Romset	%1$s
 Roteer Roteer links Roteer rechts Rotatieinstellingen Samplesnelheid Voorbeeldtekst - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Samplecontrole Resultaat	SLECHT
 Samplecontrole Resultaat	Niet Nodig
 Samplecontrole Resultaat	OK
 Opslaan Sla cheat op Configuratie opslaan State opslaan Sla invoernamen op in bestand Scherm Scherm #%d Scherm '%1$s' Schermoriëntatie	Horizontaal
 Schermoriëntatie	Verticaal
 Schermomdraaien wordt in cocktailmodus niet ondersteund.
 Zoek: %1$s_ Selecteer nieuwe machine Selecteer toegangsmodus Selecteer categorie: Selecteer cheat om sneltoets te maken Kies eigen filter: Selecteer bestandsformaat Selectielijst - Zoeken:  Kies Kies sneltoets Instellingen Toon alles Laat DATs zien Toon muispointer Toon zijpanelen Gelijktijdig tegenstrijdig BIOS-selectiemenu overslaan Sla informatie bij opstarten over Softwareonderdelen selectiemenu overslaan Slaap Schuifinstellingen Insteekapparaten Software is een variant van: %1$-.100s Software is hoofdsoftware Softwareonderdeel selectie: Geluid Geluid	Onvolledig
 Geluid	Geen
 Geluid	OK
 Geluid	Niet geïmplementeerd
 Geluid opties Geluid: Niet volledig Geluid: Geen Geluid: OK Geluid: Niet geïmplementeerd Snelheid Start gemaximaliseerd Start nieuwe zoekopdracht State/afspeel-instellingen Steadykey Ondersteunt cocktail	Nee
 Ondersteunt opslaan	Nee
 Ondersteunt opslaan	Ja
 Ondersteund: Nee Ondersteund: Gedeeltelijk Ondersteund: Ja Wijzig onderdeelvolgorde Volgorde gewijzigd: de elementen zijn nu gesorteerd op omschrijving Volgorde gewijzigd: de elementen zijn nu gesorteerd op korte naam Gesynchroniseerde verversing Sysinfo Systeem: %1$-.100s Cassettebediening Test Test/schrijf pokewaarde Het geselecteerde spel mist één of meer benodigde ROM- of CHD-bestanden. Selecteer een ander spel.

Druk op een toets om door te gaan. Er zijn problemen bekend met deze machine

 Deze driver vereist software ingelezen in de volgende device(s):  Deze machine heeft geen BIOS. Deze machine heeft geen geluidshardware, MAME zal geen geluid produceren, dit is verwacht gedrag.
 Deze machine vereist externe artworkbestanden.
 Deze machine is nooit afgemaakt. Het kan vreemd gedrag vertonen of elementen missen, dit zijn geen emulatiefouten.
 Throttle Kaartjes uitgegeven: %1$d

 Timer Timing	Onvolledig
 Timing	Niet geïmplementeerd
 Totale tijd Trackballtoewijzing Drievoudige buffering Type Type naam of selecteer: %1$s_ Type naam of selecteer: (willekeurig) Kleurinstellingen gebruikersinterface Lettertype gebruikersinterface Instellingen lettertype gebruikersinterface Gebruikersinterface actief Niet in staat om bestand te schrijven
Zorg ervoor dat de cheatpath map bestaat Ongedaan maken van laatste zoekopdracht -- # In bedrijf: %1$d:%2$02d

 In bedrijf: %1$d:%2$02d:%3$02d

 Gebruik externe samples Gebruik afbeelding als achtergrond Gebruik dit voor het poken van de laatste slot waarde (eg. Je begon zonder een item maar kreeg het) Gebruik dit voor het poken van de slot 1 waarde (eg. Je begon met iets maar verloor het) Gebruikersinterface Waarde Vector Vectorflikker Videomodus Video opties Zichtbare vertraging WAN	Onvolledig
 WAN	Niet geïmplementeerd
 Wacht verticale sync Watch Venstermodus Schrijf X Y Jaar	%1$s
 Ja [Geen]
 [Deze optie is momenteel NIET gemount in het lopende systeem]

Optie: %1$s
Apparaat: %2$s

Als u deze optie selecteert worden de volgende delen aangezet:
 [Deze optie is momenteel gemount in het lopende systeem]

Optie: %1$s
Apparaat: %2$s

De geselecteerde optie zet de volgende delen aan:
 [compatibele lijsten] [aanmaken] [lege plek] [leeg] [mislukt] [Bestandsbeheer] [geen categorie INI-bestanden] [geen groepen in INI-bestand] [softwarelijst] Alpha Blauw Groen Rood Achtergrond Rand Variant DIP-schakelaar Muis-down achtergrondkleur Muis-down kleur Muis-over achtergrondkleur Muis-over kleur Normale tekst Normale tekst achtergrond Geselecteerde achtergrondkleur Geselecteerd kleur Sliderkleur Subitemkleur Onbeschikbaar kleur Zwart Blauw Grijs Groen Oranje Rood Zilver Violet Wit Geel Variant Muis-over Normaal Geselecteerd Onderdeel standaard LAN WAN camera opnamehardware kleurpalet communicatie besturing schijf beeld toetsenbord trommelgeheugen magneetband media microfoon muis printer beveiliging ponsband solid state opslag geluid timing kHz Beschikbaar BIOS CHD benodigd Categorie Varianten Eigen filter Favorieten Horizontaal scherm Fabrikant Mechanisch Geen CHD benodigd Geen BIOS Niet mechanisch Niet werkend Hoofdsoftware Ondersteunt opslaan Ondersteunt opslaan niet Onbeschikbaar Ongefilterd Verticaal scherm Werkend Jaar Artwork Eindbazen Kasten Categorie-INIs Bedieningspanelen Hoesjes Vizieren DATs Flyers Bedieningsuitleg INIs Iconen Logos Marquees Printplaten ROMs Scores Schermafbeeldingen Softwaremedia Tegen speelt af neemt op Voorvertoning artwork Eindbazen Kast Bedieningspanel Hoesjes Flyer Game over Bedieningsuitleg Logo Printplaat Scores Schermafbeeldingen Tegen Beschikbaar Varianten Eigen filter Apparaattype Favorieten Hoofdsoftware Gedeeltelijk ondersteund Uitgever Uitgaveregio Softwarelijst Ondersteund Onbeschikbaar Ongefilterd Niet ondersteund Jaar gestopt 