��    �     �              L-     M-     l-  .   �-     �-  �   �-     �.  �   �.     C/     L/     T/     o/  #   �/  !   �/  #   �/  #   �/  #   0  !   80  "   Z0  +   }0  +   �0     �0     �0  
   1  	   1     #1     +1  	   11     ;1     B1  	   J1     T1     \1  "   c1  	   �1  .   �1  +   �1     �1     �1     2     2  
   *2     52     I2     \2     n2     �2     �2     �2     �2     �2     �2     �2     �2     3     3      #3     D3     R3     [3     n3     �3  	   �3     �3  3   �3     �3     �3     �3     4     &4     84  	   I4  	   S4     ]4     u4     �4     �4     �4     �4     �4     �4     �4  
   �4     5     5  
   +5  "   65     Y5     s5     �5     �5     �5  3   �5     �5     �5     �5     6  ^   6     r6  /   �6      �6  .   �6     7     7     7     (7     :7     C7  	   T7     ^7  /   u7     �7     �7     �7     �7     �7     �7     �7     8     '8     :8      M8     n8     �8     �8     �8  
   �8     �8     �8     �8  &   �8     �8     9     -9  "   D9     g9  D   u9     �9     �9     �9  
   �9     �9     :  -   !:     O:     V:     j:     x:     �:     �:     �:     �:     �:     �:     �:     �:      ;     1;     G;     Y;     l;     ~;     �;     �;     �;     �;     �;     �;     <     *<     ?<     Y<     s<     �<     �<     �<     �<     �<     �<     �<     �<     �<  
   =     =     =     $=     4=     H=  $   M=     r=     y=     �=     �=     �=     �=     �=     �=     >     +>     @>     I>     Q>     ]>  !   r>  
   �>     �>     �>     �>  *   �>  )    ?  >   *?     i?     {?     �?     �?     �?     �?     �?     �?     �?     	@     @     @     %@  %   +@  
   Q@     \@     `@     e@     n@     {@     �@     �@     �@     �@     �@     �@     A     A     4A     :A  	   ?A     IA     VA  (   fA     �A     �A     �A     �A     �A     �A     �A     �A     �A     B     B     B     )B     /B     ?B     TB      bB     �B     �B     �B     �B     �B     �B     �B     �B     
C     C     -C     <C     OC     XC     xC     �C     �C     �C     �C      �C     D     -D     ED     ]D     rD     �D  $   �D     �D     �D     �D      E  
   E     E     E  	   &E     0E     9E     =E     SE     gE     |E     �E     �E     �E  	   �E     �E  !   �E     F     F     -F     9F     PF     hF     yF     �F     �F     �F     �F     �F     �F     G     G     +G     CG     RG     ^G     tG     |G     �G     �G     �G     �G     �G     �G     �G      H  �   "H     �H     I     I     I     $I     2I  '   DI  (   lI     �I  
   �I     �I     �I  	   �I  Y   �I  G   J     dJ     sJ     {J     �J     �J     �J     �J     �J     K     K     -K     ;K     JK     cK     hK  
   |K  
   �K     �K  !   �K  ,   �K  -   �K  $   'L     LL     `L  
   eL     pL     wL  !   �L     �L     �L     �L     �L     �L     M     ,M     ?M     XM     kM  <   |M     �M  2   �M     N     N     "N     9N     JN     `N  ,   zN     �N     �N     �N  '   �N     O  	   9O  
   CO     NO  
   UO     `O     sO     �O     �O  �   �O  �   ?P     �P     �P     �P     Q     Q     8Q     XQ  	   ^Q     hQ     �Q     �Q     �Q     �Q     �Q     �Q     �Q     �Q     �Q     R     R  F    R     gR  !   �R     �R     �R  
   �R     �R     �R  
   �R  $   S     *S     CS  
   JS     US     cS     �S  3   �S     �S     �S     �S     T     $T     5T     PT     gT     {T     �T     �T     �T     �T     �T     �T     �T     �T     U     U     2U  !   KU  "   mU  "   �U     �U     �U     �U     �U     �U     �U     V     V     7V     =V     NV  	   ZV     dV     yV     �V     �V  	   �V     �V     �V     �V     �V     �V     �V  	   W     W     2W     CW     UW     cW     vW     �W  2   �W  0   �W     �W     X     X     -X     :X     ?X     TX  �   jX  �   �X  ,   pY  E   �Y     �Y  ]   �Y  .   [Z  z   �Z     [     [     ([     .[     @[  
   V[     a[     }[     �[     �[     �[     �[     �[     �[     �[  	   \  '   \  &   =\  8   d\     �\     �\     �\     �\     �\     ]  e   3]  Z   �]     �]     ^     	^     ^  
   ^     *^     8^     F^     U^     h^     {^     �^     �^     �^     �^     �^     �^     �^     �^  
   �^     �^     �^     �^     _     _  �   "_  �   �_     C`     V`     _`     l`     t`     }`     �`     �`  
   �`  
   �`     �`     �`     �`     	a     a     /a     Ga     [a     na  (   �a     �a  (   �a     �a     b  #   -b  &   Qb     xb     �b     �b     �b     �b     �b     c     c     2c     Fc     Wc     kc     c     �c     �c     �c     �c     �c     �c     d     d     .d     Dd  "   ]d     �d      �d     �d     �d     �d     e     )e     Ie     ie     �e     �e     �e     �e     �e  %   
f     0f     Hf     af     tf     �f     �f     �f     �f     �f     �f      g     g      6g     Wg     sg     �g     �g     �g     �g     �g     h     2h     Rh     mh     �h     �h     �h  	   �h     �h     �h     i     i     4i     Ni     ii     |i     �i     �i     �i     �i     �i     �i     �i     j     $j     5j     Hj     ^j     yj     �j  	   �j     �j     �j     �j     �j     k     "k     8k     Rk     hk     |k     �k     �k     �k     �k     �k     l     /l     Il  #   al     �l     �l     �l     �l     �l     m     -m     Im     ^m    fm  !   �n  )   o  -   2o     `o  �   �o     Bp  �   Ip  	   q     (q     0q     Kq  *   gq  *   �q  3   �q  &   �q  )   r  2   Br  (   ur  )   �r  )   �r     �r  *   s  
   3s  
   >s     Is     Ps  	   Ys     cs     js  	   rs     |s     �s  "   �s  	   �s  /   �s  -   �s     t     *t     7t     Ft     Xt     et     �t     �t     �t     �t     �t     �t     u     u     +u     Eu  .   Ku     zu  %   }u  $   �u     �u     �u     �u     �u     v  	   v  
   $v  >   /v     nv  #   vv  %   �v     �v     �v     �v     
w  
   w     w     9w     Uw     Zw     ow  
   �w     �w     �w  #   �w     �w     �w  !   �w     x  =   .x  #   lx     �x     �x     �x     �x  9   �x     y     )y     ?y     Zy  t   cy     �y  ?   �y  *   5z  6   `z     �z     �z     �z     �z     �z     �z     {  %   {  9   D{     ~{     �{     �{     �{  '   �{     �{     |     !|     9|     Q|  !   d|     �|     �|     �|     �|  
   �|     �|     �|     }  *   }  #   @}     d}     x}     �}     �}  M   �}     ~     ~     4~     M~  #   ^~  +   �~  3   �~  	   �~     �~     �~          *     >     O     a     }     �     �  "   �  '   �     �     	�     �     4�      O�     p�     ��     ��     ��     ŀ     �     �     �     +�     D�     ]�     p�     |�     ��  
   ��     ��     ��     ��     ց      ��  
   �  
   �     '�     6�     H�     a�  -   g�     ��     ��     ��     ɂ     �     ��     �  "   &�     I�     [�     z�     ��     ��      ��  %   ��     ݃     �     �  "   �  .   +�  -   Z�  L   ��     Մ  *   �  /   �     N�     ^�  $   f�     ��     ��     ��     ��     ��     ą     ޅ  6   �  
   �     '�     +�     0�     9�     N�     ^�     u�     ��     ��     ��     ֆ     �     ��     �     %�     *�     9�     J�  4   _�     ��  
   ��     ��     ć     Ǉ     ڇ     �  $   ��     �  %   -�     S�  "   a�     ��     ��     ��     ��  &   Ȉ     �     �     �     (�     1�     H�     `�     t�     ��     ��     ��     ŉ     ܉  %   �  %   	�  #   /�  #   S�     w�     ��  %   ��     ̊     �     �     "�     B�      `�  *   ��     ��     ̋  $   Ջ     ��     �     �      �  	   )�     3�     <�     @�     [�     t�  %   ��     ��  "   Ռ     ��     �     +�  /   2�     b�     s�     ��     ��     ��     Ǎ     ڍ  "   �     �     3�     I�     f�     v�     |�     ��  !   ��     Ɏ     ێ  )   �     �     #�     C�     K�     [�     r�     ��     ��  1   ��  -   ֏    �     
�     )�     G�     N�     V�     e�  6   v�  3   ��  	   �     �     ��     �     �  h   (�  K   ��     ݒ     �     ��     �     -�     @�     U�     t�     ��     ��     ��     ��  "   ӓ     ��     ��     �     #�  !   0�  '   R�  2   z�  3   ��  *   �     �     $�     *�     >�     F�  1   `�     ��     ��  &   ��     ֕     �  )   �      :�  +   [�     ��     ��  O   ��  '   �  C   4�     x�     ��     ��     ��     ʗ     �  @   ��  #   @�  !   d�  *   ��  (   ��      ژ     ��     �     �     %�     5�     J�     X�     n�  �   ��  �   A�     �     �     -�     >�     N�     j�     ��     ��  #   ��     ��     ӛ     �  	   ��     �     #�  
   0�     ;�     S�     j�     ��  K   ��  )   ޜ  8   �  '   A�     i�     p�     ��  #   ��     ��  +   ǝ  %   �     �     �     '�      3�     T�  8   s�     ��     ��     Ҟ  '   �     �  4   +�  "   `�     ��  !   ��     ß     �     ��     �      �     0�     =�     ]�     y�     ��  #   ��  '   ՠ  2   ��  2   0�     c�     j�     ~�     ��     ��  !   ��     ͡     ߡ     ��     �     �     �     '�     >�     N�     ^�     j�     r�  
   ��     ��     ��     ��     ˢ  	   �     �     
�     '�     C�     S�     f�     u�  7   ��  6   ƣ     ��     �      �     3�     D�     J�  !   d�  �   ��  �   �  ,   ��  U   ٥     /�  h   L�  <   ��  �   �     ~�     ��     ��     ��      ̧     ��  %   ��     �     0�     5�  !   P�  (   r�     ��     ��      Ǩ     �  3   �  0   %�  A   V�     ��  "   ��  )   ٩     �  "   �  #   =�  r   a�  R   Ԫ     '�     =�     C�     I�     _�     n�     ��     ��     ��  !   ��     ګ     �     ��     �     �     �     �     $�     &�  	   .�     8�  (   <�  !   e�  !   ��  	   ��  �   ��  �   a�     ��     �     �      �     (�     1�     K�     h�  
   ��  
   ��     ��     ��     ��     ��     ��     Ȯ     ڮ     �  	   �  1   �     "�     ?�     Z�     l�     y�     ��     ��     ��     Ư     ֯     �     �     �     ��     ��     �     �     �     �     %�     -�     3�     K�     R�     ^�     g�     o�     s�     w�     �     ��     ��  	   ��     ��  	   ��     ˰     Ӱ     �     ��  	   ��     �  
   �  
   �  
   #�     .�     N�     R�     `�     s�     ��     ��     ��     ��  	   ��     ��     ��  	   α     ر  
   �  	   �     ��     �     �     *�     ;�     @�     Y�     r�     ��     ��     ��     ��     ��     ��  +   β     ��  	   �     �     �     2�     8�     >�  	   C�  
   M�     X�     ]�     e�  	   k�     u�     z�     �  	   ��     ��     ��     ��     ��  '   ��     �     �     ��     �     �     �  
   '�     2�     6�  	   C�     M�     T�     `�     g�     |�  	   ��     ��     ��     ��     ��     Դ  	   �     �     ��     �     �      �   

--- DRIVER INFO ---
Driver:  

Press any key to continue 

There are working clones of this machine: %s 
    Configuration saved    

 
Elements of this machine cannot be emulated as they require physical interaction or consist of mechanical devices. It is not possible to fully experience this machine.
 
Sound:
 
THIS MACHINE DOESN'T WORK. The emulation for this machine is not yet complete. There is nothing you can do to fix this problem except wait for the developers to improve the emulation.
 
Video:
   %1$s
   %1$s    [default: %2$s]
   %1$s    [tag: %2$s]
   Adjuster inputs    [%1$d inputs]
   Analog inputs    [%1$d inputs]
   Gambling inputs    [%1$d inputs]
   Hanafuda inputs    [%1$d inputs]
   Keyboard inputs    [%1$d inputs]
   Keypad inputs    [%1$d inputs]
   Mahjong inputs    [%1$d inputs]
   Screen '%1$s': %2$d × %3$d (H) %4$s Hz
   Screen '%1$s': %2$d × %3$d (V) %4$s Hz
   Screen '%1$s': Vector
   User inputs    [%1$d inputs]
  (default)  (locked)  COLORS  PENS %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Driver: %4$s

CPU:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d machines (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d software packages ) %1$s (%2$s) - %3$s
 %1$s - %2$s
 %1$s Brightness %1$s Contrast %1$s Gamma %1$s Horiz Position %1$s Horiz Stretch %1$s Refresh Rate %1$s Vert Position %1$s Vert Stretch %1$s Volume %1$s [root%2$s] %1$s, %2$-.100s %1$s: %2$s
 %1$s: %2$s - Search: %3$s_ %2$s
 %d total matches found %s %s
 added to favorites list. %s
 removed from favorites list. %s [internal] %s added (EP)ROM	Imperfect
 (EP)ROM	Unimplemented
 (empty) (playing) (recording) * BIOS settings:
  %1$d options    [default: %2$s]
 * CPU:
 * Configuration settings:
 * DIP switch settings:
 * Input device(s):
 * Media Options:
 * Slot Options:
 * Sound:
 * Video:
 **Error saving %s.ini** **Error saving ui.ini** , %s <set up filters> ARGB Settings About %s Activated: %s Activated: %s = %s Add %1$s Folder - Search: %2$s_ Add Folder Add To Favorites Add autofire button Add filter Adjust speed to match refresh rate Adstick Device Assignment Advanced Options Aligned only All All cheats reloaded All slots cleared and current state saved to Slot 1 Allow rewind Analog Controls Analog Controls	Yes
 Any Are you sure you want to quit?

Press ''%1$s'' to quit,
Press ''%2$s'' to return to emulation. Artwork Options Audit ROMs for %1$u machines marked unavailable Audit ROMs for all %1$u machines Auditing ROMs for machine %2$u of %3$u...
%1$s Auto Auto frame skip Auto rotate left Auto rotate right Autofire Autofire buttons Automatic Automatic save/restore Automatically toggle pause with on-screen menus BIOS BIOS Selection BIOS selection: Barcode Reader Barcode length invalid! Beam Dot Size Beam Intensity Weight Beam Width Maximum Beam Width Minimum Bilinear Filtering Bilinear filtering for snapshots Bitmap Prescaling Bold Bookkeeping Info Burn-in CPU or RAM Camera	Imperfect
 Camera	Unimplemented
 Cancel Cannot change options while recording! Cannot save over directory Capture	Imperfect
 Capture	Unimplemented
 Change %1$s Folder - Search: %2$s_ Change Folder Changes to this only take effect when "Start new search" is selected Cheat Cheat Comment:
%s Cheat Finder Cheat Name Cheat added to cheat.simple Cheat engine not available Cheat written to %s and added to cheat.simple Cheats Choose from palette Clear Watches Coin %1$c: %2$d%3$s
 Coin %1$c: NA%3$s
 Coin impulse Coin lockout Color preview: Colors Command Communications	Imperfect
 Communications	Unimplemented
 Completely unemulated features:  Configure Directories Configure Machine Configure Machine: Configure Options Confirm quit from machines Controls	Imperfect
 Controls	Unimplemented
 Create Crosshair Offset %1$s Crosshair Offset X %1$1.3f Crosshair Offset Y %1$1.3f Crosshair Options Crosshair Scale %1$s Crosshair Scale X %1$1.3f Crosshair Scale Y %1$1.3f Current %1$s Folders Current time Custom Customize UI DIP Switches Data Format Default Default name is %s Device Mapping Dial Device Assignment Difference Disabled Disabled: %s Disk	Imperfect
 Disk	Unimplemented
 Done Double-click or press %1$s to select Driver Driver is BIOS	No
 Driver is BIOS	Yes
 Driver is Clone of	%1$s
 Driver is Parent	
 Driver is clone of: %1$-.100s Driver is parent Driver: "%1$s" software list  Driver: %1$-.100s Edit autofire button Emulated Enabled Enabled: %s Enforce Aspect Ratio Enlarge images in the right panel Enter Code Error accessing %s Exit Export displayed list to file Export list in TXT format (like -listfull) Export list in XML format (like -listxml) Export list in XML format (like -listxml, but exclude devices) External DAT View Failed to load autofire menu Failed to save input name file Fast Forward File File Already Exists - Override? File Manager Filter Filter %1$u Flip X Flip Y Folders Setup Fonts Force 4:3 aspect for snapshot display Frame skip GHz GLSL Gameinit General Info General Inputs Graphics	Imperfect
 Graphics	Imperfect Colors
 Graphics	OK
 Graphics	Unimplemented
 Graphics	Wrong Colors
 Graphics: Imperfect,  Graphics: OK,  Graphics: Unimplemented,  Group HLSL Hide Both Hide Filters Hide Info/Image Hide romless machine from available list High Scores History Hotkey Hz Image Format: Image Information Images Imperfectly emulated features:  Include clones Info auto audit Infos Infos text size Input Input (general) Input (this Machine) Input Options Input port name file saved to %s Input ports Invalid sequence entered Italic Joystick Joystick deadzone Joystick saturation Keyboard	Imperfect
 Keyboard	Unimplemented
 Keyboard Inputs	Yes
 Keyboard Mode LAN	Imperfect
 LAN	Unimplemented
 Language Laserdisc '%1$s' Horiz Position Laserdisc '%1$s' Horiz Stretch Laserdisc '%1$s' Vert Position Laserdisc '%1$s' Vert Stretch Last Slot Value Left equal to right Left equal to right with bitmask Left equal to value Left greater than right Left greater than value Left less than right Left less than value Left not equal to right Left not equal to right with bitmask Left not equal to value Lightgun Lightgun Device Assignment Lines Load State Low latency MAMEinfo MARPScore MESSinfo MHz Machine Configuration Machine Information Mag. Drum	Imperfect
 Mag. Drum	Unimplemented
 Mag. Tape	Imperfect
 Mag. Tape	Unimplemented
 Maintain Aspect Ratio Mamescore Manual Manually toggle pause when needed Manufacturer	%1$s
 Master Volume Match block Mechanical Machine	No
 Mechanical Machine	Yes
 Media	Imperfect
 Media	Unimplemented
 Memory state saved to Slot %d Menu Preview Microphone	Imperfect
 Microphone	Unimplemented
 Miscellaneous Options Mouse Mouse	Imperfect
 Mouse	Unimplemented
 Mouse Device Assignment Multi-keyboard Multi-mouse Mute when unthrottled NOT SET Name:             Description:
 Natural Natural keyboard Network Devices New Barcode: New Image Name: No No category INI files found No groups found in category file No machines found. Please check the rompath specified in the %1$s.ini file.

If this is your first time using %2$s, please see the config.txt file in the docs directory for information on configuring %2$s. No save states found Non-Integer Scaling None None
 Not supported Number Of Screens Number of frames button will be pressed Number of frames button will be released Off Off frames Offscreen reload On On frames One or more ROMs/CHDs for this machine are incorrect. The machine may not run correctly.
 One or more ROMs/CHDs for this machine have not been correctly dumped.
 Other Controls Other:  Overall	NOT WORKING
 Overall	Unemulated Protection
 Overall	Working
 Overall: NOT WORKING Overall: Unemulated Protection Overall: Working Overclock %1$s sound Overclock CPU %1$s P%d Crosshair P%d Visibility Paddle Device Assignment Page Partially supported Pause Mode Pause/Stop Pedal Device Assignment Perform Compare  :  Slot %d %s %d Perform Compare  :  Slot %d %s Slot %d %s %d Perform Compare  :  Slot %d BITWISE%s Slot %d Perform Compare : Slot %d %s Slot %d Performance Options Play Play Count Player Player %1$d Controls Please enter a file extension too Plugin Options Plugins Positional Device Assignment Press %1$s to append
 Press %1$s to clear
 Press %1$s to restore default
 Press %1$s to set
 Press %s to clear hotkey Press %s to delete Press TAB to set Press a key or joystick button, or select state to overwrite Press any key to continue. Press button for hotkey or wait to leave unchanged Pressed Printer	Imperfect
 Printer	Unimplemented
 Pseudo terminals Punch Tape	Imperfect
 Punch Tape	Unimplemented
 ROM Audit 	Disabled
Samples Audit 	Disabled
 ROM Audit Result	BAD
 ROM Audit Result	OK
 Re-select last machine launched Read this image, write to another image Read this image, write to diff Read-only Read-write Record Reload All Remove %1$s Folder Remove Folder Remove From Favorites Remove last filter Required ROM/disk images for the selected software are missing or incorrect. Please acquire the correct files or select a different software item.

 Required ROM/disk images for the selected system are missing or incorrect. Please acquire the correct files or select a different system.

 Requires Artwork	No
 Requires Artwork	Yes
 Requires CHD	No
 Requires CHD	Yes
 Requires Clickable Artwork	No
 Requires Clickable Artwork	Yes
 Reset Reset All Results will be saved to %1$s Return to Machine Return to Previous Menu Revision: %1$s Rewind Rewind capacity Romset	%1$s
 Rotate Rotate left Rotate right Rotation Options Sample Rate Sample text - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Samples Audit Result	BAD
 Samples Audit Result	None Needed
 Samples Audit Result	OK
 Save Save Cheat Save Configuration Save Machine Configuration Save State Save current memory state to Slot %d Save input names to file Screen Screen #%d Screen '%1$s' Screen Orientation	Horizontal
 Screen Orientation	Vertical
 Screen flipping in cocktail mode is not supported.
 Search: %1$s_ Select New Machine Select access mode Select an input for autofire Select category: Select cheat to set hotkey Select custom filters: Select image format Select initial contents Select state to load Selection List - Search:  Set Set hotkeys Settings Show All Show DATs view Show mouse pointer Show side panels Simultaneous contradictory Skip BIOS selection menu Skip imperfect emulation warnings Skip information screen at startup Skip software parts selection menu Sleep Slider Controls Slot %d Slot 1 Value Slot Devices Software is clone of: %1$-.100s Software is parent Software part selection: Sound Sound	Imperfect
 Sound	None
 Sound	OK
 Sound	Unimplemented
 Sound Options Sound: Imperfect Sound: None Sound: OK Sound: Unimplemented Speed Start Machine Start Out Maximized Start new search State/Playback Options Steadykey Support Cocktail	No
 Support Save	No
 Support Save	Yes
 Supported: No Supported: Partial Supported: Yes Switch Item Ordering Switched Order: entries now ordered by description Switched Order: entries now ordered by shortname Synchronized Refresh Sysinfo System: %1$-.100s Tape Control Test Test Cheat %08X_%02X Test/Write Poke Value The selected game is missing one or more required ROM or CHD images. Please select a different game.

Press any key to continue. The software selected is missing one or more required ROM or CHD images.
Please acquire the correct files or select a different one. There are known problems with this machine

 This driver requires images to be loaded in the following device(s):  This machine has no BIOS. This machine has no sound hardware, MAME will produce no sounds, this is expected behaviour.
 This machine requires external artwork files.
 This machine was never completed. It may exhibit strange behavior or missing elements that are not bugs in the emulation.
 Throttle Tickets dispensed: %1$d

 Timer Timing	Imperfect
 Timing	Unimplemented
 Total time Trackball Device Assignment Triple Buffering Type Type name is empty Type name or select: %1$s_ Type name or select: (random) UI Color Settings UI Font UI Fonts Settings UI active UI controls disabled
Use %1$s to toggle UI controls enabled
Use %1$s to toggle Unable to write file
Ensure that cheatpath folder exists Undo last search -- # Uptime: %1$d:%2$02d

 Uptime: %1$d:%2$02d:%3$02d

 Use External Samples Use image as background Use this if you want to poke %s Use this if you want to poke the Last Slot value (eg. You started without an item but finally got it) Use this if you want to poke the Slot 1 value (eg. You started with something but lost it) User Interface Value Vector Vector Flicker Video Mode Video Options Visible Delay WAN	Imperfect
 WAN	Unimplemented
 Wait Vertical Sync Warning Information Watch Window Mode Write X X Only X or Y (Auto) Y Y Only Year	%1$s
 Yes You can enter any type name Zoom to Screen Area Zoom to screen area [None]
 [This option is NOT currently mounted in the running system]

Option: %1$s
Device: %2$s

If you select this option, the following items will be enabled:
 [This option is currently mounted in the running system]

Option: %1$s
Device: %2$s

The selected option enables the following items:
 [compatible lists] [create] [empty slot] [empty] [failed] [file manager] [no category INI files] [no groups in INI file] [root%1$s] [root%2$s] [software list] color-channelAlpha color-channelBlue color-channelGreen color-channelRed color-optionBackground color-optionBorder color-optionClone color-optionDIP switch color-optionMouse down background color color-optionMouse down color color-optionMouse over background color color-optionMouse over color color-optionNormal text color-optionNormal text background color-optionSelected background color color-optionSelected color color-optionSlider color color-optionSubitem color color-optionUnavailable color color-presetBlack color-presetBlue color-presetGray color-presetGreen color-presetOrange color-presetRed color-presetSilver color-presetViolet color-presetWhite color-presetYellow color-sampleClone color-sampleMouse Over color-sampleNormal color-sampleSelected color-sampleSubitem default emulation-featureLAN emulation-featureWAN emulation-featurecamera emulation-featurecapture hardware emulation-featurecolor palette emulation-featurecommunications emulation-featurecontrols emulation-featuredisk emulation-featuregraphics emulation-featurekeyboard emulation-featuremagnetic drum emulation-featuremagnetic tape emulation-featuremedia emulation-featuremicrophone emulation-featuremouse emulation-featureprinter emulation-featureprotection emulation-featurepunch tape emulation-featuresolid state storage emulation-featuresound emulation-featuretiming incorrect checksum incorrect length kHz machine-filterAvailable machine-filterBIOS machine-filterCHD Required machine-filterCategory machine-filterClones machine-filterCustom Filter machine-filterFavorites machine-filterHorizontal Screen machine-filterManufacturer machine-filterMechanical machine-filterNo CHD Required machine-filterNot BIOS machine-filterNot Mechanical machine-filterNot Working machine-filterParents machine-filterSave Supported machine-filterSave Unsupported machine-filterUnavailable machine-filterUnfiltered machine-filterVertical Screen machine-filterWorking machine-filterYear not found path-optionArtwork path-optionArtwork Previews path-optionBosses path-optionCabinets path-optionCategory INIs path-optionControl Panels path-optionCovers path-optionCrosshairs path-optionDATs path-optionFlyers path-optionHowTo path-optionINIs path-optionIcons path-optionLogos path-optionMarquees path-optionPCBs path-optionROMs path-optionScores path-optionSnapshots path-optionSoftware Media path-optionVersus playing recording selmenu-artworkArtwork Preview selmenu-artworkBosses selmenu-artworkCabinet selmenu-artworkControl Panel selmenu-artworkCovers selmenu-artworkFlyer selmenu-artworkGame Over selmenu-artworkHowTo selmenu-artworkPCB selmenu-artworkScores selmenu-artworkSnapshots selmenu-artworkVersus software-filterAvailable software-filterClones software-filterCustom Filter software-filterDevice Type software-filterFavorites software-filterParents software-filterPartially Supported software-filterPublisher software-filterRelease Region software-filterSoftware List software-filterSupported software-filterUnavailable software-filterUnfiltered software-filterUnsupported software-filterYear stopped Project-Id-Version: MAME
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-17 07:08+1100
PO-Revision-Date: 2021-07-02 11:50-0300
Last-Translator: Felipe <felipefplzx@gmail.com>
Language-Team: MAME Language Team
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.0
 

--- INFO do DRIVER ---
Driver:  

Pressione qualquer tecla para continuar 

Há clones desta máquina que funcionam: %s 
    Configuração salva    

 
Os elementos desta máquina não podem ser emulados como eles requerem interação física ou consistem de dispositivos mecânicos. Não é possível experimentar esta máquina na totalidade.
 
Som:
 
ESTA MÁQUINA NÃO FUNCIONA. A emulação pra esta máquina ainda não está completa. Não há nada que você possa fazer pra consertar este problema exceto aguardar os desenvolvedores melhorarem a emulação.
 
Vídeo:
   %1$s
   %1$s    [padrão: %2$s]
   %1$s    [etiqueta: %2$s]
   Entradas reguladoras    [%1$d entradas]
   Entradas analógicas    [%1$d entradas]
   Entradas pra jogos de apostas    [%1$d entradas]
   Entrada hanafuda    [%1$d entradas]
   Entradas do teclado    [%1$d entradas]
   Entradas do teclado numérico   [%1$d entradas]
  Entradas do Mahjong    [%1$d entradas]
   Tela '%1$s': %2$d × %3$d (H) %4$s Hz
   Tela '%1$s': %2$d × %3$d (V) %4$s Hz
   Tela '%1$s': Vetor
   Entradas do usuário    [%1$d entradas]
  (padrão)  (travado)  CORES  CANETAS %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Driver: %4$s

CPU:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d máquinas (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d pacotes do software ) %1$s (%2$s) - %3$s
 %1$s - %2$s
 Brilho da %1$s Contraste da %1$s Gama da %1$s Posição Horizontal da %1$s Extensão Horizontal da %1$s Taxa de Atualização da %1$s Posição Vertical da %1$s Extensão Vertical da %1$s Volume %1$s %1$s [root%2$s] %1$s, %2$-.100s %1$s: %2$s
 %1$s: %2$s - Busca: %3$s_ %2$s
 Um total de %d combinações foram encontradas %s %s
 adicionado a lista dos favoritos. %s
 removido da lista dos favoritos. %s [interno] %s adicionado (EP)ROM	Imperfeito
 (EP)ROM	Não Implementado
 (vazio) (jogando) (gravando) * Configurações da BIOS:
  %1$d opções    [padrão: %2$s]
 * CPU:
 * Definição das configurações:
 * Configurações das chaves do DIP:
 * Dispositivo(s) de entrada:
 * Opções da Mídia:
 * Opções de Slot:
 * Som:
 * Vídeo:
 **Erro ao salvar o %s.ini** **Erro ao salvar o ui.ini** , %s <configurar filtros> Configurações do ARGB Sobre o %s Ativado: %s Ativado: %s = %s Adicionar %1$s Pasta - Busca: %2$s_ Adicionar Pasta Adicionar aos Favoritos Adicionar botão do auto-disparar Adicionar filtro Ajustar a velocidade pra combinar com a taxa da atualização Atribuição do Dispositivo Adstick Opções Avançadas Só alinhado Tudo Todas as trapaças recarregadas Todos os slots limpos e o state atual foi salvo no Slot 1 Permitir a rebobinação Controles Analógicos Controles Analógicos	Sim
 Qualquer Você tem certeza que você quer sair?

Pressione ''%1$s'' pra sair,
Pressione ''%2$s'' pra retornar pra emulação. Opções do Trabalho de Arte Auditar as ROMS pra %1$u máquinas marcadas como indisponíveis Auditar as ROMs pra toda as %1$u máquinas Auditando as ROMs para a máquina %2$u de %3$u...
%1$s Automático Auto frameskip Auto rotacionar pra esquerda Auto rotacionar pra direita Auto-disparar Botões do auto-disparar Automático Salvamento/Restauração Automáticos Alternar automaticamente a pausa quando nos menus na tela BIOS Seleção de BIOS Seleção da BIOS: Leitor do Código de Barras Tamanho do código de barras inválido! Tamanho do Ponto do Beam Peso da Intensidade do Beam Largura Máxima do Beam Largura Mínima do Beam Filtragem Bilinear Filtragem bilinear pras snapshots Pré-dimensionamento do Bitmap Negrito Informação da Contabilidade Queima CPU ou RAM Câmera	Imperfeita
 Câmera	Não Implementada
 Cancelar Não pode mudar as opções quando gravar! Não pode salvar sobre o diretório Captura	Imperfeita
 Captura	Não Implementada
 Mudar %1$s Pasta - Busca: %2$s_ Mudar a Pasta As mudanças nisto só tem efeito quando "Iniciar nova busca" for selecionada Trapaça Comentário da Trapaça:
%s Localizador de Trapaças Nome da Trapaça Trapaça adicionada em cheat.simple A engine da trapaça não está disponível Trapaça gravada em %s e adicionada em cheat.simple Trapaças Escolher da paleta Limpar Observações Ficha %1$c: %2$d%3$s
 Ficha %1$c: NA%3$s
 Impulso da ficha Bloqueio da ficha Pré-visualização da cor: Cores Comando Comunicações	Imperfeitas
 Comunicações	Não Implementadas
 Funções completamente não emuladas:  Configurar Diretórios Configurar a Máquina Configurar Máquina: Opções da Configuração Confirmar a saída das máquinas Controles	Imperfeitos
 Controles	Não Implementados
 Criar Deslocamento da Mira %1$s Deslocamento da Mira X %1$1.3f Deslocamento da Mira Y %1$1.3f Opções de Mira Escala da Mira da %1$s Escala da Mira X %1$1.3f Escala da Mira Y %1$1.3f %1$s Pastas Atuais Tempo Atual Personalizado Personalizar a IU Chaves DIP Formato dos Dados Padrão O nome padrão é %s Mapeamento do dispositivo Atribuição do Dispositivo Dial Diferença Desativado Desativado: %s Disco	Imperfeito
 Disco	Não Implementado
 Feito Clique duplo ou pressione %1$s pra selecionar Driver O Driver é BIOS	Não
 O Driver é BIOS	Sim
 O Driver é Clone do	%1$s
 O Driver é Pai	
 O Driver é clone de: %1$-.100s O driver é pai Driver: "%1$s" lista de softwares  Driver: %1$-.100s Editar botão do auto-disparar Emulado Ativado Ativado: %s Forçar a Proporção do Aspecto Aumentar as imagens no painel direito Insira o Código Erro ao acessar o %s Sair Exportar lista exibida pro arquivo Exportar lista no formato TXT (tipo -listfull) Exportar lista no formato XML (tipo -listxml) Exportar lista no formato XML (tipo -listxml, mas excluindo os dispositivos) Visualização Externa do DAT Falhou em carregar o menu do auto-disparar Falhou em salvar o arquivo dos nomes de entrada Avanço Rápido Arquivo O Arquivo já Existe - Sobrescrever? Gerenciador de Arquivos Filtro Filtro %1$u Giro X Giro Y Configuração das Pastas Fontes Manter o aspecto 4:3 para as exibições dos snapshots Frame skip GHz GLSL Gameinit Informações Gerais Entradas Gerais Gráficos	Imperfeitos
 Gráficos	Cores Imperfeitas
 Gráficos	OK
 Gráficos	Não Implementados
 Graficos	Cores Erradas
 Gráficos: Imperfeitos,  Gráficos: OK,  Gráficos: Não Implementados,  Grupo HLSL Esconder Ambos Esconder Filtros Esconder Info/Imagem Esconder máquinas sem roms da lista de disponíveis Maiores Pontuações Histórico Tecla de Atalho Hz Formato da Imagem: Informação da Imagem Imagens Funções emuladas imperfeitamente:  Incluir clones Informação automática da auditoria Informações Tamanho do texto das informações Entrada Entrada (geral) Entrada (esta máquina) Opções de Entrada Nome das portas de entrada salvo em %s Portas de entrada Sequência inválida inserida Itálico Joystick Zona morta do joystick Saturação do joystick Teclado	Imperfeito
 Teclado	Não Implementado
 Entradas do Teclado	Sim
 Modo Teclado LAN	Imperfeita
 LAN	Não Implementada
 Idioma Laserdisc '%1$s' Posição Horizontal Laserdisc '%1$s' Extensão Horizontal Laserdisc '%1$s' Posição Vertical Laserdisc '%1$s' Extensão Vertical Último Valor do Slot Esquerda igual a direita Esquerdo igual ao direito com bitmask Esquerda igual ao valor Esquerda maior do que a direita Esquerdo maior do que o valor Esquerda menor do que a direita Esquerdo menor do que o valor Esquerda não é igual a direita Esquerdo não igual ao direito com bitmask Esquerda não é igual ao valor Lightgun Atribuição do Dispositivo Lightgun Linhas Carregar State Latência baixa MAMEinfo MARPScore MESSinfo MHz Configuração da Máquina Informação da Máquina Bateria Magnética	Imperfeita
 Bateria Magnética	Não Implementada
 Fita Magnética	Imperfeita
 Fita Magnética	Não Implementada
 Manter a Proporção do Aspecto Pontuação do MAM Manual Alternar a pausa manualmente quando necessário Fabricante	%1$s
 Volume Mestre Combinar com o bloco Máquina Mecânica	Não
 Máquina Mecânica	Sim
 Mídia	Imperfeita
 Mídia	Não Implementada
 State da memória salvo no Slot %d Pré-Visualização do Menu Microfone	Imperfeito
 Microfone	Não Implementado
 Opções Mistas Mouse Mouse	Imperfeito
 Mouse	Não Implementado
 Atribuição do Dispositivo Mouse Teclado Múltiplo Mouse Múltiplo Mudo quando não há limite de velocidade NÃO DEFINIDO Nome:             Descrição:
 Natural Teclado natural Dispositivos das Redes Novo Código de Barras: Novo Nome da Imagem: Não Nenhum arquivo INI contendo categorias foi achado Nenhum grupo achado no arquivo das categorias Nenhuma máquina encontrada. Por favor verifique o caminho da rom especificado no arquivo %1$s.ini .

Se esta é a sua primeira vez usando o %2$s, por favor veja o arquivo config.txt no diretório dos documentos para informação sobre a configuração do %2$s. Não foram achados save states Dimensionamento Não-Integral Nenhum Nenhum
 Não suportado Número de Telas Número dos botões dos frames que serão pressionados Número dos botões dos frames que serão liberados Desligado Frames desligados Recarga fora da tela Ligado Frames ligados Uma ou mais ROMs/CHDs pra esta máquina estão incorretas. A máquina pode não funcionar corretamente.
 Uma ou mais ROMs/CHDs pra essa máquina não foram dumpadas corretamente. 
 Outros Controles Outro:  Geral	NÃO FUNCIONANDO
 Geral	Proteção não Emulada
 Geral	Funcionando
 Geral: NÃO FUNCIONA Geral: Proteção Não Emulada Geral: Funcionando Overclock do som %1$s Overclock da CPU %1$s Mira do P%d Visibilidade do P%d Atribuição do Dispositivo Paddle Página Parcialmente suportado Modo de Pausa Pausar/Parar Atribuição do Dispositivo Pedal Realizar Comparação  :  Slot %d %s %d Realizar Comparação  :  Slot %d %s Slot %d %s %d Realizar Comparação  :  Slot %d BITWISE%s Slot %d Realizar Comparação : Slot %d %s Slot %d Opções de Performance Jogar Contador de Jogadas Jogador Controles do Jogador %1$d Por favor insira também uma extensão do arquivo Opções dos Plugins Plugins Atribuição do Dispositivo Posicional Pressione o %1$s pra anexar
 Pressione o %1$s pra limpar
 Pressione o %1$s pra restaurar o padrão
 Pressione o %1$s pra configurar
 Pressione o %s pra limpar a tecla de atalho Pressione %s pra apagar Pressione TAB pra configurar Pressione uma tecla ou botão de joystick ou selecione o state pra sobrescrever Pressione qualquer tecla pra continuar. Pressione o botão pra tecla de atalho ou espere pra sair sem mudar Pressionado Impressora	Imperfeita
 Impressora	Não Implementada
 Pseudo terminais Punch Tape	Imperfeita
 Punch Tape	Não Implementada
 Auditoria da ROM 	Desativada
Auditoria das Amostras 	Desativada
 Resultado da Auditoria da ROM	RUIM
 Resultado da Auditoria da ROM	OK
 Re-selecionar a última máquina executada Ler esta imagem, gravar pra outra imagem Ler esta imagem, gravar pra diff Somente-leitura Somente-gravação Gravar Recarregar Tudo Remover a Pasta %1$s Remover Pasta Remover dos Favoritos Remover o último filtro As imagens das ROMs/Discos requeridas para o sistema selecionado estão ausentes ou são incorretas. Por favor adquira os arquivos corretos ou selecione um item de software diferente.

 As imagens das ROMs/Discos requeridas para o sistema selecionado estão ausentes ou são incorretas. Por favor adquira os arquivos corretos ou selecione um sistema diferente.

 Requer Trabalho de Arte	Não
 Requer Trabalho de Arte	Sim
 Requer CHD	Não
 Requer CHD	Sim
 Requer Arte Clicável	Não
 Requer Arte Clicável	Sim
 Resetar Resetar Tudo Os resultados serão salvos em %1$s Retorna pra Máquina Retornar pro Menu Anterior Revisão: %1$s Rebobinar Capacidade de rebobinação Romset	%1$s
 Rotacionar Rotacionar pra esquerda Rotacionar pra direita Opções de Rotação Taxa das Amostras Texto de amostra - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Resultado da Auditoria das Amostras	RUIM
 Resultado da Auditoria das Amostras	Nenhuma Necessária
 Resultado da Auditoria das Amostras	OK
 Salvar Salvar Trapaça Salvar Configuração Salvar a Configuração da Máquina Salvar State Salvar o state da memória atual no Slot %d Salvar os nomes de entrada no arquivo Tela Tela #%d Tela '%1$s' Orientação da Tela	Horizontal
 Orientação da Tela	Vertical
 A inversão da tela no modo coquetel não é suportada.
 Busca: %1$s_ Selecionar Nova Máquina Selecione o modo de acesso Selecione uma entrada pra auto-disparar Selecionar categoria: Selecione uma trapaça pra definir a tecla de atalho Selecionar filtros personalizados: Selecione o formato da imagem Selecionar os conteúdos iniciais Selecione o state pra carregar Lista de Seleção - Busca:  Definir Definir teclas de atalho Configurações Mostrar Tudo Mostrar visualização dos DATs Mostrar o ponteiro do mouse Mostrar os painéis laterais Contraditório simultâneo Ignorar a tela de seleção da BIOS Ignorar avisos de emulação imperfeita Ignorar a tela de informações na inicialização Ignorar o menu da seleção das partes do software Dormir Controles do Slider Slot %d Valor do Slot 1 Dispositivos de Slot O software é clone de: %1$-.100s O software é pai Seleção da parte do software: Som Som	Imperfeito
 Som	Nenhum
 Som	OK
 Som	Não Implementado
 Opções de Som Som: Imperfeito Som: Nenhum Som: OK Som: Não Implementado Velocidade Iniciar Máquina Iniciar Maximizado Iniciar nova busca Opções do State/Playback Steadykey Suporte ao Coquetel	Não
 Suporte pro Salvamento	Não
 Suporte pro Salvamento	Sim
 Suportado: Não Suportado: Parcial Suportado: Sim Trocar a Ordem dos Itens Ordem Trocada: entradas agora ordenadas por descrição Ordem Trocada: entradas agora ordenadas por nome curto Atualização Sincronizada Sysinfo Sistema: %1$-.100s Controle da Fita Teste Testar Trapaça %08X_%02X Testar/Gravar o Valor da Cutucada O jogos selecionado tem falta de uma ou mais imagens de ROM ou CHD. Por favor selecione um jogo diferente.

Pressione qualquer tecla para continuar. O software selecionado tem falta de uma ou mais imagens de ROM ou CHD.
Por favor adquira os arquivos corretos ou selecione um arquivo diferente. Há problemas conhecidos com esta máquina

 Este driver requer que as imagens sejam carregadas no(s) seguinte(s) dispositivo(s):  Esta máquina não tem BIOS. Esta máquina não tem hardware de som, o MAME não produzirá sons, isto é um comportamento esperado.
 Esta máquina requer arquivos externos do trabalho de arte.
 Esta máquina nunca foi completada. Ela pode exibir um comportamento estranho ou faltar alguns elementos que não são bugs na emulação.
 Suprimir Bilhetes distribuídos: %1$d

 Cronômetro Cronometragem	Imperfeita
 Cronometragem	Não Implementada
 Tempo Total Atribuição do Dispositivo Trackball Buffering Triplo Tipo O tipo de nome está vazio Digite o nome ou selecione: %1$s_ Digite o nome ou selecione: (aleatório) Configurações das Cores da IU Fonte da IU Configurações das Fontes da IU IU ativa Controles da IU desativados
Use o %1$s pra alternar Controles da IU ativados
Use o %1$s pra alternar Incapaz de gravar no arquivo
Garanta que a pasta cheatpath exista Desfazer a última busca -- # Tempo de operação: %1$d:%2$02d

 Tempo de operação: %1$d:%2$02d:%3$02d

 Usar Amostras Externas Usar imagem como cenário de fundo Use isto se você quer cutucar o %s Use isto se você quer cutucar o valor do Último Slot (ex: Você começou sem um item mas finalmente o conseguiu) Use isto se você quer cutucar o Slot 1 (ex: Você começou com algo mas o perdeu) Interface do Usuário Valor Vetor Tremulação Vetorial Modo de Vídeo Opções de Vídeo Atraso Visível WAN	Imperfeita
 WAN	Não Implementada
 Aguardar Sincronização Vertical Informação de Aviso Observar Modo Janela Gravar X Só o X X ou Y (Auto) Y Só o Y Ano	%1$s
 Sim Você pode inserir qualquer tipo de nome Aumentar o Zoom pra Área da Tela Aumentar o zoom pra área da tela [Nenhum]
 [Esta opção NÃO está montada atualmente no sistema em execução]

Opção: %1$s
Dispositivo: %2$s

Se você selecionar esta opção os seguintes itens serão ativados:
 [Esta opção está montada atualmente no sistema em execução]

Opção: %1$s
Dispositivo: %2$s

A opção selecionada ativa os seguintes itens:
 [listas compatíveis] [criar] [slot vazio] [vazio] [falhou] [gerenciador de arquivos] [arquivos INI sem categoria] [sem grupos no arquivo INI] [root%1$s] [root%2$s] [lista de software] Alpha Azul Verde Vermelho Cenário de fundo Borda Clone Chave DIP Cor da subposição do cenário do fundo do mouse Cor da subposição do mouse Mouse sobre a cor de fundo Mouse sobre a cor Texto normal Fundo do texto normal Cor de fundo selecionada Cor selecionada Cor do slider Cor do sub-item Cor indisponível Preto Azul Cinza Verde Laranja Vermelho Prata Violeta Branco Amarelo Clone Sobreposição do Mouse Normal Selecionado Sub-item padrão LAN WAN câmera capturar hardware paleta das cores comunicações controles disco gráficos teclado bateria magnética fita magnética mídia microfone mouse impressora proteção punch tape armazenamento em estado sólido som cronometragem checksum incorreto tamanho incorreto kHz Disponível BIOS CHD Requerido Categoria Clones Filtro Personalizado Favoritos Tela Horizontal Fabricante Mecânico Nenhum CHD Requerido Sem BIOS Não Mecânico Não Funcionando Pais Com Suporte a Salvamento Sem Suporte a Salvamento Indisponível Não Filtrado Tela Vertical Funcionando Ano não foi achado Trabalhos de Arte Pré-Visualização dos Trabalhos das Artes Chefes Gabinetes INIs da Categoria Painéis de Controle Capas Miras DATs Panfletos Como Fazer INIs Ícones Logos Marquises PCBs ROMs Pontuações Snapshots Mídia do Software Versus jogando gravando Pré-Visualização do Trabalho de Arte Chefes Gabinete Painel de controle Capas Panfleto Fim do Jogo Como Fazer PCB Pontuações Snapshots Versus Disponível Clones Filtro Personalizado Tipo de Dispositivo Favoritos Pais Parcialmente Suportado Editor Região do Lançamento Lista de Softwares Suportado Indisponível Não Filtrado Não Suportado Ano parado 