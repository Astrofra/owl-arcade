��    ^                   �%     �%     &  .   8&     g&     �&  �   �&     I'     R'     Z'     u'  #   �'  !   �'  #   �'  #   �'  #   (  !   >(  "   `(  +   �(  +   �(     �(     �(  
   )  	   )     ))     1)  	   7)     A)     H)  	   P)     Z)     b)  "   i)  	   �)  .   �)  +   �)     �)     *  
   *     *     .*     A*     S*     f*     x*     �*     �*     �*     �*     �*     �*     �*      �*     +     '+  	   0+     :+  3   F+     z+     �+     �+     �+     �+     �+  	   �+  	   �+      ,     ,     0,     5,     F,     T,     b,     u,  
   �,     �,  
   �,     �,     �,     �,     �,     �,     -     $-  ^   (-     �-  .   �-     �-     �-     �-     �-     �-     .     .     ).     9.     H.     `.     v.     �.     �.     �.     �.     �.     �.  
   �.     �.     �.     /     /  "   4/     W/  D   e/     �/     �/     �/  
   �/     �/     �/  -   0     ?0     F0     Z0     h0     }0     �0     �0     �0     �0     �0      �0     �0     �0     1     #1     >1     R1     j1     q1     �1     �1     �1     �1     �1     �1     2     -2     :2     A2     N2     [2     g2     o2     �2     �2     �2     �2     �2     �2     �2  $   �2     3     3     &3     :3     S3     f3     �3     �3     �3     �3     �3     �3     �3  !   �3  
   4     $4     74     <4  *   Z4  )   �4  >   �4     �4      5     5     ,5     15     Q5     ^5     e5     q5     x5     5     �5  %   �5  
   �5     �5     �5     �5     �5     �5     6     6     *6     B6     Y6     o6     ~6     �6     �6  	   �6     �6     �6  (   �6     �6     �6     7     7     '7     .7     N7     ]7     m7     s7     �7     �7     �7      �7     �7     �7     �7     �7     8     8     -8     E8     Z8     h8     w8     �8     �8     �8     �8     �8     9      #9     D9     X9     p9  $   �9     �9     �9     �9     �9  
   �9     �9  	    :     
:     :     :     -:  	   A:     K:     ^:     l:     x:     �:     �:     �:     �:     �:     �:      ;     ;     &;     >;     M;     Y;     y;     �;     �;     �;     �;     �;     �;      �;  �   �;     �<     �<     �<     �<     �<     �<     =  Y   =  G   j=     �=     �=     �=     �=     >     >     :>     K>     `>     s>     �>     �>  
   �>     �>     �>     �>  
   �>     �>  !   �>     ?     $?     ,?     I?     Z?     u?     �?     �?  ,   �?     �?     �?  '   @     0@  	   O@  
   Y@     d@  
   k@     v@     �@     �@     �@     �@     �@     �@     �@     A     -A     MA  	   SA     ]A     {A     �A     �A     �A     �A     �A     �A     �A     �A     �A     	B  F   B     \B  !   vB     �B     �B  
   �B     �B  
   �B     �B     �B  
   �B     
C     C     7C  3   TC     �C     �C     �C     �C     �C     �C     �C     D     -D     1D     =D     FD     OD     ^D     qD     �D     �D  "   �D  "   �D     �D     E     E     E     ?E     RE     kE     qE     �E  	   �E     �E     �E     �E     �E  	   �E     �E     �E     �E     F     "F  	   9F     CF     XF     iF     {F     �F     �F     �F  2   �F  0   �F     $G     9G     AG     SG     `G  �   eG  ,   �G  E   H     YH  ]   sH  .   �H  z    I     {I     �I     �I     �I     �I  
   �I     �I     �I     J     	J     $J     BJ     TJ     nJ     vJ  	   �J  8   �J     �J     �J     �J     K     )K     AK     PK     VK     ]K  
   lK     wK     �K     �K     �K     �K     �K     �K     �K     �K     �K  
   �K     �K     �K  �   �K  �   �L     M     /M     8M     EM     MM     VM     eM     }M     �M     �M     �M     �M     �M     �M     
N     N     1N  (   IN     rN  (   �N     �N     �N  #   �N  &   O     ;O     WO     qO     �O     �O     �O     �O     �O     �O     	P     P     .P     BP     UP     iP     |P     �P     �P     �P     �P     �P     �P     Q      Q     @Q     [Q     rQ     �Q     �Q     �Q     �Q     �Q     R     ,R     ER     IR     bR     vR     �R     �R     �R     �R      �R     S     3S     MS     lS     �S     �S     �S     �S     �S     T     -T     GT     fT     }T     �T     �T     �T     �T     �T     U     U     2U     IU     ZU     mU     U     �U     �U     �U     �U     �U     �U     �U     V     /V     BV  	   JV     TV     tV     �V     �V     �V     �V     �V      W     W     )W     @W     ZW     qW     �W     �W     �W     �W     �W  #   X     2X     LX     kX     �X     �X     �X     �X     �X     Y  �  Y      �Z  2   �Z  4   �Z  $   ([     M[  �   V[     \     \     \     0\  )   G\  $   q\  *   �\  &   �\  &   �\  (   ]  %   8]  /   ^]  /   �]     �]  &   �]     ^     ^     ^     "^  	   *^     4^     ;^  	   C^     M^     U^  "   \^  	   ^  /   �^  )   �^     �^     �^  
   _     _     &_     A_     Z_     r_     �_     �_     �_     �_     �_     �_     �_  $   �_  $   `     ?`     K`     [`     g`  :   s`     �`     �`     �`     �`     
a     a  	   4a  	   >a  $   Ha  $   ma     �a     �a     �a     �a     �a  &   �a     b     b     0b     Cb     Ub     ib     nb     �b  !   �b     �b  �   �b     [c  /   rc     �c      �c     �c      �c  &   	d     0d     5d     Bd     Pd     ]d      vd     �d     �d     �d     �d     �d     �d     e     "e     /e     Be  	   ^e  %   he  "   �e     �e  U   �e     f     f     1f  	   >f  "   Hf     kf  :   �f     �f     �f     �f     �f     g     #g     0g     Eg     Sg     Zg  !   ag     �g     �g     �g  #   �g     �g  #   h     0h     9h     Ph     lh     �h     �h     �h     �h     �h     i      i     2i     Oi     \i     hi     qi     �i     �i     �i     �i     �i     �i     j  4   	j     >j     Fj     ]j     rj     �j  #   �j     �j     �j     k     %k  	   .k     8k     Fk  &   bk     �k     �k  	   �k  %   �k  /   �k  .   l  L   Al     �l  4   �l  	   �l     �l  )   �l     m     m     %m     1m     Em     Wm     km  4   xm     �m     �m     �m     �m     �m     �m  !   n  
   (n     3n     On     mn     �n     �n     �n     �n     �n     �n     �n  A   �n     3o  
   Eo     Po  "   ^o     �o  $   �o     �o  '   �o     �o     �o     p     *p     Hp  *   Yp     �p     �p     �p     �p     �p     �p     �p     �p     q     'q     7q     Pq  %   Xq  &   ~q  #   �q  $   �q     �q      r     #r     5r     Ir  "   \r     r     �r     �r     �r     �r  	   �r     �r  	   �r     �r     �r  "   	s     ,s     ;s     Ls  !   ^s     �s     �s     �s     �s     �s     �s     t     t     ,t     Ft     Ut     ht      vt  
   �t     �t     �t     �t     �t     �t  $   �t  )   u  �   <u     2v     8v     ?v     Rv     iv  $   mv     �v  u   �v  L   w     Xw     ow  (   �w     �w     �w  (   �w     x     -x     Dx     Yx     jx     px  
   �x     �x     �x  	   �x     �x     �x  .   �x     y     y     (y     Cy  3   _y     �y     �y     �y  @   �y     z     3z  7   Iz  )   �z  
   �z     �z     �z     �z     �z     �z     {     #{     <{     [{     x{     �{  (   �{  &   �{     �{     �{  (   |     8|     M|     j|     x|     �|     �|     �|     �|     �|     �|  
   �|  G   �|  !   =}  #   _}     �}  	   �}     �}     �}     �}  )   �}  
   
~     ~     $~  "   6~      Y~  @   z~     �~     �~     �~     �~  5        A     T     l  
   �     �     �     �     �     �     �  #   �  $   #�  0   H�  '   y�     ��     ��     ��  $   ɀ     �     �     �     %�     7�  	   D�     N�     i�     |�     ��  	   ��     ��     ��     Ё     �     ��     �  !    �     B�     _�     z�     ��     ��      ��  C   ׂ  C   �     _�     ~�     ��     ��     ��  �   ��  /   i�  R   ��  !   �  _   �  5   n�  �   ��  
   K�     V�     r�     {�  &   ��     ��     І     �     ��  /   ��  >   *�  *   i�  !   ��  #   ��  1   ڇ     �  R   &�  %   y�     ��     ��     ֈ     �     �     $�     )�     0�  
   ?�     J�     ]�     t�     ��  ,   ��  
   ʉ     Չ     �     �     �  
   �     ��     ��  �   �  �   ��     /�  
   C�     N�     b�     i�     z�     ��     ��     ċ     ԋ     ڋ     ߋ     �     �     ��     ��     ��     �     ,�     A�     a�     v�     ��     ��     ��     Ό     �     ��     �     �     �     $�     *�     1�     5�     <�     D�     J�     O�  	   T�     ^�     e�     m�     z�     ��     ��     ��     ��     ��  
   ��     ��     ��     ȍ     э     ֍     ލ     �     �     �  
   �     �     �  	   %�     /�     5�  	   P�     Z�  
   r�  
   }�     ��  
   ��     ��     ��     ˎ     Ҏ     �     �     �     #�     9�     F�     K�     Z�     r�     x�     ��     ��     ��     ��     ��     ��     ��     Ï     ȏ     Ώ     ԏ     ݏ     �     �     ��     �     �  	   �  	   (�     2�     J�     P�     Y�     _�  	   e�     o�     x�     }�     ��     ��     ��  
   ��     ��     ��  
   Ԑ  	   ߐ     �     �     �     �     ,�     :�     G�     X�     d�     w�     |�   

--- DRIVER INFO ---
Driver:  

Press any key to continue 

There are working clones of this machine: %s 
    Configuration saved    

 
Sound:
 
THIS MACHINE DOESN'T WORK. The emulation for this machine is not yet complete. There is nothing you can do to fix this problem except wait for the developers to improve the emulation.
 
Video:
   %1$s
   %1$s    [default: %2$s]
   %1$s    [tag: %2$s]
   Adjuster inputs    [%1$d inputs]
   Analog inputs    [%1$d inputs]
   Gambling inputs    [%1$d inputs]
   Hanafuda inputs    [%1$d inputs]
   Keyboard inputs    [%1$d inputs]
   Keypad inputs    [%1$d inputs]
   Mahjong inputs    [%1$d inputs]
   Screen '%1$s': %2$d × %3$d (H) %4$s Hz
   Screen '%1$s': %2$d × %3$d (V) %4$s Hz
   Screen '%1$s': Vector
   User inputs    [%1$d inputs]
  (default)  (locked)  COLORS  PENS %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Driver: %4$s

CPU:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d machines (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d software packages ) %1$s Brightness %1$s Contrast %1$s Gamma %1$s Horiz Position %1$s Horiz Stretch %1$s Refresh Rate %1$s Vert Position %1$s Vert Stretch %1$s Volume %1$s, %2$-.100s %1$s: %2$s
 %1$s: %2$s - Search: %3$s_ %2$s
 %d total matches found %s %s
 added to favorites list. %s
 removed from favorites list. %s [internal] %s added (playing) (recording) * BIOS settings:
  %1$d options    [default: %2$s]
 * CPU:
 * Configuration settings:
 * DIP switch settings:
 * Input device(s):
 * Media Options:
 * Slot Options:
 * Sound:
 * Video:
 **Error saving %s.ini** **Error saving ui.ini** , %s <set up filters> ARGB Settings Activated: %s Activated: %s = %s Add %1$s Folder - Search: %2$s_ Add Folder Add To Favorites Add filter Adstick Device Assignment Advanced Options All All cheats reloaded Analog Controls Analog Controls	Yes
 Any Are you sure you want to quit?

Press ''%1$s'' to quit,
Press ''%2$s'' to return to emulation. Artwork Options Auditing ROMs for machine %2$u of %3$u...
%1$s Auto Auto frame skip Auto rotate left Auto rotate right Automatic save/restore BIOS BIOS Selection BIOS selection: Barcode Reader Barcode length invalid! Beam Intensity Weight Beam Width Maximum Beam Width Minimum Bilinear Filtering Bitmap Prescaling Bold Bookkeeping Info Burn-in CPU or RAM Camera	Imperfect
 Camera	Unimplemented
 Cancel Cannot save over directory Change %1$s Folder - Search: %2$s_ Change Folder Changes to this only take effect when "Start new search" is selected Cheat Cheat Comment:
%s Cheat Finder Cheat Name Cheat added to cheat.simple Cheat engine not available Cheat written to %s and added to cheat.simple Cheats Choose from palette Clear Watches Coin %1$c: %2$d%3$s
 Coin %1$c: NA%3$s
 Coin impulse Coin lockout Color preview: Colors Command Completely unemulated features:  Configure Directories Configure Machine Configure Options Confirm quit from machines Controls	Imperfect
 Controls	Unimplemented
 Create Crosshair Offset %1$s Crosshair Offset X %1$1.3f Crosshair Offset Y %1$1.3f Crosshair Options Crosshair Scale %1$s Crosshair Scale X %1$1.3f Crosshair Scale Y %1$1.3f Current %1$s Folders Current time Custom Customize UI DIP Switches Data Format Default Default name is %s Device Mapping Dial Device Assignment Disabled Disabled: %s Disk	Imperfect
 Disk	Unimplemented
 Done Double-click or press %1$s to select Driver Driver is BIOS	No
 Driver is BIOS	Yes
 Driver is Clone of	%1$s
 Driver is Parent	
 Driver is clone of: %1$-.100s Driver is parent Driver: "%1$s" software list  Driver: %1$-.100s Emulated Enabled Enabled: %s Enforce Aspect Ratio Enlarge images in the right panel Enter Code Error accessing %s Exit Export displayed list to file Export list in TXT format (like -listfull) Export list in XML format (like -listxml) Export list in XML format (like -listxml, but exclude devices) External DAT View Failed to save input name file Fast Forward File File Already Exists - Override? File Manager Filter Filter %1$u Flip X Flip Y Folders Setup Fonts Force 4:3 aspect for snapshot display Frame skip GLSL Gameinit General Info General Inputs Graphics	Imperfect
 Graphics	Imperfect Colors
 Graphics	OK
 Graphics	Unimplemented
 Graphics	Wrong Colors
 Graphics: Imperfect,  Graphics: OK,  Graphics: Unimplemented,  Group HLSL Hide Both Hide Filters Hide Info/Image Hide romless machine from available list High Scores History Image Format: Image Information Images Imperfectly emulated features:  Include clones Info auto audit Infos Infos text size Input (general) Input (this Machine) Input Options Input port name file saved to %s Input ports Italic Joystick Joystick deadzone Joystick saturation Keyboard	Imperfect
 Keyboard	Unimplemented
 Keyboard Inputs	Yes
 Keyboard Mode LAN	Imperfect
 LAN	Unimplemented
 Language Laserdisc '%1$s' Horiz Position Laserdisc '%1$s' Horiz Stretch Laserdisc '%1$s' Vert Position Laserdisc '%1$s' Vert Stretch Left equal to right Left equal to right with bitmask Left equal to value Left greater than value Left less than value Left not equal to right with bitmask Left not equal to value Lightgun Lightgun Device Assignment Lines Load State MAMEinfo MARPScore MESSinfo MHz Machine Configuration Machine Information Mamescore Manufacturer	%1$s
 Master Volume Match block Mechanical Machine	No
 Mechanical Machine	Yes
 Menu Preview Microphone	Imperfect
 Microphone	Unimplemented
 Miscellaneous Options Mouse Mouse	Imperfect
 Mouse	Unimplemented
 Mouse Device Assignment Multi-keyboard Multi-mouse Name:             Description:
 Natural Natural keyboard Network Devices New Barcode: New Image Name: No No category INI files found No groups found in category file No machines found. Please check the rompath specified in the %1$s.ini file.

If this is your first time using %2$s, please see the config.txt file in the docs directory for information on configuring %2$s. None None
 Not supported Number Of Screens Off Offscreen reload On One or more ROMs/CHDs for this machine are incorrect. The machine may not run correctly.
 One or more ROMs/CHDs for this machine have not been correctly dumped.
 Other Controls Overall	NOT WORKING
 Overall	Unemulated Protection
 Overall	Working
 Overall: NOT WORKING Overall: Unemulated Protection Overall: Working Overclock %1$s sound Overclock CPU %1$s Paddle Device Assignment Page Partially supported Pause/Stop Pedal Device Assignment Performance Options Play Play Count Player Please enter a file extension too Plugin Options Plugins Positional Device Assignment Press TAB to set Press any key to continue. Printer	Imperfect
 Printer	Unimplemented
 Pseudo terminals ROM Audit 	Disabled
Samples Audit 	Disabled
 ROM Audit Result	BAD
 ROM Audit Result	OK
 Read this image, write to another image Read this image, write to diff Read-only Read-write Record Reload All Remove %1$s Folder Remove Folder Remove From Favorites Remove last filter Requires Artwork	No
 Requires Artwork	Yes
 Requires CHD	No
 Requires CHD	Yes
 Requires Clickable Artwork	No
 Requires Clickable Artwork	Yes
 Reset Reset All Results will be saved to %1$s Return to Machine Return to Previous Menu Revision: %1$s Rewind Rewind capacity Romset	%1$s
 Rotate Rotate left Rotate right Rotation Options Sample Rate Sample text - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Samples Audit Result	BAD
 Samples Audit Result	None Needed
 Samples Audit Result	OK
 Save Save Cheat Save Configuration Save State Save input names to file Screen Screen #%d Screen '%1$s' Screen Orientation	Horizontal
 Screen Orientation	Vertical
 Screen flipping in cocktail mode is not supported.
 Search: %1$s_ Select New Machine Select access mode Select category: Select cheat to set hotkey Select custom filters: Select image format Selection List - Search:  Set Set hotkeys Settings Show All Show DATs view Show mouse pointer Show side panels Simultaneous contradictory Skip BIOS selection menu Skip information screen at startup Skip software parts selection menu Sleep Slider Controls Slot Devices Software is clone of: %1$-.100s Software is parent Software part selection: Sound Sound	Imperfect
 Sound	None
 Sound	OK
 Sound	Unimplemented
 Sound Options Sound: Imperfect Sound: None Sound: OK Sound: Unimplemented Speed Start Out Maximized Start new search State/Playback Options Steadykey Support Cocktail	No
 Support Save	No
 Support Save	Yes
 Supported: No Supported: Partial Supported: Yes Switch Item Ordering Switched Order: entries now ordered by description Switched Order: entries now ordered by shortname Synchronized Refresh Sysinfo System: %1$-.100s Tape Control Test The selected game is missing one or more required ROM or CHD images. Please select a different game.

Press any key to continue. There are known problems with this machine

 This driver requires images to be loaded in the following device(s):  This machine has no BIOS. This machine has no sound hardware, MAME will produce no sounds, this is expected behaviour.
 This machine requires external artwork files.
 This machine was never completed. It may exhibit strange behavior or missing elements that are not bugs in the emulation.
 Throttle Tickets dispensed: %1$d

 Timer Timing	Imperfect
 Timing	Unimplemented
 Total time Trackball Device Assignment Triple Buffering Type Type name or select: %1$s_ Type name or select: (random) UI Color Settings UI Customization Settings UI Font UI Fonts Settings UI active Unable to write file
Ensure that cheatpath folder exists Undo last search -- # Uptime: %1$d:%2$02d

 Uptime: %1$d:%2$02d:%3$02d

 Use External Samples Use image as background User Interface Value Vector Vector Flicker Video Mode Video Options Visible Delay WAN	Imperfect
 WAN	Unimplemented
 Wait Vertical Sync Watch Window Mode Write X Y Year	%1$s
 Yes [None]
 [This option is NOT currently mounted in the running system]

Option: %1$s
Device: %2$s

If you select this option, the following items will be enabled:
 [This option is currently mounted in the running system]

Option: %1$s
Device: %2$s

The selected option enables the following items:
 [compatible lists] [create] [empty slot] [empty] [failed] [file manager] [no category INI files] [no groups in INI file] [software list] color-channelAlpha color-channelBlue color-channelGreen color-channelRed color-optionBackground color-optionBorder color-optionClone color-optionDIP switch color-optionMouse down background color color-optionMouse down color color-optionMouse over background color color-optionMouse over color color-optionNormal text color-optionNormal text background color-optionSelected background color color-optionSelected color color-optionSlider color color-optionSubitem color color-optionUnavailable color color-presetBlack color-presetBlue color-presetGray color-presetGreen color-presetOrange color-presetRed color-presetSilver color-presetViolet color-presetWhite color-presetYellow color-sampleClone color-sampleMouse Over color-sampleNormal color-sampleSelected color-sampleSubitem default emulation-featureLAN emulation-featureWAN emulation-featurecamera emulation-featurecolor palette emulation-featurecontrols emulation-featuredisk emulation-featuregraphics emulation-featurekeyboard emulation-featuremicrophone emulation-featuremouse emulation-featureprinter emulation-featureprotection emulation-featuresound emulation-featuretiming kHz machine-filterAvailable machine-filterBIOS machine-filterCHD Required machine-filterCategory machine-filterClones machine-filterCustom Filter machine-filterFavorites machine-filterHorizontal Screen machine-filterManufacturer machine-filterMechanical machine-filterNo CHD Required machine-filterNot BIOS machine-filterNot Mechanical machine-filterNot Working machine-filterParents machine-filterSave Supported machine-filterSave Unsupported machine-filterUnavailable machine-filterUnfiltered machine-filterVertical Screen machine-filterWorking machine-filterYear path-optionArtwork path-optionArtwork Previews path-optionBosses path-optionCabinets path-optionCategory INIs path-optionControl Panels path-optionCovers path-optionCrosshairs path-optionDATs path-optionFlyers path-optionHowTo path-optionINIs path-optionIcons path-optionLogos path-optionMarquees path-optionPCBs path-optionROMs path-optionScores path-optionSnapshots path-optionSoftware Media path-optionVersus playing recording selmenu-artworkArtwork Preview selmenu-artworkBosses selmenu-artworkCabinet selmenu-artworkCovers selmenu-artworkFlyer selmenu-artworkGame Over selmenu-artworkHowTo selmenu-artworkLogo selmenu-artworkPCB selmenu-artworkScores selmenu-artworkSnapshots selmenu-artworkVersus software-filterAvailable software-filterClones software-filterCustom Filter software-filterDevice Type software-filterFavorites software-filterParents software-filterPartially Supported software-filterPublisher software-filterRelease Region software-filterSoftware List software-filterSupported software-filterUnavailable software-filterUnfiltered software-filterUnsupported software-filterYear stopped Project-Id-Version: MAME
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-10-17 07:08+1100
PO-Revision-Date: 2018-05-02 11:34+0200
Last-Translator: Lothar Serra Mari <rootfather@scummvm.org>
Language-Team: MAME Language Team
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.0.7
 

--- TREIBER INFO ---
Treiber:  

Drücken Sie eine beliebige Taste zum Fortfahren 

Es gibt funktionsfähige Klone dieser Maschine: %s 
    Konfiguration gespeichert    

 
Sound:
 
DIESE MASCHINE FUNKTIONIERT NICHT. Die Emulation dieser Maschine ist noch nicht vollständig. Sie können nur darauf warten, dass die Entwickler die Emulation verbessern.
 
Video:
   %1$s
   %1$s    [Standard: %2$s]
   %1$s    [tag: %2$s]
   Einstellerreingaben    [%1$d Eingaben]
   Analogeingaben    [%1$d Eingaben]
   Glücksspieleingaben    [%1$d Eingaben]
   Hanafudaeingaben    [%1$d Eingaben]
   Tastatureingaben    [%1$d Eingaben]
   Tastenfeldeingaben    [%1$d Eingaben]
   Mahjongeingaben    [%1$d Eingaben]
   Bildschirm '%1$s': %2$d × %3$d (H) %4$s Hz
   Bildschirm '%1$s': %2$d × %3$d (V) %4$s Hz
   Bildschirm '%1$s': Vektor
   Benutzereingaben    [%1$d Eingaben]
  (Standard)  (gesperrt)  FARBEN  STIFTE %1$-.100s %1$.3f %1$1.2f %1$3.0f%% %1$3ddB %1$d%% %1$s
%2$s %3$s
Driver: %4$s

CPU:
 %1$s %2$s %1$s %2$s ( %3$d / %4$d Maschinen (%5$d BIOS) ) %1$s %2$s ( %3$d / %4$d Programm-Pakete ) %1$s Helligkeit %1$s Kontrast %1$s Gamma %1$s Horizontale Position %1$s Horizontale Streckung %1$s Aktualisierungsrate %1$s Vertikale Position %1$s Vertikale Streckung %1$s Lautstärke %1$s, %2$-.100s %1$s: %2$s
 %1$s: %2$s - Suche: %3$s_ %2$s
 %d Gesamttreffer gefunden %s %s
 zur Favoritenliste hinzugefügt. %s
 von der Favoritenliste entfernt. %s [intern] %s hinzugefügt (spiele ab) (nehme auf) * BIOS-Einstellungen:
  %1$d Optionen    [Standard: %2$s]
 * CPU:
 * Konfigurationseinstellungen:
 * DIP Schalter-Einstellungen:
 * Eingabegerät(e):
 * Medienoptionen:
 * Steckplatzoptionen:
 * Sound:
 * Video:
 **Fehler beim Speichern von %s.ini** **Fehler beim Speichern von ui.ini** , %s <Filter erstellen> ARGB-Einstellungen Aktiviert: %s Aktiviert: %s =%s %1$s-Ordner hinzufügen - Suche: %2$s_ Ordner hinzufügen Zu Favoriten hinzufügen Filter hinzufügen ADStick-Zuordnung Erweiterte Optionen Alle Alle Cheats neu geladen Analoge Bedienelemente Analoge Bedienelemente	Vorhanden
 Jeder Sind Sie sicher, dass Sie die Emulation verlassen wollen?

Zum Beenden drücken Sie ''%1$s''.
Um zur Emulation zurückzukehren drücken Sie ''%2$s''. Illustrations-Optionen Prüfe ROMs für Maschine %2$u von %3$u...
%1$s Auto Bilder automatisch überspringen Automatisch nach links rotieren Automatisch nach rechts rotieren Automatisch speichern/wiederherstellen BIOS BIOS-Auswahl BIOS-Auswahl: Barcodeleser Barcodelänge ungültig! Gewichtung der Strahlintensität Maximale Strahlbreite Minimale Strahlbreite Bilineare Filterung Bitmap-Vorskalierung Fett Buchhaltungsinformationen Bildschirm eingebrannt CPU oder RAM Kamera	Fehlerhaft
 Kamera	Nicht implementiert
 Abbrechen Kann Verzeichnis nicht überschreiben %1$s-Ordner ändern - Suche: %2$s_ Ordner wechseln Änderungen hieran werden erst wirksam nachdem "Neue Suche starten" ausgewählt wurde Cheat Cheat-Kommentar:
%s Cheat-Finder Cheatname Cheat zu cheat.simple hinzugefügt Cheatmodul nicht verfügbar Cheat nach %s geschrieben und zu cheat.simple hinzugefügt Cheats Aus Palette wählen Überwachungen löschen Münzen %1$c: %2$d%3$s
 Münzen %1$c: NA%3$s
 Münz-Impuls Münzeinwurf sperren Farbvorschau: Farben Befehl Komplett unemulierte Funktionen:  Verzeichnisse konfigurieren Maschine konfigurieren Optionen konfigurieren Beim Verlassen Bestätigung fordern Bedienelemente	Fehlerhaft
 Bedienelemente	Nicht implementiert
 Erzeugen Fadenkreuzversatz %1$s Fadenkreuzversatz X %1$1.3f Fadenkreuzversatz Y %1$1.3f Fadenkreuzeinstellungen Fadenkreuzskalierung %1$s Fadenkreuzskalierung X %1$1.3f Fadenkreuzskalierung Y %1$1.3f Aktuelle %1$s-Ordner Aktuelle Spielzeit Benutzerdefiniert Benutzeroberfläche anpassen DIP-Schalter Datenformat Standard Standardname ist %s Geräte-Zuordnung Wählscheiben-Zuordnung Deaktiviert Deaktiviert: %s Festplatte	Fehlerhaft
 Festplatte	Nicht implementiert
 Fertig Zum Auswählen, doppelklicken oder drücken Sie %1$s Treiber Treiber ist BIOS	Nein
 Treiber ist BIOS	Ja
 Treiber ist ein Klon von	%1$s
 Treiber ist Ursprungs-Treiber	
 Treiber ist ein Klon von: %1$-.100s Treiber ist Ursprungs-Treiber Treiber: "%1$s" Softwareliste  Treiber: %1$-.100s Emuliert Aktiviert Aktiviert: %s Seitenverhältnis erzwingen Bilder im rechten Bereich vergrößern Code eingeben Fehler beim Zugriff auf %s Verlassen Angezeigte Liste in Datei exportieren Liste im TXT-Format exportieren (wie -listfull) Liste im XML-Format exportieren (wie -listxml) Liste im XML-Format exportieren (wie -listxml, Geräte jedoch ausschließen) Externe DAT-Anzeige Speichern der Datei für Eingabenamen fehlgeschlagen Vorspulen Datei Datei existiert bereits - Überschreiben? Dateimanager Filter Filter %1$u Horizontal spiegeln Vertikal spiegeln Ordnereinstellungen Schriftarten 4:3-Seitenverhältnis für Bildschirmfotos erzwingen Bilder auslassen GLSL Gameinit Allgemeine Informationen Haupteingaben Grafik	Fehlerhaft
 Grafik	Farbwiedergabe fehlerhaft
 Grafik	OK
 Grafik	Nicht implementiert
 Grafik	Farbwiedergabe falsch
 Grafik: Fehlerhaft,  Grafik: OK,  Grafik: Nicht implementiert,  Gruppe HLSL Beide ausblenden Filter ausblenden Info/Bild ausblenden Machine ohne ROMs aus Liste der verfügbaren Maschinen ausblenden Rekordpunktzahlen Geschichte Image-Format: Informationen über diese Software Images Nicht perfekt emulierte Funktionen:  Klone einschließen Informationen über automatisches Audit Informationen Textgröße für Informationen Eingabe (allgemein) Eingabe (für diese Maschine) Eingabe-Optionen Datei mit Eingabenamen nach %s gespeichert Eingaben Kursiv Joystick Joystick-Totbereich Joystick-Empfindlichkeit Tastatur	Fehlerhaft
 Tastatur	Nicht implementiert
 Tastatureingaben	Vorhanden
 Tastaturmodus LAN	Fehlerhaft
 LAN	Nicht implementiert
 Sprache Laserdisc '%1$s' Horizontale Position Laserdisc '%1$s' Horizontale Streckung Laserdisc '%1$s' Vertikale Position Laserdisc '%1$s' Vertikale Streckung Links gleich rechts Links gleich rechts als Bitmaske Links gleich Wert Links größer Wert Links kleiner Wert Links ungleich rechts als Bitmaske Links ungleich Wert Lichtpistole Lightgun-Zuordnung Linien Lade Status MAME-Info MARP-Spielstand MESS-Info MHz Konfiguration der Maschine Informationen über diese Maschine Mamespielstand Hersteller	%1$s
 Gesamtlautstärke Prüfe Block auf Übereinstimmung Maschine ist mechanisch	Nein
 Maschine ist mechanisch	Ja
 Menüvorschau Mikrofon	Fehlerhaft
 Mikrofon	Nicht implementiert
 Verschiedene Einstellungen Maus Maus	Fehlerhaft
 Maus	Nicht implementiert
 Maus-Zuordnung Mehrere Tastaturen Mehrfach-Maus Name:             Beschreibung:
 Natürlich Natürliche Tastatur Netzwerkgeräte Neuer Barcode: Neuer Image-Name: Nein Keine Kategorie INI Dateien gefunden Keine Gruppen in Kategorie Datei gefunden Keine Maschinen gefunden. Bitte überprüfen Sie den in der ini-Datei %1$s.ini angegebenen Pfad zu den ROM-Dateien. 

 Wenn Sie %2$s zum ersten Mal verwenden, lesen Sie bitte die Datei config.txt im Verzeichnis "docs" zur Konfiguration von %2$s. Keine Keine
 Nicht unterstützt Anzahl der Bildschirme Aus Nachladen außerhalb des Bildschirms An Ein oder mehrere ROMs/CHDs für diese Maschine sind nicht korrekt. Eventuell wird die Maschine nicht richtig laufen.
 Ein oder mehrere ROMs/CHDs dieser Maschine wurden nicht korrekt ausgelesen.
 Weitere Bedienelemente Insgesamt	FUNKTIONIERT NICHT
 Insgesamt	Nicht emulierter Kopierschutz
 Insgesamt	Funktioniert
 Insgesamt: FUNKTIONIERT NICHT Insgesamt: Nicht emulierter Kopierschutz Insgesamt: Funktioniert Sound %1$s übertakten CPU %1$s übertakten Paddle-Zuordnung Seite Teilweise unterstützt Pause/Stop Pedal-Zuordnung Leistungs-Optionen Abspielen Anzahl Spiele Spieler Bitte geben Sie auch eine Dateierweiterung ein Erweiterungs-Optionen Erweiterungen Positionsgeräte-Zuordnung Zum Setzen drücken Sie TAB Drücken Sie eine beliebige Taste, um fortzufahren. Drucker	Fehlerhaft
 Drucker	Nicht implementiert
 Pseudoterminals ROM-Überprüfung	Deaktiviert
Samples-Überprüfung	Deaktiviert
 ROM-Überprüfung	FEHLERHAFT
 ROM-Überprüfung	OK
 Lesen von diesem Image, Schreiben auf ein anderes Image Lesen von diesem Image, Schreiben in diff Nur lesend Lesend und schreibend Aufnahme Alle neu laden %1$s-Ordner entfernen Ordner entfernen Aus Favoriten entfernen Letzten Filter entfernen Benötigt Illustrationen	Nein
 Benötigt Illustrationen	Ja
 Benötigt CHD	Nein
 Benötigt CHD	Ja
 Benötigt klickbare Illustrationen	Nein
 Benötigt klickbare Illustrationen	Ja
 Zurücksetzen Alle zurücksetzen Ergebnisse werden unter %1$s gespeichert Zurück zur Maschine Zurück zum vorherigen Menü Version: %1$s Zurückspulen Rückspulfähigkeit ROM-Satz	%1$s
 Drehen Nach links rotieren Nach rechts drehen Rotations-Optionen Samplerate Beispieltext - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Samples-Überprüfung	FEHLERHAFT
 Samples-Überprüfung	Nicht nötig
 Samples-Überprüfung	OK
 Speichern Cheat speichern Konfiguration speichern Speichere Status Datei mit Eingabenamen in Datei speichern Bildschirm Bildschirm #%d Bildschirm '%1$s' Bildschirm-Ausrichtung	Horizontal
 Bildschirm-Ausrichtung	Vertikal
 Die Bildschirmdrehung im Cocktailmodus wird nicht unterstützt.
 Suche: %1$s_ Neue Maschine auswählen Zugriffsart wählen Kategorie auswählen: Wählen Sie den Cheat aus, um den Hotkey einzustellen Filter auswählen: Image-Format auswählen Auswahlliste - Suche:  Einstellen Hotkeys einstellen Einstellungen Alle anzeigen DATs Ansicht anzeigen Mauszeiger anzeigen Seitenbereiche anzeigen Widersprüchliche Eingaben erkennen Menü zur BIOS-Auswahl überspringen Informations-Bildschirm beim Start überspringen Menü zur Softwareauswahl überspringen Schlafe Schieberegler Steckplatzgeräte Programm ist ein Klon von: %1$-.100s Programm ist Ursprungs-Programm Softwareauswahl: Sound Sound	Fehlerhaft
 Sound	Keine
 Sound	OK
 Sound	Nicht implementiert
 Soundeinstellungen Sound: Fehlerhaft Sound: Keine Sound: OK Sound: Nicht implementiert Geschwindigkeit Maximiert starten Neue Suche starten Status/Wiedergabe-Optionen Mehrfacheingabe Unterstützt Cocktail-Modus	Nein
 Unterstützt Speichern	Nein
 Unterstützt Speichern	Ja
 Unterstützt: Nein Unterstützt: Teilweise Unterstützt: Ja Sortierung der Einträge ändern Sortierung geändert: Einträge sind nun sortiert nach Beschreibung Sortierung geändert: Einträge sind nun sortiert nach Kurzem Namen Synchronisierter Bildneuaufbau Sysinfo System: %1$-.100s Bedienung Cassettendeck Test Der gewählten Maschine fehlen ein oder mehrere benötigte ROM- oder CHD-Abbilder. Bitte wählen Sie eine andere Maschine aus.

Drücken Sie eine beliebige Taste, um fortzufahren. Es gibt bekannte Probleme mit dieser Maschine

 Für diese Maschine müssen Abbilder in die folgenden Geräte eingehängt werden:  Diese Maschine besitzt kein BIOS. Diese Maschine besitzt keine Sound-Hardware; MAME wird erwartungsgemäß keine Töne ausgeben.
 Die Maschine benötigt externe Illustrationsdateien.
 Diese Maschine wurde niemals fertiggestellt. Sie kann seltsames Verhalten oder fehlende Elemente aufweisen, die nicht auf Fehler der Emulation zurückzuführen sind.
 Drosselung Ausgegebene Tickets: %1$d

 Stoppuhr Zeitlicher Ablauf	Fehlerhaft
 Zeitlicher Ablauf	Nicht implementiert
 Gesamtspielzeit Trackball-Zuordnung Dreifachpufferung Typ Geben Sie den Namen ein oder wählen Sie: %1$s_ Geben Sie den Namen ein oder wählen Sie: (zufällige Auswahl) Farbeinstellungen für Benutzeroberfläche Einstellungen Benutzeroberfläche Schriftart für Benutzeroberfläche Schriftart-Einstellungen für Benutzeroberfläche Benutzeroberfläche aktiv Kann Datei nicht schreiben
Überprüfen Sie ob das cheatpath Verzeichnis existiert Letzte Suche rückgängig machen -- # Laufzeit: %1$d:%2$02d

 Laufzeit: %1$d:%2$02d:%3$02d

 Verwende externe Samples Bild als Hintergrund verwenden Benutzerschnittstelle Wert Vektor Vektorflimmern Videomodus Videoeinstellungen Sichtbare Verzögerung WAN	Fehlerhaft
 WAN	Nicht implementiert
 Warten auf vertikalen Synchronisationsimpuls Überwache Fenstermodus Schreibe X Y Jahr	%1$s
 Ja [Keine]
 [Diese Option ist momentan im laufenden System NICHT aktiviert]

Option: %1$s
Gerät: %2$s

Bei Auswahl dieser Option werden folgende Elemente aktiviert:
 [Diese Option ist momentan im laufenden System aktiviert]

Option: %1$s
Gerät: %2$s

Die gewählte Option aktiviert die folgenden Elemente:
 [Kompatible Listen] [erzeugen] [leerer Steckplatz] [leer] [fehlgeschlagen] [Dateimanager] [Keine Kategorie INI Dateien] [Keine Gruppen in INI Datei] [Softwareliste] Alpha Blau Grün Rot Hintergrund Rand Klon DIP-Schalter Hintergrundfarbe für Mausklick Farbe für Mausklick Hintergrundfarbe für Mouseover Farbe für Mouseover Normaler Text Normaler Texthintergrund Hintergrundfarbe für Auswahl Farbe für Auswahl Schieberegler-Farbe Farbe für Unterelement Nicht verfügbare Farbe Schwarz Blau Grau Grün Orange Rot Silber Violett Weiß Gelb Klon Mouseover Normal Auswahl Unterelement Standard LAN WAN Kamera Farbpalette Bedienelemente Festplatte Grafik Tastatur Mikrofon Maus Drucker Kopierschutz Sound Zeitlicher Ablauf kHz Verfügbar BIOS CHD benötigt Kategorie Klone Benutzerdefinierter Filter Favoriten Horizontaler Bildschirm Hersteller Mechanisch Keine CHD benötigt Nicht BIOS Nicht mechanisch Funktioniert nicht Eltern Speichern unterstützt Speichern nicht unterstützt Nicht verfügbar Ungefiltert Vertikaler Bildschirm Funktioniert Jahr Illustrationen Vorschau Illustrationen Bosse Gehäuse Kategorie INIs Bedienfelder Cover Fadenkreuze DATs Flyer Handbuch INIs Icons Logos Marquees Platinen ROMs Spielstände Schnappschüsse Software-Medien Gegen spiele ab nehme auf Vorschau Illustrationen Bosse Gehäuse Cover Flyer Game Over Handbuch Logo Platine Spielstände Schnappschüsse Gegen Verfügbar Klone Benutzerdefinierter Filter Gerätetyp Favoriten Eltern Teilweise unterstützt Herausgeber Veröffentlichungsregion Softwareliste Unterstützt Nicht verfügbar Ungefiltert Nicht unterstützt Jahr gestoppt 